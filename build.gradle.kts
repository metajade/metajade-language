import de.itemis.mps.gradle.*
import de.itemis.mps.gradle.downloadJBR.DownloadJbrForPlatform
import java.util.Date

buildscript {
    dependencies {
        classpath("de.itemis.mps:mps-gradle-plugin:1.11.+")
    }

    dependencyLocking { lockAllConfigurations() }
}

plugins {
    base
    java
    `maven-publish`
    id("download-jbr") version "1.11.+"
}

val jbrVers = "11_0_11-b1341.60"

downloadJbr {
    jbrVersion = jbrVers
}

// detect if we are in a CI build
val ciBuild = project.hasProperty("forceCI")

// Detect jdk location, required to start ant with tools.jar on classpath otherwise javac and tests will fail
val jdk_home: String = if (project.hasProperty("java11_home")) {
    project.findProperty("java11_home") as String
} else if (System.getenv("JB_JAVA11_HOME") != null) {
    System.getenv("JB_JAVA11_HOME")!!
} else {
    val expected = JavaVersion.VERSION_11
    if (JavaVersion.current() != expected) {
        throw GradleException("This build script requires Java 11 but you are currently using ${JavaVersion.current()}.\nWhat you can do:\n"
                + "  * Use project property java11_home to point to the Java 11 JDK.\n"
                + "  * Use environment variable JB_JAVA11_HOME to point to the Java 11 JDK\n"
                + "  * Run Gradle using Java 11")
    }
    System.getProperty("java.home")!!
}

// Check JDK location
if (!File(jdk_home, "lib").exists()) {
    throw GradleException("Unable to locate JDK home folder. Detected folder is: $jdk_home")
}

logger.info("Using JDK at {}", jdk_home)

val dependencyRepositories = listOf("https://artifacts.itemis.cloud/repository/maven-mps")

// Dependency versions
val mpsVersion = "2021.2.+"
val mbeddrVersion = "2021.2.+"
val iets3Version = "2021.2.+"

// Project versions
val major = "2021"
val minor = "2"

val gitlabEndpoint = System.getenv("CI_API_V4_URL")

if (ciBuild) {
    version = "$major.$minor"
} else {
    version = "$major.$minor-SNAPSHOT"
}

configurations {
    val mps by creating
    val languageLibs by creating
    val antLib by creating
    val jbrWin by creating
    val jbrMac by creating
    val jbrLinux by creating

    dependencies {
        mps("com.jetbrains:mps:$mpsVersion")
        languageLibs("com.mbeddr:platform:$mbeddrVersion")
        languageLibs("org.iets3:opensource:$iets3Version")
        antLib("org.apache.ant:ant-junit:1.10.6")       
        jbrWin("com.jetbrains.jdk:jbr_jcef:$jbrVers:windows-x64@tgz")
        jbrMac("com.jetbrains.jdk:jbr_jcef:$jbrVers:osx-x64@tgz")
        jbrLinux("com.jetbrains.jdk:jbr_jcef:$jbrVers:linux-x64@tgz")
    }
}

dependencyLocking { lockAllConfigurations() }

repositories {
    for (repoUrl in dependencyRepositories) {
        maven {
            url = uri(repoUrl)
        }
    }
    mavenCentral()
}

val skipResolveMps = project.hasProperty("mpsHomeDir")
val mpsHomeDir = rootProject.file(project.findProperty("mpsHomeDir") ?: "$buildDir/mps")

val resolveMps = if (skipResolveMps) {
    tasks.register("resolveMps") {
        doLast {
            logger.info("MPS resolution skipped")
            logger.info("MPS home: {}", mpsHomeDir.getAbsolutePath())
        }
    }
} else {
    tasks.register("resolveMps", Copy::class) {
        dependsOn(configurations["mps"])
        from({
            configurations["mps"].resolve().map(::zipTree)
        })
        into(mpsHomeDir)
    }
}

// tools needed for compiler support in ant calls
val buildScriptClasspath = project.configurations["antLib"] +
        project.files("$project.jdk_home/lib/tools.jar")

val artifactsDir = file("$buildDir/artifacts")
val dependenciesDir = file("$buildDir/dependencies")
val jdkDir = file("$buildDir/jdkDir")

// ___________________ utilities ___________________

val defaultScriptArgs = mapOf(
    "mps.home" to mpsHomeDir,
    "iets3.github.opensource.artifacts" to "$dependenciesDir/org.iets3.opensource",
    "mbeddr.artifacts.platform" to "$dependenciesDir/com.mbeddr.platform",
    "build.dir" to buildDir,
    "version" to version,
    "build.date" to Date(),
    //incremental build support
    "mps.generator.skipUnmodifiedModels" to true
)

// enables https://github.com/mbeddr/mps-gradle-plugin#providing-global-defaults
extra["itemis.mps.gradle.ant.defaultScriptArgs"] = defaultScriptArgs.map { "-D${it.key}=${it.value}" }
extra["itemis.mps.gradle.ant.defaultScriptClasspath"] = buildScriptClasspath
extra["itemis.mps.gradle.ant.defaultJavaExecutable"] = File(jdk_home, "bin/java")

tasks {
    val configureJava by registering {
        val downloadJbr = named("downloadJbr", DownloadJbrForPlatform::class)
        dependsOn(downloadJbr)
        doLast {
            extra["itemis.mps.gradle.ant.defaultScriptArgs"] = defaultScriptArgs.map { "-D${it.key}=${it.value}" }
            extra["itemis.mps.gradle.ant.defaultScriptClasspath"] = buildScriptClasspath
            extra["itemis.mps.gradle.ant.defaultJavaExecutable"] = downloadJbr.get().javaExecutable
        }
    }

    val resolveLanguageLibs by registering(Copy::class) {
        dependsOn(configureJava)
        from({ configurations["languageLibs"].resolve().map(::zipTree) })
        into(dependenciesDir)
    }

    val build_metajade by registering(BuildLanguages::class) {
        dependsOn(resolveMps, resolveLanguageLibs)
        script = "./build.xml"
    }

    val package_metajade by registering(Zip::class) {
        dependsOn(build_metajade)
        archiveBaseName.set("metajade")
        from(artifactsDir)
        include("metajade.languages/**")
    }

    val run_metajade_tests by registering(TestLanguages::class) {
        description = "Will execute all tests from command line"
        script = "./build.xml"
    }

    assemble { dependsOn(package_metajade) }

    val cleanMps by registering(Delete::class) {
        delete(fileTree(projectDir) { include("**/classes_gen/**", "**/source_gen/**", "**/source_gen.caches/**", "tmp/**") })
    }

    //clean { dependsOn(cleanMps) }
    val rebuild by registering { dependsOn(clean, build_metajade) }
}

// Declare the ID of the Gitlab Repository in which the build happens
val projectId = "45247131"

publishing {
    repositories {
        maven {
            url = uri("$gitlabEndpoint/projects/$projectId/packages/maven")
            name = "MetaJade"
            credentials(HttpHeaderCredentials::class) {
                name = "Job-Token"
                value = System.getenv("CI_JOB_TOKEN")
            }
            authentication {
               create("header", HttpHeaderAuthentication::class)              
            }
        }
    }
    publications {
        create<MavenPublication>("MetaJade") {
            groupId = "metajade" 
            artifactId = "metajade.languages"
            artifact(tasks.named("package_metajade"))
            pom.withXml {
                val dependenciesNode = asNode().appendNode("dependencies")
                configurations["languageLibs"].resolvedConfiguration.firstLevelModuleDependencies.forEach {
                    val dependencyNode = dependenciesNode.appendNode("dependency")
                    dependencyNode.appendNode("groupId", it.moduleGroup)
                    dependencyNode.appendNode("artifactId", it.moduleName)
                    dependencyNode.appendNode("version", it.moduleVersion)
                    dependencyNode.appendNode("type", it.moduleArtifacts.first().type)
                }
                configurations["mps"].resolvedConfiguration.firstLevelModuleDependencies.forEach {
                    val dependencyNode = dependenciesNode.appendNode("dependency")
                    dependencyNode.appendNode("groupId", it.moduleGroup)
                    dependencyNode.appendNode("artifactId", it.moduleName)
                    dependencyNode.appendNode("version", it.moduleVersion)
                    dependencyNode.appendNode("type", it.moduleArtifacts.first().type)
                    dependencyNode.appendNode("scope", "provided")
                }
            }
        }
    }
}
