<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:b0f3cf14-1c2e-4882-b77c-6277e17e479f(MetaJade.agent.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="9" />
    <devkit ref="78434eb8-b0e5-444b-850d-e7c4ad2da9ab(jetbrains.mps.devkit.aspect.structure)" />
  </languages>
  <imports>
    <import index="hm2y" ref="r:66e07cb4-a4b0-4bf3-a36d-5e9ed1ff1bd3(org.iets3.core.expr.base.structure)" />
    <import index="os74" ref="r:0a69036a-4d31-45e1-887d-acbe5b8039f5(MetaJade.core.structure)" />
    <import index="zzzn" ref="r:af0af2e7-f7e1-4536-83b5-6bf010d4afd2(org.iets3.core.expr.lambda.structure)" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="982eb8df-2c96-4bd7-9963-11712ea622e5" name="jetbrains.mps.lang.resources">
      <concept id="8974276187400029883" name="jetbrains.mps.lang.resources.structure.FileIcon" flags="ng" index="1QGGSu">
        <property id="2756621024541341363" name="file" index="1iqoE4" />
      </concept>
    </language>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="6714410169261853888" name="conceptId" index="EcuMT" />
        <property id="4628067390765907488" name="conceptShortDescription" index="R4oN_" />
        <property id="4628067390765956802" name="abstract" index="R5$K7" />
        <property id="5092175715804935370" name="conceptAlias" index="34LRSv" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
      </concept>
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <property id="1096454100552" name="rootable" index="19KtqR" />
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="6327362524875300597" name="icon" index="rwd14" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599893252" name="sourceCardinality" index="20lbJX" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <property id="241647608299431140" name="linkId" index="IQ2ns" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1TIwiD" id="3dwesrXQjRO">
    <property role="EcuMT" value="3702022420523728372" />
    <property role="TrG5h" value="Agent" />
    <property role="19KtqR" value="true" />
    <property role="34LRSv" value="Agent" />
    <property role="R4oN_" value="A JADE-like agent" />
    <ref role="1TJDcQ" to="os74:2tMu91FTK9z" resolve="AbstractChunk" />
    <node concept="PrWs8" id="3PPOQnmxRe8" role="PzmwI">
      <ref role="PrY4T" to="os74:3PPOQnmxRe5" resolve="IStatementContainer" />
    </node>
    <node concept="PrWs8" id="3PPOQnmA6oz" role="PzmwI">
      <ref role="PrY4T" to="os74:3PPOQnmA6ox" resolve="ITopLevelExprContainer" />
    </node>
    <node concept="1QGGSu" id="eOcwJtbqoN" role="rwd14">
      <property role="1iqoE4" value="${module}/icons/agent.png" />
    </node>
    <node concept="1TJgyj" id="Oow4MZ5IlU" role="1TKVEi">
      <property role="IQ2ns" value="943645189045937530" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="setup" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" to="hm2y:6sdnDbSla17" resolve="Expression" />
    </node>
    <node concept="1TJgyj" id="Oow4MZ5IlW" role="1TKVEi">
      <property role="IQ2ns" value="943645189045937532" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="inputParams" />
      <property role="20lbJX" value="fLJekj5/_0__n" />
      <ref role="20lvS9" node="Oow4MZ5Inv" resolve="AgentArgument" />
    </node>
  </node>
  <node concept="1TIwiD" id="1QtqUxWNUXN">
    <property role="EcuMT" value="2133980169836146547" />
    <property role="TrG5h" value="DeregisterService" />
    <property role="34LRSv" value="deregister" />
    <property role="R4oN_" value="Deregister a service offered by the Agent" />
    <property role="3GE5qa" value="statements" />
    <ref role="1TJDcQ" node="1wDV7BCuPNV" resolve="AbstractDfExpression" />
    <node concept="1QGGSu" id="7dT_6n1on58" role="rwd14">
      <property role="1iqoE4" value="${module}/icons/agent.png" />
    </node>
  </node>
  <node concept="1TIwiD" id="1QtqUxWNUXM">
    <property role="EcuMT" value="2133980169836146546" />
    <property role="TrG5h" value="RegisterService" />
    <property role="34LRSv" value="register" />
    <property role="R4oN_" value="Register a service offered by the Agent" />
    <property role="3GE5qa" value="statements" />
    <ref role="1TJDcQ" node="1wDV7BCuPNV" resolve="AbstractDfExpression" />
    <node concept="1QGGSu" id="7dT_6n1on5b" role="rwd14">
      <property role="1iqoE4" value="${module}/icons/agent.png" />
    </node>
  </node>
  <node concept="1TIwiD" id="1QtqUxWNUXL">
    <property role="EcuMT" value="2133980169836146545" />
    <property role="TrG5h" value="SearchService" />
    <property role="34LRSv" value="search" />
    <property role="R4oN_" value="Search for a service offered by an Agent" />
    <property role="3GE5qa" value="statements" />
    <ref role="1TJDcQ" node="1wDV7BCuPNV" resolve="AbstractDfExpression" />
    <node concept="1QGGSu" id="7dT_6n1on5e" role="rwd14">
      <property role="1iqoE4" value="${module}/icons/agent.png" />
    </node>
  </node>
  <node concept="1TIwiD" id="1wDV7BCuPNV">
    <property role="EcuMT" value="1741182739291004155" />
    <property role="3GE5qa" value="statements" />
    <property role="TrG5h" value="AbstractDfExpression" />
    <property role="R5$K7" value="true" />
    <ref role="1TJDcQ" to="os74:1wDV7BCtyf3" resolve="AbstractExpression" />
    <node concept="1TJgyj" id="3JB$QJ8_ndk" role="1TKVEi">
      <property role="IQ2ns" value="4316580858993931092" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="service" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" to="hm2y:6sdnDbSla17" resolve="Expression" />
    </node>
  </node>
  <node concept="1TIwiD" id="1wDV7BCwUkH">
    <property role="EcuMT" value="1741182739291546925" />
    <property role="TrG5h" value="AgentDependency" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="2aA5E1nGlc" role="PzmwI">
      <ref role="PrY4T" to="os74:2aA5E1npHs" resolve="IChunkDep" />
    </node>
  </node>
  <node concept="1TIwiD" id="Oow4MZ5Inv">
    <property role="EcuMT" value="943645189045937631" />
    <property role="TrG5h" value="AgentArgument" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="1ekdGpN3mFQ" role="PzmwI">
      <ref role="PrY4T" to="zzzn:6zmBjqUkws9" resolve="IArgument" />
    </node>
    <node concept="1TJgyj" id="1ekdGpN7NxI" role="1TKVEi">
      <property role="IQ2ns" value="1410812809236658286" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="value" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" to="hm2y:6sdnDbSla17" resolve="Expression" />
    </node>
  </node>
  <node concept="1TIwiD" id="7HKtlVl5Fas">
    <property role="EcuMT" value="8894738314219139740" />
    <property role="TrG5h" value="AgentArgRef" />
    <ref role="1TJDcQ" to="hm2y:6sdnDbSla17" resolve="Expression" />
    <node concept="1TJgyj" id="7HKtlVl5Fat" role="1TKVEi">
      <property role="20lbJX" value="fLJekj4/1" />
      <property role="IQ2ns" value="8894738314219139741" />
      <property role="20kJfa" value="arg" />
      <ref role="20lvS9" node="Oow4MZ5Inv" resolve="AgentArgument" />
    </node>
    <node concept="PrWs8" id="1ekdGpN3mSV" role="PzmwI">
      <ref role="PrY4T" to="hm2y:3kzwyUOs05a" resolve="ISingleSymbolRef" />
    </node>
  </node>
</model>

