<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:2720bdfa-481a-4d2b-9917-aa7bca265015(MetaJade.agent.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="14" />
    <use id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions" version="4" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="hm2y" ref="r:66e07cb4-a4b0-4bf3-a36d-5e9ed1ff1bd3(org.iets3.core.expr.base.structure)" />
    <import index="tpco" ref="r:00000000-0000-4000-0000-011c89590284(jetbrains.mps.lang.core.editor)" />
    <import index="21iy" ref="r:3c601dd3-56a8-40ab-a417-66a36a925be1(MetaJade.core.editor)" />
    <import index="zzzn" ref="r:af0af2e7-f7e1-4536-83b5-6bf010d4afd2(org.iets3.core.expr.lambda.structure)" />
    <import index="skfy" ref="r:b0f3cf14-1c2e-4882-b77c-6277e17e479f(MetaJade.agent.structure)" implicit="true" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="1071666914219" name="jetbrains.mps.lang.editor.structure.ConceptEditorDeclaration" flags="ig" index="24kQdi" />
      <concept id="6822301196700715228" name="jetbrains.mps.lang.editor.structure.ConceptEditorHintDeclarationReference" flags="ig" index="2aJ2om">
        <reference id="5944657839026714445" name="hint" index="2$4xQ3" />
      </concept>
      <concept id="1140524381322" name="jetbrains.mps.lang.editor.structure.CellModel_ListWithRole" flags="ng" index="2czfm3">
        <child id="1140524464360" name="cellLayout" index="2czzBx" />
        <child id="1140524464359" name="emptyCellModel" index="2czzBI" />
      </concept>
      <concept id="1106270549637" name="jetbrains.mps.lang.editor.structure.CellLayout_Horizontal" flags="nn" index="2iRfu4" />
      <concept id="1106270571710" name="jetbrains.mps.lang.editor.structure.CellLayout_Vertical" flags="nn" index="2iRkQZ" />
      <concept id="1237303669825" name="jetbrains.mps.lang.editor.structure.CellLayout_Indent" flags="nn" index="l2Vlx" />
      <concept id="1237307900041" name="jetbrains.mps.lang.editor.structure.IndentLayoutIndentStyleClassItem" flags="ln" index="lj46D" />
      <concept id="1142886221719" name="jetbrains.mps.lang.editor.structure.QueryFunction_NodeCondition" flags="in" index="pkWqt" />
      <concept id="1142886811589" name="jetbrains.mps.lang.editor.structure.ConceptFunctionParameter_node" flags="nn" index="pncrf" />
      <concept id="4242538589859161874" name="jetbrains.mps.lang.editor.structure.ExplicitHintsSpecification" flags="ng" index="2w$q5c">
        <child id="4242538589859162459" name="hints" index="2w$qW5" />
      </concept>
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="1078939183254" name="jetbrains.mps.lang.editor.structure.CellModel_Component" flags="sg" stub="3162947552742194261" index="PMmxH">
        <reference id="1078939183255" name="editorComponent" index="PMmxG" />
      </concept>
      <concept id="1186414536763" name="jetbrains.mps.lang.editor.structure.BooleanStyleSheetItem" flags="ln" index="VOi$J">
        <property id="1186414551515" name="flag" index="VOm3f" />
        <child id="1223387335081" name="query" index="3n$kyP" />
      </concept>
      <concept id="1186414860679" name="jetbrains.mps.lang.editor.structure.EditableStyleClassItem" flags="ln" index="VPxyj" />
      <concept id="1186414928363" name="jetbrains.mps.lang.editor.structure.SelectableStyleSheetItem" flags="ln" index="VPM3Z" />
      <concept id="1186415722038" name="jetbrains.mps.lang.editor.structure.FontSizeStyleClassItem" flags="ln" index="VSNWy">
        <property id="1221209241505" name="value" index="1lJzqX" />
      </concept>
      <concept id="1088013125922" name="jetbrains.mps.lang.editor.structure.CellModel_RefCell" flags="sg" stub="730538219795941030" index="1iCGBv">
        <child id="1088186146602" name="editorComponent" index="1sWHZn" />
      </concept>
      <concept id="1381004262292414836" name="jetbrains.mps.lang.editor.structure.ICellStyle" flags="ng" index="1k5N5V">
        <reference id="1381004262292426837" name="parentStyleClass" index="1k5W1q" />
      </concept>
      <concept id="1223387125302" name="jetbrains.mps.lang.editor.structure.QueryFunction_Boolean" flags="in" index="3nzxsE" />
      <concept id="1088185857835" name="jetbrains.mps.lang.editor.structure.InlineEditorComponent" flags="ig" index="1sVBvm" />
      <concept id="1139848536355" name="jetbrains.mps.lang.editor.structure.CellModel_WithRole" flags="ng" index="1$h60E">
        <property id="1139852716018" name="noTargetText" index="1$x2rV" />
        <property id="1140017977771" name="readOnly" index="1Intyy" />
        <reference id="1140103550593" name="relationDeclaration" index="1NtTu8" />
      </concept>
      <concept id="1073389214265" name="jetbrains.mps.lang.editor.structure.EditorCellModel" flags="ng" index="3EYTF0">
        <child id="1142887637401" name="renderingCondition" index="pqm2j" />
      </concept>
      <concept id="1073389446423" name="jetbrains.mps.lang.editor.structure.CellModel_Collection" flags="sn" stub="3013115976261988961" index="3EZMnI">
        <child id="1106270802874" name="cellLayout" index="2iSdaV" />
        <child id="1073389446424" name="childCellModel" index="3EZMnx" />
      </concept>
      <concept id="1073389577006" name="jetbrains.mps.lang.editor.structure.CellModel_Constant" flags="sn" stub="3610246225209162225" index="3F0ifn">
        <property id="1082639509531" name="nullText" index="ilYzB" />
        <property id="1073389577007" name="text" index="3F0ifm" />
      </concept>
      <concept id="1073389658414" name="jetbrains.mps.lang.editor.structure.CellModel_Property" flags="sg" stub="730538219796134133" index="3F0A7n" />
      <concept id="1219418625346" name="jetbrains.mps.lang.editor.structure.IStyleContainer" flags="ng" index="3F0Thp">
        <child id="1219418656006" name="styleItem" index="3F10Kt" />
      </concept>
      <concept id="1073389882823" name="jetbrains.mps.lang.editor.structure.CellModel_RefNode" flags="sg" stub="730538219795960754" index="3F1sOY">
        <child id="5861024100072578575" name="addHints" index="3xwHhi" />
      </concept>
      <concept id="1073390211982" name="jetbrains.mps.lang.editor.structure.CellModel_RefNodeList" flags="sg" stub="2794558372793454595" index="3F2HdR">
        <child id="7279578193766667846" name="addHints" index="78xua" />
      </concept>
      <concept id="1198256887712" name="jetbrains.mps.lang.editor.structure.CellModel_Indent" flags="ng" index="3XFhqQ" />
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1081516740877" name="jetbrains.mps.baseLanguage.structure.NotExpression" flags="nn" index="3fqX7Q">
        <child id="1081516765348" name="expression" index="3fr31v" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1139621453865" name="jetbrains.mps.lang.smodel.structure.Node_IsInstanceOfOperation" flags="nn" index="1mIQ4w">
        <child id="1177027386292" name="conceptArgument" index="cj9EA" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
    </language>
  </registry>
  <node concept="24kQdi" id="3zQKcEGbcYP">
    <ref role="1XX52x" to="skfy:3dwesrXQjRO" resolve="Agent" />
    <node concept="3EZMnI" id="3JB$QJ89k$s" role="2wV5jI">
      <node concept="2iRkQZ" id="3JB$QJ89k$t" role="2iSdaV" />
      <node concept="3EZMnI" id="3zQKcEGbhM4" role="3EZMnx">
        <node concept="2iRkQZ" id="3zQKcEGbhM5" role="2iSdaV" />
        <node concept="3EZMnI" id="3zQKcEGbcYU" role="3EZMnx">
          <node concept="3F0ifn" id="3zQKcEGbcYW" role="3EZMnx">
            <property role="3F0ifm" value="agent" />
            <ref role="1k5W1q" to="21iy:7kuDSjsqD3B" resolve="metajadeKeyword" />
            <node concept="VSNWy" id="28q_2hFtnWd" role="3F10Kt">
              <property role="1lJzqX" value="21" />
            </node>
            <node concept="VPM3Z" id="28q_2hFxsrJ" role="3F10Kt" />
          </node>
          <node concept="3F0A7n" id="3zQKcEGbcZ4" role="3EZMnx">
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
            <node concept="VSNWy" id="28q_2hFtnWf" role="3F10Kt">
              <property role="1lJzqX" value="21" />
            </node>
          </node>
          <node concept="l2Vlx" id="7cuAgGhd418" role="2iSdaV" />
        </node>
        <node concept="3F0ifn" id="28q_2hFuO2f" role="3EZMnx">
          <node concept="VPM3Z" id="28q_2hFuOgG" role="3F10Kt" />
        </node>
        <node concept="3EZMnI" id="4YVd2MB9$8K" role="3EZMnx">
          <node concept="3XFhqQ" id="4YVd2MB9$9h" role="3EZMnx" />
          <node concept="PMmxH" id="4YVd2MB9$9n" role="3EZMnx">
            <ref role="PMmxG" to="21iy:4YVd2MB9$6U" resolve="AbstractChunkFullImports_Component" />
          </node>
          <node concept="l2Vlx" id="4YVd2MB9$8P" role="2iSdaV" />
        </node>
        <node concept="3F0ifn" id="7dT_6n1vhsB" role="3EZMnx">
          <node concept="VPM3Z" id="7dT_6n1vht0" role="3F10Kt" />
        </node>
        <node concept="3EZMnI" id="3PPOQnn3kdP" role="3EZMnx">
          <node concept="3XFhqQ" id="3PPOQnn2YgM" role="3EZMnx" />
          <node concept="3F0ifn" id="Oow4MZ5IM9" role="3EZMnx">
            <property role="3F0ifm" value="input" />
            <node concept="VPM3Z" id="2o21hVNUcdP" role="3F10Kt" />
          </node>
          <node concept="3F2HdR" id="Oow4MZ5IMh" role="3EZMnx">
            <ref role="1NtTu8" to="skfy:Oow4MZ5IlW" resolve="inputParams" />
            <node concept="2iRkQZ" id="Oow4MZ5JlO" role="2czzBx" />
            <node concept="3F0ifn" id="Oow4MZ5JoR" role="2czzBI">
              <property role="ilYzB" value="&lt;none&gt;" />
              <node concept="VPxyj" id="Oow4MZ5JoT" role="3F10Kt">
                <property role="VOm3f" value="true" />
              </node>
            </node>
            <node concept="2w$q5c" id="6cOMG4RZn8X" role="78xua">
              <node concept="2aJ2om" id="6cOMG4RZn8Y" role="2w$qW5">
                <ref role="2$4xQ3" to="21iy:2WQ3iBRGNcR" resolve="PseudocodeMapPresentation" />
              </node>
              <node concept="2aJ2om" id="6cOMG4RZn91" role="2w$qW5">
                <ref role="2$4xQ3" to="21iy:2WQ3iBRGN5o" resolve="PseudocodeSetPresentation" />
              </node>
              <node concept="2aJ2om" id="6cOMG4RZn94" role="2w$qW5">
                <ref role="2$4xQ3" to="21iy:wO0wsRnNTe" resolve="PseudocodeListPresentation" />
              </node>
            </node>
          </node>
          <node concept="l2Vlx" id="4YVd2MB9$9O" role="2iSdaV" />
        </node>
        <node concept="3F0ifn" id="Oow4MZ5IMu" role="3EZMnx">
          <node concept="VPM3Z" id="Oow4MZ5IMv" role="3F10Kt" />
        </node>
        <node concept="3EZMnI" id="Oow4MZ5IMo" role="3EZMnx">
          <node concept="3XFhqQ" id="Oow4MZ5IMp" role="3EZMnx" />
          <node concept="3EZMnI" id="Oow4MZ5IPR" role="3EZMnx">
            <node concept="3EZMnI" id="Oow4MZ5IQ2" role="3EZMnx">
              <node concept="3F0ifn" id="Oow4MZ5IQd" role="3EZMnx">
                <property role="3F0ifm" value="setup" />
                <node concept="VPM3Z" id="2o21hVNUcdR" role="3F10Kt" />
              </node>
              <node concept="3F0ifn" id="Oow4MZ5IQj" role="3EZMnx">
                <property role="3F0ifm" value="as" />
                <ref role="1k5W1q" to="21iy:5PmnIeGXqcA" resolve="metajadeComment" />
                <node concept="lj46D" id="Oow4MZ5J$X" role="3F10Kt">
                  <property role="VOm3f" value="true" />
                  <node concept="3nzxsE" id="Oow4MZ5J$Y" role="3n$kyP">
                    <node concept="3clFbS" id="Oow4MZ5J$Z" role="2VODD2">
                      <node concept="3clFbF" id="Oow4MZ5J_0" role="3cqZAp">
                        <node concept="2OqwBi" id="Oow4MZ5J_1" role="3clFbG">
                          <node concept="2OqwBi" id="Oow4MZ5J_2" role="2Oq$k0">
                            <node concept="pncrf" id="Oow4MZ5J_3" role="2Oq$k0" />
                            <node concept="3TrEf2" id="Oow4MZ5J_4" role="2OqNvi">
                              <ref role="3Tt5mk" to="skfy:Oow4MZ5IlU" resolve="setup" />
                            </node>
                          </node>
                          <node concept="1mIQ4w" id="Oow4MZ5J_5" role="2OqNvi">
                            <node concept="chp4Y" id="Oow4MZ5J_6" role="cj9EA">
                              <ref role="cht4Q" to="hm2y:YXKE79ImBi" resolve="IWantNewLine" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="pkWqt" id="Oow4MZ5L5U" role="pqm2j">
                  <node concept="3clFbS" id="Oow4MZ5L5V" role="2VODD2">
                    <node concept="3clFbF" id="Oow4MZ5L76" role="3cqZAp">
                      <node concept="3fqX7Q" id="Oow4MZ5L78" role="3clFbG">
                        <node concept="2OqwBi" id="Oow4MZ5L79" role="3fr31v">
                          <node concept="2OqwBi" id="Oow4MZ5L7a" role="2Oq$k0">
                            <node concept="pncrf" id="Oow4MZ5L7b" role="2Oq$k0" />
                            <node concept="3TrEf2" id="1MgVVihpoGR" role="2OqNvi">
                              <ref role="3Tt5mk" to="skfy:Oow4MZ5IlU" resolve="setup" />
                            </node>
                          </node>
                          <node concept="1mIQ4w" id="Oow4MZ5L7c" role="2OqNvi">
                            <node concept="chp4Y" id="Oow4MZ5L7d" role="cj9EA">
                              <ref role="cht4Q" to="zzzn:49WTic8ig5D" resolve="BlockExpression" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3F1sOY" id="Oow4MZ5IQr" role="3EZMnx">
                <ref role="1NtTu8" to="skfy:Oow4MZ5IlU" resolve="setup" />
                <node concept="2w$q5c" id="1ekdGpN4L7z" role="3xwHhi">
                  <node concept="2aJ2om" id="1ekdGpN4L7$" role="2w$qW5">
                    <ref role="2$4xQ3" to="21iy:7cuAgGgXqbY" resolve="PseudocodeLocalValPresentation" />
                  </node>
                  <node concept="2aJ2om" id="6cOMG4RZn8L" role="2w$qW5">
                    <ref role="2$4xQ3" to="21iy:wO0wsRnNTe" resolve="PseudocodeListPresentation" />
                  </node>
                  <node concept="2aJ2om" id="6cOMG4RZn8O" role="2w$qW5">
                    <ref role="2$4xQ3" to="21iy:2WQ3iBRGN5o" resolve="PseudocodeSetPresentation" />
                  </node>
                  <node concept="2aJ2om" id="6cOMG4RZn8S" role="2w$qW5">
                    <ref role="2$4xQ3" to="21iy:2WQ3iBRGNcR" resolve="PseudocodeMapPresentation" />
                  </node>
                </node>
              </node>
              <node concept="l2Vlx" id="Oow4MZ5IQ7" role="2iSdaV" />
            </node>
            <node concept="2iRkQZ" id="Oow4MZ5IPW" role="2iSdaV" />
          </node>
          <node concept="l2Vlx" id="Oow4MZ5IMt" role="2iSdaV" />
        </node>
        <node concept="3F0ifn" id="3PPOQnmZw44" role="3EZMnx">
          <node concept="VPM3Z" id="3PPOQnmZP4n" role="3F10Kt" />
        </node>
        <node concept="3EZMnI" id="5rieTHdIyf0" role="3EZMnx">
          <node concept="3XFhqQ" id="5rieTHdIyic" role="3EZMnx" />
          <node concept="PMmxH" id="3PPOQnmI2YA" role="3EZMnx">
            <ref role="PMmxG" to="21iy:3PPOQnmI2QP" resolve="TopLevelExprContainer_Component" />
          </node>
          <node concept="l2Vlx" id="5rieTHdIyf1" role="2iSdaV" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="1QtqUxWT8o2">
    <property role="3GE5qa" value="statements" />
    <ref role="1XX52x" to="skfy:1QtqUxWNUXN" resolve="DeregisterService" />
    <node concept="3EZMnI" id="1QtqUxWT8o4" role="2wV5jI">
      <node concept="PMmxH" id="1QtqUxWT8ob" role="3EZMnx">
        <ref role="PMmxG" to="tpco:2wZex4PafBj" resolve="alias" />
        <ref role="1k5W1q" to="21iy:7kuDSjsqD3B" resolve="metajadeKeyword" />
        <node concept="VPxyj" id="6qPaRrEcEHA" role="3F10Kt" />
      </node>
      <node concept="3F0ifn" id="1QtqUxWT8og" role="3EZMnx">
        <property role="3F0ifm" value="service" />
        <ref role="1k5W1q" to="21iy:7kuDSjsqD3B" resolve="metajadeKeyword" />
      </node>
      <node concept="3F1sOY" id="3JB$QJ8_VUM" role="3EZMnx">
        <ref role="1NtTu8" to="skfy:3JB$QJ8_ndk" resolve="service" />
      </node>
      <node concept="l2Vlx" id="7dT_6n1opym" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="5PmnIeGXalw">
    <property role="3GE5qa" value="statements" />
    <ref role="1XX52x" to="skfy:1QtqUxWNUXL" resolve="SearchService" />
    <node concept="3EZMnI" id="5PmnIeGXaly" role="2wV5jI">
      <node concept="PMmxH" id="5PmnIeGXalD" role="3EZMnx">
        <ref role="PMmxG" to="tpco:2wZex4PafBj" resolve="alias" />
        <ref role="1k5W1q" to="21iy:7kuDSjsqD3B" resolve="metajadeKeyword" />
        <node concept="VPxyj" id="6qPaRrEcEGq" role="3F10Kt" />
      </node>
      <node concept="3F0ifn" id="2aA5E13oGn" role="3EZMnx">
        <property role="3F0ifm" value="service" />
        <ref role="1k5W1q" to="21iy:5PmnIeGXqcA" resolve="metajadeComment" />
      </node>
      <node concept="3F1sOY" id="3JB$QJ8_ndG" role="3EZMnx">
        <property role="1$x2rV" value="&lt;no service&gt;" />
        <ref role="1NtTu8" to="skfy:3JB$QJ8_ndk" resolve="service" />
      </node>
      <node concept="2iRfu4" id="2aA5E12EjR" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="1QtqUxWT8oR">
    <property role="3GE5qa" value="statements" />
    <ref role="1XX52x" to="skfy:1QtqUxWNUXM" resolve="RegisterService" />
    <node concept="3EZMnI" id="1QtqUxWT8pm" role="2wV5jI">
      <node concept="2iRkQZ" id="1QtqUxWT8pn" role="2iSdaV" />
      <node concept="3EZMnI" id="1QtqUxWT8oT" role="3EZMnx">
        <node concept="3F0ifn" id="1QtqUxWT8p5" role="3EZMnx">
          <property role="3F0ifm" value="register service" />
          <ref role="1k5W1q" to="21iy:7kuDSjsqD3B" resolve="metajadeKeyword" />
        </node>
        <node concept="3F1sOY" id="3JB$QJ8_VUw" role="3EZMnx">
          <ref role="1NtTu8" to="skfy:3JB$QJ8_ndk" resolve="service" />
        </node>
        <node concept="l2Vlx" id="4YVd2MB3kYy" role="2iSdaV" />
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="7HKtlVl5Fau">
    <ref role="1XX52x" to="skfy:7HKtlVl5Fas" resolve="AgentArgRef" />
    <node concept="1iCGBv" id="7HKtlVl5Fav" role="2wV5jI">
      <ref role="1NtTu8" to="skfy:7HKtlVl5Fat" resolve="arg" />
      <node concept="1sVBvm" id="7HKtlVl5Faw" role="1sWHZn">
        <node concept="3F0A7n" id="7HKtlVl5Fax" role="2wV5jI">
          <property role="1Intyy" value="true" />
          <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="1ekdGpN3mGi">
    <ref role="1XX52x" to="skfy:Oow4MZ5Inv" resolve="AgentArgument" />
    <node concept="3EZMnI" id="1ekdGpN3mHO" role="2wV5jI">
      <node concept="3F0A7n" id="1ekdGpN3mHY" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="1ekdGpN3mI4" role="3EZMnx">
        <property role="3F0ifm" value="of type" />
        <ref role="1k5W1q" to="21iy:5PmnIeGXqcA" resolve="metajadeComment" />
      </node>
      <node concept="3F1sOY" id="1ekdGpN3mIm" role="3EZMnx">
        <ref role="1NtTu8" to="zzzn:6zmBjqUkwsc" resolve="type" />
      </node>
      <node concept="3F0ifn" id="1ekdGpN7NDM" role="3EZMnx">
        <property role="3F0ifm" value="with default value" />
        <ref role="1k5W1q" to="21iy:5PmnIeGXqcA" resolve="metajadeComment" />
      </node>
      <node concept="3F1sOY" id="1ekdGpN7NDS" role="3EZMnx">
        <ref role="1NtTu8" to="skfy:1ekdGpN7NxI" resolve="value" />
      </node>
      <node concept="l2Vlx" id="1ekdGpN3mHR" role="2iSdaV" />
    </node>
  </node>
</model>

