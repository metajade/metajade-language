<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:894992de-976d-4786-85c1-9a5b3889a386(MetaJade.agent.typesystem)">
  <persistence version="9" />
  <languages>
    <use id="7a5dda62-9140-4668-ab76-d5ed1746f2b2" name="jetbrains.mps.lang.typesystem" version="5" />
    <devkit ref="00000000-0000-4000-0000-1de82b3a4936(jetbrains.mps.devkit.aspect.typesystem)" />
  </languages>
  <imports>
    <import index="5qo5" ref="r:6d93ddb1-b0b0-4eee-8079-51303666672a(org.iets3.core.expr.simpleTypes.structure)" />
    <import index="hm2y" ref="r:66e07cb4-a4b0-4bf3-a36d-5e9ed1ff1bd3(org.iets3.core.expr.base.structure)" />
    <import index="os74" ref="r:0a69036a-4d31-45e1-887d-acbe5b8039f5(MetaJade.core.structure)" implicit="true" />
    <import index="skfy" ref="r:b0f3cf14-1c2e-4882-b77c-6277e17e479f(MetaJade.agent.structure)" implicit="true" />
    <import index="zzzn" ref="r:af0af2e7-f7e1-4536-83b5-6bf010d4afd2(org.iets3.core.expr.lambda.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1081516740877" name="jetbrains.mps.baseLanguage.structure.NotExpression" flags="nn" index="3fqX7Q">
        <child id="1081516765348" name="expression" index="3fr31v" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1080120340718" name="jetbrains.mps.baseLanguage.structure.AndExpression" flags="nn" index="1Wc70l" />
    </language>
    <language id="3a13115c-633c-4c5c-bbcc-75c4219e9555" name="jetbrains.mps.lang.quotation">
      <concept id="5455284157994012186" name="jetbrains.mps.lang.quotation.structure.NodeBuilderInitLink" flags="ng" index="2pIpSj">
        <reference id="5455284157994012188" name="link" index="2pIpSl" />
        <child id="1595412875168045827" name="initValue" index="28nt2d" />
      </concept>
      <concept id="5455284157993863837" name="jetbrains.mps.lang.quotation.structure.NodeBuilder" flags="nn" index="2pJPEk">
        <child id="5455284157993863838" name="quotedNode" index="2pJPEn" />
      </concept>
      <concept id="5455284157993863840" name="jetbrains.mps.lang.quotation.structure.NodeBuilderNode" flags="nn" index="2pJPED">
        <reference id="5455284157993910961" name="concept" index="2pJxaS" />
        <child id="5455284157993911099" name="values" index="2pJxcM" />
      </concept>
    </language>
    <language id="7a5dda62-9140-4668-ab76-d5ed1746f2b2" name="jetbrains.mps.lang.typesystem">
      <concept id="1185788614172" name="jetbrains.mps.lang.typesystem.structure.NormalTypeClause" flags="ng" index="mw_s8">
        <child id="1185788644032" name="normalType" index="mwGJk" />
      </concept>
      <concept id="1175517767210" name="jetbrains.mps.lang.typesystem.structure.ReportErrorStatement" flags="nn" index="2MkqsV">
        <child id="1175517851849" name="errorString" index="2MkJ7o" />
      </concept>
      <concept id="1195213580585" name="jetbrains.mps.lang.typesystem.structure.AbstractCheckingRule" flags="ig" index="18hYwZ">
        <child id="1195213635060" name="body" index="18ibNy" />
      </concept>
      <concept id="1195214364922" name="jetbrains.mps.lang.typesystem.structure.NonTypesystemRule" flags="ig" index="18kY7G" />
      <concept id="3937244445246642777" name="jetbrains.mps.lang.typesystem.structure.AbstractReportStatement" flags="ng" index="1urrMJ">
        <child id="3937244445246642781" name="nodeToReport" index="1urrMF" />
      </concept>
      <concept id="1176544042499" name="jetbrains.mps.lang.typesystem.structure.Node_TypeOperation" flags="nn" index="3JvlWi" />
      <concept id="1174642788531" name="jetbrains.mps.lang.typesystem.structure.ConceptReference" flags="ig" index="1YaCAy">
        <reference id="1174642800329" name="concept" index="1YaFvo" />
      </concept>
      <concept id="1174643105530" name="jetbrains.mps.lang.typesystem.structure.InferenceRule" flags="ig" index="1YbPZF" />
      <concept id="1174648085619" name="jetbrains.mps.lang.typesystem.structure.AbstractRule" flags="ng" index="1YuPPy">
        <child id="1174648101952" name="applicableNode" index="1YuTPh" />
      </concept>
      <concept id="1174650418652" name="jetbrains.mps.lang.typesystem.structure.ApplicableNodeReference" flags="nn" index="1YBJjd">
        <reference id="1174650432090" name="applicableNode" index="1YBMHb" />
      </concept>
      <concept id="1174657487114" name="jetbrains.mps.lang.typesystem.structure.TypeOfExpression" flags="nn" index="1Z2H0r">
        <child id="1174657509053" name="term" index="1Z2MuG" />
      </concept>
      <concept id="1174658326157" name="jetbrains.mps.lang.typesystem.structure.CreateEquationStatement" flags="nn" index="1Z5TYs" />
      <concept id="1174660718586" name="jetbrains.mps.lang.typesystem.structure.AbstractEquationStatement" flags="nn" index="1Zf1VF">
        <child id="1174660783413" name="leftExpression" index="1ZfhK$" />
        <child id="1174660783414" name="rightExpression" index="1ZfhKB" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1139621453865" name="jetbrains.mps.lang.smodel.structure.Node_IsInstanceOfOperation" flags="nn" index="1mIQ4w">
        <child id="1177027386292" name="conceptArgument" index="cj9EA" />
      </concept>
      <concept id="1172008320231" name="jetbrains.mps.lang.smodel.structure.Node_IsNotNullOperation" flags="nn" index="3x8VRR" />
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1YbPZF" id="3JB$QJ8ocfC">
    <property role="TrG5h" value="typeof_SearchService" />
    <property role="3GE5qa" value="statements" />
    <node concept="3clFbS" id="3JB$QJ8ocfD" role="18ibNy">
      <node concept="1Z5TYs" id="3JB$QJ8ocp_" role="3cqZAp">
        <node concept="mw_s8" id="3JB$QJ8ocpC" role="1ZfhK$">
          <node concept="1Z2H0r" id="3JB$QJ8ocfJ" role="mwGJk">
            <node concept="1YBJjd" id="3JB$QJ8ochB" role="1Z2MuG">
              <ref role="1YBMHb" node="3JB$QJ8ocfF" resolve="searchService" />
            </node>
          </node>
        </node>
        <node concept="mw_s8" id="18DldbSY4do" role="1ZfhKB">
          <node concept="2pJPEk" id="18DldbSY4dk" role="mwGJk">
            <node concept="2pJPED" id="2aA5E15Ta4" role="2pJPEn">
              <ref role="2pJxaS" to="hm2y:2rOWEwsEjcg" resolve="OptionType" />
              <node concept="2pIpSj" id="3EtpyNPU2sn" role="2pJxcM">
                <ref role="2pIpSl" to="hm2y:2rOWEwsEjch" resolve="baseType" />
                <node concept="2pJPED" id="3EtpyNPUdsp" role="28nt2d">
                  <ref role="2pJxaS" to="os74:3JB$QJ8oGPX" resolve="AIDType" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="3JB$QJ8ocfF" role="1YuTPh">
      <property role="TrG5h" value="searchService" />
      <ref role="1YaFvo" to="skfy:1QtqUxWNUXL" resolve="SearchService" />
    </node>
  </node>
  <node concept="1YbPZF" id="3JB$QJ8octM">
    <property role="TrG5h" value="typeof_RegisterService" />
    <property role="3GE5qa" value="statements" />
    <node concept="3clFbS" id="3JB$QJ8octN" role="18ibNy">
      <node concept="1Z5TYs" id="3JB$QJ8ocyJ" role="3cqZAp">
        <node concept="mw_s8" id="3JB$QJ8ocz3" role="1ZfhKB">
          <node concept="2pJPEk" id="3JB$QJ8ocyZ" role="mwGJk">
            <node concept="2pJPED" id="3JB$QJ8ocz1" role="2pJPEn">
              <ref role="2pJxaS" to="os74:80jriwjIN" resolve="UnitType" />
            </node>
          </node>
        </node>
        <node concept="mw_s8" id="3JB$QJ8ocyM" role="1ZfhK$">
          <node concept="1Z2H0r" id="3JB$QJ8octT" role="mwGJk">
            <node concept="1YBJjd" id="3JB$QJ8ocvL" role="1Z2MuG">
              <ref role="1YBMHb" node="3JB$QJ8octP" resolve="registerService" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="3JB$QJ8octP" role="1YuTPh">
      <property role="TrG5h" value="registerService" />
      <ref role="1YaFvo" to="skfy:1QtqUxWNUXM" resolve="RegisterService" />
    </node>
  </node>
  <node concept="1YbPZF" id="3JB$QJ8oc$I">
    <property role="TrG5h" value="typeof_DeregisterService" />
    <property role="3GE5qa" value="statements" />
    <node concept="3clFbS" id="3JB$QJ8oc$J" role="18ibNy">
      <node concept="1Z5TYs" id="3JB$QJ8ocFb" role="3cqZAp">
        <node concept="mw_s8" id="3JB$QJ8ocFv" role="1ZfhKB">
          <node concept="2pJPEk" id="3JB$QJ8ocFr" role="mwGJk">
            <node concept="2pJPED" id="3JB$QJ8ocFt" role="2pJPEn">
              <ref role="2pJxaS" to="os74:80jriwjIN" resolve="UnitType" />
            </node>
          </node>
        </node>
        <node concept="mw_s8" id="3JB$QJ8ocFe" role="1ZfhK$">
          <node concept="1Z2H0r" id="3JB$QJ8oc$P" role="mwGJk">
            <node concept="1YBJjd" id="3JB$QJ8ocAH" role="1Z2MuG">
              <ref role="1YBMHb" node="3JB$QJ8oc$L" resolve="deregisterService" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="3JB$QJ8oc$L" role="1YuTPh">
      <property role="TrG5h" value="deregisterService" />
      <ref role="1YaFvo" to="skfy:1QtqUxWNUXN" resolve="DeregisterService" />
    </node>
  </node>
  <node concept="1YbPZF" id="581O36OihgK">
    <property role="TrG5h" value="typeof_AbstractDfOperation" />
    <property role="3GE5qa" value="statements" />
    <node concept="3clFbS" id="581O36OihgL" role="18ibNy">
      <node concept="1Z5TYs" id="581O36OihgR" role="3cqZAp">
        <node concept="mw_s8" id="581O36OihgS" role="1ZfhKB">
          <node concept="2pJPEk" id="581O36OihgT" role="mwGJk">
            <node concept="2pJPED" id="581O36OihgU" role="2pJPEn">
              <ref role="2pJxaS" to="5qo5:4rZeNQ6OYR7" resolve="StringType" />
            </node>
          </node>
        </node>
        <node concept="mw_s8" id="581O36OihgV" role="1ZfhK$">
          <node concept="1Z2H0r" id="581O36OihgW" role="mwGJk">
            <node concept="2OqwBi" id="581O36OihyC" role="1Z2MuG">
              <node concept="1YBJjd" id="581O36OihgX" role="2Oq$k0">
                <ref role="1YBMHb" node="581O36OihgN" resolve="abstractDfOperation" />
              </node>
              <node concept="3TrEf2" id="581O36OihT7" role="2OqNvi">
                <ref role="3Tt5mk" to="skfy:3JB$QJ8_ndk" resolve="service" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="581O36OihgN" role="1YuTPh">
      <property role="TrG5h" value="abstractDfOperation" />
      <ref role="1YaFvo" to="skfy:1wDV7BCuPNV" resolve="AbstractDfExpression" />
    </node>
  </node>
  <node concept="1YbPZF" id="1ekdGpN3Don">
    <property role="TrG5h" value="typeof_AgentArgRef" />
    <node concept="3clFbS" id="1ekdGpN3Doo" role="18ibNy">
      <node concept="1Z5TYs" id="1ekdGpN3Dya" role="3cqZAp">
        <node concept="mw_s8" id="1ekdGpN3DyG" role="1ZfhKB">
          <node concept="1Z2H0r" id="1ekdGpN3DyC" role="mwGJk">
            <node concept="2OqwBi" id="1ekdGpN3DKD" role="1Z2MuG">
              <node concept="1YBJjd" id="1ekdGpN3DyX" role="2Oq$k0">
                <ref role="1YBMHb" node="1ekdGpN3Doq" resolve="a" />
              </node>
              <node concept="3TrEf2" id="1ekdGpN3E6D" role="2OqNvi">
                <ref role="3Tt5mk" to="skfy:7HKtlVl5Fat" resolve="arg" />
              </node>
            </node>
          </node>
        </node>
        <node concept="mw_s8" id="1ekdGpN3Dyd" role="1ZfhK$">
          <node concept="1Z2H0r" id="1ekdGpN3Dou" role="mwGJk">
            <node concept="1YBJjd" id="1ekdGpN3Dqo" role="1Z2MuG">
              <ref role="1YBMHb" node="1ekdGpN3Doq" resolve="a" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="1ekdGpN3Doq" role="1YuTPh">
      <property role="TrG5h" value="a" />
      <ref role="1YaFvo" to="skfy:7HKtlVl5Fas" resolve="AgentArgRef" />
    </node>
  </node>
  <node concept="18kY7G" id="1ekdGpN54u3">
    <property role="TrG5h" value="check_AgentArgument" />
    <node concept="3clFbS" id="1ekdGpN54u4" role="18ibNy">
      <node concept="3clFbJ" id="1ekdGpN54xa" role="3cqZAp">
        <node concept="3fqX7Q" id="1ekdGpN54xb" role="3clFbw">
          <node concept="2OqwBi" id="1ekdGpN54xc" role="3fr31v">
            <node concept="2OqwBi" id="1ekdGpN54xd" role="2Oq$k0">
              <node concept="1YBJjd" id="1ekdGpN54xf" role="2Oq$k0">
                <ref role="1YBMHb" node="1ekdGpN54u6" resolve="agentArgument" />
              </node>
              <node concept="3TrEf2" id="1ekdGpN54xh" role="2OqNvi">
                <ref role="3Tt5mk" to="zzzn:6zmBjqUkwsc" resolve="type" />
              </node>
            </node>
            <node concept="1mIQ4w" id="1ekdGpN54xi" role="2OqNvi">
              <node concept="chp4Y" id="1ekdGpN54xj" role="cj9EA">
                <ref role="cht4Q" to="5qo5:4rZeNQ6OYR7" resolve="StringType" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbS" id="1ekdGpN54xk" role="3clFbx">
          <node concept="2MkqsV" id="1ekdGpN54xl" role="3cqZAp">
            <node concept="Xl_RD" id="1ekdGpN54xm" role="2MkJ7o">
              <property role="Xl_RC" value="Only parameters of type string are supported in agents" />
            </node>
            <node concept="2OqwBi" id="1ekdGpN54xn" role="1urrMF">
              <node concept="1YBJjd" id="1ekdGpN54xp" role="2Oq$k0">
                <ref role="1YBMHb" node="1ekdGpN54u6" resolve="agentArgument" />
              </node>
              <node concept="3TrEf2" id="1ekdGpN54xr" role="2OqNvi">
                <ref role="3Tt5mk" to="zzzn:6zmBjqUkwsc" resolve="type" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3clFbJ" id="1ekdGpN7NS3" role="3cqZAp">
        <node concept="3clFbS" id="1ekdGpN7NS5" role="3clFbx">
          <node concept="2MkqsV" id="1ekdGpN7PGK" role="3cqZAp">
            <node concept="Xl_RD" id="1ekdGpN7PGL" role="2MkJ7o">
              <property role="Xl_RC" value="Default values must be of type string" />
            </node>
            <node concept="2OqwBi" id="1ekdGpN7PGM" role="1urrMF">
              <node concept="1YBJjd" id="1ekdGpN7PGN" role="2Oq$k0">
                <ref role="1YBMHb" node="1ekdGpN54u6" resolve="agentArgument" />
              </node>
              <node concept="3TrEf2" id="1ekdGpN8sRw" role="2OqNvi">
                <ref role="3Tt5mk" to="skfy:1ekdGpN7NxI" resolve="value" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1Wc70l" id="1ekdGpN87Z2" role="3clFbw">
          <node concept="2OqwBi" id="1ekdGpN89af" role="3uHU7B">
            <node concept="2OqwBi" id="1ekdGpN88lz" role="2Oq$k0">
              <node concept="1YBJjd" id="1ekdGpN881k" role="2Oq$k0">
                <ref role="1YBMHb" node="1ekdGpN54u6" resolve="agentArgument" />
              </node>
              <node concept="3TrEf2" id="1ekdGpN88Sb" role="2OqNvi">
                <ref role="3Tt5mk" to="skfy:1ekdGpN7NxI" resolve="value" />
              </node>
            </node>
            <node concept="3x8VRR" id="1ekdGpN8a2Q" role="2OqNvi" />
          </node>
          <node concept="3fqX7Q" id="1ekdGpN7NSw" role="3uHU7w">
            <node concept="2OqwBi" id="1ekdGpN7PeL" role="3fr31v">
              <node concept="2OqwBi" id="1ekdGpN7OMt" role="2Oq$k0">
                <node concept="2OqwBi" id="1ekdGpN7O7z" role="2Oq$k0">
                  <node concept="1YBJjd" id="1ekdGpN7NSK" role="2Oq$k0">
                    <ref role="1YBMHb" node="1ekdGpN54u6" resolve="agentArgument" />
                  </node>
                  <node concept="3TrEf2" id="1ekdGpN7O_7" role="2OqNvi">
                    <ref role="3Tt5mk" to="skfy:1ekdGpN7NxI" resolve="value" />
                  </node>
                </node>
                <node concept="3JvlWi" id="1ekdGpN7P3W" role="2OqNvi" />
              </node>
              <node concept="1mIQ4w" id="1ekdGpN7PAe" role="2OqNvi">
                <node concept="chp4Y" id="1ekdGpN7PDh" role="cj9EA">
                  <ref role="cht4Q" to="5qo5:4rZeNQ6OYR7" resolve="StringType" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="1ekdGpN54u6" role="1YuTPh">
      <property role="TrG5h" value="agentArgument" />
      <ref role="1YaFvo" to="skfy:Oow4MZ5Inv" resolve="AgentArgument" />
    </node>
  </node>
  <node concept="18kY7G" id="4iLN_rGhedv">
    <property role="TrG5h" value="check_Agent" />
    <node concept="3clFbS" id="4iLN_rGhedw" role="18ibNy">
      <node concept="3clFbJ" id="4iLN_rGhiyD" role="3cqZAp">
        <node concept="2OqwBi" id="4iLN_rGhlcO" role="3clFbw">
          <node concept="2OqwBi" id="4iLN_rGhkMC" role="2Oq$k0">
            <node concept="2OqwBi" id="4iLN_rGhiYY" role="2Oq$k0">
              <node concept="1YBJjd" id="4iLN_rGhiyP" role="2Oq$k0">
                <ref role="1YBMHb" node="4iLN_rGhedy" resolve="agent" />
              </node>
              <node concept="3TrEf2" id="4iLN_rGhkxn" role="2OqNvi">
                <ref role="3Tt5mk" to="skfy:Oow4MZ5IlU" resolve="setup" />
              </node>
            </node>
            <node concept="3JvlWi" id="4iLN_rGhl3g" role="2OqNvi" />
          </node>
          <node concept="1mIQ4w" id="4iLN_rGhlp2" role="2OqNvi">
            <node concept="chp4Y" id="4iLN_rGhlre" role="cj9EA">
              <ref role="cht4Q" to="hm2y:7VuYlCQZ3ll" resolve="JoinType" />
            </node>
          </node>
        </node>
        <node concept="3clFbS" id="4iLN_rGhiyF" role="3clFbx">
          <node concept="2MkqsV" id="4iLN_rGhltG" role="3cqZAp">
            <node concept="Xl_RD" id="4iLN_rGhltS" role="2MkJ7o">
              <property role="Xl_RC" value="The setup cannot return a join type" />
            </node>
            <node concept="2OqwBi" id="4iLN_rGhlQw" role="1urrMF">
              <node concept="1YBJjd" id="4iLN_rGhluK" role="2Oq$k0">
                <ref role="1YBMHb" node="4iLN_rGhedy" resolve="agent" />
              </node>
              <node concept="3TrEf2" id="4iLN_rGhmH$" role="2OqNvi">
                <ref role="3Tt5mk" to="skfy:Oow4MZ5IlU" resolve="setup" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="4iLN_rGhedy" role="1YuTPh">
      <property role="TrG5h" value="agent" />
      <ref role="1YaFvo" to="skfy:3dwesrXQjRO" resolve="Agent" />
    </node>
  </node>
</model>

