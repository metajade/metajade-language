<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:9bc9d7aa-ccc3-46bd-805d-a743ad42928e(MetaJade.behaviour.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="9" />
    <devkit ref="78434eb8-b0e5-444b-850d-e7c4ad2da9ab(jetbrains.mps.devkit.aspect.structure)" />
  </languages>
  <imports>
    <import index="os74" ref="r:0a69036a-4d31-45e1-887d-acbe5b8039f5(MetaJade.core.structure)" />
    <import index="hm2y" ref="r:66e07cb4-a4b0-4bf3-a36d-5e9ed1ff1bd3(org.iets3.core.expr.base.structure)" />
    <import index="zzzn" ref="r:af0af2e7-f7e1-4536-83b5-6bf010d4afd2(org.iets3.core.expr.lambda.structure)" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="982eb8df-2c96-4bd7-9963-11712ea622e5" name="jetbrains.mps.lang.resources">
      <concept id="8974276187400029883" name="jetbrains.mps.lang.resources.structure.FileIcon" flags="ng" index="1QGGSu">
        <property id="2756621024541341363" name="file" index="1iqoE4" />
      </concept>
    </language>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="6714410169261853888" name="conceptId" index="EcuMT" />
        <property id="4628067390765907488" name="conceptShortDescription" index="R4oN_" />
        <property id="4628067390765956802" name="abstract" index="R5$K7" />
        <property id="5092175715804935370" name="conceptAlias" index="34LRSv" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
      </concept>
      <concept id="1169125989551" name="jetbrains.mps.lang.structure.structure.InterfaceConceptDeclaration" flags="ig" index="PlHQZ" />
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <property id="1096454100552" name="rootable" index="19KtqR" />
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="6327362524875300597" name="icon" index="rwd14" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599893252" name="sourceCardinality" index="20lbJX" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <property id="241647608299431140" name="linkId" index="IQ2ns" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1TIwiD" id="3ft8RRq803L">
    <property role="EcuMT" value="3737182289224794353" />
    <property role="TrG5h" value="CyclicBehaviour" />
    <property role="R4oN_" value="Behaviour that executes in a loop" />
    <property role="3GE5qa" value="behaviourKind" />
    <property role="34LRSv" value="cyclic behaviour" />
    <ref role="1TJDcQ" node="28q_2hFwn7T" resolve="BehaviourKind" />
  </node>
  <node concept="1TIwiD" id="3ft8RRq803K">
    <property role="EcuMT" value="3737182289224794352" />
    <property role="TrG5h" value="OneShotBehaviour" />
    <property role="R4oN_" value="Behaviour that executes only one time" />
    <property role="3GE5qa" value="behaviourKind" />
    <property role="34LRSv" value="one shot behaviour" />
    <ref role="1TJDcQ" node="28q_2hFwn7T" resolve="BehaviourKind" />
  </node>
  <node concept="1TIwiD" id="2WQ3iBRxHZI">
    <property role="EcuMT" value="3401921042422620142" />
    <property role="TrG5h" value="Behaviour" />
    <property role="19KtqR" value="true" />
    <property role="34LRSv" value="Behaviour" />
    <property role="R4oN_" value="A Behaviour" />
    <ref role="1TJDcQ" to="os74:2tMu91FTK9z" resolve="AbstractChunk" />
    <node concept="PrWs8" id="2aA5E1v5Zc" role="PzmwI">
      <ref role="PrY4T" to="os74:3PPOQnmxRe5" resolve="IStatementContainer" />
    </node>
    <node concept="PrWs8" id="2aA5E1v5Zd" role="PzmwI">
      <ref role="PrY4T" to="os74:3PPOQnmA6ox" resolve="ITopLevelExprContainer" />
    </node>
    <node concept="1TJgyj" id="eOcwJt9V3S" role="1TKVEi">
      <property role="IQ2ns" value="266893304458096888" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="behaviourType" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="28q_2hFwn7T" resolve="BehaviourKind" />
    </node>
    <node concept="1TJgyj" id="1MgVVihphq0" role="1TKVEi">
      <property role="IQ2ns" value="2058408588520789632" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="action" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" to="hm2y:6sdnDbSla17" resolve="Expression" />
    </node>
    <node concept="1TJgyj" id="3I8nNvsHZqc" role="1TKVEi">
      <property role="IQ2ns" value="4289783338601281164" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="inputParams" />
      <property role="20lbJX" value="fLJekj5/_0__n" />
      <ref role="20lvS9" node="3I8nNvsK_Sc" resolve="BehaviourArgument" />
    </node>
    <node concept="1QGGSu" id="eOcwJtbqoR" role="rwd14">
      <property role="1iqoE4" value="${module}/icons/behaviour.png" />
    </node>
  </node>
  <node concept="1TIwiD" id="1bFpwwbUd34">
    <property role="EcuMT" value="1363295496955285700" />
    <property role="TrG5h" value="ActivateBehaviour" />
    <property role="3GE5qa" value="statements.activation" />
    <property role="34LRSv" value="activate" />
    <property role="R4oN_" value="Activate a behaviour" />
    <ref role="1TJDcQ" node="1wDV7BCtyqF" resolve="AbstractBehaviourUnaryExpression" />
    <node concept="1QGGSu" id="7dT_6n1dpcf" role="rwd14">
      <property role="1iqoE4" value="${module}/icons/behaviour.png" />
    </node>
  </node>
  <node concept="1TIwiD" id="1bFpwwbUd3a">
    <property role="EcuMT" value="1363295496955285706" />
    <property role="3GE5qa" value="statements" />
    <property role="TrG5h" value="DeactivateBehaviour" />
    <property role="34LRSv" value="deactivate" />
    <property role="R4oN_" value="Deactivate a behaviour" />
    <ref role="1TJDcQ" node="1wDV7BCtyqF" resolve="AbstractBehaviourUnaryExpression" />
    <node concept="1QGGSu" id="7dT_6n1dpch" role="rwd14">
      <property role="1iqoE4" value="${module}/icons/behaviour.png" />
    </node>
  </node>
  <node concept="1TIwiD" id="28q_2hFwn7T">
    <property role="TrG5h" value="BehaviourKind" />
    <property role="3GE5qa" value="" />
    <property role="EcuMT" value="8895927397242506253" />
    <property role="34LRSv" value="&lt;behaviour kind&gt;" />
    <property role="R5$K7" value="true" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
  </node>
  <node concept="1TIwiD" id="1wDV7BCtyqF">
    <property role="TrG5h" value="AbstractBehaviourUnaryExpression" />
    <property role="3GE5qa" value="statements" />
    <property role="EcuMT" value="1363295496955285701" />
    <property role="R5$K7" value="true" />
    <ref role="1TJDcQ" node="1ZLt6evg52E" resolve="AbstractBehaviourExpression" />
    <node concept="1TJgyj" id="3A64HzfQOAD" role="1TKVEi">
      <property role="IQ2ns" value="4145021229450611113" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="behaviour" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" to="hm2y:6sdnDbSla17" resolve="Expression" />
    </node>
  </node>
  <node concept="1TIwiD" id="1wDV7BCwUmH">
    <property role="EcuMT" value="1741182739291547053" />
    <property role="TrG5h" value="BehaviourDependency" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="2aA5E1p0YK" role="PzmwI">
      <ref role="PrY4T" to="os74:2aA5E1npHs" resolve="IChunkDep" />
    </node>
  </node>
  <node concept="1TIwiD" id="3I8nNvsK_Sc">
    <property role="EcuMT" value="4289783338601963020" />
    <property role="3GE5qa" value="" />
    <property role="TrG5h" value="BehaviourArgument" />
    <property role="R4oN_" value="--" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="3I8nNvsK_Sd" role="PzmwI">
      <ref role="PrY4T" to="zzzn:6zmBjqUkws9" resolve="IArgument" />
    </node>
  </node>
  <node concept="1TIwiD" id="3I8nNvsKOG3">
    <property role="EcuMT" value="4289783338602023683" />
    <property role="3GE5qa" value="" />
    <property role="TrG5h" value="BehaviourArgRef" />
    <property role="R4oN_" value="--" />
    <ref role="1TJDcQ" to="hm2y:6sdnDbSla17" resolve="Expression" />
    <node concept="PrWs8" id="3kzwyUOs2JV" role="PzmwI">
      <ref role="PrY4T" to="hm2y:3kzwyUOs05a" resolve="ISingleSymbolRef" />
    </node>
    <node concept="1TJgyj" id="3I8nNvsKOG6" role="1TKVEi">
      <property role="IQ2ns" value="4289783338602023686" />
      <property role="20kJfa" value="arg" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="3I8nNvsK_Sc" resolve="BehaviourArgument" />
    </node>
  </node>
  <node concept="1TIwiD" id="2aA5E17Yjd">
    <property role="EcuMT" value="39011061266900173" />
    <property role="3GE5qa" value="statements.activation" />
    <property role="TrG5h" value="ActivateSequentialBehaviour" />
    <property role="34LRSv" value="activate in sequence" />
    <property role="R4oN_" value="Activate behaviours in sequence" />
    <ref role="1TJDcQ" node="1ZLt6evg52E" resolve="AbstractBehaviourExpression" />
    <node concept="PrWs8" id="2aA5E18opz" role="PzmwI">
      <ref role="PrY4T" node="2aA5E18opx" resolve="IBehavioursContainer" />
    </node>
    <node concept="1QGGSu" id="2aA5E18BfD" role="rwd14">
      <property role="1iqoE4" value="${module}/icons/behaviour.png" />
    </node>
  </node>
  <node concept="PlHQZ" id="2aA5E18opx">
    <property role="EcuMT" value="39011061267007073" />
    <property role="3GE5qa" value="statements" />
    <property role="TrG5h" value="IBehavioursContainer" />
    <node concept="1TJgyj" id="2aA5E180mD" role="1TKVEi">
      <property role="IQ2ns" value="39011061266908585" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="behaviours" />
      <property role="20lbJX" value="fLJekj6/_1__n" />
      <ref role="20lvS9" to="hm2y:6sdnDbSla17" resolve="Expression" />
    </node>
  </node>
  <node concept="1TIwiD" id="3A64HzfQE29">
    <property role="EcuMT" value="4145021229450567817" />
    <property role="TrG5h" value="BehaviourType" />
    <property role="3GE5qa" value="expression" />
    <property role="34LRSv" value="behaviour" />
    <ref role="1TJDcQ" to="hm2y:6sdnDbSlaok" resolve="Type" />
  </node>
  <node concept="1TIwiD" id="3A64HzfQE2a">
    <property role="EcuMT" value="4145021229450567818" />
    <property role="TrG5h" value="BehaviourReference" />
    <property role="3GE5qa" value="expression" />
    <ref role="1TJDcQ" to="hm2y:6sdnDbSla17" resolve="Expression" />
    <node concept="1TJgyj" id="3A64HzfQE2d" role="1TKVEi">
      <property role="IQ2ns" value="4145021229450567821" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="inputParams" />
      <property role="20lbJX" value="fLJekj5/_0__n" />
      <ref role="20lvS9" to="hm2y:6sdnDbSla17" resolve="Expression" />
    </node>
    <node concept="1TJgyj" id="3A64HzfQE2b" role="1TKVEi">
      <property role="IQ2ns" value="4145021229450567819" />
      <property role="20kJfa" value="behaviour" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="2WQ3iBRxHZI" resolve="Behaviour" />
    </node>
  </node>
  <node concept="1TIwiD" id="1ZLt6evg52E">
    <property role="EcuMT" value="2301748855785410730" />
    <property role="3GE5qa" value="statements" />
    <property role="TrG5h" value="AbstractBehaviourExpression" />
    <ref role="1TJDcQ" to="os74:1wDV7BCtyf3" resolve="AbstractExpression" />
  </node>
</model>

