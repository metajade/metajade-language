<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:a2628c3a-488e-457d-a6d5-206fb390397c(MetaJade.behaviour.typesystem)">
  <persistence version="9" />
  <languages>
    <use id="7a5dda62-9140-4668-ab76-d5ed1746f2b2" name="jetbrains.mps.lang.typesystem" version="5" />
    <devkit ref="00000000-0000-4000-0000-1de82b3a4936(jetbrains.mps.devkit.aspect.typesystem)" />
  </languages>
  <imports>
    <import index="os74" ref="r:0a69036a-4d31-45e1-887d-acbe5b8039f5(MetaJade.core.structure)" implicit="true" />
    <import index="gv6i" ref="r:9bc9d7aa-ccc3-46bd-805d-a743ad42928e(MetaJade.behaviour.structure)" implicit="true" />
    <import index="hm2y" ref="r:66e07cb4-a4b0-4bf3-a36d-5e9ed1ff1bd3(org.iets3.core.expr.base.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1082485599095" name="jetbrains.mps.baseLanguage.structure.BlockStatement" flags="nn" index="9aQIb">
        <child id="1082485599096" name="statements" index="9aQI4" />
      </concept>
      <concept id="1154032098014" name="jetbrains.mps.baseLanguage.structure.AbstractLoopStatement" flags="nn" index="2LF5Ji">
        <child id="1154032183016" name="body" index="2LFqv$" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1082485599094" name="ifFalseStatement" index="9aQIa" />
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1073239437375" name="jetbrains.mps.baseLanguage.structure.NotEqualsExpression" flags="nn" index="3y3z36" />
    </language>
    <language id="3a13115c-633c-4c5c-bbcc-75c4219e9555" name="jetbrains.mps.lang.quotation">
      <concept id="5455284157993863837" name="jetbrains.mps.lang.quotation.structure.NodeBuilder" flags="nn" index="2pJPEk">
        <child id="5455284157993863838" name="quotedNode" index="2pJPEn" />
      </concept>
      <concept id="5455284157993863840" name="jetbrains.mps.lang.quotation.structure.NodeBuilderNode" flags="nn" index="2pJPED">
        <reference id="5455284157993910961" name="concept" index="2pJxaS" />
      </concept>
    </language>
    <language id="7a5dda62-9140-4668-ab76-d5ed1746f2b2" name="jetbrains.mps.lang.typesystem">
      <concept id="1185788614172" name="jetbrains.mps.lang.typesystem.structure.NormalTypeClause" flags="ng" index="mw_s8">
        <child id="1185788644032" name="normalType" index="mwGJk" />
      </concept>
      <concept id="1175517767210" name="jetbrains.mps.lang.typesystem.structure.ReportErrorStatement" flags="nn" index="2MkqsV">
        <child id="1175517851849" name="errorString" index="2MkJ7o" />
      </concept>
      <concept id="1195213580585" name="jetbrains.mps.lang.typesystem.structure.AbstractCheckingRule" flags="ig" index="18hYwZ">
        <child id="1195213635060" name="body" index="18ibNy" />
      </concept>
      <concept id="1195214364922" name="jetbrains.mps.lang.typesystem.structure.NonTypesystemRule" flags="ig" index="18kY7G" />
      <concept id="3937244445246642777" name="jetbrains.mps.lang.typesystem.structure.AbstractReportStatement" flags="ng" index="1urrMJ">
        <child id="3937244445246642781" name="nodeToReport" index="1urrMF" />
      </concept>
      <concept id="1176544042499" name="jetbrains.mps.lang.typesystem.structure.Node_TypeOperation" flags="nn" index="3JvlWi" />
      <concept id="1174642788531" name="jetbrains.mps.lang.typesystem.structure.ConceptReference" flags="ig" index="1YaCAy">
        <reference id="1174642800329" name="concept" index="1YaFvo" />
      </concept>
      <concept id="1174643105530" name="jetbrains.mps.lang.typesystem.structure.InferenceRule" flags="ig" index="1YbPZF" />
      <concept id="1174648085619" name="jetbrains.mps.lang.typesystem.structure.AbstractRule" flags="ng" index="1YuPPy">
        <child id="1174648101952" name="applicableNode" index="1YuTPh" />
      </concept>
      <concept id="1174650418652" name="jetbrains.mps.lang.typesystem.structure.ApplicableNodeReference" flags="nn" index="1YBJjd">
        <reference id="1174650432090" name="applicableNode" index="1YBMHb" />
      </concept>
      <concept id="1174657487114" name="jetbrains.mps.lang.typesystem.structure.TypeOfExpression" flags="nn" index="1Z2H0r">
        <child id="1174657509053" name="term" index="1Z2MuG" />
      </concept>
      <concept id="1174658326157" name="jetbrains.mps.lang.typesystem.structure.CreateEquationStatement" flags="nn" index="1Z5TYs" />
      <concept id="1174660718586" name="jetbrains.mps.lang.typesystem.structure.AbstractEquationStatement" flags="nn" index="1Zf1VF">
        <child id="1174660783413" name="leftExpression" index="1ZfhK$" />
        <child id="1174660783414" name="rightExpression" index="1ZfhKB" />
      </concept>
      <concept id="1174663118805" name="jetbrains.mps.lang.typesystem.structure.CreateLessThanInequationStatement" flags="nn" index="1ZobV4" />
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1179168000618" name="jetbrains.mps.lang.smodel.structure.Node_GetIndexInParentOperation" flags="nn" index="2bSWHS" />
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1139621453865" name="jetbrains.mps.lang.smodel.structure.Node_IsInstanceOfOperation" flags="nn" index="1mIQ4w">
        <child id="1177027386292" name="conceptArgument" index="cj9EA" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="540871147943773365" name="jetbrains.mps.baseLanguage.collections.structure.SingleArgumentSequenceOperation" flags="nn" index="25WWJ4">
        <child id="540871147943773366" name="argument" index="25WWJ7" />
      </concept>
      <concept id="1153943597977" name="jetbrains.mps.baseLanguage.collections.structure.ForEachStatement" flags="nn" index="2Gpval">
        <child id="1153944400369" name="variable" index="2Gsz3X" />
        <child id="1153944424730" name="inputSequence" index="2GsD0m" />
      </concept>
      <concept id="1153944193378" name="jetbrains.mps.baseLanguage.collections.structure.ForEachVariable" flags="nr" index="2GrKxI" />
      <concept id="1153944233411" name="jetbrains.mps.baseLanguage.collections.structure.ForEachVariableReference" flags="nn" index="2GrUjf">
        <reference id="1153944258490" name="variable" index="2Gs0qQ" />
      </concept>
      <concept id="1162934736510" name="jetbrains.mps.baseLanguage.collections.structure.GetElementOperation" flags="nn" index="34jXtK" />
      <concept id="1162935959151" name="jetbrains.mps.baseLanguage.collections.structure.GetSizeOperation" flags="nn" index="34oBXx" />
    </language>
  </registry>
  <node concept="1YbPZF" id="3JB$QJ88UXT">
    <property role="TrG5h" value="typeof_AbstractBehaviourOperation" />
    <property role="3GE5qa" value="statements" />
    <node concept="3clFbS" id="3JB$QJ88UXU" role="18ibNy">
      <node concept="1Z5TYs" id="3JB$QJ88V2I" role="3cqZAp">
        <node concept="mw_s8" id="3JB$QJ88V32" role="1ZfhKB">
          <node concept="2pJPEk" id="3JB$QJ88V2Y" role="mwGJk">
            <node concept="2pJPED" id="3JB$QJ88V30" role="2pJPEn">
              <ref role="2pJxaS" to="os74:80jriwjIN" resolve="UnitType" />
            </node>
          </node>
        </node>
        <node concept="mw_s8" id="3JB$QJ88V2L" role="1ZfhK$">
          <node concept="1Z2H0r" id="3JB$QJ88UY0" role="mwGJk">
            <node concept="1YBJjd" id="3JB$QJ88UZS" role="1Z2MuG">
              <ref role="1YBMHb" node="3JB$QJ88UXW" resolve="abo" />
            </node>
          </node>
        </node>
      </node>
      <node concept="1Z5TYs" id="3A64HzfQQ7d" role="3cqZAp">
        <node concept="mw_s8" id="3A64HzfQQ7G" role="1ZfhKB">
          <node concept="2pJPEk" id="3A64HzfQQ7C" role="mwGJk">
            <node concept="2pJPED" id="3A64HzfQQ7E" role="2pJPEn">
              <ref role="2pJxaS" to="gv6i:3A64HzfQE29" resolve="BehaviourType" />
            </node>
          </node>
        </node>
        <node concept="mw_s8" id="3A64HzfQQ7g" role="1ZfhK$">
          <node concept="1Z2H0r" id="3A64HzfQPjw" role="mwGJk">
            <node concept="2OqwBi" id="3A64HzfQP_d" role="1Z2MuG">
              <node concept="1YBJjd" id="3A64HzfQPlx" role="2Oq$k0">
                <ref role="1YBMHb" node="3JB$QJ88UXW" resolve="abo" />
              </node>
              <node concept="3TrEf2" id="3A64HzfQPW8" role="2OqNvi">
                <ref role="3Tt5mk" to="gv6i:3A64HzfQOAD" resolve="behaviour" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="3JB$QJ88UXW" role="1YuTPh">
      <property role="TrG5h" value="abo" />
      <ref role="1YaFvo" to="gv6i:1wDV7BCtyqF" resolve="AbstractBehaviourUnaryExpression" />
    </node>
  </node>
  <node concept="1YbPZF" id="3I8nNvsLcUy">
    <property role="TrG5h" value="typeof_BehaviourArgRef" />
    <property role="3GE5qa" value="" />
    <node concept="3clFbS" id="3I8nNvsLcUz" role="18ibNy">
      <node concept="1Z5TYs" id="49WTic8fTfG" role="3cqZAp">
        <node concept="mw_s8" id="49WTic8fTg0" role="1ZfhKB">
          <node concept="1Z2H0r" id="49WTic8fTfW" role="mwGJk">
            <node concept="2OqwBi" id="49WTic8fThA" role="1Z2MuG">
              <node concept="1YBJjd" id="49WTic8fTgh" role="2Oq$k0">
                <ref role="1YBMHb" node="3I8nNvsLcU_" resolve="b" />
              </node>
              <node concept="3TrEf2" id="49WTic8gjkh" role="2OqNvi">
                <ref role="3Tt5mk" to="gv6i:3I8nNvsKOG6" resolve="arg" />
              </node>
            </node>
          </node>
        </node>
        <node concept="mw_s8" id="49WTic8fTfJ" role="1ZfhK$">
          <node concept="1Z2H0r" id="49WTic8fTdG" role="mwGJk">
            <node concept="1YBJjd" id="49WTic8fTdW" role="1Z2MuG">
              <ref role="1YBMHb" node="3I8nNvsLcU_" resolve="b" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="3I8nNvsLcU_" role="1YuTPh">
      <property role="TrG5h" value="b" />
      <ref role="1YaFvo" to="gv6i:3I8nNvsKOG3" resolve="BehaviourArgRef" />
    </node>
  </node>
  <node concept="1YbPZF" id="2aA5E187cS">
    <property role="TrG5h" value="typeof_ActivateSequentialBehaviour" />
    <property role="3GE5qa" value="statements.activation" />
    <node concept="3clFbS" id="2aA5E187cT" role="18ibNy">
      <node concept="1Z5TYs" id="2aA5E187me" role="3cqZAp">
        <node concept="mw_s8" id="2aA5E187my" role="1ZfhKB">
          <node concept="2pJPEk" id="2aA5E187mu" role="mwGJk">
            <node concept="2pJPED" id="2aA5E187mw" role="2pJPEn">
              <ref role="2pJxaS" to="os74:80jriwjIN" resolve="UnitType" />
            </node>
          </node>
        </node>
        <node concept="mw_s8" id="2aA5E187mh" role="1ZfhK$">
          <node concept="1Z2H0r" id="2aA5E187d2" role="mwGJk">
            <node concept="1YBJjd" id="2aA5E187eU" role="1Z2MuG">
              <ref role="1YBMHb" node="2aA5E187cV" resolve="acb" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="2aA5E187cV" role="1YuTPh">
      <property role="TrG5h" value="acb" />
      <ref role="1YaFvo" to="gv6i:2aA5E17Yjd" resolve="ActivateSequentialBehaviour" />
    </node>
  </node>
  <node concept="1YbPZF" id="3A64HzfQE6X">
    <property role="TrG5h" value="typeof_BehaviourDeclaration" />
    <property role="3GE5qa" value="expression" />
    <node concept="3clFbS" id="3A64HzfQE6Y" role="18ibNy">
      <node concept="1Z5TYs" id="3A64HzfQFrH" role="3cqZAp">
        <node concept="mw_s8" id="3A64HzfQFrI" role="1ZfhKB">
          <node concept="2pJPEk" id="3A64HzfQFrJ" role="mwGJk">
            <node concept="2pJPED" id="3A64HzfQFrK" role="2pJPEn">
              <ref role="2pJxaS" to="gv6i:3A64HzfQE29" resolve="BehaviourType" />
            </node>
          </node>
        </node>
        <node concept="mw_s8" id="3A64HzfQFrL" role="1ZfhK$">
          <node concept="1Z2H0r" id="3A64HzfQFrM" role="mwGJk">
            <node concept="1YBJjd" id="3A64HzfQHRd" role="1Z2MuG">
              <ref role="1YBMHb" node="3A64HzfQE70" resolve="bd" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3clFbH" id="3A64HzfQFr1" role="3cqZAp" />
      <node concept="3clFbJ" id="3A64HzfQEFr" role="3cqZAp">
        <node concept="3y3z36" id="3A64HzfQEFs" role="3clFbw">
          <node concept="2OqwBi" id="3A64HzfQEFt" role="3uHU7w">
            <node concept="2OqwBi" id="3A64HzfQEFu" role="2Oq$k0">
              <node concept="2OqwBi" id="3A64HzfQEFv" role="2Oq$k0">
                <node concept="1YBJjd" id="3A64HzfQEFw" role="2Oq$k0">
                  <ref role="1YBMHb" node="3A64HzfQE70" resolve="bd" />
                </node>
                <node concept="3TrEf2" id="3A64HzfQEFx" role="2OqNvi">
                  <ref role="3Tt5mk" to="gv6i:3A64HzfQE2b" resolve="behaviour" />
                </node>
              </node>
              <node concept="3Tsc0h" id="3A64HzfQEFy" role="2OqNvi">
                <ref role="3TtcxE" to="gv6i:3I8nNvsHZqc" resolve="inputParams" />
              </node>
            </node>
            <node concept="34oBXx" id="3A64HzfQEFz" role="2OqNvi" />
          </node>
          <node concept="2OqwBi" id="3A64HzfQEF$" role="3uHU7B">
            <node concept="2OqwBi" id="3A64HzfQEF_" role="2Oq$k0">
              <node concept="1YBJjd" id="3A64HzfQEFA" role="2Oq$k0">
                <ref role="1YBMHb" node="3A64HzfQE70" resolve="bd" />
              </node>
              <node concept="3Tsc0h" id="3A64HzfQEFB" role="2OqNvi">
                <ref role="3TtcxE" to="gv6i:3A64HzfQE2d" resolve="inputParams" />
              </node>
            </node>
            <node concept="34oBXx" id="3A64HzfQEFC" role="2OqNvi" />
          </node>
        </node>
        <node concept="3clFbS" id="3A64HzfQEFD" role="3clFbx">
          <node concept="2MkqsV" id="3A64HzfQEFE" role="3cqZAp">
            <node concept="Xl_RD" id="3A64HzfQEFF" role="2MkJ7o">
              <property role="Xl_RC" value="wrong number of params" />
            </node>
            <node concept="1YBJjd" id="3A64HzfQEFG" role="1urrMF">
              <ref role="1YBMHb" node="3A64HzfQE70" resolve="bd" />
            </node>
          </node>
        </node>
        <node concept="9aQIb" id="3A64HzfQEFH" role="9aQIa">
          <node concept="3clFbS" id="3A64HzfQEFI" role="9aQI4">
            <node concept="2Gpval" id="3A64HzfQEFJ" role="3cqZAp">
              <node concept="2GrKxI" id="3A64HzfQEFK" role="2Gsz3X">
                <property role="TrG5h" value="p" />
              </node>
              <node concept="2OqwBi" id="3A64HzfQEFL" role="2GsD0m">
                <node concept="1YBJjd" id="3A64HzfQEFM" role="2Oq$k0">
                  <ref role="1YBMHb" node="3A64HzfQE70" resolve="bd" />
                </node>
                <node concept="3Tsc0h" id="3A64HzfQEFN" role="2OqNvi">
                  <ref role="3TtcxE" to="gv6i:3A64HzfQE2d" resolve="inputParams" />
                </node>
              </node>
              <node concept="3clFbS" id="3A64HzfQEFO" role="2LFqv$">
                <node concept="1ZobV4" id="3A64HzfQEFP" role="3cqZAp">
                  <node concept="mw_s8" id="3A64HzfQEFQ" role="1ZfhK$">
                    <node concept="1Z2H0r" id="3A64HzfQEFR" role="mwGJk">
                      <node concept="2GrUjf" id="3A64HzfQEFS" role="1Z2MuG">
                        <ref role="2Gs0qQ" node="3A64HzfQEFK" resolve="p" />
                      </node>
                    </node>
                  </node>
                  <node concept="mw_s8" id="3A64HzfQEFT" role="1ZfhKB">
                    <node concept="1Z2H0r" id="3A64HzfQEFU" role="mwGJk">
                      <node concept="2OqwBi" id="3A64HzfQEFV" role="1Z2MuG">
                        <node concept="2OqwBi" id="3A64HzfQEFW" role="2Oq$k0">
                          <node concept="2OqwBi" id="3A64HzfQEFX" role="2Oq$k0">
                            <node concept="1YBJjd" id="3A64HzfQEFY" role="2Oq$k0">
                              <ref role="1YBMHb" node="3A64HzfQE70" resolve="bd" />
                            </node>
                            <node concept="3TrEf2" id="3A64HzfQEFZ" role="2OqNvi">
                              <ref role="3Tt5mk" to="gv6i:3A64HzfQE2b" resolve="behaviour" />
                            </node>
                          </node>
                          <node concept="3Tsc0h" id="3A64HzfQEG0" role="2OqNvi">
                            <ref role="3TtcxE" to="gv6i:3I8nNvsHZqc" resolve="inputParams" />
                          </node>
                        </node>
                        <node concept="34jXtK" id="3A64HzfQEG1" role="2OqNvi">
                          <node concept="2OqwBi" id="3A64HzfQEG2" role="25WWJ7">
                            <node concept="2bSWHS" id="3A64HzfQEG3" role="2OqNvi" />
                            <node concept="2GrUjf" id="3A64HzfQEG4" role="2Oq$k0">
                              <ref role="2Gs0qQ" node="3A64HzfQEFK" resolve="p" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="3A64HzfQE70" role="1YuTPh">
      <property role="TrG5h" value="bd" />
      <ref role="1YaFvo" to="gv6i:3A64HzfQE2a" resolve="BehaviourReference" />
    </node>
  </node>
  <node concept="1YbPZF" id="7d1Ve7QQLfY">
    <property role="TrG5h" value="typeof_IBehavioursContainer" />
    <property role="3GE5qa" value="statements" />
    <node concept="3clFbS" id="7d1Ve7QQLfZ" role="18ibNy">
      <node concept="2Gpval" id="7d1Ve7QQWG8" role="3cqZAp">
        <node concept="2GrKxI" id="7d1Ve7QQWGa" role="2Gsz3X">
          <property role="TrG5h" value="b" />
        </node>
        <node concept="2OqwBi" id="7d1Ve7QQWPT" role="2GsD0m">
          <node concept="1YBJjd" id="7d1Ve7QQWGD" role="2Oq$k0">
            <ref role="1YBMHb" node="7d1Ve7QQLg1" resolve="iBehavioursContainer" />
          </node>
          <node concept="3Tsc0h" id="7d1Ve7QQX2m" role="2OqNvi">
            <ref role="3TtcxE" to="gv6i:2aA5E180mD" resolve="behaviours" />
          </node>
        </node>
        <node concept="3clFbS" id="7d1Ve7QQWGe" role="2LFqv$">
          <node concept="1Z5TYs" id="7d1Ve7QQX9e" role="3cqZAp">
            <node concept="mw_s8" id="7d1Ve7QQX9y" role="1ZfhKB">
              <node concept="2pJPEk" id="7d1Ve7QQX9u" role="mwGJk">
                <node concept="2pJPED" id="7d1Ve7QQX9w" role="2pJPEn">
                  <ref role="2pJxaS" to="gv6i:3A64HzfQE29" resolve="BehaviourType" />
                </node>
              </node>
            </node>
            <node concept="mw_s8" id="7d1Ve7QQX9h" role="1ZfhK$">
              <node concept="1Z2H0r" id="7d1Ve7QQX3c" role="mwGJk">
                <node concept="2GrUjf" id="7d1Ve7QQX56" role="1Z2MuG">
                  <ref role="2Gs0qQ" node="7d1Ve7QQWGa" resolve="b" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="7d1Ve7QQLg1" role="1YuTPh">
      <property role="TrG5h" value="iBehavioursContainer" />
      <ref role="1YaFvo" to="gv6i:2aA5E18opx" resolve="IBehavioursContainer" />
    </node>
  </node>
  <node concept="18kY7G" id="4iLN_rGhfe$">
    <property role="TrG5h" value="check_Behaviour" />
    <node concept="3clFbS" id="4iLN_rGhfe_" role="18ibNy">
      <node concept="3clFbJ" id="4iLN_rGhfeF" role="3cqZAp">
        <node concept="2OqwBi" id="4iLN_rGhh3s" role="3clFbw">
          <node concept="2OqwBi" id="4iLN_rGhgDi" role="2Oq$k0">
            <node concept="2OqwBi" id="4iLN_rGhfF0" role="2Oq$k0">
              <node concept="1YBJjd" id="4iLN_rGhfeR" role="2Oq$k0">
                <ref role="1YBMHb" node="4iLN_rGhfeB" resolve="behaviour" />
              </node>
              <node concept="3TrEf2" id="4iLN_rGhgo1" role="2OqNvi">
                <ref role="3Tt5mk" to="gv6i:1MgVVihphq0" resolve="action" />
              </node>
            </node>
            <node concept="3JvlWi" id="4iLN_rGhgTS" role="2OqNvi" />
          </node>
          <node concept="1mIQ4w" id="4iLN_rGhhdD" role="2OqNvi">
            <node concept="chp4Y" id="4iLN_rGhhfP" role="cj9EA">
              <ref role="cht4Q" to="hm2y:7VuYlCQZ3ll" resolve="JoinType" />
            </node>
          </node>
        </node>
        <node concept="3clFbS" id="4iLN_rGhfeH" role="3clFbx">
          <node concept="2MkqsV" id="4iLN_rGhhij" role="3cqZAp">
            <node concept="Xl_RD" id="4iLN_rGhhiv" role="2MkJ7o">
              <property role="Xl_RC" value="The action cannot return a join type" />
            </node>
            <node concept="2OqwBi" id="4iLN_rGhhEP" role="1urrMF">
              <node concept="1YBJjd" id="4iLN_rGhhj5" role="2Oq$k0">
                <ref role="1YBMHb" node="4iLN_rGhfeB" resolve="behaviour" />
              </node>
              <node concept="3TrEf2" id="4iLN_rGhiwF" role="2OqNvi">
                <ref role="3Tt5mk" to="gv6i:1MgVVihphq0" resolve="action" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="4iLN_rGhfeB" role="1YuTPh">
      <property role="TrG5h" value="behaviour" />
      <ref role="1YaFvo" to="gv6i:2WQ3iBRxHZI" resolve="Behaviour" />
    </node>
  </node>
</model>

