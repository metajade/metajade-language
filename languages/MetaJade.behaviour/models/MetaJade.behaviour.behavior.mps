<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:60300f8d-83f2-4f2b-8de3-db4438f2e21c(MetaJade.behaviour.behavior)">
  <persistence version="9" />
  <languages>
    <use id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel" version="19" />
    <use id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior" version="2" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="pbu6" ref="r:83e946de-2a7f-4a4c-b3c9-4f671aa7f2db(org.iets3.core.expr.base.behavior)" />
    <import index="hwgx" ref="r:fd2980c8-676c-4b19-b524-18c70e02f8b7(com.mbeddr.core.base.behavior)" />
    <import index="gv6i" ref="r:9bc9d7aa-ccc3-46bd-805d-a743ad42928e(MetaJade.behaviour.structure)" implicit="true" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior">
      <concept id="1225194240794" name="jetbrains.mps.lang.behavior.structure.ConceptBehavior" flags="ng" index="13h7C7">
        <reference id="1225194240799" name="concept" index="13h7C2" />
        <child id="1225194240805" name="method" index="13h7CS" />
        <child id="1225194240801" name="constructor" index="13h7CW" />
      </concept>
      <concept id="1225194413805" name="jetbrains.mps.lang.behavior.structure.ConceptConstructorDeclaration" flags="in" index="13hLZK" />
      <concept id="1225194472830" name="jetbrains.mps.lang.behavior.structure.ConceptMethodDeclaration" flags="ng" index="13i0hz">
        <property id="1225194472832" name="isVirtual" index="13i0it" />
        <property id="1225194472834" name="isAbstract" index="13i0iv" />
        <reference id="1225194472831" name="overriddenMethod" index="13i0hy" />
      </concept>
      <concept id="1225194691553" name="jetbrains.mps.lang.behavior.structure.ThisNodeExpression" flags="nn" index="13iPFW" />
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242875" name="jetbrains.mps.baseLanguage.structure.PlusExpression" flags="nn" index="3cpWs3" />
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2" />
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="1224414427926" name="jetbrains.mps.baseLanguage.collections.structure.SequenceCreator" flags="nn" index="kMnCb">
        <child id="1224414456414" name="elementType" index="kMuH3" />
      </concept>
      <concept id="1151689724996" name="jetbrains.mps.baseLanguage.collections.structure.SequenceType" flags="in" index="A3Dl8">
        <child id="1151689745422" name="elementType" index="A3Ik2" />
      </concept>
    </language>
  </registry>
  <node concept="13h7C7" id="3I8nNvsLcCM">
    <property role="3GE5qa" value="" />
    <ref role="13h7C2" to="gv6i:3I8nNvsKOG3" resolve="BehaviourArgRef" />
    <node concept="13i0hz" id="49WTic8fT72" role="13h7CS">
      <property role="TrG5h" value="renderReadable" />
      <property role="13i0it" value="false" />
      <property role="13i0iv" value="false" />
      <ref role="13i0hy" to="pbu6:4Y0vh0cfqjE" resolve="renderReadable" />
      <node concept="3Tm1VV" id="49WTic8fT73" role="1B3o_S" />
      <node concept="3clFbS" id="49WTic8fT74" role="3clF47">
        <node concept="3clFbF" id="49WTic8fT75" role="3cqZAp">
          <node concept="2OqwBi" id="49WTic8fT76" role="3clFbG">
            <node concept="2OqwBi" id="49WTic8fT77" role="2Oq$k0">
              <node concept="13iPFW" id="49WTic8fT78" role="2Oq$k0" />
              <node concept="3TrEf2" id="49WTic8ggDO" role="2OqNvi">
                <ref role="3Tt5mk" to="gv6i:3I8nNvsKOG6" resolve="arg" />
              </node>
            </node>
            <node concept="3TrcHB" id="49WTic8fT7a" role="2OqNvi">
              <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="49WTic8fT7b" role="3clF45" />
    </node>
    <node concept="13i0hz" id="22hm_0zvyXU" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="target" />
      <ref role="13i0hy" to="pbu6:6rGLT0TevFd" resolve="target" />
      <node concept="3Tm1VV" id="22hm_0zvyXV" role="1B3o_S" />
      <node concept="3clFbS" id="22hm_0zvyXY" role="3clF47">
        <node concept="3clFbF" id="22hm_0zvyZQ" role="3cqZAp">
          <node concept="2OqwBi" id="22hm_0zvz1Z" role="3clFbG">
            <node concept="13iPFW" id="22hm_0zvyZP" role="2Oq$k0" />
            <node concept="3TrEf2" id="22hm_0zvz6B" role="2OqNvi">
              <ref role="3Tt5mk" to="gv6i:3I8nNvsKOG6" resolve="arg" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tqbb2" id="22hm_0zvyXZ" role="3clF45" />
    </node>
    <node concept="13hLZK" id="3I8nNvsLcCN" role="13h7CW">
      <node concept="3clFbS" id="3I8nNvsLcCO" role="2VODD2" />
    </node>
  </node>
  <node concept="13h7C7" id="2aA5E1v69$">
    <ref role="13h7C2" to="gv6i:2WQ3iBRxHZI" resolve="Behaviour" />
    <node concept="13i0hz" id="7dT_6n17Mxu" role="13h7CS">
      <property role="TrG5h" value="asString" />
      <ref role="13i0hy" to="pbu6:6iqfHNBPkjP" resolve="asString" />
      <node concept="3Tm1VV" id="7dT_6n17Mxv" role="1B3o_S" />
      <node concept="3clFbS" id="7dT_6n17Mxy" role="3clF47">
        <node concept="3clFbF" id="6iqfHNBPm8k" role="3cqZAp">
          <node concept="3cpWs3" id="6iqfHNBPmaD" role="3clFbG">
            <node concept="2OqwBi" id="6iqfHNBPmfP" role="3uHU7w">
              <node concept="13iPFW" id="6iqfHNBPmaK" role="2Oq$k0" />
              <node concept="3TrcHB" id="6iqfHNBPmrk" role="2OqNvi">
                <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
              </node>
            </node>
            <node concept="Xl_RD" id="6iqfHNBPm8j" role="3uHU7B">
              <property role="Xl_RC" value="[Behaviour Chunk] " />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="7dT_6n17Mxz" role="3clF45" />
    </node>
    <node concept="13i0hz" id="7dT_6n17Mx4" role="13h7CS">
      <property role="TrG5h" value="externallyReferenceableContentsInChunk" />
      <ref role="13i0hy" to="hwgx:6clJcrKmX4x" resolve="externallyReferenceableContentsInChunk" />
      <node concept="3Tm1VV" id="7dT_6n17Mx5" role="1B3o_S" />
      <node concept="3clFbS" id="7dT_6n17Mx9" role="3clF47">
        <node concept="3clFbF" id="1ZLt6euWy3f" role="3cqZAp">
          <node concept="2ShNRf" id="1ZLt6euWy3d" role="3clFbG">
            <node concept="kMnCb" id="1ZLt6euWyG1" role="2ShVmc">
              <node concept="3Tqbb2" id="1ZLt6euWyGq" role="kMuH3" />
            </node>
          </node>
        </node>
      </node>
      <node concept="A3Dl8" id="7dT_6n17Mxa" role="3clF45">
        <node concept="3Tqbb2" id="7dT_6n17Mxb" role="A3Ik2" />
      </node>
    </node>
    <node concept="13hLZK" id="2aA5E1v69_" role="13h7CW">
      <node concept="3clFbS" id="2aA5E1v69A" role="2VODD2" />
    </node>
  </node>
</model>

