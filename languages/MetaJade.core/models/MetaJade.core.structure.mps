<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:0a69036a-4d31-45e1-887d-acbe5b8039f5(MetaJade.core.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="9" />
    <devkit ref="78434eb8-b0e5-444b-850d-e7c4ad2da9ab(jetbrains.mps.devkit.aspect.structure)" />
  </languages>
  <imports>
    <import index="yv47" ref="r:da65683e-ff6f-430d-ab68-32a77df72c93(org.iets3.core.expr.toplevel.structure)" />
    <import index="av4b" ref="r:ba7faab6-2b80-43d5-8b95-0c440665312c(org.iets3.core.expr.tests.structure)" />
    <import index="vs0r" ref="r:f7764ca4-8c75-4049-922b-08516400a727(com.mbeddr.core.base.structure)" />
    <import index="4kwy" ref="r:657c9fde-2f36-4e61-ae17-20f02b8630ad(org.iets3.core.base.structure)" />
    <import index="hm2y" ref="r:66e07cb4-a4b0-4bf3-a36d-5e9ed1ff1bd3(org.iets3.core.expr.base.structure)" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="982eb8df-2c96-4bd7-9963-11712ea622e5" name="jetbrains.mps.lang.resources">
      <concept id="8974276187400029883" name="jetbrains.mps.lang.resources.structure.FileIcon" flags="ng" index="1QGGSu">
        <property id="2756621024541341363" name="file" index="1iqoE4" />
      </concept>
    </language>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="3348158742936976480" name="jetbrains.mps.lang.structure.structure.EnumerationMemberDeclaration" flags="ng" index="25R33">
        <property id="1421157252384165432" name="memberId" index="3tVfz5" />
        <property id="672037151186491528" name="presentation" index="1L1pqM" />
      </concept>
      <concept id="3348158742936976479" name="jetbrains.mps.lang.structure.structure.EnumerationDeclaration" flags="ng" index="25R3W">
        <child id="3348158742936976577" name="members" index="25R1y" />
      </concept>
      <concept id="1082978164218" name="jetbrains.mps.lang.structure.structure.DataTypeDeclaration" flags="ng" index="AxPO6">
        <property id="7791109065626895363" name="datatypeId" index="3F6X1D" />
      </concept>
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="6714410169261853888" name="conceptId" index="EcuMT" />
        <property id="4628067390765907488" name="conceptShortDescription" index="R4oN_" />
        <property id="4628067390765956802" name="abstract" index="R5$K7" />
        <property id="5092175715804935370" name="conceptAlias" index="34LRSv" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
        <child id="1071489727084" name="propertyDeclaration" index="1TKVEl" />
      </concept>
      <concept id="1169125989551" name="jetbrains.mps.lang.structure.structure.InterfaceConceptDeclaration" flags="ig" index="PlHQZ">
        <child id="1169127546356" name="extends" index="PrDN$" />
      </concept>
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <property id="1096454100552" name="rootable" index="19KtqR" />
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="6327362524875300597" name="icon" index="rwd14" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288299" name="jetbrains.mps.lang.structure.structure.PropertyDeclaration" flags="ig" index="1TJgyi">
        <property id="241647608299431129" name="propertyId" index="IQ2nx" />
        <reference id="1082985295845" name="dataType" index="AX2Wp" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599893252" name="sourceCardinality" index="20lbJX" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <property id="241647608299431140" name="linkId" index="IQ2ns" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1TIwiD" id="2tMu91EWsLG">
    <property role="EcuMT" value="2842466876489059436" />
    <property role="TrG5h" value="TestSuite" />
    <property role="19KtqR" value="true" />
    <property role="34LRSv" value="Test Suite" />
    <property role="R4oN_" value="Unit testing functions" />
    <property role="3GE5qa" value="" />
    <ref role="1TJDcQ" to="av4b:ub9nkyK62f" resolve="TestSuite" />
    <node concept="1QGGSu" id="1mu$Ayqxm4R" role="rwd14">
      <property role="1iqoE4" value="${module}/icons/testsuite.png" />
    </node>
  </node>
  <node concept="1TIwiD" id="2tMu91FTK9z">
    <property role="TrG5h" value="AbstractChunk" />
    <property role="EcuMT" value="8295235836482040281" />
    <property role="R4oN_" value="--" />
    <property role="R5$K7" value="true" />
    <property role="3GE5qa" value="chunk" />
    <ref role="1TJDcQ" to="vs0r:6clJcrJYOUA" resolve="Chunk" />
    <node concept="1TJgyj" id="28q_2hFENgT" role="1TKVEi">
      <property role="IQ2ns" value="2457439430830142521" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="imports" />
      <property role="20lbJX" value="fLJekj5/_0__n" />
      <ref role="20lvS9" node="2aA5E1npHs" resolve="IChunkDep" />
    </node>
    <node concept="PrWs8" id="6HHp2WndiBB" role="PzmwI">
      <ref role="PrY4T" to="vs0r:4qSf1u1TQeO" resolve="IContainerOfUniqueNames" />
    </node>
    <node concept="PrWs8" id="6iqfHNBPlMI" role="PzmwI">
      <ref role="PrY4T" to="hm2y:6iqfHNBPkjp" resolve="IContainmentStackMember" />
    </node>
    <node concept="PrWs8" id="5ElkanQ9DBL" role="PzmwI">
      <ref role="PrY4T" to="hm2y:5ElkanQ81eS" resolve="IDocumentableWordContainer" />
    </node>
    <node concept="PrWs8" id="7dT_6n0X3ZY" role="PzmwI">
      <ref role="PrY4T" to="4kwy:cJpacq5T0O" resolve="IValidNamedConcept" />
    </node>
    <node concept="PrWs8" id="51K8u7RELJw" role="PzmwI">
      <ref role="PrY4T" to="hm2y:ORfz$DS6_k" resolve="IMayAllowEffect" />
    </node>
  </node>
  <node concept="1TIwiD" id="28q_2hFej6t">
    <property role="EcuMT" value="2457439430822670749" />
    <property role="TrG5h" value="Module" />
    <property role="34LRSv" value="Module" />
    <property role="19KtqR" value="true" />
    <property role="R4oN_" value="a module" />
    <property role="3GE5qa" value="module" />
    <ref role="1TJDcQ" node="2tMu91FTK9z" resolve="AbstractChunk" />
    <node concept="1QGGSu" id="28q_2hFej6w" role="rwd14">
      <property role="1iqoE4" value="${module}/icons/module.png" />
    </node>
    <node concept="PrWs8" id="2aA5E1v86T" role="PzmwI">
      <ref role="PrY4T" node="3PPOQnmA6ox" resolve="ITopLevelExprContainer" />
    </node>
  </node>
  <node concept="1TIwiD" id="1wDV7BCwUzw">
    <property role="EcuMT" value="1741182739291547872" />
    <property role="TrG5h" value="ModuleDependency" />
    <property role="3GE5qa" value="module" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="2aA5E1nukh" role="PzmwI">
      <ref role="PrY4T" node="2aA5E1npHs" resolve="IChunkDep" />
    </node>
  </node>
  <node concept="1TIwiD" id="1wDV7BCtyf3">
    <property role="EcuMT" value="1741182739290661826" />
    <property role="TrG5h" value="AbstractExpression" />
    <property role="R5$K7" value="true" />
    <property role="3GE5qa" value="statements" />
    <ref role="1TJDcQ" to="hm2y:6sdnDbSla17" resolve="Expression" />
    <node concept="PrWs8" id="4iR98LHXp9K" role="PzmwI">
      <ref role="PrY4T" to="hm2y:6KxoTHgLv_I" resolve="IMayHaveEffect" />
    </node>
    <node concept="PrWs8" id="22Cof6WFB_s" role="PzmwI">
      <ref role="PrY4T" to="hm2y:6NJfo6_rTeO" resolve="IBigExpression" />
    </node>
  </node>
  <node concept="PlHQZ" id="3PPOQnmxRe5">
    <property role="TrG5h" value="IStatementContainer" />
    <property role="R4oN_" value="--" />
    <property role="EcuMT" value="1741182739292157391" />
    <property role="3GE5qa" value="statements" />
  </node>
  <node concept="PlHQZ" id="3PPOQnmA6ox">
    <property role="EcuMT" value="4428678242934220321" />
    <property role="TrG5h" value="ITopLevelExprContainer" />
    <property role="3GE5qa" value="toplevel" />
    <node concept="1TJgyj" id="1wDV7BCzfng" role="1TKVEi">
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="contents" />
      <property role="20lbJX" value="fLJekj5/_0__n" />
      <property role="IQ2ns" value="1741182739292157392" />
      <ref role="20lvS9" to="yv47:2uR5X5ayM7T" resolve="IToplevelExprContent" />
    </node>
  </node>
  <node concept="PlHQZ" id="2aA5E1npHs">
    <property role="TrG5h" value="IChunkDep" />
    <property role="EcuMT" value="2569313154175320625" />
    <property role="3GE5qa" value="chunk" />
    <node concept="1TJgyj" id="2aA5E1e5qp" role="1TKVEi">
      <property role="IQ2ns" value="39011061268502169" />
      <property role="20kJfa" value="chunk" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" to="vs0r:6clJcrJYOUA" resolve="Chunk" />
    </node>
    <node concept="PrWs8" id="2aA5E1e5qn" role="PrDN$">
      <ref role="PrY4T" to="vs0r:6clJcrJZLbn" resolve="IChunkDependency" />
    </node>
  </node>
  <node concept="1TIwiD" id="80jriwjIN">
    <property role="EcuMT" value="2253134785231795" />
    <property role="TrG5h" value="UnitType" />
    <ref role="1TJDcQ" to="hm2y:6sdnDbSlaok" resolve="Type" />
  </node>
  <node concept="1TIwiD" id="1ZLt6euI0yq">
    <property role="EcuMT" value="2301748855776479386" />
    <property role="TrG5h" value="NonStaticFunctionCall" />
    <ref role="1TJDcQ" to="yv47:49WTic8gFfG" resolve="FunctionCall" />
  </node>
  <node concept="1TIwiD" id="6OKQOIXjSYm">
    <property role="EcuMT" value="7868029667361394582" />
    <property role="3GE5qa" value="aid.operations" />
    <property role="TrG5h" value="GetLocalName" />
    <property role="34LRSv" value="localName" />
    <property role="R4oN_" value="get the local name of the agent" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="6OKQOIXjVBr" role="PzmwI">
      <ref role="PrY4T" node="6OKQOIXjSYo" resolve="IAgentIdOp" />
    </node>
  </node>
  <node concept="1TIwiD" id="6OKQOIXjVBv">
    <property role="EcuMT" value="7868029667361405407" />
    <property role="3GE5qa" value="aid.operations" />
    <property role="TrG5h" value="GetName" />
    <property role="34LRSv" value="name" />
    <property role="R4oN_" value="get the name of the agent" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="6OKQOIXjVJs" role="PzmwI">
      <ref role="PrY4T" node="6OKQOIXjSYo" resolve="IAgentIdOp" />
    </node>
  </node>
  <node concept="PlHQZ" id="6OKQOIXjSYo">
    <property role="TrG5h" value="IAgentIdOp" />
    <property role="3GE5qa" value="aid.operations" />
    <property role="EcuMT" value="7868029667361394584" />
    <node concept="PrWs8" id="6OKQOIXjSYp" role="PrDN$">
      <ref role="PrY4T" to="hm2y:7NJy08a3O9a" resolve="IDotTarget" />
    </node>
  </node>
  <node concept="1TIwiD" id="6OKQOIXjSXU">
    <property role="EcuMT" value="7868029667361394554" />
    <property role="TrG5h" value="AIDDeclaration" />
    <property role="3GE5qa" value="aid" />
    <property role="34LRSv" value="AID" />
    <ref role="1TJDcQ" to="hm2y:6sdnDbSla17" resolve="Expression" />
    <node concept="1TJgyj" id="7AVCbhhTTHU" role="1TKVEi">
      <property role="IQ2ns" value="8771781395562863482" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="localName" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" to="hm2y:6sdnDbSla17" resolve="Expression" />
    </node>
    <node concept="1TJgyj" id="7AVCbhhTTHW" role="1TKVEi">
      <property role="IQ2ns" value="8771781395562863484" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="platformId" />
      <ref role="20lvS9" to="hm2y:6sdnDbSla17" resolve="Expression" />
    </node>
  </node>
  <node concept="1TIwiD" id="3EtpyNPSerU">
    <property role="EcuMT" value="4223644368833144570" />
    <property role="TrG5h" value="AIDReference" />
    <property role="3GE5qa" value="aid" />
    <ref role="1TJDcQ" to="hm2y:6sdnDbSla17" resolve="Expression" />
    <node concept="1TJgyj" id="3EtpyNPSerV" role="1TKVEi">
      <property role="20lbJX" value="fLJekj4/1" />
      <property role="IQ2ns" value="4223644368833144571" />
      <property role="20kJfa" value="aidDeclaration" />
      <ref role="20lvS9" node="6OKQOIXjSXU" resolve="AIDDeclaration" />
    </node>
  </node>
  <node concept="1TIwiD" id="3JB$QJ8oGPX">
    <property role="EcuMT" value="4316580858990611837" />
    <property role="TrG5h" value="AIDType" />
    <property role="3GE5qa" value="aid" />
    <property role="34LRSv" value="AID" />
    <ref role="1TJDcQ" to="hm2y:6sdnDbSlaok" resolve="Type" />
  </node>
  <node concept="25R3W" id="4MI7ZB$IHcG">
    <property role="3F6X1D" value="5525388950974944044" />
    <property role="TrG5h" value="PerformativeDeclaration" />
    <property role="3GE5qa" value="performative" />
    <node concept="25R33" id="4MI7ZB$IHcH" role="25R1y">
      <property role="3tVfz5" value="5525388950974944045" />
      <property role="TrG5h" value="AcceptProposal" />
      <property role="1L1pqM" value="Accept Proposal" />
    </node>
    <node concept="25R33" id="4MI7ZB$IHcI" role="25R1y">
      <property role="3tVfz5" value="5525388950974944046" />
      <property role="TrG5h" value="Agree" />
      <property role="1L1pqM" value="Agree" />
    </node>
    <node concept="25R33" id="1MgVVihmOQe" role="25R1y">
      <property role="3tVfz5" value="2058408588520148366" />
      <property role="TrG5h" value="Cancel" />
      <property role="1L1pqM" value="Cancel" />
    </node>
    <node concept="25R33" id="4MI7ZB$IHcL" role="25R1y">
      <property role="3tVfz5" value="5525388950974944049" />
      <property role="TrG5h" value="CallForProposal" />
      <property role="1L1pqM" value="Call For Proposal" />
    </node>
    <node concept="25R33" id="4MI7ZB$IHcU" role="25R1y">
      <property role="3tVfz5" value="5525388950974944058" />
      <property role="TrG5h" value="Confirm" />
      <property role="1L1pqM" value="Confirm" />
    </node>
    <node concept="25R33" id="4MI7ZB$IHcZ" role="25R1y">
      <property role="3tVfz5" value="5525388950974944063" />
      <property role="TrG5h" value="Disconfirm" />
      <property role="1L1pqM" value="Disconfirm" />
    </node>
    <node concept="25R33" id="4MI7ZB$IHd5" role="25R1y">
      <property role="3tVfz5" value="5525388950974944069" />
      <property role="TrG5h" value="Failure" />
      <property role="1L1pqM" value="Failure" />
    </node>
    <node concept="25R33" id="4MI7ZB$IHdc" role="25R1y">
      <property role="3tVfz5" value="5525388950974944076" />
      <property role="TrG5h" value="Inform" />
      <property role="1L1pqM" value="Inform" />
    </node>
    <node concept="25R33" id="1MgVVihmORg" role="25R1y">
      <property role="3tVfz5" value="2058408588520148432" />
      <property role="TrG5h" value="InformIf" />
      <property role="1L1pqM" value="Inform If" />
    </node>
    <node concept="25R33" id="4MI7ZB$IHd$" role="25R1y">
      <property role="3tVfz5" value="5525388950974944100" />
      <property role="TrG5h" value="InformRef" />
      <property role="1L1pqM" value="Inform Ref" />
    </node>
    <node concept="25R33" id="4MI7ZB$IHdH" role="25R1y">
      <property role="3tVfz5" value="5525388950974944109" />
      <property role="TrG5h" value="NotUnderstood" />
      <property role="1L1pqM" value="Not Understood" />
    </node>
    <node concept="25R33" id="4MI7ZB$IHe2" role="25R1y">
      <property role="3tVfz5" value="5525388950974944130" />
      <property role="TrG5h" value="Propose" />
      <property role="1L1pqM" value="Propose" />
    </node>
    <node concept="25R33" id="4MI7ZB$IHeP" role="25R1y">
      <property role="3tVfz5" value="5525388950974944181" />
      <property role="TrG5h" value="QueryIf" />
      <property role="1L1pqM" value="Query If" />
    </node>
    <node concept="25R33" id="4MI7ZB$IHf3" role="25R1y">
      <property role="3tVfz5" value="5525388950974944195" />
      <property role="TrG5h" value="QueryRef" />
      <property role="1L1pqM" value="Query Ref" />
    </node>
    <node concept="25R33" id="4MI7ZB$IHfi" role="25R1y">
      <property role="3tVfz5" value="5525388950974944210" />
      <property role="TrG5h" value="Refuse" />
      <property role="1L1pqM" value="Refuse" />
    </node>
    <node concept="25R33" id="4MI7ZB$IHfy" role="25R1y">
      <property role="3tVfz5" value="5525388950974944226" />
      <property role="TrG5h" value="RejectProposal" />
      <property role="1L1pqM" value="Reject Proposal" />
    </node>
    <node concept="25R33" id="4MI7ZB$IHfN" role="25R1y">
      <property role="3tVfz5" value="5525388950974944243" />
      <property role="TrG5h" value="Request" />
      <property role="1L1pqM" value="Request" />
    </node>
    <node concept="25R33" id="4MI7ZB$IHg5" role="25R1y">
      <property role="3tVfz5" value="5525388950974944261" />
      <property role="TrG5h" value="RequestWhen" />
      <property role="1L1pqM" value="Request When" />
    </node>
    <node concept="25R33" id="4MI7ZB$IHgo" role="25R1y">
      <property role="3tVfz5" value="5525388950974944280" />
      <property role="TrG5h" value="RequestWhenever" />
      <property role="1L1pqM" value="Request Whenever" />
    </node>
    <node concept="25R33" id="4MI7ZB$IHgG" role="25R1y">
      <property role="3tVfz5" value="5525388950974944300" />
      <property role="TrG5h" value="Subscribe" />
      <property role="1L1pqM" value="Subscribe" />
    </node>
    <node concept="25R33" id="4MI7ZB$IHee" role="25R1y">
      <property role="3tVfz5" value="5525388950974944142" />
      <property role="TrG5h" value="Proxy" />
      <property role="1L1pqM" value="Proxy" />
    </node>
    <node concept="25R33" id="4MI7ZB$IHdR" role="25R1y">
      <property role="3tVfz5" value="5525388950974944119" />
      <property role="TrG5h" value="Propagate" />
      <property role="1L1pqM" value="Propagate" />
    </node>
  </node>
  <node concept="1TIwiD" id="6OKQOIXeu_H">
    <property role="EcuMT" value="7868029667359975789" />
    <property role="TrG5h" value="PerformativeType" />
    <property role="3GE5qa" value="performative" />
    <property role="34LRSv" value="performative" />
    <property role="R4oN_" value="Performative of an ACL message" />
    <ref role="1TJDcQ" to="hm2y:6sdnDbSlaok" resolve="Type" />
  </node>
  <node concept="PlHQZ" id="Oow4MYDA1t">
    <property role="TrG5h" value="IDefinePerformative" />
    <property role="EcuMT" value="943645189038563415" />
    <node concept="1TJgyi" id="Oow4MYDA1o" role="1TKVEl">
      <property role="IQ2nx" value="943645189038563416" />
      <property role="TrG5h" value="performative" />
      <ref role="AX2Wp" node="4MI7ZB$IHcG" resolve="PerformativeDeclaration" />
    </node>
  </node>
  <node concept="1TIwiD" id="28$c8FRSadP">
    <property role="EcuMT" value="2460144669873447797" />
    <property role="TrG5h" value="Log" />
    <property role="34LRSv" value="log" />
    <property role="3GE5qa" value="statements" />
    <ref role="1TJDcQ" node="1wDV7BCtyf3" resolve="AbstractExpression" />
    <node concept="1TJgyj" id="28$c8FRYT6O" role="1TKVEi">
      <property role="IQ2ns" value="2460144669875212724" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="value" />
      <ref role="20lvS9" to="hm2y:6sdnDbSla17" resolve="Expression" />
    </node>
  </node>
  <node concept="PlHQZ" id="27lcHhoqvHJ">
    <property role="EcuMT" value="2437910660888787823" />
    <property role="TrG5h" value="ITopLevelStatement" />
    <property role="3GE5qa" value="toplevel" />
    <node concept="PrWs8" id="27lcHhoqvHK" role="PrDN$">
      <ref role="PrY4T" to="yv47:2uR5X5ayM7T" resolve="IToplevelExprContent" />
    </node>
  </node>
  <node concept="1TIwiD" id="5zd1xVkqXAQ">
    <property role="EcuMT" value="6398777375045966262" />
    <property role="TrG5h" value="Stop" />
    <property role="34LRSv" value="stop" />
    <property role="3GE5qa" value="statements" />
    <ref role="1TJDcQ" node="1wDV7BCtyf3" resolve="AbstractExpression" />
  </node>
</model>

