<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:7f49141c-8a6f-413d-806c-571ec4aac218(MetaJade.ontology.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="14" />
    <use id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions" version="4" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="21iy" ref="r:3c601dd3-56a8-40ab-a417-66a36a925be1(MetaJade.core.editor)" />
    <import index="yv47" ref="r:da65683e-ff6f-430d-ab68-32a77df72c93(org.iets3.core.expr.toplevel.structure)" />
    <import index="oq0c" ref="r:6c6155f0-4bbe-4af5-8c26-244d570e21e4(org.iets3.core.expr.base.plugin)" />
    <import index="3jzb" ref="r:606f7dcd-0d13-4ed2-8b48-a19323636f6b(MetaJade.ontology.structure)" implicit="true" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
    <import index="tpco" ref="r:00000000-0000-4000-0000-011c89590284(jetbrains.mps.lang.core.editor)" implicit="true" />
  </imports>
  <registry>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="1071666914219" name="jetbrains.mps.lang.editor.structure.ConceptEditorDeclaration" flags="ig" index="24kQdi" />
      <concept id="1176897764478" name="jetbrains.mps.lang.editor.structure.QueryFunction_NodeFactory" flags="in" index="4$FPG" />
      <concept id="6822301196700715228" name="jetbrains.mps.lang.editor.structure.ConceptEditorHintDeclarationReference" flags="ig" index="2aJ2om">
        <reference id="5944657839026714445" name="hint" index="2$4xQ3" />
      </concept>
      <concept id="1140524381322" name="jetbrains.mps.lang.editor.structure.CellModel_ListWithRole" flags="ng" index="2czfm3">
        <child id="1176897874615" name="nodeFactory" index="4_6I_" />
        <child id="1140524464360" name="cellLayout" index="2czzBx" />
        <child id="1140524464359" name="emptyCellModel" index="2czzBI" />
      </concept>
      <concept id="1106270571710" name="jetbrains.mps.lang.editor.structure.CellLayout_Vertical" flags="nn" index="2iRkQZ" />
      <concept id="1237303669825" name="jetbrains.mps.lang.editor.structure.CellLayout_Indent" flags="nn" index="l2Vlx" />
      <concept id="4242538589859161874" name="jetbrains.mps.lang.editor.structure.ExplicitHintsSpecification" flags="ng" index="2w$q5c">
        <child id="4242538589859162459" name="hints" index="2w$qW5" />
      </concept>
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="1239814640496" name="jetbrains.mps.lang.editor.structure.CellLayout_VerticalGrid" flags="nn" index="2EHx9g" />
      <concept id="1160493135005" name="jetbrains.mps.lang.editor.structure.CellMenuPart_PropertyValues_GetValues" flags="in" index="MLZmj" />
      <concept id="1164824717996" name="jetbrains.mps.lang.editor.structure.CellMenuDescriptor" flags="ng" index="OXEIz">
        <child id="1164824815888" name="cellMenuPart" index="OY2wv" />
      </concept>
      <concept id="1164833692343" name="jetbrains.mps.lang.editor.structure.CellMenuPart_PropertyValues" flags="ng" index="PvTIS">
        <child id="1164833692344" name="valuesFunction" index="PvTIR" />
      </concept>
      <concept id="1078939183254" name="jetbrains.mps.lang.editor.structure.CellModel_Component" flags="sg" stub="3162947552742194261" index="PMmxH">
        <reference id="1078939183255" name="editorComponent" index="PMmxG" />
      </concept>
      <concept id="1186414536763" name="jetbrains.mps.lang.editor.structure.BooleanStyleSheetItem" flags="ln" index="VOi$J">
        <property id="1186414551515" name="flag" index="VOm3f" />
      </concept>
      <concept id="1186414860679" name="jetbrains.mps.lang.editor.structure.EditableStyleClassItem" flags="ln" index="VPxyj" />
      <concept id="1186414928363" name="jetbrains.mps.lang.editor.structure.SelectableStyleSheetItem" flags="ln" index="VPM3Z" />
      <concept id="1186415722038" name="jetbrains.mps.lang.editor.structure.FontSizeStyleClassItem" flags="ln" index="VSNWy">
        <property id="1221209241505" name="value" index="1lJzqX" />
      </concept>
      <concept id="1381004262292414836" name="jetbrains.mps.lang.editor.structure.ICellStyle" flags="ng" index="1k5N5V">
        <reference id="1381004262292426837" name="parentStyleClass" index="1k5W1q" />
      </concept>
      <concept id="1139848536355" name="jetbrains.mps.lang.editor.structure.CellModel_WithRole" flags="ng" index="1$h60E">
        <reference id="1140103550593" name="relationDeclaration" index="1NtTu8" />
      </concept>
      <concept id="1073389214265" name="jetbrains.mps.lang.editor.structure.EditorCellModel" flags="ng" index="3EYTF0">
        <child id="1164826688380" name="menuDescriptor" index="P5bDN" />
      </concept>
      <concept id="1073389446423" name="jetbrains.mps.lang.editor.structure.CellModel_Collection" flags="sn" stub="3013115976261988961" index="3EZMnI">
        <child id="1106270802874" name="cellLayout" index="2iSdaV" />
        <child id="1073389446424" name="childCellModel" index="3EZMnx" />
      </concept>
      <concept id="1073389577006" name="jetbrains.mps.lang.editor.structure.CellModel_Constant" flags="sn" stub="3610246225209162225" index="3F0ifn">
        <property id="1082639509531" name="nullText" index="ilYzB" />
        <property id="1073389577007" name="text" index="3F0ifm" />
      </concept>
      <concept id="1073389658414" name="jetbrains.mps.lang.editor.structure.CellModel_Property" flags="sg" stub="730538219796134133" index="3F0A7n" />
      <concept id="1219418625346" name="jetbrains.mps.lang.editor.structure.IStyleContainer" flags="ng" index="3F0Thp">
        <child id="1219418656006" name="styleItem" index="3F10Kt" />
      </concept>
      <concept id="1073390211982" name="jetbrains.mps.lang.editor.structure.CellModel_RefNodeList" flags="sg" stub="2794558372793454595" index="3F2HdR">
        <child id="7279578193766667846" name="addHints" index="78xua" />
      </concept>
      <concept id="1163613822479" name="jetbrains.mps.lang.editor.structure.CellMenuPart_Abstract_editedNode" flags="nn" index="3GMtW1" />
      <concept id="1198256887712" name="jetbrains.mps.lang.editor.structure.CellModel_Indent" flags="ng" index="3XFhqQ" />
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1180636770613" name="jetbrains.mps.lang.smodel.structure.SNodeCreator" flags="nn" index="3zrR0B">
        <child id="1180636770616" name="createdType" index="3zrR0E" />
      </concept>
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
    </language>
  </registry>
  <node concept="24kQdi" id="4MI7ZB$Dtqg">
    <ref role="1XX52x" to="3jzb:4MI7ZB$Dtqf" resolve="Ontology" />
    <node concept="3EZMnI" id="3JB$QJ8aLfN" role="2wV5jI">
      <node concept="2iRkQZ" id="3JB$QJ8aLfO" role="2iSdaV" />
      <node concept="3EZMnI" id="4MI7ZB$DBk0" role="3EZMnx">
        <node concept="2iRkQZ" id="4MI7ZB$DBk1" role="2iSdaV" />
        <node concept="3EZMnI" id="4MI7ZB$Dtqk" role="3EZMnx">
          <node concept="3F0ifn" id="4MI7ZB$IyYa" role="3EZMnx">
            <property role="3F0ifm" value="ontology" />
            <ref role="1k5W1q" to="21iy:7kuDSjsqD3B" resolve="metajadeKeyword" />
            <node concept="VSNWy" id="7dT_6n10e$z" role="3F10Kt">
              <property role="1lJzqX" value="21" />
            </node>
            <node concept="VPM3Z" id="7dT_6n10e$C" role="3F10Kt" />
          </node>
          <node concept="3F0A7n" id="4MI7ZB$Dtqr" role="3EZMnx">
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
            <node concept="VSNWy" id="7dT_6n10e$G" role="3F10Kt">
              <property role="1lJzqX" value="21" />
            </node>
          </node>
          <node concept="l2Vlx" id="7dT_6n17KEK" role="2iSdaV" />
        </node>
        <node concept="3F0ifn" id="7dT_6n1vdD7" role="3EZMnx">
          <node concept="VPM3Z" id="7dT_6n1vdDD" role="3F10Kt" />
        </node>
        <node concept="3EZMnI" id="4MI7ZB$DBkl" role="3EZMnx">
          <node concept="VPM3Z" id="4MI7ZB$DBkn" role="3F10Kt" />
          <node concept="3XFhqQ" id="4MI7ZB$DBk$" role="3EZMnx" />
          <node concept="3F2HdR" id="51gvYr6KOT6" role="3EZMnx">
            <ref role="1NtTu8" to="3jzb:ub9nkyK62i" resolve="contents" />
            <node concept="2iRkQZ" id="51gvYr6KOTc" role="2czzBx" />
            <node concept="3F0ifn" id="51gvYr6L0PQ" role="2czzBI">
              <property role="ilYzB" value="element schemas" />
            </node>
            <node concept="4$FPG" id="51gvYr6PosD" role="4_6I_">
              <node concept="3clFbS" id="51gvYr6PosE" role="2VODD2">
                <node concept="3clFbF" id="51gvYr6PouI" role="3cqZAp">
                  <node concept="2ShNRf" id="51gvYr6PouG" role="3clFbG">
                    <node concept="3zrR0B" id="51gvYr6PpTo" role="2ShVmc">
                      <node concept="3Tqbb2" id="51gvYr6PpTq" role="3zrR0E">
                        <ref role="ehGHo" to="3jzb:2aA5E1$b4j" resolve="IOntologyElement" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="2w$q5c" id="6cOMG4RZnw7" role="78xua">
              <node concept="2aJ2om" id="6cOMG4RZnw8" role="2w$qW5">
                <ref role="2$4xQ3" to="21iy:2WQ3iBRGN5o" resolve="PseudocodeSetPresentation" />
              </node>
              <node concept="2aJ2om" id="6cOMG4RZnwb" role="2w$qW5">
                <ref role="2$4xQ3" to="21iy:wO0wsRnNTe" resolve="PseudocodeListPresentation" />
              </node>
              <node concept="2aJ2om" id="6cOMG4RZnwe" role="2w$qW5">
                <ref role="2$4xQ3" to="21iy:2WQ3iBRGNcR" resolve="PseudocodeMapPresentation" />
              </node>
            </node>
          </node>
          <node concept="l2Vlx" id="7dT_6n17IP$" role="2iSdaV" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="7dT_6n1a1vt">
    <property role="3GE5qa" value="elements" />
    <ref role="1XX52x" to="3jzb:7dT_6n1a_NX" resolve="ElementSchema" />
    <node concept="3EZMnI" id="11foXHHQY7J" role="2wV5jI">
      <node concept="2iRkQZ" id="11foXHHQY7K" role="2iSdaV" />
      <node concept="3EZMnI" id="11foXHHQXIp" role="3EZMnx">
        <node concept="PMmxH" id="7dT_6n1a3tz" role="3EZMnx">
          <ref role="PMmxG" to="tpco:2wZex4PafBj" resolve="alias" />
          <node concept="VPxyj" id="7dT_6n1a3Ma" role="3F10Kt" />
        </node>
        <node concept="3F0A7n" id="11foXHHQY7z" role="3EZMnx">
          <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          <node concept="OXEIz" id="64wA3CCizQY" role="P5bDN">
            <node concept="PvTIS" id="64wA3CCizR0" role="OY2wv">
              <node concept="MLZmj" id="64wA3CCizR1" role="PvTIR">
                <node concept="3clFbS" id="64wA3CCizR2" role="2VODD2">
                  <node concept="3clFbF" id="64wA3CCizR3" role="3cqZAp">
                    <node concept="2YIFZM" id="64wA3CCizR4" role="3clFbG">
                      <ref role="37wK5l" to="oq0c:UwUtc1okvZ" resolve="proposals" />
                      <ref role="1Pybhc" to="oq0c:UwUtc1nzGQ" resolve="NC" />
                      <node concept="3GMtW1" id="64wA3CCizR5" role="37wK5m" />
                      <node concept="2OqwBi" id="64wA3CCizR6" role="37wK5m">
                        <node concept="3GMtW1" id="64wA3CCizR7" role="2Oq$k0" />
                        <node concept="3TrcHB" id="64wA3CCizR8" role="2OqNvi">
                          <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3F0ifn" id="1MgVVihrZOe" role="3EZMnx">
          <property role="3F0ifm" value="{" />
        </node>
        <node concept="l2Vlx" id="3PPOQnmweOo" role="2iSdaV" />
      </node>
      <node concept="3EZMnI" id="3PPOQnmwexa" role="3EZMnx">
        <node concept="l2Vlx" id="3PPOQnmwexb" role="2iSdaV" />
        <node concept="3XFhqQ" id="11foXHHR03G" role="3EZMnx" />
        <node concept="3F2HdR" id="11foXHHR041" role="3EZMnx">
          <ref role="1NtTu8" to="yv47:xu7xcKioz5" resolve="members" />
          <node concept="2EHx9g" id="11foXHHR04a" role="2czzBx" />
          <node concept="3F0ifn" id="11foXHHR04e" role="2czzBI">
            <property role="3F0ifm" value="" />
            <property role="ilYzB" value="&lt;no entry&gt;" />
            <node concept="VPxyj" id="11foXHHR04h" role="3F10Kt">
              <property role="VOm3f" value="true" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3EZMnI" id="1MgVVihs55E" role="3EZMnx">
        <node concept="l2Vlx" id="1MgVVihs55F" role="2iSdaV" />
        <node concept="3F0ifn" id="1MgVVihrBGs" role="3EZMnx">
          <property role="3F0ifm" value="}" />
        </node>
      </node>
      <node concept="3F0ifn" id="1MgVVihs4cf" role="3EZMnx">
        <node concept="VPM3Z" id="1MgVVihsmgu" role="3F10Kt" />
      </node>
    </node>
  </node>
</model>

