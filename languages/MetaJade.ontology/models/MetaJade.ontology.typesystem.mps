<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:7f0b66e3-b2f7-43f6-81f1-827968c6fb35(MetaJade.ontology.typesystem)">
  <persistence version="9" />
  <languages>
    <use id="7a5dda62-9140-4668-ab76-d5ed1746f2b2" name="jetbrains.mps.lang.typesystem" version="5" />
    <devkit ref="00000000-0000-4000-0000-1de82b3a4936(jetbrains.mps.devkit.aspect.typesystem)" />
  </languages>
  <imports>
    <import index="yv47" ref="r:da65683e-ff6f-430d-ab68-32a77df72c93(org.iets3.core.expr.toplevel.structure)" />
    <import index="5qo5" ref="r:6d93ddb1-b0b0-4eee-8079-51303666672a(org.iets3.core.expr.simpleTypes.structure)" />
    <import index="pbu6" ref="r:83e946de-2a7f-4a4c-b3c9-4f671aa7f2db(org.iets3.core.expr.base.behavior)" implicit="true" />
    <import index="tpcu" ref="r:00000000-0000-4000-0000-011c89590282(jetbrains.mps.lang.core.behavior)" implicit="true" />
    <import index="700h" ref="r:61b1de80-490d-4fee-8e95-b956503290e9(org.iets3.core.expr.collections.structure)" implicit="true" />
    <import index="os74" ref="r:0a69036a-4d31-45e1-887d-acbe5b8039f5(MetaJade.core.structure)" implicit="true" />
    <import index="3jzb" ref="r:606f7dcd-0d13-4ed2-8b48-a19323636f6b(MetaJade.ontology.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1080223426719" name="jetbrains.mps.baseLanguage.structure.OrExpression" flags="nn" index="22lmx$" />
      <concept id="1154032098014" name="jetbrains.mps.baseLanguage.structure.AbstractLoopStatement" flags="nn" index="2LF5Ji">
        <child id="1154032183016" name="body" index="2LFqv$" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242875" name="jetbrains.mps.baseLanguage.structure.PlusExpression" flags="nn" index="3cpWs3" />
      <concept id="1079359253375" name="jetbrains.mps.baseLanguage.structure.ParenthesizedExpression" flags="nn" index="1eOMI4">
        <child id="1079359253376" name="expression" index="1eOMHV" />
      </concept>
      <concept id="1081516740877" name="jetbrains.mps.baseLanguage.structure.NotExpression" flags="nn" index="3fqX7Q">
        <child id="1081516765348" name="expression" index="3fr31v" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
    </language>
    <language id="7a5dda62-9140-4668-ab76-d5ed1746f2b2" name="jetbrains.mps.lang.typesystem">
      <concept id="1175517767210" name="jetbrains.mps.lang.typesystem.structure.ReportErrorStatement" flags="nn" index="2MkqsV">
        <child id="1175517851849" name="errorString" index="2MkJ7o" />
      </concept>
      <concept id="1195213580585" name="jetbrains.mps.lang.typesystem.structure.AbstractCheckingRule" flags="ig" index="18hYwZ">
        <child id="1195213635060" name="body" index="18ibNy" />
      </concept>
      <concept id="1195214364922" name="jetbrains.mps.lang.typesystem.structure.NonTypesystemRule" flags="ig" index="18kY7G" />
      <concept id="3937244445246642777" name="jetbrains.mps.lang.typesystem.structure.AbstractReportStatement" flags="ng" index="1urrMJ">
        <child id="3937244445246642781" name="nodeToReport" index="1urrMF" />
      </concept>
      <concept id="1174642788531" name="jetbrains.mps.lang.typesystem.structure.ConceptReference" flags="ig" index="1YaCAy">
        <reference id="1174642800329" name="concept" index="1YaFvo" />
      </concept>
      <concept id="1174648085619" name="jetbrains.mps.lang.typesystem.structure.AbstractRule" flags="ng" index="1YuPPy">
        <child id="1174648101952" name="applicableNode" index="1YuTPh" />
      </concept>
      <concept id="1174650418652" name="jetbrains.mps.lang.typesystem.structure.ApplicableNodeReference" flags="nn" index="1YBJjd">
        <reference id="1174650432090" name="applicableNode" index="1YBMHb" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="1139621453865" name="jetbrains.mps.lang.smodel.structure.Node_IsInstanceOfOperation" flags="nn" index="1mIQ4w">
        <child id="1177027386292" name="conceptArgument" index="cj9EA" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="1153943597977" name="jetbrains.mps.baseLanguage.collections.structure.ForEachStatement" flags="nn" index="2Gpval">
        <child id="1153944400369" name="variable" index="2Gsz3X" />
        <child id="1153944424730" name="inputSequence" index="2GsD0m" />
      </concept>
      <concept id="1153944193378" name="jetbrains.mps.baseLanguage.collections.structure.ForEachVariable" flags="nr" index="2GrKxI" />
      <concept id="1153944233411" name="jetbrains.mps.baseLanguage.collections.structure.ForEachVariableReference" flags="nn" index="2GrUjf">
        <reference id="1153944258490" name="variable" index="2Gs0qQ" />
      </concept>
    </language>
  </registry>
  <node concept="18kY7G" id="4hvwZTOZa$Z">
    <property role="TrG5h" value="check_ElementSchema" />
    <property role="3GE5qa" value="elements" />
    <node concept="3clFbS" id="4hvwZTOZa_0" role="18ibNy">
      <node concept="2Gpval" id="4hvwZTOZcIm" role="3cqZAp">
        <node concept="2GrKxI" id="4hvwZTOZcIo" role="2Gsz3X">
          <property role="TrG5h" value="m" />
        </node>
        <node concept="3clFbS" id="4hvwZTOZcIs" role="2LFqv$">
          <node concept="3clFbJ" id="4hvwZTOZcKC" role="3cqZAp">
            <node concept="3clFbS" id="4hvwZTOZcKE" role="3clFbx">
              <node concept="2MkqsV" id="4hvwZTOZhUl" role="3cqZAp">
                <node concept="3cpWs3" id="4hvwZTOZmyn" role="2MkJ7o">
                  <node concept="Xl_RD" id="4hvwZTOZmJS" role="3uHU7w">
                    <property role="Xl_RC" value=" cannot be used in this context" />
                  </node>
                  <node concept="3cpWs3" id="4hvwZTOZlRF" role="3uHU7B">
                    <node concept="Xl_RD" id="4hvwZTOZhUx" role="3uHU7B">
                      <property role="Xl_RC" value="Type " />
                    </node>
                    <node concept="2OqwBi" id="4hvwZTOZNQG" role="3uHU7w">
                      <node concept="2OqwBi" id="4hvwZTOZlXF" role="2Oq$k0">
                        <node concept="2GrUjf" id="4hvwZTOZlXI" role="2Oq$k0">
                          <ref role="2Gs0qQ" node="4hvwZTOZcIo" resolve="m" />
                        </node>
                        <node concept="2qgKlT" id="4hvwZTP053H" role="2OqNvi">
                          <ref role="37wK5l" to="pbu6:4WLweXm3SW5" resolve="type" />
                        </node>
                      </node>
                      <node concept="2qgKlT" id="4hvwZTOZO4H" role="2OqNvi">
                        <ref role="37wK5l" to="tpcu:hEwIMiw" resolve="getPresentation" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="2OqwBi" id="4hvwZTOZYfk" role="1urrMF">
                  <node concept="2GrUjf" id="4hvwZTOZhUG" role="2Oq$k0">
                    <ref role="2Gs0qQ" node="4hvwZTOZcIo" resolve="m" />
                  </node>
                  <node concept="2qgKlT" id="4hvwZTOZZkz" role="2OqNvi">
                    <ref role="37wK5l" to="pbu6:4WLweXm3SW5" resolve="type" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3fqX7Q" id="4hvwZTOZhMd" role="3clFbw">
              <node concept="1eOMI4" id="4hvwZTOZqMX" role="3fr31v">
                <node concept="22lmx$" id="4hvwZTOZz73" role="1eOMHV">
                  <node concept="22lmx$" id="4hvwZTOZwm8" role="3uHU7B">
                    <node concept="22lmx$" id="4hvwZTOZvFq" role="3uHU7B">
                      <node concept="22lmx$" id="4hvwZTOZv2m" role="3uHU7B">
                        <node concept="22lmx$" id="4hvwZTOZslF" role="3uHU7B">
                          <node concept="22lmx$" id="4hvwZTOZs3U" role="3uHU7B">
                            <node concept="22lmx$" id="4hvwZTOZr$M" role="3uHU7B">
                              <node concept="2OqwBi" id="4hvwZTOZhMf" role="3uHU7B">
                                <node concept="2OqwBi" id="4hvwZTOZhMg" role="2Oq$k0">
                                  <node concept="2GrUjf" id="4hvwZTOZhMj" role="2Oq$k0">
                                    <ref role="2Gs0qQ" node="4hvwZTOZcIo" resolve="m" />
                                  </node>
                                  <node concept="2qgKlT" id="4hvwZTP031T" role="2OqNvi">
                                    <ref role="37wK5l" to="pbu6:4WLweXm3SW5" resolve="type" />
                                  </node>
                                </node>
                                <node concept="1mIQ4w" id="4hvwZTOZhMl" role="2OqNvi">
                                  <node concept="chp4Y" id="4hvwZTOZhNC" role="cj9EA">
                                    <ref role="cht4Q" to="5qo5:4rZeNQ6OYR7" resolve="StringType" />
                                  </node>
                                </node>
                              </node>
                              <node concept="2OqwBi" id="4hvwZTOZrFb" role="3uHU7w">
                                <node concept="2OqwBi" id="4hvwZTOZrFc" role="2Oq$k0">
                                  <node concept="2GrUjf" id="4hvwZTOZrFf" role="2Oq$k0">
                                    <ref role="2Gs0qQ" node="4hvwZTOZcIo" resolve="m" />
                                  </node>
                                  <node concept="2qgKlT" id="4hvwZTP03Rk" role="2OqNvi">
                                    <ref role="37wK5l" to="pbu6:4WLweXm3SW5" resolve="type" />
                                  </node>
                                </node>
                                <node concept="1mIQ4w" id="4hvwZTOZrFh" role="2OqNvi">
                                  <node concept="chp4Y" id="4hvwZTOZrFi" role="cj9EA">
                                    <ref role="cht4Q" to="5qo5:4rZeNQ6Oerp" resolve="IntegerType" />
                                  </node>
                                </node>
                              </node>
                            </node>
                            <node concept="2OqwBi" id="4hvwZTOZs7P" role="3uHU7w">
                              <node concept="2OqwBi" id="4hvwZTOZs7Q" role="2Oq$k0">
                                <node concept="2GrUjf" id="4hvwZTOZs7T" role="2Oq$k0">
                                  <ref role="2Gs0qQ" node="4hvwZTOZcIo" resolve="m" />
                                </node>
                                <node concept="2qgKlT" id="4hvwZTP042n" role="2OqNvi">
                                  <ref role="37wK5l" to="pbu6:4WLweXm3SW5" resolve="type" />
                                </node>
                              </node>
                              <node concept="1mIQ4w" id="4hvwZTOZs7V" role="2OqNvi">
                                <node concept="chp4Y" id="4hvwZTOZs7W" role="cj9EA">
                                  <ref role="cht4Q" to="5qo5:4rZeNQ6Oetc" resolve="RealType" />
                                </node>
                              </node>
                            </node>
                          </node>
                          <node concept="2OqwBi" id="4hvwZTOZsqC" role="3uHU7w">
                            <node concept="2OqwBi" id="4hvwZTOZsqD" role="2Oq$k0">
                              <node concept="2GrUjf" id="4hvwZTOZsqG" role="2Oq$k0">
                                <ref role="2Gs0qQ" node="4hvwZTOZcIo" resolve="m" />
                              </node>
                              <node concept="2qgKlT" id="4hvwZTP04dx" role="2OqNvi">
                                <ref role="37wK5l" to="pbu6:4WLweXm3SW5" resolve="type" />
                              </node>
                            </node>
                            <node concept="1mIQ4w" id="4hvwZTOZsqI" role="2OqNvi">
                              <node concept="chp4Y" id="4hvwZTOZsqJ" role="cj9EA">
                                <ref role="cht4Q" to="5qo5:6sdnDbSlaon" resolve="BooleanType" />
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="2OqwBi" id="4hvwZTOZvep" role="3uHU7w">
                          <node concept="2OqwBi" id="4hvwZTOZveq" role="2Oq$k0">
                            <node concept="2GrUjf" id="4hvwZTOZvet" role="2Oq$k0">
                              <ref role="2Gs0qQ" node="4hvwZTOZcIo" resolve="m" />
                            </node>
                            <node concept="2qgKlT" id="4hvwZTP04oA" role="2OqNvi">
                              <ref role="37wK5l" to="pbu6:4WLweXm3SW5" resolve="type" />
                            </node>
                          </node>
                          <node concept="1mIQ4w" id="4hvwZTOZvev" role="2OqNvi">
                            <node concept="chp4Y" id="4hvwZTOZvew" role="cj9EA">
                              <ref role="cht4Q" to="700h:6zmBjqUinsw" resolve="ListType" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="2OqwBi" id="4hvwZTOZvMr" role="3uHU7w">
                        <node concept="2OqwBi" id="4hvwZTOZvMs" role="2Oq$k0">
                          <node concept="2GrUjf" id="4hvwZTOZvMv" role="2Oq$k0">
                            <ref role="2Gs0qQ" node="4hvwZTOZcIo" resolve="m" />
                          </node>
                          <node concept="2qgKlT" id="4hvwZTP04zY" role="2OqNvi">
                            <ref role="37wK5l" to="pbu6:4WLweXm3SW5" resolve="type" />
                          </node>
                        </node>
                        <node concept="1mIQ4w" id="4hvwZTOZvMx" role="2OqNvi">
                          <node concept="chp4Y" id="4hvwZTOZvMy" role="cj9EA">
                            <ref role="cht4Q" to="700h:7GwCuf2Wbm7" resolve="SetType" />
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="2OqwBi" id="4hvwZTOZwub" role="3uHU7w">
                      <node concept="2OqwBi" id="4hvwZTOZwuc" role="2Oq$k0">
                        <node concept="2GrUjf" id="4hvwZTOZwuf" role="2Oq$k0">
                          <ref role="2Gs0qQ" node="4hvwZTOZcIo" resolve="m" />
                        </node>
                        <node concept="2qgKlT" id="4hvwZTP04Ja" role="2OqNvi">
                          <ref role="37wK5l" to="pbu6:4WLweXm3SW5" resolve="type" />
                        </node>
                      </node>
                      <node concept="1mIQ4w" id="4hvwZTOZwuh" role="2OqNvi">
                        <node concept="chp4Y" id="4hvwZTOZxdu" role="cj9EA">
                          <ref role="cht4Q" to="os74:3JB$QJ8oGPX" resolve="AIDType" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="2OqwBi" id="4hvwZTOZzmc" role="3uHU7w">
                    <node concept="2OqwBi" id="4hvwZTOZzmd" role="2Oq$k0">
                      <node concept="2GrUjf" id="4hvwZTOZzmg" role="2Oq$k0">
                        <ref role="2Gs0qQ" node="4hvwZTOZcIo" resolve="m" />
                      </node>
                      <node concept="2qgKlT" id="4hvwZTP04Tn" role="2OqNvi">
                        <ref role="37wK5l" to="pbu6:4WLweXm3SW5" resolve="type" />
                      </node>
                    </node>
                    <node concept="1mIQ4w" id="4hvwZTOZzmi" role="2OqNvi">
                      <node concept="chp4Y" id="4hvwZTOZzmj" role="cj9EA">
                        <ref role="cht4Q" to="yv47:7D7uZV2dYz2" resolve="RecordType" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2OqwBi" id="4hvwZTOZbiy" role="2GsD0m">
          <node concept="1YBJjd" id="4hvwZTOZaQb" role="2Oq$k0">
            <ref role="1YBMHb" node="4hvwZTOZa_2" resolve="elementSchema" />
          </node>
          <node concept="3Tsc0h" id="4hvwZTOZc05" role="2OqNvi">
            <ref role="3TtcxE" to="yv47:xu7xcKioz5" resolve="members" />
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="4hvwZTOZa_2" role="1YuTPh">
      <property role="TrG5h" value="elementSchema" />
      <ref role="1YaFvo" to="3jzb:7dT_6n1a_NX" resolve="ElementSchema" />
    </node>
  </node>
</model>

