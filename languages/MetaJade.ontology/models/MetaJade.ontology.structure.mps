<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:606f7dcd-0d13-4ed2-8b48-a19323636f6b(MetaJade.ontology.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="9" />
    <devkit ref="78434eb8-b0e5-444b-850d-e7c4ad2da9ab(jetbrains.mps.devkit.aspect.structure)" />
  </languages>
  <imports>
    <import index="yv47" ref="r:da65683e-ff6f-430d-ab68-32a77df72c93(org.iets3.core.expr.toplevel.structure)" />
    <import index="os74" ref="r:0a69036a-4d31-45e1-887d-acbe5b8039f5(MetaJade.core.structure)" />
  </imports>
  <registry>
    <language id="982eb8df-2c96-4bd7-9963-11712ea622e5" name="jetbrains.mps.lang.resources">
      <concept id="8974276187400029883" name="jetbrains.mps.lang.resources.structure.FileIcon" flags="ng" index="1QGGSu">
        <property id="2756621024541341363" name="file" index="1iqoE4" />
      </concept>
    </language>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="6714410169261853888" name="conceptId" index="EcuMT" />
        <property id="4628067390765907488" name="conceptShortDescription" index="R4oN_" />
        <property id="4628067390765956802" name="abstract" index="R5$K7" />
        <property id="5092175715804935370" name="conceptAlias" index="34LRSv" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
      </concept>
      <concept id="1169125989551" name="jetbrains.mps.lang.structure.structure.InterfaceConceptDeclaration" flags="ig" index="PlHQZ" />
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <property id="1096454100552" name="rootable" index="19KtqR" />
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="6327362524875300597" name="icon" index="rwd14" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599893252" name="sourceCardinality" index="20lbJX" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <property id="241647608299431140" name="linkId" index="IQ2ns" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1TIwiD" id="4MI7ZB$Dtqf">
    <property role="EcuMT" value="5525388950973568655" />
    <property role="TrG5h" value="Ontology" />
    <property role="19KtqR" value="true" />
    <property role="R4oN_" value="Knowledge representation to be used by JADE agents" />
    <property role="34LRSv" value="Ontology" />
    <ref role="1TJDcQ" to="os74:2tMu91FTK9z" resolve="AbstractChunk" />
    <node concept="1TJgyj" id="ub9nkyK62i" role="1TKVEi">
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="contents" />
      <property role="20lbJX" value="fLJekj5/_0__n" />
      <property role="IQ2ns" value="543569365052711058" />
      <ref role="20lvS9" node="7dT_6n1a_NX" resolve="ElementSchema" />
    </node>
    <node concept="1QGGSu" id="eOcwJtbqoT" role="rwd14">
      <property role="1iqoE4" value="${module}/icons/ontology.png" />
    </node>
  </node>
  <node concept="1TIwiD" id="7dT_6n1a0fs">
    <property role="EcuMT" value="8320844951296934876" />
    <property role="3GE5qa" value="elements" />
    <property role="TrG5h" value="AgentActionSchema" />
    <property role="34LRSv" value="action" />
    <property role="R4oN_" value="request to execute a task" />
    <ref role="1TJDcQ" node="7dT_6n1a_NX" resolve="ElementSchema" />
    <node concept="1QGGSu" id="7dT_6n1bjuc" role="rwd14">
      <property role="1iqoE4" value="${module}/icons/ontology.png" />
    </node>
    <node concept="PrWs8" id="51gvYr6KKHa" role="PzmwI">
      <ref role="PrY4T" node="2aA5E1$b4j" resolve="IOntologyElement" />
    </node>
  </node>
  <node concept="1TIwiD" id="7dT_6n1a0ft">
    <property role="EcuMT" value="8320844951296934877" />
    <property role="3GE5qa" value="elements" />
    <property role="TrG5h" value="ConceptSchema" />
    <property role="34LRSv" value="concept" />
    <property role="R4oN_" value="exchange structured data" />
    <ref role="1TJDcQ" node="7dT_6n1a_NX" resolve="ElementSchema" />
    <node concept="1QGGSu" id="7dT_6n1bjQ$" role="rwd14">
      <property role="1iqoE4" value="${module}/icons/ontology.png" />
    </node>
    <node concept="PrWs8" id="51gvYr6KKGn" role="PzmwI">
      <ref role="PrY4T" node="2aA5E1$b4j" resolve="IOntologyElement" />
    </node>
  </node>
  <node concept="1TIwiD" id="7dT_6n1a0fu">
    <property role="EcuMT" value="8320844951296934878" />
    <property role="3GE5qa" value="elements" />
    <property role="TrG5h" value="PredicateSchema" />
    <property role="34LRSv" value="predicate" />
    <property role="R4oN_" value="request to check a proposition" />
    <ref role="1TJDcQ" node="7dT_6n1a_NX" resolve="ElementSchema" />
    <node concept="1QGGSu" id="7dT_6n1bjQy" role="rwd14">
      <property role="1iqoE4" value="${module}/icons/ontology.png" />
    </node>
    <node concept="PrWs8" id="51gvYr6KKEN" role="PzmwI">
      <ref role="PrY4T" node="2aA5E1$b4j" resolve="IOntologyElement" />
    </node>
  </node>
  <node concept="1TIwiD" id="7dT_6n1a_NX">
    <property role="EcuMT" value="8320844951297088765" />
    <property role="3GE5qa" value="elements" />
    <property role="TrG5h" value="ElementSchema" />
    <property role="R5$K7" value="true" />
    <ref role="1TJDcQ" to="yv47:7D7uZV2dYyQ" resolve="RecordDeclaration" />
  </node>
  <node concept="PlHQZ" id="2aA5E1$b4j">
    <property role="EcuMT" value="39011061274292499" />
    <property role="3GE5qa" value="elements" />
    <property role="TrG5h" value="IOntologyElement" />
  </node>
</model>

