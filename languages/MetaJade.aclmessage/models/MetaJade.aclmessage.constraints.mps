<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:b351a5e0-c0a5-46d5-9cc1-6518f9b87c40(MetaJade.aclmessage.constraints)">
  <persistence version="9" />
  <languages>
    <use id="5dae8159-ab99-46bb-a40d-0cee30ee7018" name="jetbrains.mps.lang.constraints.rules.kinds" version="0" />
    <use id="ea3159bf-f48e-4720-bde2-86dba75f0d34" name="jetbrains.mps.lang.context.defs" version="0" />
    <use id="e51810c5-7308-4642-bcb6-469e61b5dd18" name="jetbrains.mps.lang.constraints.msg.specification" version="0" />
    <use id="134c38d4-e3af-4d9e-b069-1c7df0a4005d" name="jetbrains.mps.lang.constraints.rules.skeleton" version="0" />
    <use id="b3551702-269c-4f05-ba61-58060cef4292" name="jetbrains.mps.lang.rulesAndMessages" version="0" />
    <use id="3f4bc5f5-c6c1-4a28-8b10-c83066ffa4a1" name="jetbrains.mps.lang.constraints" version="6" />
    <use id="13744753-c81f-424a-9c1b-cf8943bf4e86" name="jetbrains.mps.lang.sharedConcepts" version="0" />
    <use id="3ad5badc-1d9c-461c-b7b1-fa2fcd0a0ae7" name="jetbrains.mps.lang.context" version="0" />
    <use id="ad93155d-79b2-4759-b10c-55123e763903" name="jetbrains.mps.lang.messages" version="0" />
    <use id="7a5dda62-9140-4668-ab76-d5ed1746f2b2" name="jetbrains.mps.lang.typesystem" version="5" />
    <devkit ref="00000000-0000-4000-0000-5604ebd4f22c(jetbrains.mps.devkit.aspect.constraints)" />
  </languages>
  <imports>
    <import index="os74" ref="r:0a69036a-4d31-45e1-887d-acbe5b8039f5(MetaJade.core.structure)" />
    <import index="hm2y" ref="r:66e07cb4-a4b0-4bf3-a36d-5e9ed1ff1bd3(org.iets3.core.expr.base.structure)" />
    <import index="o8zo" ref="r:314576fc-3aee-4386-a0a5-a38348ac317d(jetbrains.mps.scope)" />
    <import index="hk02" ref="r:7c1264b7-fd03-4e6c-b708-6c839cbe4cda(MetaJade.aclmessage.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1081516740877" name="jetbrains.mps.baseLanguage.structure.NotExpression" flags="nn" index="3fqX7Q">
        <child id="1081516765348" name="expression" index="3fr31v" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
    </language>
    <language id="3f4bc5f5-c6c1-4a28-8b10-c83066ffa4a1" name="jetbrains.mps.lang.constraints">
      <concept id="6702802731807351367" name="jetbrains.mps.lang.constraints.structure.ConstraintFunction_CanBeAChild" flags="in" index="9S07l" />
      <concept id="1202989658459" name="jetbrains.mps.lang.constraints.structure.ConstraintFunctionParameter_parentNode" flags="nn" index="nLn13" />
      <concept id="8966504967485224688" name="jetbrains.mps.lang.constraints.structure.ConstraintFunctionParameter_contextNode" flags="nn" index="2rP1CM" />
      <concept id="5564765827938091039" name="jetbrains.mps.lang.constraints.structure.ConstraintFunction_ReferentSearchScope_Scope" flags="ig" index="3dgokm" />
      <concept id="1213093968558" name="jetbrains.mps.lang.constraints.structure.ConceptConstraints" flags="ng" index="1M2fIO">
        <reference id="1213093996982" name="concept" index="1M2myG" />
        <child id="6702802731807737306" name="canBeChild" index="9Vyp8" />
        <child id="1213100494875" name="referent" index="1Mr941" />
      </concept>
      <concept id="1148687176410" name="jetbrains.mps.lang.constraints.structure.NodeReferentConstraint" flags="ng" index="1N5Pfh">
        <reference id="1148687202698" name="applicableLink" index="1N5Vy1" />
        <child id="1148687345559" name="searchScopeFactory" index="1N6uqs" />
      </concept>
    </language>
    <language id="7a5dda62-9140-4668-ab76-d5ed1746f2b2" name="jetbrains.mps.lang.typesystem">
      <concept id="1176544042499" name="jetbrains.mps.lang.typesystem.structure.Node_TypeOperation" flags="nn" index="3JvlWi" />
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1138411891628" name="jetbrains.mps.lang.smodel.structure.SNodeOperation" flags="nn" index="eCIE_">
        <child id="1144104376918" name="parameter" index="1xVPHs" />
      </concept>
      <concept id="2396822768958367367" name="jetbrains.mps.lang.smodel.structure.AbstractTypeCastExpression" flags="nn" index="$5XWr">
        <child id="6733348108486823193" name="leftExpression" index="1m5AlR" />
        <child id="3906496115198199033" name="conceptArgument" index="3oSUPX" />
      </concept>
      <concept id="1171407110247" name="jetbrains.mps.lang.smodel.structure.Node_GetAncestorOperation" flags="nn" index="2Xjw5R" />
      <concept id="2644386474301421077" name="jetbrains.mps.lang.smodel.structure.LinkIdRefExpression" flags="nn" index="359W_D">
        <reference id="2644386474301421078" name="conceptDeclaration" index="359W_E" />
        <reference id="2644386474301421079" name="linkDeclaration" index="359W_F" />
      </concept>
      <concept id="1139621453865" name="jetbrains.mps.lang.smodel.structure.Node_IsInstanceOfOperation" flags="nn" index="1mIQ4w">
        <child id="1177027386292" name="conceptArgument" index="cj9EA" />
      </concept>
      <concept id="1172008320231" name="jetbrains.mps.lang.smodel.structure.Node_IsNotNullOperation" flags="nn" index="3x8VRR" />
      <concept id="1144100932627" name="jetbrains.mps.lang.smodel.structure.OperationParm_Inclusion" flags="ng" index="1xIGOp" />
      <concept id="1144101972840" name="jetbrains.mps.lang.smodel.structure.OperationParm_Concept" flags="ng" index="1xMEDy">
        <child id="1207343664468" name="conceptArgument" index="ri$Ld" />
      </concept>
      <concept id="1140137987495" name="jetbrains.mps.lang.smodel.structure.SNodeTypeCastExpression" flags="nn" index="1PxgMI" />
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2" />
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1M2fIO" id="4i_VsgbvgVU">
    <property role="3GE5qa" value="statements" />
    <ref role="1M2myG" to="hk02:4i_Vsgbvgoq" resolve="ReceivedMessageArgRef" />
    <node concept="1N5Pfh" id="6OKQOIXpHhC" role="1Mr941">
      <ref role="1N5Vy1" to="hk02:4i_VsgbvgoW" resolve="arg" />
      <node concept="3dgokm" id="6OKQOIXpHhG" role="1N6uqs">
        <node concept="3clFbS" id="6OKQOIXpHhI" role="2VODD2">
          <node concept="3clFbF" id="6OKQOIXq02u" role="3cqZAp">
            <node concept="2YIFZM" id="6OKQOIXpH$s" role="3clFbG">
              <ref role="1Pybhc" to="o8zo:7ipADkTevLm" resolve="SimpleRoleScope" />
              <ref role="37wK5l" to="o8zo:6t3ylNOzI9Y" resolve="forNamedElements" />
              <node concept="2OqwBi" id="6OKQOIXpIXb" role="37wK5m">
                <node concept="2rP1CM" id="6OKQOIXpIPv" role="2Oq$k0" />
                <node concept="2Xjw5R" id="6OKQOIXpJ7J" role="2OqNvi">
                  <node concept="1xMEDy" id="6OKQOIXpJ7L" role="1xVPHs">
                    <node concept="chp4Y" id="6OKQOIXpJbV" role="ri$Ld">
                      <ref role="cht4Q" to="hk02:7HOFHncqzmZ" resolve="ReceiveMessage" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="359W_D" id="6OKQOIXpKIX" role="37wK5m">
                <ref role="359W_E" to="hk02:7HOFHncqzmZ" resolve="ReceiveMessage" />
                <ref role="359W_F" to="hk02:4qBaryPhIZI" resolve="arg" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="9S07l" id="4i_VsgbvgVV" role="9Vyp8">
      <node concept="3clFbS" id="4i_VsgbvgVW" role="2VODD2">
        <node concept="3clFbF" id="4i_VsgbvgZQ" role="3cqZAp">
          <node concept="2OqwBi" id="4i_VsgbvidE" role="3clFbG">
            <node concept="2OqwBi" id="4i_Vsgbvhca" role="2Oq$k0">
              <node concept="nLn13" id="4i_VsgbvgZP" role="2Oq$k0" />
              <node concept="2Xjw5R" id="4i_Vsgbvhnf" role="2OqNvi">
                <node concept="1xMEDy" id="4i_Vsgbvhnh" role="1xVPHs">
                  <node concept="chp4Y" id="4i_VsgbvhuO" role="ri$Ld">
                    <ref role="cht4Q" to="hk02:7HOFHncqzmZ" resolve="ReceiveMessage" />
                  </node>
                </node>
                <node concept="1xIGOp" id="4i_VsgbvhOI" role="1xVPHs" />
              </node>
            </node>
            <node concept="3x8VRR" id="4i_VsgbviUi" role="2OqNvi" />
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="1M2fIO" id="1$PYe99c8Yb">
    <property role="3GE5qa" value="operations" />
    <ref role="1M2myG" to="hk02:1$PYe99c8Ki" resolve="IMessageOp" />
    <node concept="9S07l" id="2lACGaVBYQU" role="9Vyp8">
      <node concept="3clFbS" id="2lACGaVBYQV" role="2VODD2">
        <node concept="3cpWs8" id="6XBPhggEzun" role="3cqZAp">
          <node concept="3cpWsn" id="6XBPhggEzuo" role="3cpWs9">
            <property role="TrG5h" value="tt" />
            <node concept="3Tqbb2" id="6XBPhggEzup" role="1tU5fm" />
            <node concept="2OqwBi" id="6XBPhggEzuq" role="33vP2m">
              <node concept="2OqwBi" id="6XBPhggEzur" role="2Oq$k0">
                <node concept="1PxgMI" id="6XBPhggEzus" role="2Oq$k0">
                  <node concept="nLn13" id="6XBPhggEzut" role="1m5AlR" />
                  <node concept="chp4Y" id="6XBPhggEzx8" role="3oSUPX">
                    <ref role="cht4Q" to="hm2y:7NJy08a3O99" resolve="DotExpression" />
                  </node>
                </node>
                <node concept="3TrEf2" id="6XBPhggEzuu" role="2OqNvi">
                  <ref role="3Tt5mk" to="hm2y:4rZeNQ6NgXF" resolve="expr" />
                </node>
              </node>
              <node concept="3JvlWi" id="6XBPhggEzuv" role="2OqNvi" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2lACGaVCYf8" role="3cqZAp">
          <node concept="2OqwBi" id="2lACGaVCYpU" role="3clFbG">
            <node concept="37vLTw" id="2lACGaVCYf6" role="2Oq$k0">
              <ref role="3cqZAo" node="6XBPhggEzuo" resolve="tt" />
            </node>
            <node concept="1mIQ4w" id="2lACGaVCY$Y" role="2OqNvi">
              <node concept="chp4Y" id="2lACGaVCYEU" role="cj9EA">
                <ref role="cht4Q" to="hk02:1$PYe999WiB" resolve="MessageType" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="1M2fIO" id="2lACGaVDxPa">
    <ref role="1M2myG" to="hk02:1$PYe999WiB" resolve="MessageType" />
    <node concept="9S07l" id="2lACGaVDxPb" role="9Vyp8">
      <node concept="3clFbS" id="2lACGaVDxPc" role="2VODD2">
        <node concept="3clFbF" id="2lACGaVDxP$" role="3cqZAp">
          <node concept="3fqX7Q" id="2lACGaVDxPy" role="3clFbG">
            <node concept="2OqwBi" id="2lACGaVDy2T" role="3fr31v">
              <node concept="nLn13" id="2lACGaVDxTP" role="2Oq$k0" />
              <node concept="1mIQ4w" id="2lACGaVDykv" role="2OqNvi">
                <node concept="chp4Y" id="2lACGaVDyu6" role="cj9EA">
                  <ref role="cht4Q" to="hk02:1$PYe999WiB" resolve="MessageType" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="1M2fIO" id="4AcBPh2BEk5">
    <property role="3GE5qa" value="statements" />
    <ref role="1M2myG" to="hk02:1wDV7BCtyf4" resolve="AbstractMessageExpression" />
    <node concept="9S07l" id="4AcBPh2BEk6" role="9Vyp8">
      <node concept="3clFbS" id="4AcBPh2BEk7" role="2VODD2">
        <node concept="3clFbF" id="4AcBPh2BEnT" role="3cqZAp">
          <node concept="2OqwBi" id="vQ6nfZNbCx" role="3clFbG">
            <node concept="2OqwBi" id="vQ6nfZNbCy" role="2Oq$k0">
              <node concept="nLn13" id="7_YVLVlbwQs" role="2Oq$k0" />
              <node concept="2Xjw5R" id="vQ6nfZNbC$" role="2OqNvi">
                <node concept="1xMEDy" id="vQ6nfZNbC_" role="1xVPHs">
                  <node concept="chp4Y" id="3A64HzfO_6U" role="ri$Ld">
                    <ref role="cht4Q" to="os74:2tMu91FTK9z" resolve="AbstractChunk" />
                  </node>
                </node>
                <node concept="1xIGOp" id="3NjOp6FKOJt" role="1xVPHs" />
              </node>
            </node>
            <node concept="3x8VRR" id="vQ6nfZNbCB" role="2OqNvi" />
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

