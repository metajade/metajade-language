<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:43571e3f-361d-460d-b23e-0de512d43d28(MetaJade.aclmessage.behavior)">
  <persistence version="9" />
  <languages>
    <use id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel" version="19" />
    <use id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior" version="2" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="hk02" ref="r:7c1264b7-fd03-4e6c-b708-6c839cbe4cda(MetaJade.aclmessage.structure)" />
    <import index="pbu6" ref="r:83e946de-2a7f-4a4c-b3c9-4f671aa7f2db(org.iets3.core.expr.base.behavior)" />
    <import index="oq0c" ref="r:6c6155f0-4bbe-4af5-8c26-244d570e21e4(org.iets3.core.expr.base.plugin)" />
  </imports>
  <registry>
    <language id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior">
      <concept id="1225194240794" name="jetbrains.mps.lang.behavior.structure.ConceptBehavior" flags="ng" index="13h7C7">
        <reference id="1225194240799" name="concept" index="13h7C2" />
        <child id="1225194240805" name="method" index="13h7CS" />
        <child id="1225194240801" name="constructor" index="13h7CW" />
      </concept>
      <concept id="1225194413805" name="jetbrains.mps.lang.behavior.structure.ConceptConstructorDeclaration" flags="in" index="13hLZK" />
      <concept id="1225194472830" name="jetbrains.mps.lang.behavior.structure.ConceptMethodDeclaration" flags="ng" index="13i0hz">
        <property id="1225194472832" name="isVirtual" index="13i0it" />
        <property id="1225194472834" name="isAbstract" index="13i0iv" />
        <reference id="1225194472831" name="overriddenMethod" index="13i0hy" />
      </concept>
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580123137" name="jetbrains.mps.baseLanguage.structure.BooleanConstant" flags="nn" index="3clFbT">
        <property id="1068580123138" name="value" index="3clFbU" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1212685548494" name="jetbrains.mps.baseLanguage.structure.ClassCreator" flags="nn" index="1pGfFk" />
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="13h7C7" id="7AVCbhhYKLi">
    <property role="3GE5qa" value="operations" />
    <ref role="13h7C2" to="hk02:7AVCbhhYKKm" resolve="GetSenderOp" />
    <node concept="13i0hz" id="7AVCbhhYKLO" role="13h7CS">
      <property role="TrG5h" value="renderReadable" />
      <ref role="13i0hy" to="pbu6:6kR0qIbI2yi" resolve="renderReadable" />
      <node concept="3Tm1VV" id="7AVCbhhYKLP" role="1B3o_S" />
      <node concept="3clFbS" id="7AVCbhhYKLQ" role="3clF47">
        <node concept="3clFbF" id="7AVCbhhYKLR" role="3cqZAp">
          <node concept="Xl_RD" id="7AVCbhhYKLS" role="3clFbG">
            <property role="Xl_RC" value="sender" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="7AVCbhhYKLT" role="3clF45" />
    </node>
    <node concept="13hLZK" id="7AVCbhhYKLj" role="13h7CW">
      <node concept="3clFbS" id="7AVCbhhYKLk" role="2VODD2" />
    </node>
  </node>
  <node concept="13h7C7" id="7AVCbhhYXCG">
    <property role="3GE5qa" value="operations" />
    <ref role="13h7C2" to="hk02:7AVCbhhYXBK" resolve="GetReceiversOp" />
    <node concept="13i0hz" id="7AVCbhhYXDe" role="13h7CS">
      <property role="TrG5h" value="renderReadable" />
      <ref role="13i0hy" to="pbu6:6kR0qIbI2yi" resolve="renderReadable" />
      <node concept="3Tm1VV" id="7AVCbhhYXDf" role="1B3o_S" />
      <node concept="3clFbS" id="7AVCbhhYXDg" role="3clF47">
        <node concept="3clFbF" id="7AVCbhhYXDh" role="3cqZAp">
          <node concept="Xl_RD" id="7AVCbhhYXDi" role="3clFbG">
            <property role="Xl_RC" value="receivers" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="7AVCbhhYXDj" role="3clF45" />
    </node>
    <node concept="13hLZK" id="7AVCbhhYXCH" role="13h7CW">
      <node concept="3clFbS" id="7AVCbhhYXCI" role="2VODD2" />
    </node>
  </node>
  <node concept="13h7C7" id="7AVCbhi0iPH">
    <property role="3GE5qa" value="operations" />
    <ref role="13h7C2" to="hk02:7AVCbhi0iOL" resolve="GetContentOp" />
    <node concept="13i0hz" id="7AVCbhi0iQf" role="13h7CS">
      <property role="TrG5h" value="renderReadable" />
      <ref role="13i0hy" to="pbu6:6kR0qIbI2yi" resolve="renderReadable" />
      <node concept="3Tm1VV" id="7AVCbhi0iQg" role="1B3o_S" />
      <node concept="3clFbS" id="7AVCbhi0iQh" role="3clF47">
        <node concept="3clFbF" id="7AVCbhi0iQi" role="3cqZAp">
          <node concept="Xl_RD" id="7AVCbhi0iQj" role="3clFbG">
            <property role="Xl_RC" value="content" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="7AVCbhi0iQk" role="3clF45" />
    </node>
    <node concept="13hLZK" id="7AVCbhi0iPI" role="13h7CW">
      <node concept="3clFbS" id="7AVCbhi0iPJ" role="2VODD2" />
    </node>
  </node>
  <node concept="13h7C7" id="4iLN_rFYH$d">
    <property role="3GE5qa" value="statements" />
    <ref role="13h7C2" to="hk02:3uK1rwojlW1" resolve="ComposeMessage" />
    <node concept="13i0hz" id="6GySMNjYGbw" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="effectDescriptor" />
      <ref role="13i0hy" to="pbu6:6GySMNjjWfO" resolve="effectDescriptor" />
      <node concept="3Tm1VV" id="6GySMNjYGbx" role="1B3o_S" />
      <node concept="3clFbS" id="6GySMNjYGbE" role="3clF47">
        <node concept="3clFbF" id="6GySMNjjWxj" role="3cqZAp">
          <node concept="2ShNRf" id="6GySMNjjWxh" role="3clFbG">
            <node concept="1pGfFk" id="6GySMNjk5nJ" role="2ShVmc">
              <ref role="37wK5l" to="oq0c:3ni3WieuVew" resolve="EffectDescriptor" />
              <node concept="3clFbT" id="6GySMNjruqn" role="37wK5m" />
              <node concept="3clFbT" id="51K8u7REK11" role="37wK5m">
                <property role="3clFbU" value="true" />
              </node>
              <node concept="3clFbT" id="6GySMNjk5oc" role="37wK5m" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3uibUv" id="6GySMNjYGbF" role="3clF45">
        <ref role="3uigEE" to="oq0c:3ni3WieuV7z" resolve="EffectDescriptor" />
      </node>
    </node>
    <node concept="13hLZK" id="4iLN_rFYH$e" role="13h7CW">
      <node concept="3clFbS" id="4iLN_rFYH$f" role="2VODD2" />
    </node>
  </node>
</model>

