<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:84bec450-6da5-44ca-b622-9d15a863d9b4(MetaJade.aclmessage.typesystem)">
  <persistence version="9" />
  <languages>
    <use id="7a5dda62-9140-4668-ab76-d5ed1746f2b2" name="jetbrains.mps.lang.typesystem" version="5" />
    <devkit ref="00000000-0000-4000-0000-1de82b3a4936(jetbrains.mps.devkit.aspect.typesystem)" />
  </languages>
  <imports>
    <import index="hk02" ref="r:7c1264b7-fd03-4e6c-b708-6c839cbe4cda(MetaJade.aclmessage.structure)" />
    <import index="3jzb" ref="r:606f7dcd-0d13-4ed2-8b48-a19323636f6b(MetaJade.ontology.structure)" />
    <import index="hm2y" ref="r:66e07cb4-a4b0-4bf3-a36d-5e9ed1ff1bd3(org.iets3.core.expr.base.structure)" />
    <import index="os74" ref="r:0a69036a-4d31-45e1-887d-acbe5b8039f5(MetaJade.core.structure)" implicit="true" />
    <import index="5qo5" ref="r:6d93ddb1-b0b0-4eee-8079-51303666672a(org.iets3.core.expr.simpleTypes.structure)" implicit="true" />
    <import index="700h" ref="r:61b1de80-490d-4fee-8e95-b956503290e9(org.iets3.core.expr.collections.structure)" implicit="true" />
    <import index="yv47" ref="r:da65683e-ff6f-430d-ab68-32a77df72c93(org.iets3.core.expr.toplevel.structure)" implicit="true" />
    <import index="pbu6" ref="r:83e946de-2a7f-4a4c-b3c9-4f671aa7f2db(org.iets3.core.expr.base.behavior)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1080223426719" name="jetbrains.mps.baseLanguage.structure.OrExpression" flags="nn" index="22lmx$" />
      <concept id="1082485599095" name="jetbrains.mps.baseLanguage.structure.BlockStatement" flags="nn" index="9aQIb">
        <child id="1082485599096" name="statements" index="9aQI4" />
      </concept>
      <concept id="4836112446988635817" name="jetbrains.mps.baseLanguage.structure.UndefinedType" flags="in" index="2jxLKc" />
      <concept id="1154032098014" name="jetbrains.mps.baseLanguage.structure.AbstractLoopStatement" flags="nn" index="2LF5Ji">
        <child id="1154032183016" name="body" index="2LFqv$" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1082485599094" name="ifFalseStatement" index="9aQIa" />
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242875" name="jetbrains.mps.baseLanguage.structure.PlusExpression" flags="nn" index="3cpWs3" />
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1081516740877" name="jetbrains.mps.baseLanguage.structure.NotExpression" flags="nn" index="3fqX7Q">
        <child id="1081516765348" name="expression" index="3fr31v" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1080120340718" name="jetbrains.mps.baseLanguage.structure.AndExpression" flags="nn" index="1Wc70l" />
    </language>
    <language id="3a13115c-633c-4c5c-bbcc-75c4219e9555" name="jetbrains.mps.lang.quotation">
      <concept id="5455284157994012186" name="jetbrains.mps.lang.quotation.structure.NodeBuilderInitLink" flags="ng" index="2pIpSj">
        <reference id="5455284157994012188" name="link" index="2pIpSl" />
        <child id="1595412875168045827" name="initValue" index="28nt2d" />
      </concept>
      <concept id="5455284157993863837" name="jetbrains.mps.lang.quotation.structure.NodeBuilder" flags="nn" index="2pJPEk">
        <child id="5455284157993863838" name="quotedNode" index="2pJPEn" />
      </concept>
      <concept id="5455284157993863840" name="jetbrains.mps.lang.quotation.structure.NodeBuilderNode" flags="nn" index="2pJPED">
        <reference id="5455284157993910961" name="concept" index="2pJxaS" />
        <child id="5455284157993911099" name="values" index="2pJxcM" />
      </concept>
      <concept id="8182547171709752110" name="jetbrains.mps.lang.quotation.structure.NodeBuilderExpression" flags="nn" index="36biLy">
        <child id="8182547171709752112" name="expression" index="36biLW" />
      </concept>
    </language>
    <language id="7a5dda62-9140-4668-ab76-d5ed1746f2b2" name="jetbrains.mps.lang.typesystem">
      <concept id="1185788614172" name="jetbrains.mps.lang.typesystem.structure.NormalTypeClause" flags="ng" index="mw_s8">
        <child id="1185788644032" name="normalType" index="mwGJk" />
      </concept>
      <concept id="1185805035213" name="jetbrains.mps.lang.typesystem.structure.WhenConcreteStatement" flags="nn" index="nvevp">
        <child id="1185805047793" name="body" index="nvhr_" />
        <child id="1185805056450" name="argument" index="nvjzm" />
        <child id="1205761991995" name="argumentRepresentator" index="2X0Ygz" />
      </concept>
      <concept id="1175147569072" name="jetbrains.mps.lang.typesystem.structure.AbstractSubtypingRule" flags="ig" index="2sgdUx">
        <child id="1175147624276" name="body" index="2sgrp5" />
      </concept>
      <concept id="1175517767210" name="jetbrains.mps.lang.typesystem.structure.ReportErrorStatement" flags="nn" index="2MkqsV">
        <child id="1175517851849" name="errorString" index="2MkJ7o" />
      </concept>
      <concept id="1205762105978" name="jetbrains.mps.lang.typesystem.structure.WhenConcreteVariableDeclaration" flags="ng" index="2X1qdy" />
      <concept id="1205762656241" name="jetbrains.mps.lang.typesystem.structure.WhenConcreteVariableReference" flags="nn" index="2X3wrD">
        <reference id="1205762683928" name="whenConcreteVar" index="2X3Bk0" />
      </concept>
      <concept id="1201607707634" name="jetbrains.mps.lang.typesystem.structure.InequationReplacementRule" flags="ig" index="35pCF_">
        <child id="1201607798918" name="supertypeNode" index="35pZ6h" />
      </concept>
      <concept id="1195213580585" name="jetbrains.mps.lang.typesystem.structure.AbstractCheckingRule" flags="ig" index="18hYwZ">
        <child id="1195213635060" name="body" index="18ibNy" />
      </concept>
      <concept id="1195214364922" name="jetbrains.mps.lang.typesystem.structure.NonTypesystemRule" flags="ig" index="18kY7G" />
      <concept id="3937244445246642777" name="jetbrains.mps.lang.typesystem.structure.AbstractReportStatement" flags="ng" index="1urrMJ">
        <child id="3937244445246642781" name="nodeToReport" index="1urrMF" />
      </concept>
      <concept id="1176544042499" name="jetbrains.mps.lang.typesystem.structure.Node_TypeOperation" flags="nn" index="3JvlWi" />
      <concept id="1174642788531" name="jetbrains.mps.lang.typesystem.structure.ConceptReference" flags="ig" index="1YaCAy">
        <reference id="1174642800329" name="concept" index="1YaFvo" />
      </concept>
      <concept id="1174643105530" name="jetbrains.mps.lang.typesystem.structure.InferenceRule" flags="ig" index="1YbPZF" />
      <concept id="1174648085619" name="jetbrains.mps.lang.typesystem.structure.AbstractRule" flags="ng" index="1YuPPy">
        <child id="1174648101952" name="applicableNode" index="1YuTPh" />
      </concept>
      <concept id="1174650418652" name="jetbrains.mps.lang.typesystem.structure.ApplicableNodeReference" flags="nn" index="1YBJjd">
        <reference id="1174650432090" name="applicableNode" index="1YBMHb" />
      </concept>
      <concept id="1174657487114" name="jetbrains.mps.lang.typesystem.structure.TypeOfExpression" flags="nn" index="1Z2H0r">
        <child id="1174657509053" name="term" index="1Z2MuG" />
      </concept>
      <concept id="1174658326157" name="jetbrains.mps.lang.typesystem.structure.CreateEquationStatement" flags="nn" index="1Z5TYs" />
      <concept id="1174660718586" name="jetbrains.mps.lang.typesystem.structure.AbstractEquationStatement" flags="nn" index="1Zf1VF">
        <child id="1174660783413" name="leftExpression" index="1ZfhK$" />
        <child id="1174660783414" name="rightExpression" index="1ZfhKB" />
      </concept>
      <concept id="1174663118805" name="jetbrains.mps.lang.typesystem.structure.CreateLessThanInequationStatement" flags="nn" index="1ZobV4" />
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="4705942098322609812" name="jetbrains.mps.lang.smodel.structure.EnumMember_IsOperation" flags="ng" index="21noJN">
        <child id="4705942098322609813" name="member" index="21noJM" />
      </concept>
      <concept id="4705942098322467729" name="jetbrains.mps.lang.smodel.structure.EnumMemberReference" flags="ng" index="21nZrQ">
        <reference id="4705942098322467736" name="decl" index="21nZrZ" />
      </concept>
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1138411891628" name="jetbrains.mps.lang.smodel.structure.SNodeOperation" flags="nn" index="eCIE_">
        <child id="1144104376918" name="parameter" index="1xVPHs" />
      </concept>
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="2396822768958367367" name="jetbrains.mps.lang.smodel.structure.AbstractTypeCastExpression" flags="nn" index="$5XWr">
        <child id="6733348108486823193" name="leftExpression" index="1m5AlR" />
        <child id="3906496115198199033" name="conceptArgument" index="3oSUPX" />
      </concept>
      <concept id="1171305280644" name="jetbrains.mps.lang.smodel.structure.Node_GetDescendantsOperation" flags="nn" index="2Rf3mk" />
      <concept id="1139621453865" name="jetbrains.mps.lang.smodel.structure.Node_IsInstanceOfOperation" flags="nn" index="1mIQ4w">
        <child id="1177027386292" name="conceptArgument" index="cj9EA" />
      </concept>
      <concept id="1171999116870" name="jetbrains.mps.lang.smodel.structure.Node_IsNullOperation" flags="nn" index="3w_OXm" />
      <concept id="1172008320231" name="jetbrains.mps.lang.smodel.structure.Node_IsNotNullOperation" flags="nn" index="3x8VRR" />
      <concept id="1144101972840" name="jetbrains.mps.lang.smodel.structure.OperationParm_Concept" flags="ng" index="1xMEDy">
        <child id="1207343664468" name="conceptArgument" index="ri$Ld" />
      </concept>
      <concept id="1140137987495" name="jetbrains.mps.lang.smodel.structure.SNodeTypeCastExpression" flags="nn" index="1PxgMI" />
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="1153943597977" name="jetbrains.mps.baseLanguage.collections.structure.ForEachStatement" flags="nn" index="2Gpval">
        <child id="1153944400369" name="variable" index="2Gsz3X" />
        <child id="1153944424730" name="inputSequence" index="2GsD0m" />
      </concept>
      <concept id="1153944193378" name="jetbrains.mps.baseLanguage.collections.structure.ForEachVariable" flags="nr" index="2GrKxI" />
      <concept id="1153944233411" name="jetbrains.mps.baseLanguage.collections.structure.ForEachVariableReference" flags="nn" index="2GrUjf">
        <reference id="1153944258490" name="variable" index="2Gs0qQ" />
      </concept>
      <concept id="1165525191778" name="jetbrains.mps.baseLanguage.collections.structure.GetFirstOperation" flags="nn" index="1uHKPH" />
      <concept id="1176501494711" name="jetbrains.mps.baseLanguage.collections.structure.IsNotEmptyOperation" flags="nn" index="3GX2aA" />
    </language>
  </registry>
  <node concept="1YbPZF" id="3JB$QJ87rLG">
    <property role="TrG5h" value="typeof_ReceiveMessage" />
    <property role="3GE5qa" value="statements" />
    <node concept="3clFbS" id="3JB$QJ87rLH" role="18ibNy">
      <node concept="3clFbJ" id="3NjOp6FUyjs" role="3cqZAp">
        <node concept="3clFbS" id="3NjOp6FUyju" role="3clFbx">
          <node concept="1Z5TYs" id="3NjOp6FUzND" role="3cqZAp">
            <node concept="mw_s8" id="3NjOp6FUzNH" role="1ZfhK$">
              <node concept="1Z2H0r" id="3NjOp6FUzNI" role="mwGJk">
                <node concept="1YBJjd" id="3NjOp6FUzNJ" role="1Z2MuG">
                  <ref role="1YBMHb" node="3JB$QJ87rLJ" resolve="receiveMessage" />
                </node>
              </node>
            </node>
            <node concept="mw_s8" id="3NjOp6FUzOP" role="1ZfhKB">
              <node concept="1Z2H0r" id="3NjOp6FUzOL" role="mwGJk">
                <node concept="2OqwBi" id="3NjOp6FU$49" role="1Z2MuG">
                  <node concept="1YBJjd" id="3NjOp6FUzP6" role="2Oq$k0">
                    <ref role="1YBMHb" node="3JB$QJ87rLJ" resolve="receiveMessage" />
                  </node>
                  <node concept="3TrEf2" id="3NjOp6FYiS4" role="2OqNvi">
                    <ref role="3Tt5mk" to="hk02:4qBaryPhIZI" resolve="arg" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2OqwBi" id="3NjOp6FUzn$" role="3clFbw">
          <node concept="2OqwBi" id="3NjOp6FUy_U" role="2Oq$k0">
            <node concept="1YBJjd" id="3NjOp6FUykd" role="2Oq$k0">
              <ref role="1YBMHb" node="3JB$QJ87rLJ" resolve="receiveMessage" />
            </node>
            <node concept="3TrEf2" id="3NjOp6FUza4" role="2OqNvi">
              <ref role="3Tt5mk" to="hk02:4qBaryPhIZF" resolve="onReceive" />
            </node>
          </node>
          <node concept="3x8VRR" id="3NjOp6FUzDS" role="2OqNvi" />
        </node>
        <node concept="9aQIb" id="3NjOp6FUzJR" role="9aQIa">
          <node concept="3clFbS" id="3NjOp6FUzJS" role="9aQI4">
            <node concept="1Z5TYs" id="4qBaryPJfXM" role="3cqZAp">
              <node concept="mw_s8" id="4qBaryPJfXG" role="1ZfhKB">
                <node concept="2pJPEk" id="4qBaryPJfXO" role="mwGJk">
                  <node concept="2pJPED" id="4qBaryPJfXK" role="2pJPEn">
                    <ref role="2pJxaS" to="os74:80jriwjIN" resolve="UnitType" />
                  </node>
                </node>
              </node>
              <node concept="mw_s8" id="4qBaryPJfXE" role="1ZfhK$">
                <node concept="1Z2H0r" id="4qBaryPJfXI" role="mwGJk">
                  <node concept="1YBJjd" id="4qBaryPJfXC" role="1Z2MuG">
                    <ref role="1YBMHb" node="3JB$QJ87rLJ" resolve="receiveMessage" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="1Z5TYs" id="1$PYe999Xj2" role="3cqZAp">
        <node concept="mw_s8" id="1$PYe999Xj5" role="1ZfhK$">
          <node concept="1Z2H0r" id="1$PYe999Wq9" role="mwGJk">
            <node concept="2OqwBi" id="1$PYe999WEJ" role="1Z2MuG">
              <node concept="1YBJjd" id="1$PYe999Wsj" role="2Oq$k0">
                <ref role="1YBMHb" node="3JB$QJ87rLJ" resolve="receiveMessage" />
              </node>
              <node concept="3TrEf2" id="1$PYe999X9X" role="2OqNvi">
                <ref role="3Tt5mk" to="hk02:4qBaryPhIZI" resolve="arg" />
              </node>
            </node>
          </node>
        </node>
        <node concept="mw_s8" id="1$PYe999XjN" role="1ZfhKB">
          <node concept="2pJPEk" id="1$PYe999XjJ" role="mwGJk">
            <node concept="2pJPED" id="1$PYe999XjL" role="2pJPEn">
              <ref role="2pJxaS" to="hk02:1$PYe999WiB" resolve="MessageType" />
              <node concept="2pIpSj" id="2lACGaVGUb3" role="2pJxcM">
                <ref role="2pIpSl" to="hk02:2lACGaVDxt_" resolve="baseType" />
                <node concept="36biLy" id="2lACGaVGUbi" role="28nt2d">
                  <node concept="2OqwBi" id="2lACGaVGVJO" role="36biLW">
                    <node concept="2OqwBi" id="2lACGaVGV8C" role="2Oq$k0">
                      <node concept="2OqwBi" id="2lACGaVGUt2" role="2Oq$k0">
                        <node concept="1YBJjd" id="2lACGaVGUbt" role="2Oq$k0">
                          <ref role="1YBMHb" node="3JB$QJ87rLJ" resolve="receiveMessage" />
                        </node>
                        <node concept="3TrEf2" id="2lACGaVGUUt" role="2OqNvi">
                          <ref role="3Tt5mk" to="hk02:4qBaryPhIZI" resolve="arg" />
                        </node>
                      </node>
                      <node concept="3TrEf2" id="2lACGaVGVuh" role="2OqNvi">
                        <ref role="3Tt5mk" to="hk02:6OKQOIXfaDc" resolve="type" />
                      </node>
                    </node>
                    <node concept="3TrEf2" id="2lACGaVGW1f" role="2OqNvi">
                      <ref role="3Tt5mk" to="hk02:2lACGaVDxt_" resolve="baseType" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="1Z5TYs" id="4YVd2MBdur0" role="3cqZAp">
        <node concept="mw_s8" id="4YVd2MBdurk" role="1ZfhKB">
          <node concept="2pJPEk" id="4YVd2MBdurg" role="mwGJk">
            <node concept="2pJPED" id="4YVd2MBduri" role="2pJPEn">
              <ref role="2pJxaS" to="5qo5:6sdnDbSlaon" resolve="BooleanType" />
            </node>
          </node>
        </node>
        <node concept="mw_s8" id="4YVd2MBdur3" role="1ZfhK$">
          <node concept="1Z2H0r" id="4YVd2MBduhF" role="mwGJk">
            <node concept="2OqwBi" id="2aA5E1EP97" role="1Z2MuG">
              <node concept="1YBJjd" id="4YVd2MBdujz" role="2Oq$k0">
                <ref role="1YBMHb" node="3JB$QJ87rLJ" resolve="receiveMessage" />
              </node>
              <node concept="3TrEf2" id="2aA5E1EP$H" role="2OqNvi">
                <ref role="3Tt5mk" to="hk02:6$6nD_DltfC" resolve="condition" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="3JB$QJ87rLJ" role="1YuTPh">
      <property role="TrG5h" value="receiveMessage" />
      <ref role="1YaFvo" to="hk02:7HOFHncqzmZ" resolve="ReceiveMessage" />
    </node>
  </node>
  <node concept="1YbPZF" id="3JB$QJ88SVH">
    <property role="TrG5h" value="typeof_ComposeMessage" />
    <property role="3GE5qa" value="statements" />
    <node concept="3clFbS" id="3JB$QJ88SVI" role="18ibNy">
      <node concept="1Z5TYs" id="3JB$QJ88T7i" role="3cqZAp">
        <node concept="mw_s8" id="3JB$QJ88T7l" role="1ZfhK$">
          <node concept="1Z2H0r" id="3JB$QJ88SVO" role="mwGJk">
            <node concept="1YBJjd" id="3JB$QJ88SXG" role="1Z2MuG">
              <ref role="1YBMHb" node="3JB$QJ88SVK" resolve="composeMsg" />
            </node>
          </node>
        </node>
        <node concept="mw_s8" id="4iLN_rFTcPh" role="1ZfhKB">
          <node concept="2pJPEk" id="4iLN_rFTcPd" role="mwGJk">
            <node concept="2pJPED" id="4iLN_rFTcPf" role="2pJPEn">
              <ref role="2pJxaS" to="hk02:1$PYe999WiB" resolve="MessageType" />
              <node concept="2pIpSj" id="4iLN_rFTcQF" role="2pJxcM">
                <ref role="2pIpSl" to="hk02:2lACGaVDxt_" resolve="baseType" />
                <node concept="36biLy" id="4iLN_rFTiMD" role="28nt2d">
                  <node concept="2OqwBi" id="4iLN_rFTk1I" role="36biLW">
                    <node concept="2OqwBi" id="4iLN_rFTj4i" role="2Oq$k0">
                      <node concept="1YBJjd" id="4iLN_rFTiMO" role="2Oq$k0">
                        <ref role="1YBMHb" node="3JB$QJ88SVK" resolve="composeMsg" />
                      </node>
                      <node concept="3TrEf2" id="4iLN_rFTjNi" role="2OqNvi">
                        <ref role="3Tt5mk" to="hk02:7OgJtfmIVA" resolve="type" />
                      </node>
                    </node>
                    <node concept="3TrEf2" id="4iLN_rFTkn7" role="2OqNvi">
                      <ref role="3Tt5mk" to="hk02:2lACGaVDxt_" resolve="baseType" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="1Z5TYs" id="1$PYe9991bc" role="3cqZAp">
        <node concept="mw_s8" id="1$PYe9991c4" role="1ZfhKB">
          <node concept="2pJPEk" id="1$PYe9991c0" role="mwGJk">
            <node concept="2pJPED" id="1$PYe9991c2" role="2pJPEn">
              <ref role="2pJxaS" to="os74:3JB$QJ8oGPX" resolve="AIDType" />
            </node>
          </node>
        </node>
        <node concept="mw_s8" id="1$PYe9991bf" role="1ZfhK$">
          <node concept="1Z2H0r" id="1$PYe9990c5" role="mwGJk">
            <node concept="2OqwBi" id="1$PYe9990tU" role="1Z2MuG">
              <node concept="1YBJjd" id="1$PYe9990ev" role="2Oq$k0">
                <ref role="1YBMHb" node="3JB$QJ88SVK" resolve="composeMsg" />
              </node>
              <node concept="3TrEf2" id="1$PYe999139" role="2OqNvi">
                <ref role="3Tt5mk" to="hk02:5zd1xVkp1As" resolve="receiver" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="1Z5TYs" id="7AVCbhhZdpH" role="3cqZAp">
        <node concept="mw_s8" id="7AVCbhhZdrb" role="1ZfhKB">
          <node concept="2pJPEk" id="7AVCbhhZdr7" role="mwGJk">
            <node concept="2pJPED" id="7AVCbhhZdr9" role="2pJPEn">
              <ref role="2pJxaS" to="700h:6zmBjqUinsw" resolve="ListType" />
              <node concept="2pIpSj" id="7AVCbhhZdsK" role="2pJxcM">
                <ref role="2pIpSl" to="700h:6zmBjqUily6" resolve="baseType" />
                <node concept="2pJPED" id="7AVCbhhZdsZ" role="28nt2d">
                  <ref role="2pJxaS" to="os74:3JB$QJ8oGPX" resolve="AIDType" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="mw_s8" id="7AVCbhhZdpK" role="1ZfhK$">
          <node concept="1Z2H0r" id="7AVCbhhZbMW" role="mwGJk">
            <node concept="2OqwBi" id="7AVCbhhZc5m" role="1Z2MuG">
              <node concept="1YBJjd" id="7AVCbhhZbPN" role="2Oq$k0">
                <ref role="1YBMHb" node="3JB$QJ88SVK" resolve="composeMsg" />
              </node>
              <node concept="3TrEf2" id="3EtpyNPX0Hx" role="2OqNvi">
                <ref role="3Tt5mk" to="hk02:5zd1xVkp1FD" resolve="otherReceivers" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3clFbH" id="7Ej8xW1oCDb" role="3cqZAp" />
      <node concept="3cpWs8" id="7Ej8xW1p6iR" role="3cqZAp">
        <node concept="3cpWsn" id="7Ej8xW1p6iU" role="3cpWs9">
          <property role="TrG5h" value="msgType" />
          <node concept="3Tqbb2" id="7Ej8xW1p6iP" role="1tU5fm" />
          <node concept="2OqwBi" id="7Ej8xW1oCFU" role="33vP2m">
            <node concept="2OqwBi" id="7Ej8xW1oCFV" role="2Oq$k0">
              <node concept="1YBJjd" id="7Ej8xW1oCFW" role="2Oq$k0">
                <ref role="1YBMHb" node="3JB$QJ88SVK" resolve="composeMsg" />
              </node>
              <node concept="3TrEf2" id="7Ej8xW1oCFX" role="2OqNvi">
                <ref role="3Tt5mk" to="hk02:7OgJtfmIVA" resolve="type" />
              </node>
            </node>
            <node concept="3TrEf2" id="7Ej8xW1oCFY" role="2OqNvi">
              <ref role="3Tt5mk" to="hk02:2lACGaVDxt_" resolve="baseType" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3clFbJ" id="7Ej8xW1oCEP" role="3cqZAp">
        <node concept="3clFbS" id="7Ej8xW1oCER" role="3clFbx">
          <node concept="1Z5TYs" id="7Ej8xW1ouhH" role="3cqZAp">
            <node concept="mw_s8" id="7Ej8xW1ouiy" role="1ZfhKB">
              <node concept="2pJPEk" id="7Ej8xW1ouiu" role="mwGJk">
                <node concept="2pJPED" id="7Ej8xW1ouiw" role="2pJPEn">
                  <ref role="2pJxaS" to="yv47:7D7uZV2dYz2" resolve="RecordType" />
                  <node concept="2pIpSj" id="7Ej8xW1ouk8" role="2pJxcM">
                    <ref role="2pIpSl" to="yv47:7D7uZV2dYz3" resolve="record" />
                    <node concept="36biLy" id="7Ej8xW1oukl" role="28nt2d">
                      <node concept="2OqwBi" id="7Ej8xW1oxDu" role="36biLW">
                        <node concept="1PxgMI" id="7Ej8xW1oxc4" role="2Oq$k0">
                          <node concept="chp4Y" id="7Ej8xW1oxos" role="3oSUPX">
                            <ref role="cht4Q" to="yv47:7D7uZV2dYz2" resolve="RecordType" />
                          </node>
                          <node concept="37vLTw" id="7Ej8xW1p73M" role="1m5AlR">
                            <ref role="3cqZAo" node="7Ej8xW1p6iU" resolve="msgType" />
                          </node>
                        </node>
                        <node concept="3TrEf2" id="7Ej8xW1oyPd" role="2OqNvi">
                          <ref role="3Tt5mk" to="yv47:7D7uZV2dYz3" resolve="record" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="mw_s8" id="7Ej8xW1ouhK" role="1ZfhK$">
              <node concept="1Z2H0r" id="7Ej8xW1osSs" role="mwGJk">
                <node concept="2OqwBi" id="7Ej8xW1ot8R" role="1Z2MuG">
                  <node concept="1YBJjd" id="7Ej8xW1osTb" role="2Oq$k0">
                    <ref role="1YBMHb" node="3JB$QJ88SVK" resolve="composeMsg" />
                  </node>
                  <node concept="3TrEf2" id="7Ej8xW1ou9G" role="2OqNvi">
                    <ref role="3Tt5mk" to="hk02:3I8nNvsTTJ8" resolve="content" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2OqwBi" id="7Ej8xW1oDrn" role="3clFbw">
          <node concept="1mIQ4w" id="7Ej8xW1oDP6" role="2OqNvi">
            <node concept="chp4Y" id="7Ej8xW1oDS1" role="cj9EA">
              <ref role="cht4Q" to="yv47:7D7uZV2dYz2" resolve="RecordType" />
            </node>
          </node>
          <node concept="37vLTw" id="7Ej8xW1p6LB" role="2Oq$k0">
            <ref role="3cqZAo" node="7Ej8xW1p6iU" resolve="msgType" />
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="3JB$QJ88SVK" role="1YuTPh">
      <property role="TrG5h" value="composeMsg" />
      <ref role="1YaFvo" to="hk02:3uK1rwojlW1" resolve="ComposeMessage" />
    </node>
  </node>
  <node concept="1YbPZF" id="4i_Vsgbvk2Y">
    <property role="TrG5h" value="typeof_MessageOperationArgRef" />
    <property role="3GE5qa" value="statements" />
    <node concept="3clFbS" id="4i_Vsgbvk2Z" role="18ibNy">
      <node concept="1Z5TYs" id="2lACGaVE$Uz" role="3cqZAp">
        <node concept="mw_s8" id="2lACGaVE$UE" role="1ZfhK$">
          <node concept="1Z2H0r" id="2lACGaVE$UF" role="mwGJk">
            <node concept="1YBJjd" id="2lACGaVE$UG" role="1Z2MuG">
              <ref role="1YBMHb" node="4i_Vsgbvk31" resolve="messageOperationArgRef" />
            </node>
          </node>
        </node>
        <node concept="mw_s8" id="2lACGaVEHwy" role="1ZfhKB">
          <node concept="2pJPEk" id="2lACGaVEHww" role="mwGJk">
            <node concept="2pJPED" id="2lACGaVEHwx" role="2pJPEn">
              <ref role="2pJxaS" to="hk02:1$PYe999WiB" resolve="MessageType" />
              <node concept="2pIpSj" id="2lACGaVEHxo" role="2pJxcM">
                <ref role="2pIpSl" to="hk02:2lACGaVDxt_" resolve="baseType" />
                <node concept="36biLy" id="2lACGaVGZme" role="28nt2d">
                  <node concept="2OqwBi" id="2lACGaVH0CI" role="36biLW">
                    <node concept="2OqwBi" id="2lACGaVH05a" role="2Oq$k0">
                      <node concept="2OqwBi" id="2lACGaVGZyY" role="2Oq$k0">
                        <node concept="1YBJjd" id="2lACGaVGZmp" role="2Oq$k0">
                          <ref role="1YBMHb" node="4i_Vsgbvk31" resolve="messageOperationArgRef" />
                        </node>
                        <node concept="3TrEf2" id="2lACGaVGZM1" role="2OqNvi">
                          <ref role="3Tt5mk" to="hk02:4i_VsgbvgoW" resolve="arg" />
                        </node>
                      </node>
                      <node concept="3TrEf2" id="2lACGaVH0oC" role="2OqNvi">
                        <ref role="3Tt5mk" to="hk02:6OKQOIXfaDc" resolve="type" />
                      </node>
                    </node>
                    <node concept="3TrEf2" id="2lACGaVH0TF" role="2OqNvi">
                      <ref role="3Tt5mk" to="hk02:2lACGaVDxt_" resolve="baseType" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="4i_Vsgbvk31" role="1YuTPh">
      <property role="TrG5h" value="messageOperationArgRef" />
      <ref role="1YaFvo" to="hk02:4i_Vsgbvgoq" resolve="ReceivedMessageArgRef" />
    </node>
  </node>
  <node concept="1YbPZF" id="7AVCbhhS3$C">
    <property role="TrG5h" value="typeof_PerformativeType" />
    <property role="3GE5qa" value="matches" />
    <node concept="3clFbS" id="7AVCbhhS3$D" role="18ibNy">
      <node concept="1Z5TYs" id="7AVCbhhS3I_" role="3cqZAp">
        <node concept="mw_s8" id="7AVCbhhS3IC" role="1ZfhK$">
          <node concept="1Z2H0r" id="7AVCbhhS3$J" role="mwGJk">
            <node concept="1YBJjd" id="7AVCbhhS3AB" role="1Z2MuG">
              <ref role="1YBMHb" node="7AVCbhhS3$F" resolve="performativeType" />
            </node>
          </node>
        </node>
        <node concept="mw_s8" id="7AVCbhhS7XY" role="1ZfhKB">
          <node concept="1YBJjd" id="7AVCbhhS7XW" role="mwGJk">
            <ref role="1YBMHb" node="7AVCbhhS3$F" resolve="performativeType" />
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="7AVCbhhS3$F" role="1YuTPh">
      <property role="TrG5h" value="performativeType" />
      <ref role="1YaFvo" to="os74:6OKQOIXeu_H" resolve="PerformativeType" />
    </node>
  </node>
  <node concept="1YbPZF" id="7AVCbhhTzvp">
    <property role="TrG5h" value="typeof_SenderMatch" />
    <property role="3GE5qa" value="matches" />
    <node concept="3clFbS" id="7AVCbhhTzvq" role="18ibNy">
      <node concept="1Z5TYs" id="7AVCbhhT$0y" role="3cqZAp">
        <node concept="mw_s8" id="7AVCbhhT$0U" role="1ZfhKB">
          <node concept="2pJPEk" id="7AVCbhhT$0Q" role="mwGJk">
            <node concept="2pJPED" id="7AVCbhhT$0S" role="2pJPEn">
              <ref role="2pJxaS" to="os74:3JB$QJ8oGPX" resolve="AIDType" />
            </node>
          </node>
        </node>
        <node concept="mw_s8" id="7AVCbhhT$0_" role="1ZfhK$">
          <node concept="1Z2H0r" id="7AVCbhhTzvw" role="mwGJk">
            <node concept="2OqwBi" id="7AVCbhhTzGd" role="1Z2MuG">
              <node concept="1YBJjd" id="7AVCbhhTzxo" role="2Oq$k0">
                <ref role="1YBMHb" node="7AVCbhhTzvs" resolve="senderMatch" />
              </node>
              <node concept="3TrEf2" id="7AVCbhhTzUP" role="2OqNvi">
                <ref role="3Tt5mk" to="hk02:7AVCbhhTzuX" resolve="value" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="7AVCbhhTzvs" role="1YuTPh">
      <property role="TrG5h" value="senderMatch" />
      <ref role="1YaFvo" to="hk02:7AVCbhhTzuj" resolve="SenderMatch" />
    </node>
  </node>
  <node concept="1YbPZF" id="7AVCbhhUc$1">
    <property role="TrG5h" value="typeof_ReceiverMatch" />
    <property role="3GE5qa" value="matches" />
    <node concept="3clFbS" id="7AVCbhhUc$2" role="18ibNy">
      <node concept="2Gpval" id="7AVCbhhUdFu" role="3cqZAp">
        <node concept="2GrKxI" id="7AVCbhhUdFw" role="2Gsz3X">
          <property role="TrG5h" value="r" />
        </node>
        <node concept="2OqwBi" id="7AVCbhhUdSG" role="2GsD0m">
          <node concept="1YBJjd" id="7AVCbhhUdFX" role="2Oq$k0">
            <ref role="1YBMHb" node="7AVCbhhUc$4" resolve="receiverMatch" />
          </node>
          <node concept="3Tsc0h" id="7AVCbhhUe92" role="2OqNvi">
            <ref role="3TtcxE" to="hk02:7AVCbhhUcyX" resolve="values" />
          </node>
        </node>
        <node concept="3clFbS" id="7AVCbhhUdF$" role="2LFqv$">
          <node concept="1Z5TYs" id="7AVCbhhUeml" role="3cqZAp">
            <node concept="mw_s8" id="7AVCbhhUemD" role="1ZfhKB">
              <node concept="2pJPEk" id="7AVCbhhUem_" role="mwGJk">
                <node concept="2pJPED" id="7AVCbhhUemB" role="2pJPEn">
                  <ref role="2pJxaS" to="os74:3JB$QJ8oGPX" resolve="AIDType" />
                </node>
              </node>
            </node>
            <node concept="mw_s8" id="7AVCbhhUemo" role="1ZfhK$">
              <node concept="1Z2H0r" id="7AVCbhhUece" role="mwGJk">
                <node concept="2GrUjf" id="7AVCbhhUee6" role="1Z2MuG">
                  <ref role="2Gs0qQ" node="7AVCbhhUdFw" resolve="r" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="7AVCbhhUc$4" role="1YuTPh">
      <property role="TrG5h" value="receiverMatch" />
      <ref role="1YaFvo" to="hk02:7AVCbhhUcyW" resolve="ReceiverMatch" />
    </node>
  </node>
  <node concept="1YbPZF" id="7AVCbhhYKMS">
    <property role="TrG5h" value="typeof_GetSenderOp" />
    <property role="3GE5qa" value="operations" />
    <node concept="3clFbS" id="7AVCbhhYKMT" role="18ibNy">
      <node concept="1Z5TYs" id="7AVCbhhYKPH" role="3cqZAp">
        <node concept="mw_s8" id="7AVCbhhYKPI" role="1ZfhK$">
          <node concept="1Z2H0r" id="7AVCbhhYKPJ" role="mwGJk">
            <node concept="1YBJjd" id="7AVCbhhYKPK" role="1Z2MuG">
              <ref role="1YBMHb" node="7AVCbhhYKMV" resolve="getSenderOp" />
            </node>
          </node>
        </node>
        <node concept="mw_s8" id="7AVCbhhYKRH" role="1ZfhKB">
          <node concept="2pJPEk" id="7AVCbhhYKRD" role="mwGJk">
            <node concept="2pJPED" id="7AVCbhhYKRF" role="2pJPEn">
              <ref role="2pJxaS" to="os74:3JB$QJ8oGPX" resolve="AIDType" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="7AVCbhhYKMV" role="1YuTPh">
      <property role="TrG5h" value="getSenderOp" />
      <ref role="1YaFvo" to="hk02:7AVCbhhYKKm" resolve="GetSenderOp" />
    </node>
  </node>
  <node concept="1YbPZF" id="7AVCbhhYXGF">
    <property role="TrG5h" value="typeof_GetReceiverOp" />
    <property role="3GE5qa" value="operations" />
    <node concept="3clFbS" id="7AVCbhhYXGG" role="18ibNy">
      <node concept="1Z5TYs" id="7AVCbhhYXGM" role="3cqZAp">
        <node concept="mw_s8" id="7AVCbhhYXGN" role="1ZfhK$">
          <node concept="1Z2H0r" id="7AVCbhhYXGO" role="mwGJk">
            <node concept="1YBJjd" id="7AVCbhhYXGP" role="1Z2MuG">
              <ref role="1YBMHb" node="7AVCbhhYXGI" resolve="getReceiverOp" />
            </node>
          </node>
        </node>
        <node concept="mw_s8" id="7AVCbhhYYBR" role="1ZfhKB">
          <node concept="2pJPEk" id="7AVCbhhYYBP" role="mwGJk">
            <node concept="2pJPED" id="7AVCbhhYYBQ" role="2pJPEn">
              <ref role="2pJxaS" to="700h:6zmBjqUinsw" resolve="ListType" />
              <node concept="2pIpSj" id="7AVCbhhYYDs" role="2pJxcM">
                <ref role="2pIpSl" to="700h:6zmBjqUily6" resolve="baseType" />
                <node concept="2pJPED" id="7AVCbhhYYDF" role="28nt2d">
                  <ref role="2pJxaS" to="os74:3JB$QJ8oGPX" resolve="AIDType" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="7AVCbhhYXGI" role="1YuTPh">
      <property role="TrG5h" value="getReceiverOp" />
      <ref role="1YaFvo" to="hk02:7AVCbhhYXBK" resolve="GetReceiversOp" />
    </node>
  </node>
  <node concept="1YbPZF" id="7AVCbhi0iRy">
    <property role="TrG5h" value="typeof_GetContentOp" />
    <property role="3GE5qa" value="operations" />
    <node concept="3clFbS" id="7AVCbhi0iRz" role="18ibNy">
      <node concept="nvevp" id="7AVCbhi0LlX" role="3cqZAp">
        <node concept="3clFbS" id="7AVCbhi0LlZ" role="nvhr_">
          <node concept="1Z5TYs" id="7AVCbhi0Msm" role="3cqZAp">
            <node concept="mw_s8" id="7AVCbhi0MCh" role="1ZfhKB">
              <node concept="2OqwBi" id="6uhyzyZWcPa" role="mwGJk">
                <node concept="1PxgMI" id="7AVCbhi0ME8" role="2Oq$k0">
                  <node concept="chp4Y" id="7AVCbhi0MEE" role="3oSUPX">
                    <ref role="cht4Q" to="hk02:1$PYe999WiB" resolve="MessageType" />
                  </node>
                  <node concept="2X3wrD" id="7AVCbhi0MCf" role="1m5AlR">
                    <ref role="2X3Bk0" node="7AVCbhi0Lm3" resolve="contextType" />
                  </node>
                </node>
                <node concept="3TrEf2" id="6uhyzyZWzdx" role="2OqNvi">
                  <ref role="3Tt5mk" to="hk02:2lACGaVDxt_" resolve="baseType" />
                </node>
              </node>
            </node>
            <node concept="mw_s8" id="7AVCbhi0Msp" role="1ZfhK$">
              <node concept="1Z2H0r" id="7AVCbhi0M3Y" role="mwGJk">
                <node concept="1YBJjd" id="7AVCbhi0M5Q" role="1Z2MuG">
                  <ref role="1YBMHb" node="7AVCbhi0iR_" resolve="getContentOp" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1Z2H0r" id="7AVCbhi0LmX" role="nvjzm">
          <node concept="2OqwBi" id="7AVCbhi0Lz6" role="1Z2MuG">
            <node concept="1YBJjd" id="7AVCbhi0Lnp" role="2Oq$k0">
              <ref role="1YBMHb" node="7AVCbhi0iR_" resolve="getContentOp" />
            </node>
            <node concept="2qgKlT" id="7AVCbhi0LRd" role="2OqNvi">
              <ref role="37wK5l" to="pbu6:6zmBjqUivyF" resolve="contextExpression" />
            </node>
          </node>
        </node>
        <node concept="2X1qdy" id="7AVCbhi0Lm3" role="2X0Ygz">
          <property role="TrG5h" value="contextType" />
          <node concept="2jxLKc" id="7AVCbhi0Lm4" role="1tU5fm" />
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="7AVCbhi0iR_" role="1YuTPh">
      <property role="TrG5h" value="getContentOp" />
      <ref role="1YaFvo" to="hk02:7AVCbhi0iOL" resolve="GetContentOp" />
    </node>
  </node>
  <node concept="1YbPZF" id="3A64HzfW$QS">
    <property role="TrG5h" value="typeof_IMessageMatcher" />
    <property role="3GE5qa" value="matches" />
    <node concept="3clFbS" id="3A64HzfW$QT" role="18ibNy">
      <node concept="1Z5TYs" id="3A64HzfW_00" role="3cqZAp">
        <node concept="mw_s8" id="3A64HzfW_0k" role="1ZfhKB">
          <node concept="2pJPEk" id="3A64HzfW_0g" role="mwGJk">
            <node concept="2pJPED" id="3A64HzfW_0i" role="2pJPEn">
              <ref role="2pJxaS" to="5qo5:6sdnDbSlaon" resolve="BooleanType" />
            </node>
          </node>
        </node>
        <node concept="mw_s8" id="3A64HzfW_03" role="1ZfhK$">
          <node concept="1Z2H0r" id="3A64HzfW$QZ" role="mwGJk">
            <node concept="1YBJjd" id="3A64HzfW$SR" role="1Z2MuG">
              <ref role="1YBMHb" node="3A64HzfW$QV" resolve="iMessageMatcher" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="3A64HzfW$QV" role="1YuTPh">
      <property role="TrG5h" value="iMessageMatcher" />
      <ref role="1YaFvo" to="hk02:vQ6nfZ2W1K" resolve="IMessageMatcher" />
    </node>
  </node>
  <node concept="18kY7G" id="2HiuWK4rl1L">
    <property role="TrG5h" value="check_MessageType" />
    <node concept="3clFbS" id="2HiuWK4rl1M" role="18ibNy">
      <node concept="3cpWs8" id="7Ej8xW1nVxs" role="3cqZAp">
        <node concept="3cpWsn" id="7Ej8xW1nVxv" role="3cpWs9">
          <property role="TrG5h" value="msgType" />
          <node concept="3Tqbb2" id="7Ej8xW1nVxq" role="1tU5fm">
            <ref role="ehGHo" to="hm2y:6sdnDbSlaok" resolve="Type" />
          </node>
          <node concept="2OqwBi" id="7Ej8xW1nVyZ" role="33vP2m">
            <node concept="3TrEf2" id="7Ej8xW1nVz0" role="2OqNvi">
              <ref role="3Tt5mk" to="hk02:2lACGaVDxt_" resolve="baseType" />
            </node>
            <node concept="1YBJjd" id="7Ej8xW1nVz1" role="2Oq$k0">
              <ref role="1YBMHb" node="2HiuWK4rl1O" resolve="messageType" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3clFbH" id="7Ej8xW1nWTC" role="3cqZAp" />
      <node concept="3clFbJ" id="7Ej8xW1nVhy" role="3cqZAp">
        <node concept="3clFbS" id="7Ej8xW1nVh$" role="3clFbx">
          <node concept="3clFbJ" id="7Ej8xW1nWCZ" role="3cqZAp">
            <node concept="3clFbS" id="7Ej8xW1nWD0" role="3clFbx">
              <node concept="2MkqsV" id="7Ej8xW1nWD1" role="3cqZAp">
                <node concept="Xl_RD" id="7Ej8xW1nWD2" role="2MkJ7o">
                  <property role="Xl_RC" value="The type of the message must be an ontology element" />
                </node>
                <node concept="2OqwBi" id="7Ej8xW1nWD3" role="1urrMF">
                  <node concept="1YBJjd" id="7Ej8xW1nWD4" role="2Oq$k0">
                    <ref role="1YBMHb" node="2HiuWK4rl1O" resolve="messageType" />
                  </node>
                  <node concept="3TrEf2" id="7Ej8xW1nWD5" role="2OqNvi">
                    <ref role="3Tt5mk" to="hk02:2lACGaVDxt_" resolve="baseType" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3fqX7Q" id="7Ej8xW1nWD6" role="3clFbw">
              <node concept="2OqwBi" id="7Ej8xW1nWD7" role="3fr31v">
                <node concept="2OqwBi" id="7Ej8xW1nWD8" role="2Oq$k0">
                  <node concept="1PxgMI" id="7Ej8xW1nWD9" role="2Oq$k0">
                    <node concept="chp4Y" id="7Ej8xW1nWDa" role="3oSUPX">
                      <ref role="cht4Q" to="yv47:7D7uZV2dYz2" resolve="RecordType" />
                    </node>
                    <node concept="37vLTw" id="7Ej8xW1oio_" role="1m5AlR">
                      <ref role="3cqZAo" node="7Ej8xW1nVxv" resolve="msgType" />
                    </node>
                  </node>
                  <node concept="3TrEf2" id="7Ej8xW1nWDe" role="2OqNvi">
                    <ref role="3Tt5mk" to="yv47:7D7uZV2dYz3" resolve="record" />
                  </node>
                </node>
                <node concept="1mIQ4w" id="7Ej8xW1nWDf" role="2OqNvi">
                  <node concept="chp4Y" id="7Ej8xW1nWDg" role="cj9EA">
                    <ref role="cht4Q" to="3jzb:7dT_6n1a_NX" resolve="ElementSchema" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2OqwBi" id="7Ej8xW1nViP" role="3clFbw">
          <node concept="37vLTw" id="7Ej8xW1nWxI" role="2Oq$k0">
            <ref role="3cqZAo" node="7Ej8xW1nVxv" resolve="msgType" />
          </node>
          <node concept="1mIQ4w" id="7Ej8xW1nViT" role="2OqNvi">
            <node concept="chp4Y" id="7Ej8xW1nViU" role="cj9EA">
              <ref role="cht4Q" to="yv47:7D7uZV2dYz2" resolve="RecordType" />
            </node>
          </node>
        </node>
        <node concept="9aQIb" id="7Ej8xW1o292" role="9aQIa">
          <node concept="3clFbS" id="7Ej8xW1o293" role="9aQI4">
            <node concept="2MkqsV" id="7Ej8xW1o2gM" role="3cqZAp">
              <node concept="Xl_RD" id="7Ej8xW1o2gN" role="2MkJ7o">
                <property role="Xl_RC" value="The type of the message must be an ontology element" />
              </node>
              <node concept="2OqwBi" id="7Ej8xW1o2gO" role="1urrMF">
                <node concept="1YBJjd" id="7Ej8xW1o2gP" role="2Oq$k0">
                  <ref role="1YBMHb" node="2HiuWK4rl1O" resolve="messageType" />
                </node>
                <node concept="3TrEf2" id="7Ej8xW1o2gQ" role="2OqNvi">
                  <ref role="3Tt5mk" to="hk02:2lACGaVDxt_" resolve="baseType" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="2HiuWK4rl1O" role="1YuTPh">
      <property role="TrG5h" value="messageType" />
      <ref role="1YaFvo" to="hk02:1$PYe999WiB" resolve="MessageType" />
    </node>
  </node>
  <node concept="35pCF_" id="2HiuWK4spL0">
    <property role="TrG5h" value="replaceMessageType" />
    <node concept="1YaCAy" id="2HiuWK4spL_" role="35pZ6h">
      <property role="TrG5h" value="exp" />
      <ref role="1YaFvo" to="hk02:1$PYe999WiB" resolve="MessageType" />
    </node>
    <node concept="3clFbS" id="2HiuWK4spL2" role="2sgrp5">
      <node concept="1ZobV4" id="2HiuWK4sqYu" role="3cqZAp">
        <node concept="mw_s8" id="2HiuWK4sqZt" role="1ZfhKB">
          <node concept="2OqwBi" id="2HiuWK4sr7E" role="mwGJk">
            <node concept="1YBJjd" id="2HiuWK4sqZr" role="2Oq$k0">
              <ref role="1YBMHb" node="2HiuWK4spL_" resolve="exp" />
            </node>
            <node concept="3TrEf2" id="2HiuWK4srnE" role="2OqNvi">
              <ref role="3Tt5mk" to="hk02:2lACGaVDxt_" resolve="baseType" />
            </node>
          </node>
        </node>
        <node concept="mw_s8" id="2HiuWK4sqYx" role="1ZfhK$">
          <node concept="2OqwBi" id="2HiuWK4s_Cm" role="mwGJk">
            <node concept="1YBJjd" id="2HiuWK4spNh" role="2Oq$k0">
              <ref role="1YBMHb" node="2HiuWK4spL4" resolve="msg" />
            </node>
            <node concept="3TrEf2" id="2HiuWK4s_T$" role="2OqNvi">
              <ref role="3Tt5mk" to="hk02:2lACGaVDxt_" resolve="baseType" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="2HiuWK4spL4" role="1YuTPh">
      <property role="TrG5h" value="msg" />
      <ref role="1YaFvo" to="hk02:1$PYe999WiB" resolve="MessageType" />
    </node>
  </node>
  <node concept="1YbPZF" id="7OgJtfqDts">
    <property role="TrG5h" value="typeof_ReplyToMessage" />
    <property role="3GE5qa" value="statements" />
    <node concept="3clFbS" id="7OgJtfqDtt" role="18ibNy">
      <node concept="1Z5TYs" id="7OgJtfqE2G" role="3cqZAp">
        <node concept="mw_s8" id="7OgJtfqE2H" role="1ZfhKB">
          <node concept="2pJPEk" id="7OgJtfqE2I" role="mwGJk">
            <node concept="2pJPED" id="7OgJtfqE2J" role="2pJPEn">
              <ref role="2pJxaS" to="os74:80jriwjIN" resolve="UnitType" />
            </node>
          </node>
        </node>
        <node concept="mw_s8" id="7OgJtfqE2K" role="1ZfhK$">
          <node concept="1Z2H0r" id="7OgJtfqE2L" role="mwGJk">
            <node concept="1YBJjd" id="7OgJtfqE2M" role="1Z2MuG">
              <ref role="1YBMHb" node="7OgJtfqDtv" resolve="replyToMessage" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3clFbH" id="7OgJtfqE37" role="3cqZAp" />
      <node concept="3cpWs8" id="7OgJtfqE38" role="3cqZAp">
        <node concept="3cpWsn" id="7OgJtfqE39" role="3cpWs9">
          <property role="TrG5h" value="msgType" />
          <node concept="3Tqbb2" id="7OgJtfqE3a" role="1tU5fm" />
          <node concept="2OqwBi" id="7OgJtfqE3b" role="33vP2m">
            <node concept="2OqwBi" id="7OgJtfqE3c" role="2Oq$k0">
              <node concept="1YBJjd" id="7OgJtfqE3d" role="2Oq$k0">
                <ref role="1YBMHb" node="7OgJtfqDtv" resolve="replyToMessage" />
              </node>
              <node concept="3TrEf2" id="7OgJtfqE3e" role="2OqNvi">
                <ref role="3Tt5mk" to="hk02:7OgJtfqDiN" resolve="type" />
              </node>
            </node>
            <node concept="3TrEf2" id="7OgJtfqE3f" role="2OqNvi">
              <ref role="3Tt5mk" to="hk02:2lACGaVDxt_" resolve="baseType" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3clFbJ" id="7OgJtfqE3g" role="3cqZAp">
        <node concept="3clFbS" id="7OgJtfqE3h" role="3clFbx">
          <node concept="1Z5TYs" id="7OgJtfqE3i" role="3cqZAp">
            <node concept="mw_s8" id="7OgJtfqE3j" role="1ZfhKB">
              <node concept="2pJPEk" id="7OgJtfqE3k" role="mwGJk">
                <node concept="2pJPED" id="7OgJtfqE3l" role="2pJPEn">
                  <ref role="2pJxaS" to="yv47:7D7uZV2dYz2" resolve="RecordType" />
                  <node concept="2pIpSj" id="7OgJtfqE3m" role="2pJxcM">
                    <ref role="2pIpSl" to="yv47:7D7uZV2dYz3" resolve="record" />
                    <node concept="36biLy" id="7OgJtfqE3n" role="28nt2d">
                      <node concept="2OqwBi" id="7OgJtfqE3o" role="36biLW">
                        <node concept="1PxgMI" id="7OgJtfqE3p" role="2Oq$k0">
                          <node concept="chp4Y" id="7OgJtfqE3q" role="3oSUPX">
                            <ref role="cht4Q" to="yv47:7D7uZV2dYz2" resolve="RecordType" />
                          </node>
                          <node concept="37vLTw" id="7OgJtfqE3r" role="1m5AlR">
                            <ref role="3cqZAo" node="7OgJtfqE39" resolve="msgType" />
                          </node>
                        </node>
                        <node concept="3TrEf2" id="7OgJtfqE3s" role="2OqNvi">
                          <ref role="3Tt5mk" to="yv47:7D7uZV2dYz3" resolve="record" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="mw_s8" id="7OgJtfqE3t" role="1ZfhK$">
              <node concept="1Z2H0r" id="7OgJtfqE3u" role="mwGJk">
                <node concept="2OqwBi" id="7OgJtfqE3v" role="1Z2MuG">
                  <node concept="1YBJjd" id="7OgJtfqE3w" role="2Oq$k0">
                    <ref role="1YBMHb" node="7OgJtfqDtv" resolve="replyToMessage" />
                  </node>
                  <node concept="3TrEf2" id="7OgJtfqE3x" role="2OqNvi">
                    <ref role="3Tt5mk" to="hk02:7OgJtfqDiK" resolve="content" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2OqwBi" id="7OgJtfqE3y" role="3clFbw">
          <node concept="1mIQ4w" id="7OgJtfqE3z" role="2OqNvi">
            <node concept="chp4Y" id="7OgJtfqE3$" role="cj9EA">
              <ref role="cht4Q" to="yv47:7D7uZV2dYz2" resolve="RecordType" />
            </node>
          </node>
          <node concept="37vLTw" id="7OgJtfqE3_" role="2Oq$k0">
            <ref role="3cqZAo" node="7OgJtfqE39" resolve="msgType" />
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="7OgJtfqDtv" role="1YuTPh">
      <property role="TrG5h" value="replyToMessage" />
      <ref role="1YaFvo" to="hk02:7OgJtfqDiI" resolve="ReplyToMessage" />
    </node>
  </node>
  <node concept="18kY7G" id="7OgJtfu9QK">
    <property role="TrG5h" value="check_ReplyToMessage" />
    <property role="3GE5qa" value="statements" />
    <node concept="3clFbS" id="7OgJtfu9QL" role="18ibNy">
      <node concept="3clFbJ" id="7OgJtfu9Yi" role="3cqZAp">
        <node concept="3fqX7Q" id="7OgJtfubLL" role="3clFbw">
          <node concept="2OqwBi" id="7OgJtfubLN" role="3fr31v">
            <node concept="2OqwBi" id="7OgJtfubLO" role="2Oq$k0">
              <node concept="2OqwBi" id="7OgJtfubLP" role="2Oq$k0">
                <node concept="1YBJjd" id="7OgJtfubLQ" role="2Oq$k0">
                  <ref role="1YBMHb" node="7OgJtfu9QN" resolve="replyToMessage" />
                </node>
                <node concept="3TrEf2" id="7OgJtfubLR" role="2OqNvi">
                  <ref role="3Tt5mk" to="hk02:7OgJtfqDiO" resolve="reply" />
                </node>
              </node>
              <node concept="3JvlWi" id="7OgJtfubLS" role="2OqNvi" />
            </node>
            <node concept="1mIQ4w" id="7OgJtfubLT" role="2OqNvi">
              <node concept="chp4Y" id="7OgJtfubLU" role="cj9EA">
                <ref role="cht4Q" to="hk02:1$PYe999WiB" resolve="MessageType" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbS" id="7OgJtfu9Yk" role="3clFbx">
          <node concept="2MkqsV" id="7OgJtfubMM" role="3cqZAp">
            <node concept="Xl_RD" id="7OgJtfubMY" role="2MkJ7o">
              <property role="Xl_RC" value="An expression of type message is required" />
            </node>
            <node concept="2OqwBi" id="7OgJtfuc08" role="1urrMF">
              <node concept="1YBJjd" id="7OgJtfubN9" role="2Oq$k0">
                <ref role="1YBMHb" node="7OgJtfu9QN" resolve="replyToMessage" />
              </node>
              <node concept="3TrEf2" id="7OgJtfucMc" role="2OqNvi">
                <ref role="3Tt5mk" to="hk02:7OgJtfqDiO" resolve="reply" />
              </node>
            </node>
          </node>
        </node>
        <node concept="9aQIb" id="4TWmWFY80zg" role="9aQIa">
          <node concept="3clFbS" id="4TWmWFY80zh" role="9aQI4">
            <node concept="3cpWs8" id="3uk$EjT4c2B" role="3cqZAp">
              <node concept="3cpWsn" id="3uk$EjT4c2C" role="3cpWs9">
                <property role="TrG5h" value="rec" />
                <node concept="3Tqbb2" id="3uk$EjT4c2D" role="1tU5fm" />
                <node concept="2OqwBi" id="3uk$EjT4c2E" role="33vP2m">
                  <node concept="2OqwBi" id="3uk$EjT4c2I" role="2Oq$k0">
                    <node concept="1YBJjd" id="3uk$EjT4c2J" role="2Oq$k0">
                      <ref role="1YBMHb" node="7OgJtfu9QN" resolve="replyToMessage" />
                    </node>
                    <node concept="3TrEf2" id="2jzj96ZwsW3" role="2OqNvi">
                      <ref role="3Tt5mk" to="hk02:7OgJtfqDiN" resolve="type" />
                    </node>
                  </node>
                  <node concept="3TrEf2" id="3uk$EjT4c2M" role="2OqNvi">
                    <ref role="3Tt5mk" to="hk02:2lACGaVDxt_" resolve="baseType" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbJ" id="3uk$EjT4c2N" role="3cqZAp">
              <node concept="3clFbS" id="3uk$EjT4c2O" role="3clFbx">
                <node concept="3clFbJ" id="4TWmWFY80x4" role="3cqZAp">
                  <node concept="3clFbS" id="4TWmWFY80x6" role="3clFbx">
                    <node concept="2MkqsV" id="4TWmWFY8_jl" role="3cqZAp">
                      <node concept="Xl_RD" id="4TWmWFY8_jm" role="2MkJ7o">
                        <property role="Xl_RC" value="The content of the message must be a Concept" />
                      </node>
                      <node concept="2OqwBi" id="4TWmWFY8_jn" role="1urrMF">
                        <node concept="2OqwBi" id="4TWmWFY8_jo" role="2Oq$k0">
                          <node concept="1YBJjd" id="4TWmWFY8_jp" role="2Oq$k0">
                            <ref role="1YBMHb" node="7OgJtfu9QN" resolve="replyToMessage" />
                          </node>
                          <node concept="3TrEf2" id="4TWmWFY8_jq" role="2OqNvi">
                            <ref role="3Tt5mk" to="hk02:7OgJtfqDiN" resolve="type" />
                          </node>
                        </node>
                        <node concept="3TrEf2" id="4TWmWFY8_jr" role="2OqNvi">
                          <ref role="3Tt5mk" to="hk02:2lACGaVDxt_" resolve="baseType" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3fqX7Q" id="4TWmWFY8APY" role="3clFbw">
                    <node concept="2OqwBi" id="4TWmWFY8AQ0" role="3fr31v">
                      <node concept="2OqwBi" id="4TWmWFY8AQ1" role="2Oq$k0">
                        <node concept="1PxgMI" id="4TWmWFY8AQ2" role="2Oq$k0">
                          <node concept="chp4Y" id="4TWmWFY8AQ3" role="3oSUPX">
                            <ref role="cht4Q" to="yv47:7D7uZV2dYz2" resolve="RecordType" />
                          </node>
                          <node concept="37vLTw" id="4TWmWFY8CZV" role="1m5AlR">
                            <ref role="3cqZAo" node="3uk$EjT4c2C" resolve="rec" />
                          </node>
                        </node>
                        <node concept="3TrEf2" id="4TWmWFY8AQ9" role="2OqNvi">
                          <ref role="3Tt5mk" to="yv47:7D7uZV2dYz3" resolve="record" />
                        </node>
                      </node>
                      <node concept="1mIQ4w" id="4TWmWFY8AQa" role="2OqNvi">
                        <node concept="chp4Y" id="4TWmWFY8AQb" role="cj9EA">
                          <ref role="cht4Q" to="3jzb:7dT_6n1a0ft" resolve="ConceptSchema" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="9aQIb" id="4TWmWFY8EoT" role="9aQIa">
                    <node concept="3clFbS" id="4TWmWFY8EoU" role="9aQI4">
                      <node concept="3clFbJ" id="2jzj96Zwg8C" role="3cqZAp">
                        <node concept="3clFbS" id="2jzj96Zwg8E" role="3clFbx">
                          <node concept="3clFbJ" id="2jzj96Zwmq0" role="3cqZAp">
                            <node concept="3clFbS" id="2jzj96Zwmq2" role="3clFbx">
                              <node concept="2MkqsV" id="2jzj96Zwq8_" role="3cqZAp">
                                <node concept="3cpWs3" id="2jzj96Zwq8A" role="2MkJ7o">
                                  <node concept="Xl_RD" id="2jzj96Zwq8B" role="3uHU7w">
                                    <property role="Xl_RC" value=" is not compliant with the FIPA standard" />
                                  </node>
                                  <node concept="3cpWs3" id="2jzj96Zwq8C" role="3uHU7B">
                                    <node concept="Xl_RD" id="2jzj96Zwq8D" role="3uHU7B">
                                      <property role="Xl_RC" value="A message with content of type concept that has the performative " />
                                    </node>
                                    <node concept="2OqwBi" id="2jzj96Zwq8E" role="3uHU7w">
                                      <node concept="1YBJjd" id="2jzj96Zwq8F" role="2Oq$k0">
                                        <ref role="1YBMHb" node="7OgJtfu9QN" resolve="replyToMessage" />
                                      </node>
                                      <node concept="3TrcHB" id="2jzj96Zwq8G" role="2OqNvi">
                                        <ref role="3TsBF5" to="os74:Oow4MYDA1o" resolve="performative" />
                                      </node>
                                    </node>
                                  </node>
                                </node>
                                <node concept="1YBJjd" id="2jzj96Zwq8H" role="1urrMF">
                                  <ref role="1YBMHb" node="7OgJtfu9QN" resolve="replyToMessage" />
                                </node>
                              </node>
                            </node>
                            <node concept="2OqwBi" id="2jzj96Zwp0N" role="3clFbw">
                              <node concept="2OqwBi" id="2jzj96Zwns7" role="2Oq$k0">
                                <node concept="1PxgMI" id="2jzj96Zwn0J" role="2Oq$k0">
                                  <node concept="chp4Y" id="2jzj96Zwn8p" role="3oSUPX">
                                    <ref role="cht4Q" to="yv47:7D7uZV2dYz2" resolve="RecordType" />
                                  </node>
                                  <node concept="37vLTw" id="2jzj96ZwmS0" role="1m5AlR">
                                    <ref role="3cqZAo" node="3uk$EjT4c2C" resolve="rec" />
                                  </node>
                                </node>
                                <node concept="3TrEf2" id="2jzj96ZwovU" role="2OqNvi">
                                  <ref role="3Tt5mk" to="yv47:7D7uZV2dYz3" resolve="record" />
                                </node>
                              </node>
                              <node concept="1mIQ4w" id="2jzj96ZwpPp" role="2OqNvi">
                                <node concept="chp4Y" id="2jzj96ZwpVL" role="cj9EA">
                                  <ref role="cht4Q" to="3jzb:7dT_6n1a0ft" resolve="ConceptSchema" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="22lmx$" id="2jzj96ZwkVI" role="3clFbw">
                          <node concept="22lmx$" id="2jzj96Zwjcy" role="3uHU7B">
                            <node concept="2OqwBi" id="2jzj96Zwirf" role="3uHU7B">
                              <node concept="2OqwBi" id="2jzj96Zwgt$" role="2Oq$k0">
                                <node concept="1YBJjd" id="2jzj96Zwgbx" role="2Oq$k0">
                                  <ref role="1YBMHb" node="7OgJtfu9QN" resolve="replyToMessage" />
                                </node>
                                <node concept="3TrcHB" id="2jzj96Zwh2w" role="2OqNvi">
                                  <ref role="3TsBF5" to="os74:Oow4MYDA1o" resolve="performative" />
                                </node>
                              </node>
                              <node concept="21noJN" id="2jzj96ZwiIO" role="2OqNvi">
                                <node concept="21nZrQ" id="2jzj96ZwiIQ" role="21noJM">
                                  <ref role="21nZrZ" to="os74:4MI7ZB$IHfN" resolve="Request" />
                                </node>
                              </node>
                            </node>
                            <node concept="2OqwBi" id="2jzj96ZwkfL" role="3uHU7w">
                              <node concept="2OqwBi" id="2jzj96ZwkfM" role="2Oq$k0">
                                <node concept="1YBJjd" id="2jzj96ZwkfN" role="2Oq$k0">
                                  <ref role="1YBMHb" node="7OgJtfu9QN" resolve="replyToMessage" />
                                </node>
                                <node concept="3TrcHB" id="2jzj96ZwkfO" role="2OqNvi">
                                  <ref role="3TsBF5" to="os74:Oow4MYDA1o" resolve="performative" />
                                </node>
                              </node>
                              <node concept="21noJN" id="2jzj96ZwkfP" role="2OqNvi">
                                <node concept="21nZrQ" id="2jzj96ZwkfQ" role="21noJM">
                                  <ref role="21nZrZ" to="os74:4MI7ZB$IHg5" resolve="RequestWhen" />
                                </node>
                              </node>
                            </node>
                          </node>
                          <node concept="2OqwBi" id="2jzj96Zwl2i" role="3uHU7w">
                            <node concept="2OqwBi" id="2jzj96Zwl2j" role="2Oq$k0">
                              <node concept="1YBJjd" id="2jzj96Zwl2k" role="2Oq$k0">
                                <ref role="1YBMHb" node="7OgJtfu9QN" resolve="replyToMessage" />
                              </node>
                              <node concept="3TrcHB" id="2jzj96Zwl2l" role="2OqNvi">
                                <ref role="3TsBF5" to="os74:Oow4MYDA1o" resolve="performative" />
                              </node>
                            </node>
                            <node concept="21noJN" id="2jzj96Zwl2m" role="2OqNvi">
                              <node concept="21nZrQ" id="2jzj96Zwl2n" role="21noJM">
                                <ref role="21nZrZ" to="os74:4MI7ZB$IHgo" resolve="RequestWhenever" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="2OqwBi" id="3uk$EjT4c3B" role="3clFbw">
                <node concept="1mIQ4w" id="3uk$EjT4c3C" role="2OqNvi">
                  <node concept="chp4Y" id="3uk$EjT4c3D" role="cj9EA">
                    <ref role="cht4Q" to="yv47:7D7uZV2dYz2" resolve="RecordType" />
                  </node>
                </node>
                <node concept="37vLTw" id="3uk$EjT4c3E" role="2Oq$k0">
                  <ref role="3cqZAo" node="3uk$EjT4c2C" resolve="rec" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="7OgJtfu9QN" role="1YuTPh">
      <property role="TrG5h" value="replyToMessage" />
      <ref role="1YaFvo" to="hk02:7OgJtfqDiI" resolve="ReplyToMessage" />
    </node>
  </node>
  <node concept="18kY7G" id="4hvwZTOd07o">
    <property role="TrG5h" value="check_SenderMatch" />
    <property role="3GE5qa" value="matches" />
    <node concept="3clFbS" id="4hvwZTOd07p" role="18ibNy">
      <node concept="3clFbJ" id="4hvwZTOd07v" role="3cqZAp">
        <node concept="3fqX7Q" id="4hvwZTOd2d5" role="3clFbw">
          <node concept="2OqwBi" id="4hvwZTOd2d7" role="3fr31v">
            <node concept="2OqwBi" id="4hvwZTOd2d8" role="2Oq$k0">
              <node concept="2OqwBi" id="4hvwZTOd2d9" role="2Oq$k0">
                <node concept="1YBJjd" id="4hvwZTOd2da" role="2Oq$k0">
                  <ref role="1YBMHb" node="4hvwZTOd07r" resolve="senderMatch" />
                </node>
                <node concept="3TrEf2" id="4hvwZTOd2db" role="2OqNvi">
                  <ref role="3Tt5mk" to="hk02:7AVCbhhTzuX" resolve="value" />
                </node>
              </node>
              <node concept="3JvlWi" id="4hvwZTOd2dc" role="2OqNvi" />
            </node>
            <node concept="1mIQ4w" id="4hvwZTOd2dd" role="2OqNvi">
              <node concept="chp4Y" id="4hvwZTOd2de" role="cj9EA">
                <ref role="cht4Q" to="os74:3JB$QJ8oGPX" resolve="AIDType" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbS" id="4hvwZTOd07x" role="3clFbx">
          <node concept="2MkqsV" id="4hvwZTOd2ef" role="3cqZAp">
            <node concept="Xl_RD" id="4hvwZTOd2er" role="2MkJ7o">
              <property role="Xl_RC" value="An expression of type AID is required" />
            </node>
            <node concept="2OqwBi" id="4hvwZTOd2r9" role="1urrMF">
              <node concept="1YBJjd" id="4hvwZTOd2eA" role="2Oq$k0">
                <ref role="1YBMHb" node="4hvwZTOd07r" resolve="senderMatch" />
              </node>
              <node concept="3TrEf2" id="4hvwZTOd2PY" role="2OqNvi">
                <ref role="3Tt5mk" to="hk02:7AVCbhhTzuX" resolve="value" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="4hvwZTOd07r" role="1YuTPh">
      <property role="TrG5h" value="senderMatch" />
      <ref role="1YaFvo" to="hk02:7AVCbhhTzuj" resolve="SenderMatch" />
    </node>
  </node>
  <node concept="18kY7G" id="4hvwZTOTLyK">
    <property role="TrG5h" value="check_ComposeMessage" />
    <property role="3GE5qa" value="statements" />
    <node concept="3clFbS" id="4hvwZTOTLyL" role="18ibNy">
      <node concept="3clFbJ" id="4hvwZTP08OQ" role="3cqZAp">
        <node concept="3clFbS" id="4hvwZTP08OS" role="3clFbx">
          <node concept="3clFbJ" id="4hvwZTOTLyR" role="3cqZAp">
            <node concept="1Wc70l" id="4hvwZTOTVdq" role="3clFbw">
              <node concept="3fqX7Q" id="4hvwZTOTUAM" role="3uHU7B">
                <node concept="2OqwBi" id="4hvwZTOTUAO" role="3fr31v">
                  <node concept="2OqwBi" id="4hvwZTOTUAP" role="2Oq$k0">
                    <node concept="1PxgMI" id="4hvwZTOTUAQ" role="2Oq$k0">
                      <node concept="chp4Y" id="4hvwZTOTUAR" role="3oSUPX">
                        <ref role="cht4Q" to="yv47:7D7uZV2dYz2" resolve="RecordType" />
                      </node>
                      <node concept="2OqwBi" id="4hvwZTOTUAS" role="1m5AlR">
                        <node concept="2OqwBi" id="4hvwZTOTUAT" role="2Oq$k0">
                          <node concept="1YBJjd" id="4hvwZTOTUAU" role="2Oq$k0">
                            <ref role="1YBMHb" node="4hvwZTOTLyN" resolve="composeMsg" />
                          </node>
                          <node concept="3TrEf2" id="4hvwZTOTUAV" role="2OqNvi">
                            <ref role="3Tt5mk" to="hk02:7OgJtfmIVA" resolve="type" />
                          </node>
                        </node>
                        <node concept="3TrEf2" id="4hvwZTOTUAW" role="2OqNvi">
                          <ref role="3Tt5mk" to="hk02:2lACGaVDxt_" resolve="baseType" />
                        </node>
                      </node>
                    </node>
                    <node concept="3TrEf2" id="4hvwZTOTUAX" role="2OqNvi">
                      <ref role="3Tt5mk" to="yv47:7D7uZV2dYz3" resolve="record" />
                    </node>
                  </node>
                  <node concept="1mIQ4w" id="4hvwZTOTUAY" role="2OqNvi">
                    <node concept="chp4Y" id="4hvwZTOTUAZ" role="cj9EA">
                      <ref role="cht4Q" to="3jzb:7dT_6n1a0fs" resolve="AgentActionSchema" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3fqX7Q" id="4hvwZTOTVjh" role="3uHU7w">
                <node concept="2OqwBi" id="4hvwZTOTVji" role="3fr31v">
                  <node concept="2OqwBi" id="4hvwZTOTVjj" role="2Oq$k0">
                    <node concept="1PxgMI" id="4hvwZTOTVjk" role="2Oq$k0">
                      <node concept="chp4Y" id="4hvwZTOTVjl" role="3oSUPX">
                        <ref role="cht4Q" to="yv47:7D7uZV2dYz2" resolve="RecordType" />
                      </node>
                      <node concept="2OqwBi" id="4hvwZTOTVjm" role="1m5AlR">
                        <node concept="3TrEf2" id="4hvwZTOTVjq" role="2OqNvi">
                          <ref role="3Tt5mk" to="hk02:2lACGaVDxt_" resolve="baseType" />
                        </node>
                        <node concept="2OqwBi" id="4hvwZTOTVjn" role="2Oq$k0">
                          <node concept="1YBJjd" id="4hvwZTOTVjo" role="2Oq$k0">
                            <ref role="1YBMHb" node="4hvwZTOTLyN" resolve="composeMsg" />
                          </node>
                          <node concept="3TrEf2" id="4hvwZTOTVjp" role="2OqNvi">
                            <ref role="3Tt5mk" to="hk02:7OgJtfmIVA" resolve="type" />
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="3TrEf2" id="4hvwZTOTVjr" role="2OqNvi">
                      <ref role="3Tt5mk" to="yv47:7D7uZV2dYz3" resolve="record" />
                    </node>
                  </node>
                  <node concept="1mIQ4w" id="4hvwZTOTVjs" role="2OqNvi">
                    <node concept="chp4Y" id="4hvwZTOTVjt" role="cj9EA">
                      <ref role="cht4Q" to="3jzb:7dT_6n1a0fu" resolve="PredicateSchema" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbS" id="4hvwZTOTLyT" role="3clFbx">
              <node concept="2MkqsV" id="4hvwZTOTS9J" role="3cqZAp">
                <node concept="Xl_RD" id="4hvwZTOTS9V" role="2MkJ7o">
                  <property role="Xl_RC" value="Only elements with schema Predicate or AgentAction can be sent" />
                </node>
                <node concept="2OqwBi" id="4hvwZTOTT4f" role="1urrMF">
                  <node concept="2OqwBi" id="4hvwZTOTSpQ" role="2Oq$k0">
                    <node concept="1YBJjd" id="4hvwZTOTSa6" role="2Oq$k0">
                      <ref role="1YBMHb" node="4hvwZTOTLyN" resolve="composeMsg" />
                    </node>
                    <node concept="3TrEf2" id="4hvwZTOTSR5" role="2OqNvi">
                      <ref role="3Tt5mk" to="hk02:7OgJtfmIVA" resolve="type" />
                    </node>
                  </node>
                  <node concept="3TrEf2" id="4hvwZTOTTop" role="2OqNvi">
                    <ref role="3Tt5mk" to="hk02:2lACGaVDxt_" resolve="baseType" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2OqwBi" id="4hvwZTP0aWg" role="3clFbw">
          <node concept="2OqwBi" id="4hvwZTP0a5q" role="2Oq$k0">
            <node concept="2OqwBi" id="4hvwZTP097m" role="2Oq$k0">
              <node concept="1YBJjd" id="4hvwZTP08PD" role="2Oq$k0">
                <ref role="1YBMHb" node="4hvwZTOTLyN" resolve="composeMsg" />
              </node>
              <node concept="3TrEf2" id="4hvwZTP09PG" role="2OqNvi">
                <ref role="3Tt5mk" to="hk02:7OgJtfmIVA" resolve="type" />
              </node>
            </node>
            <node concept="3TrEf2" id="4hvwZTP0aHp" role="2OqNvi">
              <ref role="3Tt5mk" to="hk02:2lACGaVDxt_" resolve="baseType" />
            </node>
          </node>
          <node concept="1mIQ4w" id="4hvwZTP0bB6" role="2OqNvi">
            <node concept="chp4Y" id="4hvwZTP0bGo" role="cj9EA">
              <ref role="cht4Q" to="yv47:7D7uZV2dYz2" resolve="RecordType" />
            </node>
          </node>
        </node>
        <node concept="9aQIb" id="4hvwZTP0bQy" role="9aQIa">
          <node concept="3clFbS" id="4hvwZTP0bQz" role="9aQI4">
            <node concept="2MkqsV" id="4hvwZTP0bTo" role="3cqZAp">
              <node concept="Xl_RD" id="4hvwZTP0bTp" role="2MkJ7o">
                <property role="Xl_RC" value="Only elements with schema Predicate or AgentAction can be sent" />
              </node>
              <node concept="2OqwBi" id="4hvwZTP0bTq" role="1urrMF">
                <node concept="2OqwBi" id="4hvwZTP0bTr" role="2Oq$k0">
                  <node concept="1YBJjd" id="4hvwZTP0bTs" role="2Oq$k0">
                    <ref role="1YBMHb" node="4hvwZTOTLyN" resolve="composeMsg" />
                  </node>
                  <node concept="3TrEf2" id="4hvwZTP0bTt" role="2OqNvi">
                    <ref role="3Tt5mk" to="hk02:7OgJtfmIVA" resolve="type" />
                  </node>
                </node>
                <node concept="3TrEf2" id="4hvwZTP0bTu" role="2OqNvi">
                  <ref role="3Tt5mk" to="hk02:2lACGaVDxt_" resolve="baseType" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="4hvwZTOTLyN" role="1YuTPh">
      <property role="TrG5h" value="composeMsg" />
      <ref role="1YaFvo" to="hk02:3uK1rwojlW1" resolve="ComposeMessage" />
    </node>
  </node>
  <node concept="18kY7G" id="6$6nD_DltGO">
    <property role="TrG5h" value="check_ReceiveMessage" />
    <property role="3GE5qa" value="statements" />
    <node concept="3clFbS" id="6$6nD_DltGP" role="18ibNy">
      <node concept="3clFbJ" id="22Cof6V9530" role="3cqZAp">
        <node concept="2OqwBi" id="22Cof6V99E$" role="3clFbw">
          <node concept="2OqwBi" id="22Cof6V95Zt" role="2Oq$k0">
            <node concept="2OqwBi" id="22Cof6V95kT" role="2Oq$k0">
              <node concept="1YBJjd" id="22Cof6V953c" role="2Oq$k0">
                <ref role="1YBMHb" node="6$6nD_DltGR" resolve="receiveMessage" />
              </node>
              <node concept="3TrEf2" id="22Cof6V95JK" role="2OqNvi">
                <ref role="3Tt5mk" to="hk02:6$6nD_DltfC" resolve="condition" />
              </node>
            </node>
            <node concept="2Rf3mk" id="22Cof6V96Qd" role="2OqNvi">
              <node concept="1xMEDy" id="22Cof6V96Qf" role="1xVPHs">
                <node concept="chp4Y" id="22Cof6V978k" role="ri$Ld">
                  <ref role="cht4Q" to="hk02:4i_Vsgbvgoq" resolve="ReceivedMessageArgRef" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3GX2aA" id="22Cof6V9iU4" role="2OqNvi" />
        </node>
        <node concept="3clFbS" id="22Cof6V9532" role="3clFbx">
          <node concept="2MkqsV" id="22Cof6V9iY$" role="3cqZAp">
            <node concept="Xl_RD" id="22Cof6V9iYE" role="2MkJ7o">
              <property role="Xl_RC" value="A reference to the received message is not available in this context" />
            </node>
            <node concept="2OqwBi" id="22Cof6V9mvK" role="1urrMF">
              <node concept="2OqwBi" id="22Cof6V9jY_" role="2Oq$k0">
                <node concept="2OqwBi" id="22Cof6V9jgY" role="2Oq$k0">
                  <node concept="1YBJjd" id="22Cof6V9j1V" role="2Oq$k0">
                    <ref role="1YBMHb" node="6$6nD_DltGR" resolve="receiveMessage" />
                  </node>
                  <node concept="3TrEf2" id="22Cof6V9jMI" role="2OqNvi">
                    <ref role="3Tt5mk" to="hk02:6$6nD_DltfC" resolve="condition" />
                  </node>
                </node>
                <node concept="2Rf3mk" id="22Cof6V9k07" role="2OqNvi">
                  <node concept="1xMEDy" id="22Cof6V9k08" role="1xVPHs">
                    <node concept="chp4Y" id="22Cof6V9k09" role="ri$Ld">
                      <ref role="cht4Q" to="hk02:4i_Vsgbvgoq" resolve="ReceivedMessageArgRef" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="1uHKPH" id="22Cof6V9q99" role="2OqNvi" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3clFbH" id="7DVIKqRyVKm" role="3cqZAp" />
      <node concept="3clFbJ" id="7DVIKqRyVL8" role="3cqZAp">
        <node concept="3clFbS" id="7DVIKqRyVLa" role="3clFbx">
          <node concept="2MkqsV" id="7DVIKqR_8VV" role="3cqZAp">
            <node concept="Xl_RD" id="7DVIKqR_8Wa" role="2MkJ7o">
              <property role="Xl_RC" value="The type of content that is expected must be specified" />
            </node>
            <node concept="2OqwBi" id="7DVIKqR_9jl" role="1urrMF">
              <node concept="2OqwBi" id="7DVIKqR_91E" role="2Oq$k0">
                <node concept="1YBJjd" id="7DVIKqR_8Y1" role="2Oq$k0">
                  <ref role="1YBMHb" node="6$6nD_DltGR" resolve="receiveMessage" />
                </node>
                <node concept="3TrEf2" id="7DVIKqR_96w" role="2OqNvi">
                  <ref role="3Tt5mk" to="hk02:4qBaryPhIZI" resolve="arg" />
                </node>
              </node>
              <node concept="3TrEf2" id="7DVIKqR_a70" role="2OqNvi">
                <ref role="3Tt5mk" to="hk02:6OKQOIXfaDc" resolve="type" />
              </node>
            </node>
          </node>
        </node>
        <node concept="2OqwBi" id="7DVIKqR_7_0" role="3clFbw">
          <node concept="2OqwBi" id="7DVIKqR_8qo" role="2Oq$k0">
            <node concept="2OqwBi" id="7DVIKqR_722" role="2Oq$k0">
              <node concept="2OqwBi" id="7DVIKqR_5AX" role="2Oq$k0">
                <node concept="1YBJjd" id="7DVIKqRyVLI" role="2Oq$k0">
                  <ref role="1YBMHb" node="6$6nD_DltGR" resolve="receiveMessage" />
                </node>
                <node concept="3TrEf2" id="7DVIKqR_6vs" role="2OqNvi">
                  <ref role="3Tt5mk" to="hk02:4qBaryPhIZI" resolve="arg" />
                </node>
              </node>
              <node concept="3TrEf2" id="7DVIKqR_7kU" role="2OqNvi">
                <ref role="3Tt5mk" to="hk02:6OKQOIXfaDc" resolve="type" />
              </node>
            </node>
            <node concept="3TrEf2" id="7DVIKqR_8P2" role="2OqNvi">
              <ref role="3Tt5mk" to="hk02:2lACGaVDxt_" resolve="baseType" />
            </node>
          </node>
          <node concept="3w_OXm" id="7DVIKqR_7Ta" role="2OqNvi" />
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="6$6nD_DltGR" role="1YuTPh">
      <property role="TrG5h" value="receiveMessage" />
      <ref role="1YaFvo" to="hk02:7HOFHncqzmZ" resolve="ReceiveMessage" />
    </node>
  </node>
  <node concept="1YbPZF" id="4iLN_rFTHdt">
    <property role="TrG5h" value="typeof_SendMessage" />
    <property role="3GE5qa" value="statements" />
    <node concept="3clFbS" id="4iLN_rFTHdu" role="18ibNy">
      <node concept="1Z5TYs" id="2jzj96ZwZWg" role="3cqZAp">
        <node concept="mw_s8" id="2jzj96ZwZWh" role="1ZfhK$">
          <node concept="1Z2H0r" id="2jzj96ZwZWi" role="mwGJk">
            <node concept="1YBJjd" id="2jzj96ZwZWj" role="1Z2MuG">
              <ref role="1YBMHb" node="4iLN_rFTHdw" resolve="sendMessage" />
            </node>
          </node>
        </node>
        <node concept="mw_s8" id="2jzj96ZwZWk" role="1ZfhKB">
          <node concept="2pJPEk" id="2jzj96ZEFLf" role="mwGJk">
            <node concept="2pJPED" id="2jzj96ZEFLh" role="2pJPEn">
              <ref role="2pJxaS" to="os74:80jriwjIN" resolve="UnitType" />
            </node>
          </node>
        </node>
      </node>
      <node concept="1Z5TYs" id="2jzj96ZwZWu" role="3cqZAp">
        <node concept="mw_s8" id="2jzj96ZwZWv" role="1ZfhKB">
          <node concept="2pJPEk" id="2jzj96ZwZWw" role="mwGJk">
            <node concept="2pJPED" id="2jzj96ZwZWx" role="2pJPEn">
              <ref role="2pJxaS" to="os74:3JB$QJ8oGPX" resolve="AIDType" />
            </node>
          </node>
        </node>
        <node concept="mw_s8" id="2jzj96ZwZWy" role="1ZfhK$">
          <node concept="1Z2H0r" id="2jzj96ZwZWz" role="mwGJk">
            <node concept="2OqwBi" id="2jzj96ZwZW$" role="1Z2MuG">
              <node concept="1YBJjd" id="2jzj96ZwZW_" role="2Oq$k0">
                <ref role="1YBMHb" node="4iLN_rFTHdw" resolve="sendMessage" />
              </node>
              <node concept="3TrEf2" id="2jzj96ZwZWA" role="2OqNvi">
                <ref role="3Tt5mk" to="hk02:2jzj96ZwVqO" resolve="receiver" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="1Z5TYs" id="2jzj96ZwZWB" role="3cqZAp">
        <node concept="mw_s8" id="2jzj96ZwZWC" role="1ZfhKB">
          <node concept="2pJPEk" id="2jzj96ZwZWD" role="mwGJk">
            <node concept="2pJPED" id="2jzj96ZwZWE" role="2pJPEn">
              <ref role="2pJxaS" to="700h:6zmBjqUinsw" resolve="ListType" />
              <node concept="2pIpSj" id="2jzj96ZwZWF" role="2pJxcM">
                <ref role="2pIpSl" to="700h:6zmBjqUily6" resolve="baseType" />
                <node concept="2pJPED" id="2jzj96ZwZWG" role="28nt2d">
                  <ref role="2pJxaS" to="os74:3JB$QJ8oGPX" resolve="AIDType" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="mw_s8" id="2jzj96ZwZWH" role="1ZfhK$">
          <node concept="1Z2H0r" id="2jzj96ZwZWI" role="mwGJk">
            <node concept="2OqwBi" id="2jzj96ZwZWJ" role="1Z2MuG">
              <node concept="1YBJjd" id="2jzj96ZwZWK" role="2Oq$k0">
                <ref role="1YBMHb" node="4iLN_rFTHdw" resolve="sendMessage" />
              </node>
              <node concept="3TrEf2" id="2jzj96ZwZWL" role="2OqNvi">
                <ref role="3Tt5mk" to="hk02:2jzj96ZwVqP" resolve="otherReceivers" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3clFbH" id="2jzj96Zx0bd" role="3cqZAp" />
      <node concept="3cpWs8" id="2jzj96Zx0cm" role="3cqZAp">
        <node concept="3cpWsn" id="2jzj96Zx0cn" role="3cpWs9">
          <property role="TrG5h" value="msgType" />
          <node concept="3Tqbb2" id="2jzj96Zx0co" role="1tU5fm" />
          <node concept="2OqwBi" id="2jzj96Zx0cp" role="33vP2m">
            <node concept="2OqwBi" id="2jzj96Zx0cq" role="2Oq$k0">
              <node concept="1YBJjd" id="2jzj96Zx0cr" role="2Oq$k0">
                <ref role="1YBMHb" node="4iLN_rFTHdw" resolve="sendMessage" />
              </node>
              <node concept="3TrEf2" id="2jzj96Zx0cs" role="2OqNvi">
                <ref role="3Tt5mk" to="hk02:2jzj96ZwVqQ" resolve="type" />
              </node>
            </node>
            <node concept="3TrEf2" id="2jzj96Zx0ct" role="2OqNvi">
              <ref role="3Tt5mk" to="hk02:2lACGaVDxt_" resolve="baseType" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3clFbJ" id="2jzj96Zx0cu" role="3cqZAp">
        <node concept="3clFbS" id="2jzj96Zx0cv" role="3clFbx">
          <node concept="1Z5TYs" id="2jzj96Zx0cw" role="3cqZAp">
            <node concept="mw_s8" id="2jzj96Zx0cx" role="1ZfhKB">
              <node concept="2pJPEk" id="2jzj96Zx0cy" role="mwGJk">
                <node concept="2pJPED" id="2jzj96Zx0cz" role="2pJPEn">
                  <ref role="2pJxaS" to="yv47:7D7uZV2dYz2" resolve="RecordType" />
                  <node concept="2pIpSj" id="2jzj96Zx0c$" role="2pJxcM">
                    <ref role="2pIpSl" to="yv47:7D7uZV2dYz3" resolve="record" />
                    <node concept="36biLy" id="2jzj96Zx0c_" role="28nt2d">
                      <node concept="2OqwBi" id="2jzj96Zx0cA" role="36biLW">
                        <node concept="1PxgMI" id="2jzj96Zx0cB" role="2Oq$k0">
                          <node concept="chp4Y" id="2jzj96Zx0cC" role="3oSUPX">
                            <ref role="cht4Q" to="yv47:7D7uZV2dYz2" resolve="RecordType" />
                          </node>
                          <node concept="37vLTw" id="2jzj96Zx0cD" role="1m5AlR">
                            <ref role="3cqZAo" node="2jzj96Zx0cn" resolve="msgType" />
                          </node>
                        </node>
                        <node concept="3TrEf2" id="2jzj96Zx0cE" role="2OqNvi">
                          <ref role="3Tt5mk" to="yv47:7D7uZV2dYz3" resolve="record" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="mw_s8" id="2jzj96Zx0cF" role="1ZfhK$">
              <node concept="1Z2H0r" id="2jzj96Zx0cG" role="mwGJk">
                <node concept="2OqwBi" id="2jzj96Zx0cH" role="1Z2MuG">
                  <node concept="1YBJjd" id="2jzj96Zx0cI" role="2Oq$k0">
                    <ref role="1YBMHb" node="4iLN_rFTHdw" resolve="sendMessage" />
                  </node>
                  <node concept="3TrEf2" id="2jzj96Zx0cJ" role="2OqNvi">
                    <ref role="3Tt5mk" to="hk02:2jzj96ZwVqN" resolve="content" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2OqwBi" id="2jzj96Zx0cK" role="3clFbw">
          <node concept="1mIQ4w" id="2jzj96Zx0cL" role="2OqNvi">
            <node concept="chp4Y" id="2jzj96Zx0cM" role="cj9EA">
              <ref role="cht4Q" to="yv47:7D7uZV2dYz2" resolve="RecordType" />
            </node>
          </node>
          <node concept="37vLTw" id="2jzj96Zx0cN" role="2Oq$k0">
            <ref role="3cqZAo" node="2jzj96Zx0cn" resolve="msgType" />
          </node>
        </node>
      </node>
      <node concept="3clFbH" id="2jzj96Zx0bL" role="3cqZAp" />
    </node>
    <node concept="1YaCAy" id="4iLN_rFTHdw" role="1YuTPh">
      <property role="TrG5h" value="sendMessage" />
      <ref role="1YaFvo" to="hk02:4iLN_rFSf_V" resolve="SendMessage" />
    </node>
  </node>
  <node concept="18kY7G" id="4iLN_rFTI9F">
    <property role="TrG5h" value="check_SendMessage" />
    <property role="3GE5qa" value="statements" />
    <node concept="3clFbS" id="4iLN_rFTI9G" role="18ibNy">
      <node concept="3cpWs8" id="4TWmWFY8D3k" role="3cqZAp">
        <node concept="3cpWsn" id="4TWmWFY8D3n" role="3cpWs9">
          <property role="TrG5h" value="rec" />
          <node concept="3Tqbb2" id="4TWmWFY8D3i" role="1tU5fm" />
          <node concept="2OqwBi" id="4TWmWFY8D51" role="33vP2m">
            <node concept="2OqwBi" id="4TWmWFY8D52" role="2Oq$k0">
              <node concept="1YBJjd" id="4TWmWFY8D53" role="2Oq$k0">
                <ref role="1YBMHb" node="4iLN_rFTI9I" resolve="sendMessage" />
              </node>
              <node concept="3TrEf2" id="4TWmWFY8D54" role="2OqNvi">
                <ref role="3Tt5mk" to="hk02:2jzj96ZwVqQ" resolve="type" />
              </node>
            </node>
            <node concept="3TrEf2" id="4TWmWFY8D55" role="2OqNvi">
              <ref role="3Tt5mk" to="hk02:2lACGaVDxt_" resolve="baseType" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3clFbJ" id="2jzj96ZwXfU" role="3cqZAp">
        <node concept="3clFbS" id="2jzj96ZwXfV" role="3clFbx">
          <node concept="3clFbJ" id="2jzj96ZwXfW" role="3cqZAp">
            <node concept="1Wc70l" id="2jzj96ZwXfX" role="3clFbw">
              <node concept="3fqX7Q" id="2jzj96ZwXfY" role="3uHU7B">
                <node concept="2OqwBi" id="2jzj96ZwXfZ" role="3fr31v">
                  <node concept="2OqwBi" id="2jzj96ZwXg0" role="2Oq$k0">
                    <node concept="1PxgMI" id="2jzj96ZwXg1" role="2Oq$k0">
                      <node concept="chp4Y" id="2jzj96ZwXg2" role="3oSUPX">
                        <ref role="cht4Q" to="yv47:7D7uZV2dYz2" resolve="RecordType" />
                      </node>
                      <node concept="37vLTw" id="4TWmWFY8DH0" role="1m5AlR">
                        <ref role="3cqZAo" node="4TWmWFY8D3n" resolve="rec" />
                      </node>
                    </node>
                    <node concept="3TrEf2" id="2jzj96ZwXg8" role="2OqNvi">
                      <ref role="3Tt5mk" to="yv47:7D7uZV2dYz3" resolve="record" />
                    </node>
                  </node>
                  <node concept="1mIQ4w" id="2jzj96ZwXg9" role="2OqNvi">
                    <node concept="chp4Y" id="2jzj96ZwXga" role="cj9EA">
                      <ref role="cht4Q" to="3jzb:7dT_6n1a0fs" resolve="AgentActionSchema" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3fqX7Q" id="2jzj96ZwXgb" role="3uHU7w">
                <node concept="2OqwBi" id="2jzj96ZwXgc" role="3fr31v">
                  <node concept="2OqwBi" id="2jzj96ZwXgd" role="2Oq$k0">
                    <node concept="1PxgMI" id="2jzj96ZwXge" role="2Oq$k0">
                      <node concept="chp4Y" id="2jzj96ZwXgf" role="3oSUPX">
                        <ref role="cht4Q" to="yv47:7D7uZV2dYz2" resolve="RecordType" />
                      </node>
                      <node concept="37vLTw" id="4TWmWFY8DQS" role="1m5AlR">
                        <ref role="3cqZAo" node="4TWmWFY8D3n" resolve="rec" />
                      </node>
                    </node>
                    <node concept="3TrEf2" id="2jzj96ZwXgl" role="2OqNvi">
                      <ref role="3Tt5mk" to="yv47:7D7uZV2dYz3" resolve="record" />
                    </node>
                  </node>
                  <node concept="1mIQ4w" id="2jzj96ZwXgm" role="2OqNvi">
                    <node concept="chp4Y" id="2jzj96ZwXgn" role="cj9EA">
                      <ref role="cht4Q" to="3jzb:7dT_6n1a0fu" resolve="PredicateSchema" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbS" id="2jzj96ZwXgo" role="3clFbx">
              <node concept="2MkqsV" id="2jzj96ZwXgp" role="3cqZAp">
                <node concept="Xl_RD" id="2jzj96ZwXgq" role="2MkJ7o">
                  <property role="Xl_RC" value="The content of the message must be a Predicate or AgentAction" />
                </node>
                <node concept="37vLTw" id="4TWmWFY8DVf" role="1urrMF">
                  <ref role="3cqZAo" node="4TWmWFY8D3n" resolve="rec" />
                </node>
              </node>
            </node>
            <node concept="9aQIb" id="4TWmWFXYNqQ" role="9aQIa">
              <node concept="3clFbS" id="4TWmWFXYNqR" role="9aQI4">
                <node concept="3clFbJ" id="4TWmWFY8Hz9" role="3cqZAp">
                  <node concept="3clFbS" id="4TWmWFY8Hzb" role="3clFbx">
                    <node concept="3clFbJ" id="3uk$EjT2SpI" role="3cqZAp">
                      <node concept="3clFbS" id="3uk$EjT2SpK" role="3clFbx">
                        <node concept="2MkqsV" id="3uk$EjT2WbH" role="3cqZAp">
                          <node concept="3cpWs3" id="3uk$EjT33gn" role="2MkJ7o">
                            <node concept="Xl_RD" id="3uk$EjT33iM" role="3uHU7w">
                              <property role="Xl_RC" value=" is not compliant with the FIPA standard" />
                            </node>
                            <node concept="3cpWs3" id="3uk$EjT2YbF" role="3uHU7B">
                              <node concept="Xl_RD" id="3uk$EjT2ZCS" role="3uHU7B">
                                <property role="Xl_RC" value="A message with content of type action that has the performative " />
                              </node>
                              <node concept="2OqwBi" id="3uk$EjT2YIY" role="3uHU7w">
                                <node concept="1YBJjd" id="3uk$EjT2YlH" role="2Oq$k0">
                                  <ref role="1YBMHb" node="4iLN_rFTI9I" resolve="sendMessage" />
                                </node>
                                <node concept="3TrcHB" id="3uk$EjT2Zo6" role="2OqNvi">
                                  <ref role="3TsBF5" to="os74:Oow4MYDA1o" resolve="performative" />
                                </node>
                              </node>
                            </node>
                          </node>
                          <node concept="1YBJjd" id="3uk$EjT2Wc7" role="1urrMF">
                            <ref role="1YBMHb" node="4iLN_rFTI9I" resolve="sendMessage" />
                          </node>
                        </node>
                      </node>
                      <node concept="22lmx$" id="3uk$EjT2VN4" role="3clFbw">
                        <node concept="22lmx$" id="3uk$EjT2V$M" role="3uHU7B">
                          <node concept="2OqwBi" id="3uk$EjT2TwX" role="3uHU7B">
                            <node concept="2OqwBi" id="3uk$EjT2SGU" role="2Oq$k0">
                              <node concept="1YBJjd" id="3uk$EjT2Sqd" role="2Oq$k0">
                                <ref role="1YBMHb" node="4iLN_rFTI9I" resolve="sendMessage" />
                              </node>
                              <node concept="3TrcHB" id="3uk$EjT2TdK" role="2OqNvi">
                                <ref role="3TsBF5" to="os74:Oow4MYDA1o" resolve="performative" />
                              </node>
                            </node>
                            <node concept="21noJN" id="3uk$EjT2TD6" role="2OqNvi">
                              <node concept="21nZrQ" id="3uk$EjT2TD8" role="21noJM">
                                <ref role="21nZrZ" to="os74:4MI7ZB$IHdc" resolve="Inform" />
                              </node>
                            </node>
                          </node>
                          <node concept="2OqwBi" id="3uk$EjT2VE2" role="3uHU7w">
                            <node concept="2OqwBi" id="3uk$EjT2VE3" role="2Oq$k0">
                              <node concept="1YBJjd" id="3uk$EjT2VE4" role="2Oq$k0">
                                <ref role="1YBMHb" node="4iLN_rFTI9I" resolve="sendMessage" />
                              </node>
                              <node concept="3TrcHB" id="3uk$EjT2VE5" role="2OqNvi">
                                <ref role="3TsBF5" to="os74:Oow4MYDA1o" resolve="performative" />
                              </node>
                            </node>
                            <node concept="21noJN" id="3uk$EjT2VE6" role="2OqNvi">
                              <node concept="21nZrQ" id="3uk$EjT2VE7" role="21noJM">
                                <ref role="21nZrZ" to="os74:1MgVVihmORg" resolve="InformIf" />
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="2OqwBi" id="3uk$EjT2W1G" role="3uHU7w">
                          <node concept="2OqwBi" id="3uk$EjT2W1H" role="2Oq$k0">
                            <node concept="1YBJjd" id="3uk$EjT2W1I" role="2Oq$k0">
                              <ref role="1YBMHb" node="4iLN_rFTI9I" resolve="sendMessage" />
                            </node>
                            <node concept="3TrcHB" id="3uk$EjT2W1J" role="2OqNvi">
                              <ref role="3TsBF5" to="os74:Oow4MYDA1o" resolve="performative" />
                            </node>
                          </node>
                          <node concept="21noJN" id="3uk$EjT2W1K" role="2OqNvi">
                            <node concept="21nZrQ" id="3uk$EjT2W1L" role="21noJM">
                              <ref role="21nZrZ" to="os74:4MI7ZB$IHd$" resolve="InformRef" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="2OqwBi" id="3uk$EjT4c3n" role="3clFbw">
                    <node concept="2OqwBi" id="3uk$EjT4c3o" role="2Oq$k0">
                      <node concept="1PxgMI" id="3uk$EjT4c3p" role="2Oq$k0">
                        <node concept="chp4Y" id="3uk$EjT4c3q" role="3oSUPX">
                          <ref role="cht4Q" to="yv47:7D7uZV2dYz2" resolve="RecordType" />
                        </node>
                        <node concept="37vLTw" id="3uk$EjT4c3r" role="1m5AlR">
                          <ref role="3cqZAo" node="4TWmWFY8D3n" resolve="rec" />
                        </node>
                      </node>
                      <node concept="3TrEf2" id="3uk$EjT4c3s" role="2OqNvi">
                        <ref role="3Tt5mk" to="yv47:7D7uZV2dYz3" resolve="record" />
                      </node>
                    </node>
                    <node concept="1mIQ4w" id="3uk$EjT4c3t" role="2OqNvi">
                      <node concept="chp4Y" id="3uk$EjT4c3u" role="cj9EA">
                        <ref role="cht4Q" to="3jzb:7dT_6n1a0fs" resolve="AgentActionSchema" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbJ" id="3uk$EjT4c2P" role="3cqZAp">
                  <node concept="3clFbS" id="3uk$EjT4c2Q" role="3clFbx">
                    <node concept="3clFbJ" id="3uk$EjT4c2R" role="3cqZAp">
                      <node concept="3clFbS" id="3uk$EjT4c2S" role="3clFbx">
                        <node concept="2MkqsV" id="3uk$EjT4c2T" role="3cqZAp">
                          <node concept="3cpWs3" id="3uk$EjT4c2U" role="2MkJ7o">
                            <node concept="Xl_RD" id="3uk$EjT4c2V" role="3uHU7w">
                              <property role="Xl_RC" value=" is not compliant with the FIPA standard" />
                            </node>
                            <node concept="3cpWs3" id="3uk$EjT4c2W" role="3uHU7B">
                              <node concept="Xl_RD" id="3uk$EjT4c2X" role="3uHU7B">
                                <property role="Xl_RC" value="A message with content of type predicate that has the performative " />
                              </node>
                              <node concept="2OqwBi" id="3uk$EjT4c2Y" role="3uHU7w">
                                <node concept="1YBJjd" id="3uk$EjT4c2Z" role="2Oq$k0">
                                  <ref role="1YBMHb" node="4iLN_rFTI9I" resolve="sendMessage" />
                                </node>
                                <node concept="3TrcHB" id="3uk$EjT4c30" role="2OqNvi">
                                  <ref role="3TsBF5" to="os74:Oow4MYDA1o" resolve="performative" />
                                </node>
                              </node>
                            </node>
                          </node>
                          <node concept="1YBJjd" id="3uk$EjT4c31" role="1urrMF">
                            <ref role="1YBMHb" node="4iLN_rFTI9I" resolve="sendMessage" />
                          </node>
                        </node>
                      </node>
                      <node concept="22lmx$" id="3uk$EjT4c32" role="3clFbw">
                        <node concept="22lmx$" id="3uk$EjT4c33" role="3uHU7B">
                          <node concept="2OqwBi" id="3uk$EjT4c34" role="3uHU7B">
                            <node concept="2OqwBi" id="3uk$EjT4c35" role="2Oq$k0">
                              <node concept="1YBJjd" id="3uk$EjT4c36" role="2Oq$k0">
                                <ref role="1YBMHb" node="4iLN_rFTI9I" resolve="sendMessage" />
                              </node>
                              <node concept="3TrcHB" id="3uk$EjT4c37" role="2OqNvi">
                                <ref role="3TsBF5" to="os74:Oow4MYDA1o" resolve="performative" />
                              </node>
                            </node>
                            <node concept="21noJN" id="3uk$EjT4c38" role="2OqNvi">
                              <node concept="21nZrQ" id="3uk$EjT4c39" role="21noJM">
                                <ref role="21nZrZ" to="os74:4MI7ZB$IHfN" resolve="Request" />
                              </node>
                            </node>
                          </node>
                          <node concept="2OqwBi" id="3uk$EjT4c3a" role="3uHU7w">
                            <node concept="2OqwBi" id="3uk$EjT4c3b" role="2Oq$k0">
                              <node concept="3TrcHB" id="3uk$EjT4c3d" role="2OqNvi">
                                <ref role="3TsBF5" to="os74:Oow4MYDA1o" resolve="performative" />
                              </node>
                              <node concept="1YBJjd" id="4TWmWFY8ORP" role="2Oq$k0">
                                <ref role="1YBMHb" node="4iLN_rFTI9I" resolve="sendMessage" />
                              </node>
                            </node>
                            <node concept="21noJN" id="3uk$EjT4c3e" role="2OqNvi">
                              <node concept="21nZrQ" id="3uk$EjT4c3f" role="21noJM">
                                <ref role="21nZrZ" to="os74:4MI7ZB$IHg5" resolve="RequestWhen" />
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="2OqwBi" id="3uk$EjT4c3g" role="3uHU7w">
                          <node concept="2OqwBi" id="4TWmWFY8Q4F" role="2Oq$k0">
                            <node concept="1YBJjd" id="4TWmWFY8PMb" role="2Oq$k0">
                              <ref role="1YBMHb" node="4iLN_rFTI9I" resolve="sendMessage" />
                            </node>
                            <node concept="3TrcHB" id="4TWmWFY8QKj" role="2OqNvi">
                              <ref role="3TsBF5" to="os74:Oow4MYDA1o" resolve="performative" />
                            </node>
                          </node>
                          <node concept="21noJN" id="3uk$EjT4c3k" role="2OqNvi">
                            <node concept="21nZrQ" id="3uk$EjT4c3l" role="21noJM">
                              <ref role="21nZrZ" to="os74:4MI7ZB$IHgo" resolve="RequestWhenever" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="2OqwBi" id="3uk$EjT4c3v" role="3clFbw">
                    <node concept="2OqwBi" id="3uk$EjT4c3w" role="2Oq$k0">
                      <node concept="1PxgMI" id="3uk$EjT4c3x" role="2Oq$k0">
                        <node concept="chp4Y" id="3uk$EjT4c3y" role="3oSUPX">
                          <ref role="cht4Q" to="yv47:7D7uZV2dYz2" resolve="RecordType" />
                        </node>
                        <node concept="37vLTw" id="3uk$EjT4c3z" role="1m5AlR">
                          <ref role="3cqZAo" node="4TWmWFY8D3n" resolve="rec" />
                        </node>
                      </node>
                      <node concept="3TrEf2" id="3uk$EjT4c3$" role="2OqNvi">
                        <ref role="3Tt5mk" to="yv47:7D7uZV2dYz3" resolve="record" />
                      </node>
                    </node>
                    <node concept="1mIQ4w" id="4TWmWFY8H4X" role="2OqNvi">
                      <node concept="chp4Y" id="4TWmWFY8Hgj" role="cj9EA">
                        <ref role="cht4Q" to="3jzb:7dT_6n1a0fu" resolve="PredicateSchema" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2OqwBi" id="2jzj96ZwXgw" role="3clFbw">
          <node concept="37vLTw" id="4TWmWFY8DGx" role="2Oq$k0">
            <ref role="3cqZAo" node="4TWmWFY8D3n" resolve="rec" />
          </node>
          <node concept="1mIQ4w" id="2jzj96ZwXgA" role="2OqNvi">
            <node concept="chp4Y" id="2jzj96ZwXgB" role="cj9EA">
              <ref role="cht4Q" to="yv47:7D7uZV2dYz2" resolve="RecordType" />
            </node>
          </node>
        </node>
        <node concept="9aQIb" id="2jzj96ZwXgC" role="9aQIa">
          <node concept="3clFbS" id="2jzj96ZwXgD" role="9aQI4">
            <node concept="2MkqsV" id="2jzj96ZwXgE" role="3cqZAp">
              <node concept="Xl_RD" id="2jzj96ZwXgF" role="2MkJ7o">
                <property role="Xl_RC" value="Only elements with schema Predicate or AgentAction can be sent" />
              </node>
              <node concept="2OqwBi" id="4TWmWFXYDgL" role="1urrMF">
                <node concept="2OqwBi" id="2jzj96ZwXgH" role="2Oq$k0">
                  <node concept="1YBJjd" id="2jzj96ZwXgI" role="2Oq$k0">
                    <ref role="1YBMHb" node="4iLN_rFTI9I" resolve="sendMessage" />
                  </node>
                  <node concept="3TrEf2" id="2jzj96ZwXgJ" role="2OqNvi">
                    <ref role="3Tt5mk" to="hk02:2jzj96ZwVqQ" resolve="type" />
                  </node>
                </node>
                <node concept="3TrEf2" id="4TWmWFXYDC9" role="2OqNvi">
                  <ref role="3Tt5mk" to="hk02:2lACGaVDxt_" resolve="baseType" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="4iLN_rFTI9I" role="1YuTPh">
      <property role="TrG5h" value="sendMessage" />
      <ref role="1YaFvo" to="hk02:4iLN_rFSf_V" resolve="SendMessage" />
    </node>
  </node>
</model>

