<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:7c1264b7-fd03-4e6c-b708-6c839cbe4cda(MetaJade.aclmessage.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="9" />
    <devkit ref="78434eb8-b0e5-444b-850d-e7c4ad2da9ab(jetbrains.mps.devkit.aspect.structure)" />
  </languages>
  <imports>
    <import index="os74" ref="r:0a69036a-4d31-45e1-887d-acbe5b8039f5(MetaJade.core.structure)" />
    <import index="hm2y" ref="r:66e07cb4-a4b0-4bf3-a36d-5e9ed1ff1bd3(org.iets3.core.expr.base.structure)" />
    <import index="4kwy" ref="r:657c9fde-2f36-4e61-ae17-20f02b8630ad(org.iets3.core.base.structure)" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="982eb8df-2c96-4bd7-9963-11712ea622e5" name="jetbrains.mps.lang.resources">
      <concept id="8974276187400029883" name="jetbrains.mps.lang.resources.structure.FileIcon" flags="ng" index="1QGGSu">
        <property id="2756621024541341363" name="file" index="1iqoE4" />
      </concept>
    </language>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="6714410169261853888" name="conceptId" index="EcuMT" />
        <property id="4628067390765907488" name="conceptShortDescription" index="R4oN_" />
        <property id="4628067390765956802" name="abstract" index="R5$K7" />
        <property id="5092175715804935370" name="conceptAlias" index="34LRSv" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
      </concept>
      <concept id="1169125989551" name="jetbrains.mps.lang.structure.structure.InterfaceConceptDeclaration" flags="ig" index="PlHQZ">
        <child id="1169127546356" name="extends" index="PrDN$" />
      </concept>
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="6327362524875300597" name="icon" index="rwd14" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599893252" name="sourceCardinality" index="20lbJX" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <property id="241647608299431140" name="linkId" index="IQ2ns" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1TIwiD" id="7HOFHncqzmZ">
    <property role="EcuMT" value="8895927397244351935" />
    <property role="TrG5h" value="ReceiveMessage" />
    <property role="3GE5qa" value="statements" />
    <property role="34LRSv" value="on receive" />
    <property role="R4oN_" value="message handler" />
    <ref role="1TJDcQ" node="1wDV7BCtyf4" resolve="AbstractMessageExpression" />
    <node concept="1QGGSu" id="7dT_6n1dpAI" role="rwd14">
      <property role="1iqoE4" value="${module}/icons/message.png" />
    </node>
    <node concept="1TJgyj" id="4qBaryPhIZI" role="1TKVEi">
      <property role="IQ2ns" value="5091083777057681390" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="arg" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="6OKQOIXfaDa" resolve="ReceivedMessageArg" />
    </node>
    <node concept="1TJgyj" id="4qBaryPhIZF" role="1TKVEi">
      <property role="IQ2ns" value="5091083777057681387" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="onReceive" />
      <ref role="20lvS9" to="hm2y:6sdnDbSla17" resolve="Expression" />
    </node>
    <node concept="1TJgyj" id="6$6nD_DltfC" role="1TKVEi">
      <property role="IQ2ns" value="7567840236832936936" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="condition" />
      <ref role="20lvS9" node="vQ6nfZ2W1K" resolve="IMessageMatcher" />
    </node>
  </node>
  <node concept="1TIwiD" id="3uK1rwojlW1">
    <property role="EcuMT" value="4012713556226957057" />
    <property role="TrG5h" value="ComposeMessage" />
    <property role="3GE5qa" value="statements" />
    <property role="34LRSv" value="compose" />
    <property role="R4oN_" value="compose an ACL Message" />
    <ref role="1TJDcQ" node="1wDV7BCtyf4" resolve="AbstractMessageExpression" />
    <node concept="1QGGSu" id="7dT_6n1dpAK" role="rwd14">
      <property role="1iqoE4" value="${module}/icons/message.png" />
    </node>
    <node concept="1TJgyj" id="3I8nNvsTTJ8" role="1TKVEi">
      <property role="IQ2ns" value="4289783338604403656" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="content" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" to="hm2y:6sdnDbSla17" resolve="Expression" />
    </node>
    <node concept="1TJgyj" id="5zd1xVkp1As" role="1TKVEi">
      <property role="IQ2ns" value="6398777375045458332" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="receiver" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" to="hm2y:6sdnDbSla17" resolve="Expression" />
    </node>
    <node concept="1TJgyj" id="5zd1xVkp1FD" role="1TKVEi">
      <property role="IQ2ns" value="6398777375045458665" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="otherReceivers" />
      <ref role="20lvS9" to="hm2y:6sdnDbSla17" resolve="Expression" />
    </node>
    <node concept="1TJgyj" id="7OgJtfmIVA" role="1TKVEi">
      <property role="IQ2ns" value="140811118311042790" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="type" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="1$PYe999WiB" resolve="MessageType" />
    </node>
  </node>
  <node concept="1TIwiD" id="1wDV7BCtyf4">
    <property role="TrG5h" value="AbstractMessageExpression" />
    <property role="3GE5qa" value="statements" />
    <property role="EcuMT" value="8895927397246300198" />
    <property role="R5$K7" value="true" />
    <ref role="1TJDcQ" to="os74:1wDV7BCtyf3" resolve="AbstractExpression" />
  </node>
  <node concept="1TIwiD" id="4i_Vsgbvgoq">
    <property role="EcuMT" value="4946621191998473754" />
    <property role="3GE5qa" value="statements" />
    <property role="TrG5h" value="ReceivedMessageArgRef" />
    <property role="R4oN_" value="--" />
    <ref role="1TJDcQ" to="hm2y:6sdnDbSla17" resolve="Expression" />
    <node concept="1TJgyj" id="4i_VsgbvgoW" role="1TKVEi">
      <property role="IQ2ns" value="4946621191998473788" />
      <property role="20kJfa" value="arg" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="6OKQOIXfaDa" resolve="ReceivedMessageArg" />
    </node>
  </node>
  <node concept="1TIwiD" id="1$PYe999WiB">
    <property role="EcuMT" value="1816631675487503527" />
    <property role="TrG5h" value="MessageType" />
    <property role="34LRSv" value="message" />
    <ref role="1TJDcQ" to="hm2y:6sdnDbSlaok" resolve="Type" />
    <node concept="1TJgyj" id="2lACGaVDxt_" role="1TKVEi">
      <property role="IQ2ns" value="2695020384283400037" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="baseType" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" to="hm2y:6sdnDbSlaok" resolve="Type" />
    </node>
  </node>
  <node concept="PlHQZ" id="1$PYe99c8Ki">
    <property role="EcuMT" value="1816631675488078866" />
    <property role="3GE5qa" value="operations" />
    <property role="TrG5h" value="IMessageOp" />
    <node concept="PrWs8" id="1$PYe99c8XI" role="PrDN$">
      <ref role="PrY4T" to="hm2y:7NJy08a3O9a" resolve="IDotTarget" />
    </node>
  </node>
  <node concept="1TIwiD" id="6OKQOIXfaDa">
    <property role="EcuMT" value="4946621191997514448" />
    <property role="3GE5qa" value="statements" />
    <property role="TrG5h" value="ReceivedMessageArg" />
    <property role="R4oN_" value="--" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="6OKQOIXfaDb" role="PzmwI">
      <ref role="PrY4T" to="4kwy:cJpacq5T0O" resolve="IValidNamedConcept" />
    </node>
    <node concept="1TJgyj" id="6OKQOIXfaDc" role="1TKVEi">
      <property role="IQ2ns" value="2695020384284204564" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="type" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="1$PYe999WiB" resolve="MessageType" />
    </node>
  </node>
  <node concept="1TIwiD" id="7AVCbhhTnhf">
    <property role="EcuMT" value="8771781395562722383" />
    <property role="TrG5h" value="PerformativeMatch" />
    <property role="34LRSv" value="performative" />
    <property role="3GE5qa" value="matches" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="vQ6nfZ2W7_" role="PzmwI">
      <ref role="PrY4T" node="vQ6nfZ2W1K" resolve="IMessageMatcher" />
    </node>
    <node concept="PrWs8" id="Oow4MYDA1q" role="PzmwI">
      <ref role="PrY4T" to="os74:Oow4MYDA1t" resolve="IDefinePerformative" />
    </node>
  </node>
  <node concept="1TIwiD" id="7AVCbhhTzuj">
    <property role="EcuMT" value="8771781395562772371" />
    <property role="3GE5qa" value="matches" />
    <property role="TrG5h" value="SenderMatch" />
    <property role="34LRSv" value="sender" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="7AVCbhhTzuX" role="1TKVEi">
      <property role="IQ2ns" value="8771781395562772413" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="value" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" to="hm2y:6sdnDbSla17" resolve="Expression" />
    </node>
    <node concept="PrWs8" id="vQ6nfZ2W7x" role="PzmwI">
      <ref role="PrY4T" node="vQ6nfZ2W1K" resolve="IMessageMatcher" />
    </node>
  </node>
  <node concept="1TIwiD" id="7AVCbhhUcyW">
    <property role="EcuMT" value="8771781395562940604" />
    <property role="3GE5qa" value="matches" />
    <property role="TrG5h" value="ReceiverMatch" />
    <property role="34LRSv" value="receiver" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="7AVCbhhUcyX" role="1TKVEi">
      <property role="IQ2ns" value="8771781395562940605" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="values" />
      <property role="20lbJX" value="fLJekj6/_1__n" />
      <ref role="20lvS9" to="os74:6OKQOIXjSXU" resolve="AIDDeclaration" />
    </node>
    <node concept="PrWs8" id="vQ6nfZ2W7z" role="PzmwI">
      <ref role="PrY4T" node="vQ6nfZ2W1K" resolve="IMessageMatcher" />
    </node>
  </node>
  <node concept="1TIwiD" id="7AVCbhhYKKm">
    <property role="EcuMT" value="8771781395564137494" />
    <property role="3GE5qa" value="operations" />
    <property role="TrG5h" value="GetSenderOp" />
    <property role="34LRSv" value="sender" />
    <property role="R4oN_" value="get the sender of the message" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="7AVCbhhYKKn" role="PzmwI">
      <ref role="PrY4T" node="1$PYe99c8Ki" resolve="IMessageOp" />
    </node>
  </node>
  <node concept="1TIwiD" id="7AVCbhhYXBK">
    <property role="EcuMT" value="8771781395564190192" />
    <property role="3GE5qa" value="operations" />
    <property role="TrG5h" value="GetReceiversOp" />
    <property role="34LRSv" value="receivers" />
    <property role="R4oN_" value="get the receivers of the message" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="7AVCbhhYXBL" role="PzmwI">
      <ref role="PrY4T" node="1$PYe99c8Ki" resolve="IMessageOp" />
    </node>
  </node>
  <node concept="1TIwiD" id="7AVCbhi0iOL">
    <property role="EcuMT" value="8771781395564539185" />
    <property role="3GE5qa" value="operations" />
    <property role="TrG5h" value="GetContentOp" />
    <property role="34LRSv" value="content" />
    <property role="R4oN_" value="get the content of a message" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="7AVCbhi0iOM" role="PzmwI">
      <ref role="PrY4T" node="1$PYe99c8Ki" resolve="IMessageOp" />
    </node>
  </node>
  <node concept="PlHQZ" id="vQ6nfZ2W1K">
    <property role="EcuMT" value="573673988527210608" />
    <property role="3GE5qa" value="matches" />
    <property role="TrG5h" value="IMessageMatcher" />
  </node>
  <node concept="1TIwiD" id="7OgJtfqDiI">
    <property role="EcuMT" value="140811118312068270" />
    <property role="TrG5h" value="ReplyToMessage" />
    <property role="3GE5qa" value="statements" />
    <property role="34LRSv" value="reply" />
    <property role="R4oN_" value="reply to a message" />
    <ref role="1TJDcQ" node="1wDV7BCtyf4" resolve="AbstractMessageExpression" />
    <node concept="1QGGSu" id="7OgJtfqDiJ" role="rwd14">
      <property role="1iqoE4" value="${module}/icons/message.png" />
    </node>
    <node concept="1TJgyj" id="7OgJtfqDiK" role="1TKVEi">
      <property role="IQ2ns" value="140811118312068272" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="content" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" to="hm2y:6sdnDbSla17" resolve="Expression" />
    </node>
    <node concept="1TJgyj" id="7OgJtfqDiN" role="1TKVEi">
      <property role="IQ2ns" value="140811118312068275" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="type" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="1$PYe999WiB" resolve="MessageType" />
    </node>
    <node concept="1TJgyj" id="7OgJtfqDiO" role="1TKVEi">
      <property role="IQ2ns" value="140811118312068276" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="reply" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" to="hm2y:6sdnDbSla17" resolve="Expression" />
    </node>
    <node concept="PrWs8" id="Oow4MYDNAh" role="PzmwI">
      <ref role="PrY4T" to="os74:Oow4MYDA1t" resolve="IDefinePerformative" />
    </node>
  </node>
  <node concept="1TIwiD" id="7DVIKqR_Yux">
    <property role="EcuMT" value="8825853532287002529" />
    <property role="3GE5qa" value="matches" />
    <property role="TrG5h" value="AndMatcher" />
    <property role="34LRSv" value="and" />
    <property role="R4oN_" value="And Matcher" />
    <ref role="1TJDcQ" node="4iLN_rFIdP8" resolve="LogicalOperator" />
  </node>
  <node concept="1TIwiD" id="4iLN_rFIdP8">
    <property role="EcuMT" value="4949964338174156104" />
    <property role="3GE5qa" value="matches" />
    <property role="TrG5h" value="LogicalOperator" />
    <property role="R5$K7" value="true" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="7GvO64iUcM9" role="1TKVEi">
      <property role="IQ2ns" value="8872038906290818185" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="left" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="vQ6nfZ2W1K" resolve="IMessageMatcher" />
    </node>
    <node concept="1TJgyj" id="7GvO64iUcMb" role="1TKVEi">
      <property role="IQ2ns" value="8872038906290818187" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="right" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="vQ6nfZ2W1K" resolve="IMessageMatcher" />
    </node>
    <node concept="PrWs8" id="4iLN_rFIdP9" role="PzmwI">
      <ref role="PrY4T" node="vQ6nfZ2W1K" resolve="IMessageMatcher" />
    </node>
  </node>
  <node concept="1TIwiD" id="4iLN_rFIdPd">
    <property role="EcuMT" value="4949964338174156109" />
    <property role="3GE5qa" value="matches" />
    <property role="TrG5h" value="OrMatcher" />
    <property role="34LRSv" value="or" />
    <property role="R4oN_" value="Or Matcher" />
    <ref role="1TJDcQ" node="4iLN_rFIdP8" resolve="LogicalOperator" />
  </node>
  <node concept="1TIwiD" id="4iLN_rFIdPg">
    <property role="EcuMT" value="4949964338174156112" />
    <property role="3GE5qa" value="matches" />
    <property role="TrG5h" value="NotOperator" />
    <property role="34LRSv" value="not" />
    <property role="R4oN_" value="Not Operator" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="4iLN_rFIdPj" role="1TKVEi">
      <property role="IQ2ns" value="4949964338174156115" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="original" />
      <ref role="20lvS9" node="vQ6nfZ2W1K" resolve="IMessageMatcher" />
    </node>
    <node concept="PrWs8" id="4iLN_rFIdPh" role="PzmwI">
      <ref role="PrY4T" node="vQ6nfZ2W1K" resolve="IMessageMatcher" />
    </node>
  </node>
  <node concept="1TIwiD" id="4iLN_rFSf_V">
    <property role="EcuMT" value="4949964338176784763" />
    <property role="3GE5qa" value="statements" />
    <property role="TrG5h" value="SendMessage" />
    <property role="34LRSv" value="send" />
    <property role="R4oN_" value="send an ACL Message" />
    <ref role="1TJDcQ" node="1wDV7BCtyf4" resolve="AbstractMessageExpression" />
    <node concept="1TJgyj" id="2jzj96ZwVqN" role="1TKVEi">
      <property role="IQ2ns" value="2658052393945773747" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="content" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" to="hm2y:6sdnDbSla17" resolve="Expression" />
    </node>
    <node concept="1TJgyj" id="2jzj96ZwVqO" role="1TKVEi">
      <property role="IQ2ns" value="2658052393945773748" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="receiver" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" to="hm2y:6sdnDbSla17" resolve="Expression" />
    </node>
    <node concept="1TJgyj" id="2jzj96ZwVqP" role="1TKVEi">
      <property role="IQ2ns" value="2658052393945773749" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="otherReceivers" />
      <ref role="20lvS9" to="hm2y:6sdnDbSla17" resolve="Expression" />
    </node>
    <node concept="1TJgyj" id="2jzj96ZwVqQ" role="1TKVEi">
      <property role="IQ2ns" value="2658052393945773750" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="type" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="1$PYe999WiB" resolve="MessageType" />
    </node>
    <node concept="PrWs8" id="350byq2IWzd" role="PzmwI">
      <ref role="PrY4T" to="os74:Oow4MYDA1t" resolve="IDefinePerformative" />
    </node>
  </node>
</model>

