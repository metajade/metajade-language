<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:f96171f3-244f-4724-ad44-1238f614cad5(MetaJade.aclmessage.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="14" />
    <use id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions" version="4" />
    <use id="9d69e719-78c8-4286-90db-fb19c107d049" name="com.mbeddr.mpsutil.grammarcells" version="2" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="21iy" ref="r:3c601dd3-56a8-40ab-a417-66a36a925be1(MetaJade.core.editor)" />
    <import index="hm2y" ref="r:66e07cb4-a4b0-4bf3-a36d-5e9ed1ff1bd3(org.iets3.core.expr.base.structure)" />
    <import index="zzzn" ref="r:af0af2e7-f7e1-4536-83b5-6bf010d4afd2(org.iets3.core.expr.lambda.structure)" />
    <import index="tpco" ref="r:00000000-0000-4000-0000-011c89590284(jetbrains.mps.lang.core.editor)" />
    <import index="c17a" ref="8865b7a8-5271-43d3-884c-6fd1d9cfdd34/java:org.jetbrains.mps.openapi.language(MPS.OpenAPI/)" />
    <import index="hk02" ref="r:7c1264b7-fd03-4e6c-b708-6c839cbe4cda(MetaJade.aclmessage.structure)" implicit="true" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
    <import index="os74" ref="r:0a69036a-4d31-45e1-887d-acbe5b8039f5(MetaJade.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="1402906326895675325" name="jetbrains.mps.lang.editor.structure.CellActionMap_FunctionParm_selectedNode" flags="nn" index="0IXxy" />
      <concept id="5991739802479784073" name="jetbrains.mps.lang.editor.structure.MenuTypeDefault" flags="ng" index="22hDWj" />
      <concept id="2000375450116454183" name="jetbrains.mps.lang.editor.structure.ISubstituteMenu" flags="ng" index="22mbnS">
        <child id="414384289274416996" name="parts" index="3ft7WO" />
      </concept>
      <concept id="2000375450116423800" name="jetbrains.mps.lang.editor.structure.SubstituteMenu" flags="ng" index="22mcaB" />
      <concept id="1071666914219" name="jetbrains.mps.lang.editor.structure.ConceptEditorDeclaration" flags="ig" index="24kQdi" />
      <concept id="1140524381322" name="jetbrains.mps.lang.editor.structure.CellModel_ListWithRole" flags="ng" index="2czfm3">
        <property id="1140524450557" name="separatorText" index="2czwfO" />
        <child id="1140524464360" name="cellLayout" index="2czzBx" />
      </concept>
      <concept id="1106270549637" name="jetbrains.mps.lang.editor.structure.CellLayout_Horizontal" flags="nn" index="2iRfu4" />
      <concept id="1106270571710" name="jetbrains.mps.lang.editor.structure.CellLayout_Vertical" flags="nn" index="2iRkQZ" />
      <concept id="1237303669825" name="jetbrains.mps.lang.editor.structure.CellLayout_Indent" flags="nn" index="l2Vlx" />
      <concept id="1237307900041" name="jetbrains.mps.lang.editor.structure.IndentLayoutIndentStyleClassItem" flags="ln" index="lj46D" />
      <concept id="1142886221719" name="jetbrains.mps.lang.editor.structure.QueryFunction_NodeCondition" flags="in" index="pkWqt" />
      <concept id="1142886811589" name="jetbrains.mps.lang.editor.structure.ConceptFunctionParameter_node" flags="nn" index="pncrf" />
      <concept id="1237385578942" name="jetbrains.mps.lang.editor.structure.IndentLayoutOnNewLineStyleClassItem" flags="ln" index="pVoyu" />
      <concept id="1177327570013" name="jetbrains.mps.lang.editor.structure.QueryFunction_SubstituteMenu_Substitute" flags="in" index="ucgPf" />
      <concept id="8478191136883534237" name="jetbrains.mps.lang.editor.structure.IExtensibleSubstituteMenuPart" flags="ng" index="upBLQ">
        <child id="8478191136883534238" name="features" index="upBLP" />
      </concept>
      <concept id="1177335944525" name="jetbrains.mps.lang.editor.structure.QueryFunction_SubstituteMenu_SubstituteString" flags="in" index="uGdhv" />
      <concept id="4242538589859161874" name="jetbrains.mps.lang.editor.structure.ExplicitHintsSpecification" flags="ng" index="2w$q5c" />
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="8329266386016608055" name="jetbrains.mps.lang.editor.structure.ApproveDelete_Operation" flags="ng" index="2xy62i">
        <child id="8329266386016685951" name="editorContext" index="2xHN3q" />
        <child id="8979250711607012232" name="cellSelector" index="3a7HXU" />
      </concept>
      <concept id="8371900013785948369" name="jetbrains.mps.lang.editor.structure.QueryFunction_SubstituteMenu_Parameter" flags="ig" index="2$S_p_" />
      <concept id="308059530142752797" name="jetbrains.mps.lang.editor.structure.SubstituteMenuPart_Parameterized" flags="ng" index="2F$Pav">
        <child id="8371900013785948359" name="part" index="2$S_pN" />
        <child id="8371900013785948365" name="parameterQuery" index="2$S_pT" />
      </concept>
      <concept id="1164824717996" name="jetbrains.mps.lang.editor.structure.CellMenuDescriptor" flags="ng" index="OXEIz">
        <child id="1164824815888" name="cellMenuPart" index="OY2wv" />
      </concept>
      <concept id="1078938745671" name="jetbrains.mps.lang.editor.structure.EditorComponentDeclaration" flags="ig" index="PKFIW" />
      <concept id="1078939183254" name="jetbrains.mps.lang.editor.structure.CellModel_Component" flags="sg" stub="3162947552742194261" index="PMmxH">
        <reference id="1078939183255" name="editorComponent" index="PMmxG" />
      </concept>
      <concept id="4323500428121233431" name="jetbrains.mps.lang.editor.structure.EditorCellId" flags="ng" index="2SqB2G" />
      <concept id="4323500428136740385" name="jetbrains.mps.lang.editor.structure.CellIdReferenceSelector" flags="ng" index="2TlHUq">
        <reference id="4323500428136742952" name="id" index="2TlMyj" />
      </concept>
      <concept id="1164914519156" name="jetbrains.mps.lang.editor.structure.CellMenuPart_ReplaceNode_CustomNodeConcept" flags="ng" index="UkePV">
        <reference id="1164914727930" name="replacementConcept" index="Ul1FP" />
      </concept>
      <concept id="1186403751766" name="jetbrains.mps.lang.editor.structure.FontStyleStyleClassItem" flags="ln" index="Vb9p2" />
      <concept id="1186414536763" name="jetbrains.mps.lang.editor.structure.BooleanStyleSheetItem" flags="ln" index="VOi$J">
        <property id="1186414551515" name="flag" index="VOm3f" />
        <child id="1223387335081" name="query" index="3n$kyP" />
      </concept>
      <concept id="1186414860679" name="jetbrains.mps.lang.editor.structure.EditableStyleClassItem" flags="ln" index="VPxyj" />
      <concept id="1630016958697344083" name="jetbrains.mps.lang.editor.structure.IMenu_Concept" flags="ng" index="2ZABuq">
        <reference id="6591946374543067572" name="conceptDeclaration" index="aqKnT" />
        <child id="5991739802479788259" name="type" index="22hAXT" />
      </concept>
      <concept id="1630016958697286851" name="jetbrains.mps.lang.editor.structure.QueryFunctionParameter_parameterObject" flags="ng" index="2ZBlsa" />
      <concept id="1630016958697057551" name="jetbrains.mps.lang.editor.structure.IMenuPartParameterized" flags="ng" index="2ZBHr6">
        <child id="1630016958697057552" name="parameterType" index="2ZBHrp" />
      </concept>
      <concept id="1233758997495" name="jetbrains.mps.lang.editor.structure.PunctuationLeftStyleClassItem" flags="ln" index="11L4FC" />
      <concept id="1233759184865" name="jetbrains.mps.lang.editor.structure.PunctuationRightStyleClassItem" flags="ln" index="11LMrY" />
      <concept id="8998492695583125082" name="jetbrains.mps.lang.editor.structure.SubstituteFeature_MatchingText" flags="ng" index="16NfWO">
        <child id="8998492695583129244" name="query" index="16NeZM" />
      </concept>
      <concept id="8998492695583129971" name="jetbrains.mps.lang.editor.structure.SubstituteFeature_DescriptionText" flags="ng" index="16NL0t">
        <child id="8998492695583129972" name="query" index="16NL0q" />
      </concept>
      <concept id="7342352913006985483" name="jetbrains.mps.lang.editor.structure.SubstituteMenuPart_Action" flags="ng" index="3eGOop">
        <child id="8612453216082699922" name="substituteHandler" index="3aKz83" />
      </concept>
      <concept id="1139535219966" name="jetbrains.mps.lang.editor.structure.CellActionMapDeclaration" flags="ig" index="1h_SRR">
        <reference id="1139535219968" name="applicableConcept" index="1h_SK9" />
        <child id="1139535219969" name="item" index="1h_SK8" />
      </concept>
      <concept id="1139535280617" name="jetbrains.mps.lang.editor.structure.CellActionMapItem" flags="lg" index="1hA7zw">
        <property id="1139535298778" name="actionId" index="1hAc7j" />
        <child id="1139535280620" name="executeFunction" index="1hA7z_" />
      </concept>
      <concept id="1139535439104" name="jetbrains.mps.lang.editor.structure.CellActionMap_ExecuteFunction" flags="in" index="1hAIg9" />
      <concept id="1088013125922" name="jetbrains.mps.lang.editor.structure.CellModel_RefCell" flags="sg" stub="730538219795941030" index="1iCGBv">
        <child id="1088186146602" name="editorComponent" index="1sWHZn" />
      </concept>
      <concept id="1381004262292414836" name="jetbrains.mps.lang.editor.structure.ICellStyle" flags="ng" index="1k5N5V">
        <reference id="1381004262292426837" name="parentStyleClass" index="1k5W1q" />
      </concept>
      <concept id="1223387125302" name="jetbrains.mps.lang.editor.structure.QueryFunction_Boolean" flags="in" index="3nzxsE" />
      <concept id="7580468736840446506" name="jetbrains.mps.lang.editor.structure.ConceptFunctionParameter_model" flags="nn" index="1rpKSd" />
      <concept id="1088185857835" name="jetbrains.mps.lang.editor.structure.InlineEditorComponent" flags="ig" index="1sVBvm" />
      <concept id="5425882385312046132" name="jetbrains.mps.lang.editor.structure.QueryFunctionParameter_SubstituteMenu_CurrentTargetNode" flags="nn" index="1yR$tW" />
      <concept id="1215007762405" name="jetbrains.mps.lang.editor.structure.FloatStyleClassItem" flags="ln" index="3$6MrZ">
        <property id="1215007802031" name="value" index="3$6WeP" />
      </concept>
      <concept id="1215007883204" name="jetbrains.mps.lang.editor.structure.PaddingLeftStyleClassItem" flags="ln" index="3$7fVu" />
      <concept id="1139848536355" name="jetbrains.mps.lang.editor.structure.CellModel_WithRole" flags="ng" index="1$h60E">
        <property id="1139852716018" name="noTargetText" index="1$x2rV" />
        <property id="1140017977771" name="readOnly" index="1Intyy" />
        <reference id="1140103550593" name="relationDeclaration" index="1NtTu8" />
      </concept>
      <concept id="1073389214265" name="jetbrains.mps.lang.editor.structure.EditorCellModel" flags="ng" index="3EYTF0">
        <reference id="1139959269582" name="actionMap" index="1ERwB7" />
        <child id="1142887637401" name="renderingCondition" index="pqm2j" />
        <child id="1164826688380" name="menuDescriptor" index="P5bDN" />
        <child id="4323500428121274054" name="id" index="2SqHTX" />
      </concept>
      <concept id="1073389446423" name="jetbrains.mps.lang.editor.structure.CellModel_Collection" flags="sn" stub="3013115976261988961" index="3EZMnI">
        <child id="1106270802874" name="cellLayout" index="2iSdaV" />
        <child id="1073389446424" name="childCellModel" index="3EZMnx" />
      </concept>
      <concept id="1073389577006" name="jetbrains.mps.lang.editor.structure.CellModel_Constant" flags="sn" stub="3610246225209162225" index="3F0ifn">
        <property id="1073389577007" name="text" index="3F0ifm" />
      </concept>
      <concept id="1073389658414" name="jetbrains.mps.lang.editor.structure.CellModel_Property" flags="sg" stub="730538219796134133" index="3F0A7n" />
      <concept id="1219418625346" name="jetbrains.mps.lang.editor.structure.IStyleContainer" flags="ng" index="3F0Thp">
        <child id="1219418656006" name="styleItem" index="3F10Kt" />
      </concept>
      <concept id="1073389882823" name="jetbrains.mps.lang.editor.structure.CellModel_RefNode" flags="sg" stub="730538219795960754" index="3F1sOY">
        <child id="5861024100072578575" name="addHints" index="3xwHhi" />
      </concept>
      <concept id="1073390211982" name="jetbrains.mps.lang.editor.structure.CellModel_RefNodeList" flags="sg" stub="2794558372793454595" index="3F2HdR" />
      <concept id="1161622981231" name="jetbrains.mps.lang.editor.structure.ConceptFunctionParameter_editorContext" flags="nn" index="1Q80Hx" />
      <concept id="1198256887712" name="jetbrains.mps.lang.editor.structure.CellModel_Indent" flags="ng" index="3XFhqQ" />
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1153417849900" name="jetbrains.mps.baseLanguage.structure.GreaterThanOrEqualsExpression" flags="nn" index="2d3UOw" />
      <concept id="4836112446988635817" name="jetbrains.mps.baseLanguage.structure.UndefinedType" flags="in" index="2jxLKc" />
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6" />
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1081506773034" name="jetbrains.mps.baseLanguage.structure.LessThanExpression" flags="nn" index="3eOVzh" />
      <concept id="1081516740877" name="jetbrains.mps.baseLanguage.structure.NotExpression" flags="nn" index="3fqX7Q">
        <child id="1081516765348" name="expression" index="3fr31v" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
    </language>
    <language id="fd392034-7849-419d-9071-12563d152375" name="jetbrains.mps.baseLanguage.closures">
      <concept id="1199569711397" name="jetbrains.mps.baseLanguage.closures.structure.ClosureLiteral" flags="nn" index="1bVj0M">
        <child id="1199569906740" name="parameter" index="1bW2Oz" />
        <child id="1199569916463" name="body" index="1bW5cS" />
      </concept>
    </language>
    <language id="9d69e719-78c8-4286-90db-fb19c107d049" name="com.mbeddr.mpsutil.grammarcells">
      <concept id="5083944728298846680" name="com.mbeddr.mpsutil.grammarcells.structure.OptionalCell" flags="ng" index="_tjkj">
        <child id="5083944728298846681" name="option" index="_tjki" />
        <child id="8945098465480008160" name="transformationText" index="ZWbT9" />
      </concept>
      <concept id="8945098465480383073" name="com.mbeddr.mpsutil.grammarcells.structure.OptionalCell_TransformationText" flags="ig" index="ZYGn8" />
    </language>
    <language id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions">
      <concept id="7776141288922801652" name="jetbrains.mps.lang.actions.structure.NF_Concept_NewInstance" flags="nn" index="q_SaT">
        <child id="3757480014665178932" name="prototype" index="1wAxWu" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1181952871644" name="jetbrains.mps.lang.smodel.structure.Concept_GetAllSubConcepts" flags="nn" index="LSoRf">
        <child id="1182506816063" name="smodel" index="1iTxcG" />
      </concept>
      <concept id="2644386474300074836" name="jetbrains.mps.lang.smodel.structure.ConceptIdRefExpression" flags="nn" index="35c_gC">
        <reference id="2644386474300074837" name="conceptDeclaration" index="35c_gD" />
      </concept>
      <concept id="6677504323281689838" name="jetbrains.mps.lang.smodel.structure.SConceptType" flags="in" index="3bZ5Sz">
        <reference id="6677504323281689839" name="conceptDeclaraton" index="3bZ5Sy" />
      </concept>
      <concept id="1139621453865" name="jetbrains.mps.lang.smodel.structure.Node_IsInstanceOfOperation" flags="nn" index="1mIQ4w">
        <child id="1177027386292" name="conceptArgument" index="cj9EA" />
      </concept>
      <concept id="1140131837776" name="jetbrains.mps.lang.smodel.structure.Node_ReplaceWithAnotherOperation" flags="nn" index="1P9Npp">
        <child id="1140131861877" name="replacementNode" index="1P9ThW" />
      </concept>
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="1204796164442" name="jetbrains.mps.baseLanguage.collections.structure.InternalSequenceOperation" flags="nn" index="23sCx2">
        <child id="1204796294226" name="closure" index="23t8la" />
      </concept>
      <concept id="1203518072036" name="jetbrains.mps.baseLanguage.collections.structure.SmartClosureParameterDeclaration" flags="ig" index="Rh6nW" />
      <concept id="1162935959151" name="jetbrains.mps.baseLanguage.collections.structure.GetSizeOperation" flags="nn" index="34oBXx" />
      <concept id="1202120902084" name="jetbrains.mps.baseLanguage.collections.structure.WhereOperation" flags="nn" index="3zZkjj" />
    </language>
  </registry>
  <node concept="24kQdi" id="7HOFHncxLqY">
    <property role="3GE5qa" value="statements" />
    <ref role="1XX52x" to="hk02:7HOFHncqzmZ" resolve="ReceiveMessage" />
    <node concept="3EZMnI" id="2aA5E1DOg0" role="2wV5jI">
      <node concept="2iRkQZ" id="2aA5E1DOg1" role="2iSdaV" />
      <node concept="3EZMnI" id="4YVd2MBdj7L" role="3EZMnx">
        <node concept="l2Vlx" id="2aA5E1C40I" role="2iSdaV" />
        <node concept="3F0ifn" id="1Dm95XvnHCh" role="3EZMnx">
          <property role="3F0ifm" value="on receive" />
          <ref role="1k5W1q" to="21iy:7kuDSjsqD3B" resolve="metajadeKeyword" />
        </node>
        <node concept="3F1sOY" id="4FZ6_9O07gX" role="3EZMnx">
          <ref role="1NtTu8" to="hk02:4qBaryPhIZI" resolve="arg" />
        </node>
        <node concept="3EZMnI" id="4qBaryPognk" role="3EZMnx">
          <node concept="_tjkj" id="2aA5E1A_so" role="3EZMnx">
            <node concept="3EZMnI" id="2aA5E1A_sE" role="_tjki">
              <node concept="3F0ifn" id="2aA5E1A_sN" role="3EZMnx">
                <property role="3F0ifm" value="where" />
                <ref role="1k5W1q" to="21iy:7kuDSjsqD3B" resolve="metajadeKeyword" />
                <node concept="3$7fVu" id="2aA5E1Cgza" role="3F10Kt">
                  <property role="3$6WeP" value="0.65" />
                </node>
              </node>
              <node concept="3F1sOY" id="2aA5E1A_sT" role="3EZMnx">
                <ref role="1NtTu8" to="hk02:6$6nD_DltfC" resolve="condition" />
                <node concept="2w$q5c" id="6OKQOIXqsYp" role="3xwHhi" />
              </node>
              <node concept="l2Vlx" id="2aA5E1BuBh" role="2iSdaV" />
            </node>
          </node>
          <node concept="l2Vlx" id="4qBaryPognp" role="2iSdaV" />
          <node concept="pVoyu" id="4qBaryPogqF" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
        <node concept="3F0ifn" id="49WTic8f4uy" role="3EZMnx">
          <property role="3F0ifm" value="do" />
          <ref role="1k5W1q" to="21iy:5PmnIeGXqcA" resolve="metajadeComment" />
          <node concept="lj46D" id="5a_u3Oz0aMb" role="3F10Kt">
            <property role="VOm3f" value="true" />
            <node concept="3nzxsE" id="5a_u3Oz0b6Y" role="3n$kyP">
              <node concept="3clFbS" id="5a_u3Oz0b6Z" role="2VODD2">
                <node concept="3clFbF" id="5a_u3Oz0be7" role="3cqZAp">
                  <node concept="2OqwBi" id="5a_u3Oz0be8" role="3clFbG">
                    <node concept="2OqwBi" id="5a_u3Oz0be9" role="2Oq$k0">
                      <node concept="pncrf" id="5a_u3Oz0bea" role="2Oq$k0" />
                      <node concept="3TrEf2" id="5a_u3Oz0beb" role="2OqNvi">
                        <ref role="3Tt5mk" to="hk02:4qBaryPhIZF" resolve="onReceive" />
                      </node>
                    </node>
                    <node concept="1mIQ4w" id="5a_u3Oz0bec" role="2OqNvi">
                      <node concept="chp4Y" id="5a_u3Oz1r72" role="cj9EA">
                        <ref role="cht4Q" to="hm2y:YXKE79ImBi" resolve="IWantNewLine" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="pkWqt" id="252QIDyl65v" role="pqm2j">
            <node concept="3clFbS" id="252QIDyl65w" role="2VODD2">
              <node concept="3clFbF" id="252QIDyl6eh" role="3cqZAp">
                <node concept="3fqX7Q" id="252QIDyl7gG" role="3clFbG">
                  <node concept="2OqwBi" id="252QIDyl7gI" role="3fr31v">
                    <node concept="2OqwBi" id="252QIDyl7gJ" role="2Oq$k0">
                      <node concept="pncrf" id="252QIDyl7gK" role="2Oq$k0" />
                      <node concept="3TrEf2" id="252QIDyl7gL" role="2OqNvi">
                        <ref role="3Tt5mk" to="hk02:4qBaryPhIZF" resolve="onReceive" />
                      </node>
                    </node>
                    <node concept="1mIQ4w" id="252QIDyl7gM" role="2OqNvi">
                      <node concept="chp4Y" id="252QIDyl7gN" role="cj9EA">
                        <ref role="cht4Q" to="zzzn:49WTic8ig5D" resolve="BlockExpression" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="pVoyu" id="5a_u3Oz05ta" role="3F10Kt">
            <property role="VOm3f" value="true" />
            <node concept="3nzxsE" id="5a_u3Oz05Kf" role="3n$kyP">
              <node concept="3clFbS" id="5a_u3Oz05Kg" role="2VODD2">
                <node concept="3clFbF" id="5a_u3Oz05Rr" role="3cqZAp">
                  <node concept="2OqwBi" id="5a_u3Oz08iy" role="3clFbG">
                    <node concept="2OqwBi" id="5a_u3Oz06o9" role="2Oq$k0">
                      <node concept="pncrf" id="5a_u3Oz05Rq" role="2Oq$k0" />
                      <node concept="3TrEf2" id="5a_u3Oz07lR" role="2OqNvi">
                        <ref role="3Tt5mk" to="hk02:4qBaryPhIZF" resolve="onReceive" />
                      </node>
                    </node>
                    <node concept="1mIQ4w" id="5a_u3Oz09Mp" role="2OqNvi">
                      <node concept="chp4Y" id="5a_u3Oz1s5e" role="cj9EA">
                        <ref role="cht4Q" to="hm2y:YXKE79ImBi" resolve="IWantNewLine" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3F1sOY" id="2aA5E1A_rT" role="3EZMnx">
          <property role="1$x2rV" value="nothing" />
          <ref role="1NtTu8" to="hk02:4qBaryPhIZF" resolve="onReceive" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="7HOFHncy6ZB">
    <property role="3GE5qa" value="statements" />
    <ref role="1XX52x" to="hk02:3uK1rwojlW1" resolve="ComposeMessage" />
    <node concept="3EZMnI" id="3I8nNvsTTJ$" role="2wV5jI">
      <node concept="2iRkQZ" id="3I8nNvsTTJ_" role="2iSdaV" />
      <node concept="3EZMnI" id="7HOFHncy6ZD" role="3EZMnx">
        <node concept="3F0ifn" id="2aA5E0Zvgu" role="3EZMnx">
          <property role="3F0ifm" value="compose" />
          <ref role="1k5W1q" to="21iy:7kuDSjsqD3B" resolve="metajadeKeyword" />
        </node>
        <node concept="3F1sOY" id="7OgJtfmIVL" role="3EZMnx">
          <ref role="1NtTu8" to="hk02:7OgJtfmIVA" resolve="type" />
        </node>
        <node concept="3F0ifn" id="3I8nNvsU1dI" role="3EZMnx">
          <property role="3F0ifm" value="{" />
        </node>
        <node concept="l2Vlx" id="28q_2hFDSEI" role="2iSdaV" />
      </node>
      <node concept="3EZMnI" id="5zd1xVkp1Cp" role="3EZMnx">
        <node concept="3XFhqQ" id="5zd1xVkp1D8" role="3EZMnx" />
        <node concept="3F0ifn" id="5zd1xVkp1De" role="3EZMnx">
          <property role="3F0ifm" value="receiver" />
          <node concept="Vb9p2" id="5zd1xVkp1F_" role="3F10Kt" />
        </node>
        <node concept="3F0ifn" id="5zd1xVkp1Dm" role="3EZMnx">
          <property role="3F0ifm" value=":" />
        </node>
        <node concept="3F1sOY" id="5zd1xVkp1Fv" role="3EZMnx">
          <ref role="1NtTu8" to="hk02:5zd1xVkp1As" resolve="receiver" />
        </node>
        <node concept="l2Vlx" id="5zd1xVkp1Cu" role="2iSdaV" />
      </node>
      <node concept="_tjkj" id="6$6nD_Dkph6" role="3EZMnx">
        <node concept="3EZMnI" id="5zd1xVkp1Ed" role="_tjki">
          <node concept="3XFhqQ" id="5zd1xVkp1F6" role="3EZMnx" />
          <node concept="3F0ifn" id="5zd1xVkp1Fc" role="3EZMnx">
            <property role="3F0ifm" value="other receveirs" />
            <node concept="Vb9p2" id="5zd1xVkp1FB" role="3F10Kt" />
          </node>
          <node concept="3F0ifn" id="5zd1xVkp1Fp" role="3EZMnx">
            <property role="3F0ifm" value=":" />
          </node>
          <node concept="3F1sOY" id="3EtpyNPWU3B" role="3EZMnx">
            <ref role="1NtTu8" to="hk02:5zd1xVkp1FD" resolve="otherReceivers" />
          </node>
          <node concept="l2Vlx" id="5zd1xVkp1Ei" role="2iSdaV" />
        </node>
        <node concept="ZYGn8" id="6$6nD_Dkpii" role="ZWbT9">
          <node concept="3clFbS" id="6$6nD_Dkpij" role="2VODD2">
            <node concept="3clFbF" id="6$6nD_Dkpir" role="3cqZAp">
              <node concept="Xl_RD" id="6$6nD_Dkpiq" role="3clFbG">
                <property role="Xl_RC" value="other" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3EZMnI" id="3I8nNvsTTM0" role="3EZMnx">
        <node concept="3XFhqQ" id="3I8nNvsTTMt" role="3EZMnx" />
        <node concept="3F0ifn" id="3I8nNvsTTMz" role="3EZMnx">
          <property role="3F0ifm" value="content" />
          <node concept="Vb9p2" id="3I8nNvsTTMR" role="3F10Kt" />
        </node>
        <node concept="3F0ifn" id="3I8nNvsTTMY" role="3EZMnx">
          <property role="3F0ifm" value=":" />
        </node>
        <node concept="3F1sOY" id="3I8nNvsTTNa" role="3EZMnx">
          <ref role="1NtTu8" to="hk02:3I8nNvsTTJ8" resolve="content" />
        </node>
        <node concept="l2Vlx" id="3I8nNvsTTM5" role="2iSdaV" />
      </node>
      <node concept="3F0ifn" id="3I8nNvsU1dO" role="3EZMnx">
        <property role="3F0ifm" value="}" />
      </node>
    </node>
  </node>
  <node concept="PKFIW" id="7dT_6n11ZJi">
    <property role="TrG5h" value="DummyForGrammarCells" />
    <ref role="1XX52x" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="3F0ifn" id="7dT_6n11ZJj" role="2wV5jI">
      <property role="3F0ifm" value="Workaround to fix contributions to BaseConcept generated by grammarCells." />
    </node>
  </node>
  <node concept="24kQdi" id="4i_VsgbvgoU">
    <property role="3GE5qa" value="statements" />
    <ref role="1XX52x" to="hk02:4i_Vsgbvgoq" resolve="ReceivedMessageArgRef" />
    <node concept="1iCGBv" id="4i_VsgbvgoY" role="2wV5jI">
      <ref role="1NtTu8" to="hk02:4i_VsgbvgoW" resolve="arg" />
      <node concept="1sVBvm" id="4i_Vsgbvgp0" role="1sWHZn">
        <node concept="3F0A7n" id="7OgJtfoDo8" role="2wV5jI">
          <property role="1Intyy" value="true" />
          <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="1$PYe999WiC">
    <ref role="1XX52x" to="hk02:1$PYe999WiB" resolve="MessageType" />
    <node concept="3EZMnI" id="2lACGaVDxAT" role="2wV5jI">
      <node concept="3F0ifn" id="2lACGaVDxB0" role="3EZMnx">
        <property role="3F0ifm" value="message" />
        <ref role="1k5W1q" to="21iy:2WQ3iBR5Hx0" resolve="metajadeType" />
      </node>
      <node concept="3F0ifn" id="22Cof6W97bk" role="3EZMnx">
        <property role="3F0ifm" value="&lt;" />
        <node concept="11L4FC" id="22Cof6W9kRl" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="11LMrY" id="22Cof6W9y0l" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F1sOY" id="22Cof6W97bu" role="3EZMnx">
        <ref role="1NtTu8" to="hk02:2lACGaVDxt_" resolve="baseType" />
      </node>
      <node concept="3F0ifn" id="22Cof6W97bA" role="3EZMnx">
        <property role="3F0ifm" value="&gt;" />
        <node concept="11L4FC" id="22Cof6W9y0g" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="2iRfu4" id="2lACGaVDxAW" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="6OKQOIXfwYl">
    <property role="3GE5qa" value="statements" />
    <ref role="1XX52x" to="hk02:6OKQOIXfaDa" resolve="ReceivedMessageArg" />
    <node concept="3EZMnI" id="6OKQOIXfwYn" role="2wV5jI">
      <node concept="3F1sOY" id="6$6nD_DkCvd" role="3EZMnx">
        <ref role="1NtTu8" to="hk02:6OKQOIXfaDc" resolve="type" />
      </node>
      <node concept="3F0ifn" id="6OKQOIXfwY$" role="3EZMnx">
        <property role="3F0ifm" value="as" />
        <ref role="1k5W1q" to="21iy:5PmnIeGXqcA" resolve="metajadeComment" />
      </node>
      <node concept="3F0A7n" id="6OKQOIXfwYG" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="l2Vlx" id="6OKQOIXfwYq" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="7AVCbhhTnhG">
    <property role="3GE5qa" value="matches" />
    <ref role="1XX52x" to="hk02:7AVCbhhTnhf" resolve="PerformativeMatch" />
    <node concept="3EZMnI" id="7AVCbhhTnhL" role="2wV5jI">
      <node concept="l2Vlx" id="7AVCbhhTnhM" role="2iSdaV" />
      <node concept="3F0ifn" id="7AVCbhhTnhI" role="3EZMnx">
        <property role="3F0ifm" value="performative is" />
      </node>
      <node concept="3F0A7n" id="7AVCbhhTnhU" role="3EZMnx">
        <ref role="1NtTu8" to="os74:Oow4MYDA1o" resolve="performative" />
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="7AVCbhhTzuI">
    <property role="3GE5qa" value="matches" />
    <ref role="1XX52x" to="hk02:7AVCbhhTzuj" resolve="SenderMatch" />
    <node concept="3EZMnI" id="7AVCbhhTzuK" role="2wV5jI">
      <node concept="3F0ifn" id="7AVCbhhTzuR" role="3EZMnx">
        <property role="3F0ifm" value="sender is" />
      </node>
      <node concept="3F1sOY" id="7AVCbhhTHCb" role="3EZMnx">
        <ref role="1NtTu8" to="hk02:7AVCbhhTzuX" resolve="value" />
      </node>
      <node concept="l2Vlx" id="7AVCbhhTzuN" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="7AVCbhhUczp">
    <property role="3GE5qa" value="matches" />
    <ref role="1XX52x" to="hk02:7AVCbhhUcyW" resolve="ReceiverMatch" />
    <node concept="3EZMnI" id="7AVCbhhUczr" role="2wV5jI">
      <node concept="3F0ifn" id="7AVCbhhUqyH" role="3EZMnx">
        <property role="3F0ifm" value="receiver is" />
        <node concept="pkWqt" id="7AVCbhhUqzn" role="pqm2j">
          <node concept="3clFbS" id="7AVCbhhUqzo" role="2VODD2">
            <node concept="3clFbF" id="7AVCbhhUqzt" role="3cqZAp">
              <node concept="3eOVzh" id="7AVCbhhUyD3" role="3clFbG">
                <node concept="3cmrfG" id="7AVCbhhUyKP" role="3uHU7w">
                  <property role="3cmrfH" value="2" />
                </node>
                <node concept="2OqwBi" id="7AVCbhhUtug" role="3uHU7B">
                  <node concept="2OqwBi" id="7AVCbhhUqNd" role="2Oq$k0">
                    <node concept="pncrf" id="7AVCbhhUqzs" role="2Oq$k0" />
                    <node concept="3Tsc0h" id="7AVCbhhUr8p" role="2OqNvi">
                      <ref role="3TtcxE" to="hk02:7AVCbhhUcyX" resolve="values" />
                    </node>
                  </node>
                  <node concept="34oBXx" id="7AVCbhhUwDe" role="2OqNvi" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3F0ifn" id="7AVCbhhUqyR" role="3EZMnx">
        <property role="3F0ifm" value="receivers are" />
        <node concept="pkWqt" id="7AVCbhhUyVW" role="pqm2j">
          <node concept="3clFbS" id="7AVCbhhUyVX" role="2VODD2">
            <node concept="3clFbF" id="7AVCbhhUyW2" role="3cqZAp">
              <node concept="2d3UOw" id="7AVCbhhUGN$" role="3clFbG">
                <node concept="3cmrfG" id="7AVCbhhUGPk" role="3uHU7w">
                  <property role="3cmrfH" value="2" />
                </node>
                <node concept="2OqwBi" id="7AVCbhhU_XS" role="3uHU7B">
                  <node concept="2OqwBi" id="7AVCbhhUzbM" role="2Oq$k0">
                    <node concept="pncrf" id="7AVCbhhUyW1" role="2Oq$k0" />
                    <node concept="3Tsc0h" id="7AVCbhhUzu1" role="2OqNvi">
                      <ref role="3TtcxE" to="hk02:7AVCbhhUcyX" resolve="values" />
                    </node>
                  </node>
                  <node concept="34oBXx" id="7AVCbhhUDCs" role="2OqNvi" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3F2HdR" id="7AVCbhhUczy" role="3EZMnx">
        <property role="2czwfO" value="," />
        <ref role="1NtTu8" to="hk02:7AVCbhhUcyX" resolve="values" />
        <node concept="l2Vlx" id="7AVCbhhUcz$" role="2czzBx" />
      </node>
      <node concept="l2Vlx" id="7AVCbhhUczu" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="7AVCbhhYKKN">
    <property role="3GE5qa" value="operations" />
    <ref role="1XX52x" to="hk02:7AVCbhhYKKm" resolve="GetSenderOp" />
    <node concept="3F0ifn" id="7AVCbhhYKKP" role="2wV5jI">
      <property role="3F0ifm" value="sender" />
    </node>
  </node>
  <node concept="24kQdi" id="7AVCbhhYXCd">
    <property role="3GE5qa" value="operations" />
    <ref role="1XX52x" to="hk02:7AVCbhhYXBK" resolve="GetReceiversOp" />
    <node concept="3F0ifn" id="7AVCbhhYXCf" role="2wV5jI">
      <property role="3F0ifm" value="receivers" />
    </node>
  </node>
  <node concept="24kQdi" id="7AVCbhi0iPe">
    <property role="3GE5qa" value="operations" />
    <ref role="1XX52x" to="hk02:7AVCbhi0iOL" resolve="GetContentOp" />
    <node concept="3F0ifn" id="7AVCbhi0iPg" role="2wV5jI">
      <property role="3F0ifm" value="content" />
    </node>
  </node>
  <node concept="24kQdi" id="7OgJtfqDj8">
    <property role="3GE5qa" value="statements" />
    <ref role="1XX52x" to="hk02:7OgJtfqDiI" resolve="ReplyToMessage" />
    <node concept="3EZMnI" id="7OgJtfqDsV" role="2wV5jI">
      <node concept="3EZMnI" id="7OgJtfqKWr" role="3EZMnx">
        <node concept="3F0ifn" id="7OgJtfqKWG" role="3EZMnx">
          <property role="3F0ifm" value="reply with" />
          <ref role="1k5W1q" to="21iy:7kuDSjsqD3B" resolve="metajadeKeyword" />
        </node>
        <node concept="3F1sOY" id="7OgJtfqKWM" role="3EZMnx">
          <ref role="1NtTu8" to="hk02:7OgJtfqDiN" resolve="type" />
        </node>
        <node concept="3F0ifn" id="7OgJtfr3Se" role="3EZMnx">
          <property role="3F0ifm" value="to" />
          <ref role="1k5W1q" to="21iy:7kuDSjsqD3B" resolve="metajadeKeyword" />
        </node>
        <node concept="3F1sOY" id="7OgJtfr3Sx" role="3EZMnx">
          <ref role="1NtTu8" to="hk02:7OgJtfqDiO" resolve="reply" />
        </node>
        <node concept="3F0ifn" id="7OgJtfqLbn" role="3EZMnx">
          <property role="3F0ifm" value="{" />
        </node>
        <node concept="l2Vlx" id="7OgJtfqKWw" role="2iSdaV" />
      </node>
      <node concept="3EZMnI" id="7OgJtfqL8Y" role="3EZMnx">
        <node concept="3XFhqQ" id="7OgJtfqL9j" role="3EZMnx" />
        <node concept="3F0ifn" id="7OgJtfqLb9" role="3EZMnx">
          <property role="3F0ifm" value="performative" />
          <node concept="Vb9p2" id="7OgJtfqLbf" role="3F10Kt" />
        </node>
        <node concept="3F0ifn" id="7OgJtfqLaO" role="3EZMnx">
          <property role="3F0ifm" value=":" />
        </node>
        <node concept="3F0A7n" id="7OgJtfqLaY" role="3EZMnx">
          <ref role="1NtTu8" to="os74:Oow4MYDA1o" resolve="performative" />
        </node>
        <node concept="l2Vlx" id="7OgJtfqL93" role="2iSdaV" />
      </node>
      <node concept="3EZMnI" id="7OgJtfqL9C" role="3EZMnx">
        <node concept="3XFhqQ" id="7OgJtfqL9e" role="3EZMnx" />
        <node concept="3F0ifn" id="7OgJtfqLa9" role="3EZMnx">
          <property role="3F0ifm" value="content" />
          <node concept="Vb9p2" id="7OgJtfqLbh" role="3F10Kt" />
        </node>
        <node concept="3F0ifn" id="7OgJtfqLah" role="3EZMnx">
          <property role="3F0ifm" value=":" />
        </node>
        <node concept="3F1sOY" id="7OgJtfqLar" role="3EZMnx">
          <ref role="1NtTu8" to="hk02:7OgJtfqDiK" resolve="content" />
        </node>
        <node concept="l2Vlx" id="7OgJtfqL9E" role="2iSdaV" />
      </node>
      <node concept="3F0ifn" id="7OgJtfqLbL" role="3EZMnx">
        <property role="3F0ifm" value="}" />
      </node>
      <node concept="2iRkQZ" id="7OgJtfqDsY" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="4iLN_rFIdPJ">
    <property role="3GE5qa" value="matches" />
    <ref role="1XX52x" to="hk02:4iLN_rFIdPg" resolve="NotOperator" />
    <node concept="3EZMnI" id="7GvO64iV6ST" role="2wV5jI">
      <node concept="3F0ifn" id="7GvO64iV6T0" role="3EZMnx">
        <property role="3F0ifm" value="not" />
        <ref role="1ERwB7" node="4iLN_rFIdT8" resolve="RemoveNot" />
        <node concept="VPxyj" id="7GvO64iV6Ta" role="3F10Kt" />
        <node concept="2SqB2G" id="7GvO64iVpfz" role="2SqHTX">
          <property role="TrG5h" value="notKeyword" />
        </node>
      </node>
      <node concept="3F0ifn" id="3Di1CsLxMZb" role="3EZMnx">
        <property role="3F0ifm" value="(" />
      </node>
      <node concept="3F1sOY" id="7GvO64iV6T6" role="3EZMnx">
        <ref role="1NtTu8" to="hk02:4iLN_rFIdPj" resolve="original" />
      </node>
      <node concept="3F0ifn" id="3Di1CsLxMZp" role="3EZMnx">
        <property role="3F0ifm" value=")" />
      </node>
      <node concept="l2Vlx" id="7GvO64iV6SW" role="2iSdaV" />
    </node>
  </node>
  <node concept="1h_SRR" id="4iLN_rFIdT8">
    <property role="3GE5qa" value="matches" />
    <property role="TrG5h" value="RemoveNot" />
    <ref role="1h_SK9" to="hk02:4iLN_rFIdPg" resolve="NotOperator" />
    <node concept="1hA7zw" id="7GvO64iVp8n" role="1h_SK8">
      <property role="1hAc7j" value="g_hAxAO/delete_action_id" />
      <node concept="1hAIg9" id="7GvO64iVp8o" role="1hA7z_">
        <node concept="3clFbS" id="7GvO64iVp8p" role="2VODD2">
          <node concept="3clFbJ" id="7GvO64iVp8t" role="3cqZAp">
            <node concept="3clFbS" id="7GvO64iVp8u" role="3clFbx">
              <node concept="3cpWs6" id="7GvO64iVp8v" role="3cqZAp" />
            </node>
            <node concept="2OqwBi" id="7GvO64iVp8w" role="3clFbw">
              <node concept="0IXxy" id="7GvO64iVp8x" role="2Oq$k0" />
              <node concept="2xy62i" id="7GvO64iVp8y" role="2OqNvi">
                <node concept="1Q80Hx" id="7GvO64iVp8z" role="2xHN3q" />
                <node concept="2TlHUq" id="14TMHtHjx3W" role="3a7HXU">
                  <ref role="2TlMyj" node="7GvO64iVpfz" resolve="notKeyword" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="6tmz5v66AEC" role="3cqZAp">
            <node concept="2OqwBi" id="6tmz5v66AEY" role="3clFbG">
              <node concept="0IXxy" id="6tmz5v66AED" role="2Oq$k0" />
              <node concept="1P9Npp" id="6tmz5v66AF4" role="2OqNvi">
                <node concept="2OqwBi" id="6tmz5v66AFr" role="1P9ThW">
                  <node concept="0IXxy" id="6tmz5v66AF6" role="2Oq$k0" />
                  <node concept="3TrEf2" id="6tmz5v66AFw" role="2OqNvi">
                    <ref role="3Tt5mk" to="hk02:4iLN_rFIdPj" resolve="original" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="4iLN_rFIdZE">
    <property role="3GE5qa" value="matches" />
    <ref role="1XX52x" to="hk02:4iLN_rFIdP8" resolve="LogicalOperator" />
    <node concept="3EZMnI" id="7GvO64iUlE3" role="2wV5jI">
      <node concept="3F1sOY" id="7GvO64iUlEd" role="3EZMnx">
        <ref role="1NtTu8" to="hk02:7GvO64iUcM9" resolve="left" />
        <ref role="1ERwB7" node="4iLN_rFIe8i" resolve="DeleteLogicalOperator" />
      </node>
      <node concept="PMmxH" id="7GvO64iUlEj" role="3EZMnx">
        <ref role="PMmxG" to="tpco:2wZex4PafBj" resolve="alias" />
        <ref role="1k5W1q" to="21iy:7kuDSjsqD3B" resolve="metajadeKeyword" />
        <node concept="OXEIz" id="7GvO64iUlE$" role="P5bDN">
          <node concept="UkePV" id="7GvO64iUlEA" role="OY2wv">
            <ref role="Ul1FP" to="hk02:4iLN_rFIdP8" resolve="LogicalOperator" />
          </node>
        </node>
        <node concept="VPxyj" id="7GvO64iUlEC" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F1sOY" id="7GvO64iUlEv" role="3EZMnx">
        <ref role="1NtTu8" to="hk02:7GvO64iUcMb" resolve="right" />
        <ref role="1ERwB7" node="4iLN_rFIe3j" resolve="DeleteLogicalOperatorFromBehind" />
      </node>
      <node concept="l2Vlx" id="7GvO64iUlE6" role="2iSdaV" />
    </node>
  </node>
  <node concept="1h_SRR" id="4iLN_rFIe3j">
    <property role="3GE5qa" value="matches" />
    <property role="TrG5h" value="DeleteLogicalOperatorFromBehind" />
    <ref role="1h_SK9" to="hk02:4iLN_rFIdP8" resolve="LogicalOperator" />
    <node concept="1hA7zw" id="7GvO64iUdX1" role="1h_SK8">
      <property role="1hAc7j" value="g_hAxAO/delete_action_id" />
      <node concept="1hAIg9" id="7GvO64iUdX2" role="1hA7z_">
        <node concept="3clFbS" id="7GvO64iUdX3" role="2VODD2">
          <node concept="3clFbJ" id="14TMHtHjxJd" role="3cqZAp">
            <node concept="3clFbS" id="14TMHtHjxJe" role="3clFbx">
              <node concept="3cpWs6" id="14TMHtHjxJf" role="3cqZAp" />
            </node>
            <node concept="2OqwBi" id="14TMHtHjxJg" role="3clFbw">
              <node concept="2OqwBi" id="14TMHtHjxY2" role="2Oq$k0">
                <node concept="0IXxy" id="14TMHtHjxJh" role="2Oq$k0" />
                <node concept="3TrEf2" id="14TMHtHjySq" role="2OqNvi">
                  <ref role="3Tt5mk" to="hk02:7GvO64iUcMb" resolve="right" />
                </node>
              </node>
              <node concept="2xy62i" id="14TMHtHjxJi" role="2OqNvi">
                <node concept="1Q80Hx" id="14TMHtHjxJj" role="2xHN3q" />
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="1epj7GMxA6p" role="3cqZAp">
            <node concept="2OqwBi" id="1epj7GMxAd_" role="3clFbG">
              <node concept="1P9Npp" id="1epj7GMxBRl" role="2OqNvi">
                <node concept="2OqwBi" id="1epj7GMxBZl" role="1P9ThW">
                  <node concept="3TrEf2" id="14TMHtHjz5v" role="2OqNvi">
                    <ref role="3Tt5mk" to="hk02:7GvO64iUcM9" resolve="left" />
                  </node>
                  <node concept="0IXxy" id="1epj7GMxBS9" role="2Oq$k0" />
                </node>
              </node>
              <node concept="0IXxy" id="1epj7GMxA6o" role="2Oq$k0" />
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="1h_SRR" id="4iLN_rFIe8i">
    <property role="3GE5qa" value="matches" />
    <property role="TrG5h" value="DeleteLogicalOperator" />
    <ref role="1h_SK9" to="hk02:4iLN_rFIdP8" resolve="LogicalOperator" />
    <node concept="1hA7zw" id="7GvO64iUdjM" role="1h_SK8">
      <property role="1hAc7j" value="g_hAxAO/delete_action_id" />
      <node concept="1hAIg9" id="7GvO64iUdjN" role="1hA7z_">
        <node concept="3clFbS" id="7GvO64iUdjO" role="2VODD2">
          <node concept="3clFbJ" id="14TMHtHhYRf" role="3cqZAp">
            <node concept="3clFbS" id="14TMHtHhYRh" role="3clFbx">
              <node concept="3cpWs6" id="14TMHtHi07y" role="3cqZAp" />
            </node>
            <node concept="2OqwBi" id="14TMHtHhZzn" role="3clFbw">
              <node concept="2OqwBi" id="14TMHtHjxnu" role="2Oq$k0">
                <node concept="0IXxy" id="14TMHtHhZiY" role="2Oq$k0" />
                <node concept="3TrEf2" id="14TMHtHjyt8" role="2OqNvi">
                  <ref role="3Tt5mk" to="hk02:7GvO64iUcM9" resolve="left" />
                </node>
              </node>
              <node concept="2xy62i" id="14TMHtHi06a" role="2OqNvi">
                <node concept="1Q80Hx" id="14TMHtHi06P" role="2xHN3q" />
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="1epj7GMtBQK" role="3cqZAp">
            <node concept="2OqwBi" id="1epj7GMtBXX" role="3clFbG">
              <node concept="1P9Npp" id="1epj7GMtSK1" role="2OqNvi">
                <node concept="2OqwBi" id="1epj7GMtSRR" role="1P9ThW">
                  <node concept="3TrEf2" id="14TMHtHjyEd" role="2OqNvi">
                    <ref role="3Tt5mk" to="hk02:7GvO64iUcMb" resolve="right" />
                  </node>
                  <node concept="0IXxy" id="1epj7GMtSKF" role="2Oq$k0" />
                </node>
              </node>
              <node concept="0IXxy" id="1epj7GMtBQJ" role="2Oq$k0" />
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="22mcaB" id="4iLN_rFIeGR">
    <property role="3GE5qa" value="matches" />
    <ref role="aqKnT" to="hk02:4iLN_rFIdP8" resolve="LogicalOperator" />
    <node concept="2F$Pav" id="7GvO64iUeEK" role="3ft7WO">
      <node concept="3eGOop" id="7GvO64iUie_" role="2$S_pN">
        <node concept="ucgPf" id="7GvO64iUieB" role="3aKz83">
          <node concept="3clFbS" id="7GvO64iUieD" role="2VODD2">
            <node concept="3cpWs8" id="1wEcoXjJghy" role="3cqZAp">
              <node concept="3cpWsn" id="1wEcoXjJghz" role="3cpWs9">
                <property role="TrG5h" value="newInitializedInstance" />
                <node concept="3Tqbb2" id="1wEcoXjJgh$" role="1tU5fm">
                  <ref role="ehGHo" to="hk02:4iLN_rFIdP8" resolve="LogicalOperator" />
                </node>
                <node concept="2OqwBi" id="1wEcoXjJgh_" role="33vP2m">
                  <node concept="q_SaT" id="1wEcoXjJghA" role="2OqNvi">
                    <node concept="1yR$tW" id="1wEcoXjJghF" role="1wAxWu" />
                  </node>
                  <node concept="2ZBlsa" id="1wEcoXjJghG" role="2Oq$k0" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="1wEcoXjJghD" role="3cqZAp">
              <node concept="37vLTw" id="1wEcoXjJghE" role="3clFbG">
                <ref role="3cqZAo" node="1wEcoXjJghz" resolve="newInitializedInstance" />
              </node>
            </node>
          </node>
        </node>
        <node concept="16NfWO" id="7GvO64iUiDv" role="upBLP">
          <node concept="uGdhv" id="7GvO64iUiHc" role="16NeZM">
            <node concept="3clFbS" id="7GvO64iUiHe" role="2VODD2">
              <node concept="3clFbF" id="7GvO64iUiLF" role="3cqZAp">
                <node concept="2OqwBi" id="7GvO64iUj0S" role="3clFbG">
                  <node concept="2ZBlsa" id="7GvO64iUiLE" role="2Oq$k0" />
                  <node concept="liA8E" id="7GvO64iUjl$" role="2OqNvi">
                    <ref role="37wK5l" to="c17a:~SAbstractConcept.getConceptAlias()" resolve="getConceptAlias" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="16NL0t" id="7GvO64iUjJx" role="upBLP">
          <node concept="uGdhv" id="7GvO64iUjMC" role="16NL0q">
            <node concept="3clFbS" id="7GvO64iUjME" role="2VODD2">
              <node concept="3clFbF" id="7GvO64iUjMU" role="3cqZAp">
                <node concept="2OqwBi" id="7GvO64iUk27" role="3clFbG">
                  <node concept="2ZBlsa" id="7GvO64iUjMT" role="2Oq$k0" />
                  <node concept="liA8E" id="7GvO64iUkmN" role="2OqNvi">
                    <ref role="37wK5l" to="c17a:~SAbstractConcept.getName()" resolve="getName" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3bZ5Sz" id="7GvO64iUeET" role="2ZBHrp">
        <ref role="3bZ5Sy" to="hk02:4iLN_rFIdP8" resolve="LogicalOperator" />
      </node>
      <node concept="2$S_p_" id="7GvO64iUeEW" role="2$S_pT">
        <node concept="3clFbS" id="7GvO64iUeEX" role="2VODD2">
          <node concept="3clFbF" id="7GvO64iUeHw" role="3cqZAp">
            <node concept="2OqwBi" id="7GvO64iUg8$" role="3clFbG">
              <node concept="2OqwBi" id="7GvO64iUf8t" role="2Oq$k0">
                <node concept="35c_gC" id="7GvO64iUeHv" role="2Oq$k0">
                  <ref role="35c_gD" to="hk02:4iLN_rFIdP8" resolve="LogicalOperator" />
                </node>
                <node concept="LSoRf" id="7GvO64iUftV" role="2OqNvi">
                  <node concept="1rpKSd" id="7GvO64iUfAv" role="1iTxcG" />
                </node>
              </node>
              <node concept="3zZkjj" id="7GvO64iUgWw" role="2OqNvi">
                <node concept="1bVj0M" id="7GvO64iUgWy" role="23t8la">
                  <node concept="3clFbS" id="7GvO64iUgWz" role="1bW5cS">
                    <node concept="3clFbF" id="7GvO64iUh9Y" role="3cqZAp">
                      <node concept="3fqX7Q" id="7GvO64iUh9W" role="3clFbG">
                        <node concept="2OqwBi" id="7GvO64iUhvE" role="3fr31v">
                          <node concept="37vLTw" id="7GvO64iUha5" role="2Oq$k0">
                            <ref role="3cqZAo" node="7GvO64iUgW$" resolve="it" />
                          </node>
                          <node concept="liA8E" id="7GvO64iUi7$" role="2OqNvi">
                            <ref role="37wK5l" to="c17a:~SAbstractConcept.isAbstract()" resolve="isAbstract" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="Rh6nW" id="7GvO64iUgW$" role="1bW2Oz">
                    <property role="TrG5h" value="it" />
                    <node concept="2jxLKc" id="7GvO64iUgW_" role="1tU5fm" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="22hDWj" id="4iLN_rFIeGS" role="22hAXT" />
  </node>
  <node concept="24kQdi" id="4iLN_rFTHcK">
    <property role="3GE5qa" value="statements" />
    <ref role="1XX52x" to="hk02:4iLN_rFSf_V" resolve="SendMessage" />
    <node concept="3EZMnI" id="2jzj96ZwVvU" role="2wV5jI">
      <node concept="2iRkQZ" id="2jzj96ZwVvV" role="2iSdaV" />
      <node concept="3EZMnI" id="2jzj96ZwVvW" role="3EZMnx">
        <node concept="3F0ifn" id="2jzj96ZwVvX" role="3EZMnx">
          <property role="3F0ifm" value="send" />
          <ref role="1k5W1q" to="21iy:7kuDSjsqD3B" resolve="metajadeKeyword" />
        </node>
        <node concept="3F1sOY" id="2jzj96ZwVvY" role="3EZMnx">
          <ref role="1NtTu8" to="hk02:2jzj96ZwVqQ" resolve="type" />
        </node>
        <node concept="3F0ifn" id="2jzj96ZwVvZ" role="3EZMnx">
          <property role="3F0ifm" value="{" />
        </node>
        <node concept="l2Vlx" id="2jzj96ZwVw0" role="2iSdaV" />
      </node>
      <node concept="3EZMnI" id="2jzj96ZwVyL" role="3EZMnx">
        <node concept="3XFhqQ" id="2jzj96ZwVzs" role="3EZMnx" />
        <node concept="3F0ifn" id="2jzj96ZwVzE" role="3EZMnx">
          <property role="3F0ifm" value="performative" />
          <node concept="Vb9p2" id="2jzj96ZwV$2" role="3F10Kt" />
        </node>
        <node concept="3F0ifn" id="2jzj96ZwVzM" role="3EZMnx">
          <property role="3F0ifm" value=":" />
        </node>
        <node concept="3F0A7n" id="2jzj96ZwVzW" role="3EZMnx">
          <ref role="1NtTu8" to="os74:Oow4MYDA1o" resolve="performative" />
        </node>
        <node concept="l2Vlx" id="2jzj96ZwVyQ" role="2iSdaV" />
      </node>
      <node concept="3EZMnI" id="2jzj96ZwVw1" role="3EZMnx">
        <node concept="3XFhqQ" id="2jzj96ZwVw2" role="3EZMnx" />
        <node concept="3F0ifn" id="2jzj96ZwVw3" role="3EZMnx">
          <property role="3F0ifm" value="receiver" />
          <node concept="Vb9p2" id="2jzj96ZwVw4" role="3F10Kt" />
        </node>
        <node concept="3F0ifn" id="2jzj96ZwVw5" role="3EZMnx">
          <property role="3F0ifm" value=":" />
        </node>
        <node concept="3F1sOY" id="2jzj96ZwVw6" role="3EZMnx">
          <ref role="1NtTu8" to="hk02:2jzj96ZwVqO" resolve="receiver" />
        </node>
        <node concept="l2Vlx" id="2jzj96ZwVw7" role="2iSdaV" />
      </node>
      <node concept="_tjkj" id="2jzj96ZwVw8" role="3EZMnx">
        <node concept="3EZMnI" id="2jzj96ZwVw9" role="_tjki">
          <node concept="3XFhqQ" id="2jzj96ZwVwa" role="3EZMnx" />
          <node concept="3F0ifn" id="2jzj96ZwVwb" role="3EZMnx">
            <property role="3F0ifm" value="other receveirs" />
            <node concept="Vb9p2" id="2jzj96ZwVwc" role="3F10Kt" />
          </node>
          <node concept="3F0ifn" id="2jzj96ZwVwd" role="3EZMnx">
            <property role="3F0ifm" value=":" />
          </node>
          <node concept="3F1sOY" id="2jzj96ZwVwe" role="3EZMnx">
            <ref role="1NtTu8" to="hk02:2jzj96ZwVqP" resolve="otherReceivers" />
          </node>
          <node concept="l2Vlx" id="2jzj96ZwVwf" role="2iSdaV" />
        </node>
        <node concept="ZYGn8" id="2jzj96ZwVwg" role="ZWbT9">
          <node concept="3clFbS" id="2jzj96ZwVwh" role="2VODD2">
            <node concept="3clFbF" id="2jzj96ZwVwi" role="3cqZAp">
              <node concept="Xl_RD" id="2jzj96ZwVwj" role="3clFbG">
                <property role="Xl_RC" value="other" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3EZMnI" id="2jzj96ZwVwk" role="3EZMnx">
        <node concept="3XFhqQ" id="2jzj96ZwVwl" role="3EZMnx" />
        <node concept="3F0ifn" id="2jzj96ZwVwm" role="3EZMnx">
          <property role="3F0ifm" value="content" />
          <node concept="Vb9p2" id="2jzj96ZwVwn" role="3F10Kt" />
        </node>
        <node concept="3F0ifn" id="2jzj96ZwVwo" role="3EZMnx">
          <property role="3F0ifm" value=":" />
        </node>
        <node concept="3F1sOY" id="2jzj96ZwVwp" role="3EZMnx">
          <ref role="1NtTu8" to="hk02:2jzj96ZwVqN" resolve="content" />
        </node>
        <node concept="l2Vlx" id="2jzj96ZwVwq" role="2iSdaV" />
      </node>
      <node concept="3F0ifn" id="2jzj96ZwVwr" role="3EZMnx">
        <property role="3F0ifm" value="}" />
      </node>
    </node>
  </node>
</model>

