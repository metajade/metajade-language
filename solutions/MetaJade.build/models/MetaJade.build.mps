<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:30f811da-9b22-4a6a-979b-1eaa8b9b4314(MetaJade.build)">
  <persistence version="9" />
  <languages>
    <use id="798100da-4f0a-421a-b991-71f8c50ce5d2" name="jetbrains.mps.build" version="0" />
    <use id="0cf935df-4699-4e9c-a132-fa109541cba3" name="jetbrains.mps.build.mps" version="7" />
    <use id="3600cb0a-44dd-4a5b-9968-22924406419e" name="jetbrains.mps.build.mps.tests" version="1" />
  </languages>
  <imports>
    <import index="ffeo" ref="r:874d959d-e3b4-4d04-b931-ca849af130dd(jetbrains.mps.ide.build)" />
    <import index="al5i" ref="r:742f344d-4dc4-4862-992c-4bc94b094870(com.mbeddr.mpsutil.dev.build)" />
    <import index="ip48" ref="r:c3d6ae0c-8b10-477f-a3e9-5dc8700ceb13(org.iets3.opensource.build.build)" />
  </imports>
  <registry>
    <language id="479c7a8c-02f9-43b5-9139-d910cb22f298" name="jetbrains.mps.core.xml">
      <concept id="6666499814681415858" name="jetbrains.mps.core.xml.structure.XmlElement" flags="ng" index="2pNNFK">
        <property id="6666499814681415862" name="tagName" index="2pNNFO" />
        <child id="1622293396948928802" name="content" index="3o6s8t" />
      </concept>
      <concept id="1622293396948952339" name="jetbrains.mps.core.xml.structure.XmlText" flags="nn" index="3o6iSG">
        <property id="1622293396948953704" name="value" index="3o6i5n" />
      </concept>
    </language>
    <language id="3600cb0a-44dd-4a5b-9968-22924406419e" name="jetbrains.mps.build.mps.tests">
      <concept id="4560297596904469357" name="jetbrains.mps.build.mps.tests.structure.BuildAspect_MpsTestModules" flags="nn" index="22LTRH">
        <child id="4560297596904469360" name="modules" index="22LTRK" />
      </concept>
      <concept id="4560297596904469362" name="jetbrains.mps.build.mps.tests.structure.BuildMps_TestModule" flags="nn" index="22LTRM">
        <reference id="4560297596904469363" name="module" index="22LTRN" />
      </concept>
      <concept id="4005526075820600484" name="jetbrains.mps.build.mps.tests.structure.BuildModuleTestsPlugin" flags="ng" index="1gjT0q" />
    </language>
    <language id="798100da-4f0a-421a-b991-71f8c50ce5d2" name="jetbrains.mps.build">
      <concept id="5481553824944787378" name="jetbrains.mps.build.structure.BuildSourceProjectRelativePath" flags="ng" index="55IIr" />
      <concept id="9126048691955220717" name="jetbrains.mps.build.structure.BuildLayout_File" flags="ng" index="28jJK3">
        <child id="9126048691955220762" name="path" index="28jJRO" />
      </concept>
      <concept id="7321017245476976379" name="jetbrains.mps.build.structure.BuildRelativePath" flags="ng" index="iG8Mu">
        <child id="7321017245477039051" name="compositePart" index="iGT6I" />
      </concept>
      <concept id="4993211115183325728" name="jetbrains.mps.build.structure.BuildProjectDependency" flags="ng" index="2sgV4H">
        <reference id="5617550519002745380" name="script" index="1l3spb" />
        <child id="4129895186893471026" name="artifacts" index="2JcizS" />
      </concept>
      <concept id="927724900262033858" name="jetbrains.mps.build.structure.BuildSource_JavaOptions" flags="ng" index="2_Ic$z">
        <property id="927724900262033861" name="generateDebugInfo" index="2_Ic$$" />
        <property id="927724900262033862" name="copyResources" index="2_Ic$B" />
        <child id="927724900262033863" name="resourceSelectors" index="2_Ic$A" />
      </concept>
      <concept id="4380385936562003279" name="jetbrains.mps.build.structure.BuildString" flags="ng" index="NbPM2">
        <child id="4903714810883783243" name="parts" index="3MwsjC" />
      </concept>
      <concept id="8618885170173601777" name="jetbrains.mps.build.structure.BuildCompositePath" flags="nn" index="2Ry0Ak">
        <property id="8618885170173601779" name="head" index="2Ry0Am" />
        <child id="8618885170173601778" name="tail" index="2Ry0An" />
      </concept>
      <concept id="6647099934206700647" name="jetbrains.mps.build.structure.BuildJavaPlugin" flags="ng" index="10PD9b" />
      <concept id="7389400916848136194" name="jetbrains.mps.build.structure.BuildFolderMacro" flags="ng" index="398rNT">
        <child id="7389400916848144618" name="defaultPath" index="398pKh" />
      </concept>
      <concept id="7389400916848153117" name="jetbrains.mps.build.structure.BuildSourceMacroRelativePath" flags="ng" index="398BVA">
        <reference id="7389400916848153130" name="macro" index="398BVh" />
      </concept>
      <concept id="5617550519002745364" name="jetbrains.mps.build.structure.BuildLayout" flags="ng" index="1l3spV" />
      <concept id="5617550519002745363" name="jetbrains.mps.build.structure.BuildProject" flags="ng" index="1l3spW">
        <property id="4915877860348071612" name="fileName" index="turDy" />
        <property id="5204048710541015587" name="internalBaseDirectory" index="2DA0ip" />
        <child id="6647099934206700656" name="plugins" index="10PD9s" />
        <child id="7389400916848080626" name="parts" index="3989C9" />
        <child id="3542413272732620719" name="aspects" index="1hWBAP" />
        <child id="5617550519002745381" name="dependencies" index="1l3spa" />
        <child id="5617550519002745378" name="macros" index="1l3spd" />
        <child id="5617550519002745372" name="layout" index="1l3spN" />
      </concept>
      <concept id="8654221991637384182" name="jetbrains.mps.build.structure.BuildFileIncludesSelector" flags="ng" index="3qWCbU">
        <property id="8654221991637384184" name="pattern" index="3qWCbO" />
      </concept>
      <concept id="4701820937132344003" name="jetbrains.mps.build.structure.BuildLayout_Container" flags="ng" index="1y1bJS">
        <child id="7389400916848037006" name="children" index="39821P" />
      </concept>
      <concept id="841011766566059607" name="jetbrains.mps.build.structure.BuildStringNotEmpty" flags="ng" index="3_J27D" />
      <concept id="5248329904288051100" name="jetbrains.mps.build.structure.BuildFileIncludeSelector" flags="ng" index="3LWZYx">
        <property id="5248329904288051101" name="pattern" index="3LWZYw" />
      </concept>
      <concept id="5248329904287794596" name="jetbrains.mps.build.structure.BuildInputFiles" flags="ng" index="3LXTmp">
        <child id="5248329904287794598" name="dir" index="3LXTmr" />
        <child id="5248329904287794679" name="selectors" index="3LXTna" />
      </concept>
      <concept id="4903714810883702019" name="jetbrains.mps.build.structure.BuildTextStringPart" flags="ng" index="3Mxwew">
        <property id="4903714810883755350" name="text" index="3MwjfP" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="0cf935df-4699-4e9c-a132-fa109541cba3" name="jetbrains.mps.build.mps">
      <concept id="6592112598314586625" name="jetbrains.mps.build.mps.structure.BuildMps_IdeaPluginGroup" flags="ng" index="m$f5U">
        <reference id="6592112598314586626" name="group" index="m$f5T" />
      </concept>
      <concept id="6592112598314498932" name="jetbrains.mps.build.mps.structure.BuildMps_IdeaPlugin" flags="ng" index="m$_wf">
        <property id="6592112598314498927" name="id" index="m$_wk" />
        <child id="1359186315025500371" name="xml" index="20twgj" />
        <child id="6592112598314498931" name="version" index="m$_w8" />
        <child id="6592112598314499050" name="content" index="m$_yh" />
        <child id="6592112598314499028" name="dependencies" index="m$_yJ" />
        <child id="6592112598314499021" name="name" index="m$_yQ" />
        <child id="6592112598314855574" name="containerName" index="m_cZH" />
      </concept>
      <concept id="6592112598314498926" name="jetbrains.mps.build.mps.structure.BuildMpsLayout_Plugin" flags="ng" index="m$_wl">
        <reference id="6592112598314801433" name="plugin" index="m_rDy" />
        <child id="3570488090019868128" name="packagingType" index="pUk7w" />
      </concept>
      <concept id="6592112598314499027" name="jetbrains.mps.build.mps.structure.BuildMps_IdeaPluginDependency" flags="ng" index="m$_yC">
        <reference id="6592112598314499066" name="target" index="m$_y1" />
      </concept>
      <concept id="3570488090019868065" name="jetbrains.mps.build.mps.structure.BuildMpsLayout_AutoPluginLayoutType" flags="ng" index="pUk6x" />
      <concept id="1500819558095907805" name="jetbrains.mps.build.mps.structure.BuildMps_Group" flags="ng" index="2G$12M">
        <child id="1500819558095907806" name="modules" index="2G$12L" />
      </concept>
      <concept id="8971171305100238972" name="jetbrains.mps.build.mps.structure.BuildMps_ModuleDependencyTargetLanguage" flags="ng" index="Rbm2T">
        <reference id="3189788309731922643" name="language" index="1E1Vl2" />
      </concept>
      <concept id="868032131020265945" name="jetbrains.mps.build.mps.structure.BuildMPSPlugin" flags="ng" index="3b7kt6" />
      <concept id="5253498789149381388" name="jetbrains.mps.build.mps.structure.BuildMps_Module" flags="ng" index="3bQrTs">
        <child id="5253498789149547825" name="sources" index="3bR31x" />
        <child id="5253498789149547704" name="dependencies" index="3bR37C" />
      </concept>
      <concept id="5253498789149585690" name="jetbrains.mps.build.mps.structure.BuildMps_ModuleDependencyOnModule" flags="ng" index="3bR9La">
        <property id="5253498789149547713" name="reexport" index="3bR36h" />
        <reference id="5253498789149547705" name="module" index="3bR37D" />
      </concept>
      <concept id="763829979718664966" name="jetbrains.mps.build.mps.structure.BuildMps_ModuleResources" flags="ng" index="3rtmxn">
        <child id="763829979718664967" name="files" index="3rtmxm" />
      </concept>
      <concept id="5507251971038816436" name="jetbrains.mps.build.mps.structure.BuildMps_Generator" flags="ng" index="1yeLz9" />
      <concept id="4278635856200817744" name="jetbrains.mps.build.mps.structure.BuildMps_ModuleModelRoot" flags="ng" index="1BupzO">
        <property id="8137134783396907368" name="convert2binary" index="1Hdu6h" />
        <property id="8137134783396676838" name="extracted" index="1HemKv" />
        <property id="2889113830911481881" name="deployFolderName" index="3ZfqAx" />
        <child id="8137134783396676835" name="location" index="1HemKq" />
      </concept>
      <concept id="4278635856200826393" name="jetbrains.mps.build.mps.structure.BuildMps_ModuleDependencyJar" flags="ng" index="1BurEX">
        <child id="4278635856200826394" name="path" index="1BurEY" />
      </concept>
      <concept id="4278635856200794926" name="jetbrains.mps.build.mps.structure.BuildMps_ModuleDependencyExtendLanguage" flags="ng" index="1Busua">
        <reference id="4278635856200794928" name="language" index="1Busuk" />
      </concept>
      <concept id="3189788309731981027" name="jetbrains.mps.build.mps.structure.BuildMps_ModuleSolutionRuntime" flags="ng" index="1E0d5M">
        <reference id="3189788309731981028" name="solution" index="1E0d5P" />
      </concept>
      <concept id="3189788309731840247" name="jetbrains.mps.build.mps.structure.BuildMps_Solution" flags="ng" index="1E1JtA">
        <property id="269707337715731330" name="sourcesKind" index="aoJFB" />
      </concept>
      <concept id="3189788309731840248" name="jetbrains.mps.build.mps.structure.BuildMps_Language" flags="ng" index="1E1JtD">
        <child id="3189788309731917348" name="runtime" index="1E1XAP" />
        <child id="9200313594498201639" name="generator" index="1TViLv" />
      </concept>
      <concept id="322010710375794190" name="jetbrains.mps.build.mps.structure.BuildMps_DevKit" flags="ng" index="3LEwk6">
        <child id="322010710375805250" name="extends" index="3LEz9a" />
        <child id="322010710375832962" name="exports" index="3LEDUa" />
      </concept>
      <concept id="322010710375805242" name="jetbrains.mps.build.mps.structure.BuildMps_DevKitRef" flags="ng" index="3LEz8M">
        <reference id="322010710375805243" name="devkit" index="3LEz8N" />
      </concept>
      <concept id="322010710375832938" name="jetbrains.mps.build.mps.structure.BuildMps_DevKitExportLanguage" flags="ng" index="3LEDTy">
        <reference id="322010710375832947" name="language" index="3LEDTV" />
      </concept>
      <concept id="322010710375832954" name="jetbrains.mps.build.mps.structure.BuildMps_DevKitExportSolution" flags="ng" index="3LEDTM">
        <reference id="322010710375832955" name="solution" index="3LEDTN" />
      </concept>
      <concept id="322010710375871467" name="jetbrains.mps.build.mps.structure.BuildMps_AbstractModule" flags="ng" index="3LEN3z">
        <property id="8369506495128725901" name="compact" index="BnDLt" />
        <property id="322010710375892619" name="uuid" index="3LESm3" />
        <child id="322010710375956261" name="path" index="3LF7KH" />
      </concept>
      <concept id="7259033139236285166" name="jetbrains.mps.build.mps.structure.BuildMps_ExtractedModuleDependency" flags="nn" index="1SiIV0">
        <child id="7259033139236285167" name="dependency" index="1SiIV1" />
      </concept>
    </language>
  </registry>
  <node concept="1l3spW" id="3Laolq23y$x">
    <property role="TrG5h" value="metajade.languages" />
    <property role="turDy" value="build.xml" />
    <property role="2DA0ip" value="../../" />
    <node concept="2_Ic$z" id="5kcim5Uhld$" role="3989C9">
      <property role="2_Ic$$" value="true" />
      <property role="2_Ic$B" value="true" />
      <node concept="3LWZYx" id="5kcim5UhlkF" role="2_Ic$A">
        <property role="3LWZYw" value="**/*.png" />
      </node>
    </node>
    <node concept="10PD9b" id="3Laolq23y$y" role="10PD9s" />
    <node concept="3b7kt6" id="3Laolq23y$z" role="10PD9s" />
    <node concept="1gjT0q" id="4eY$d6AuKgE" role="10PD9s" />
    <node concept="398rNT" id="3Laolq23y$B" role="1l3spd">
      <property role="TrG5h" value="project_home" />
      <node concept="55IIr" id="3Laolq23yCr" role="398pKh" />
    </node>
    <node concept="398rNT" id="3Laolq23y$$" role="1l3spd">
      <property role="TrG5h" value="mps_home" />
      <node concept="398BVA" id="6cOMG4S7jKK" role="398pKh">
        <ref role="398BVh" node="3Laolq23y$B" resolve="project_home" />
        <node concept="2Ry0Ak" id="6cOMG4S7jL6" role="iGT6I">
          <property role="2Ry0Am" value="build" />
          <node concept="2Ry0Ak" id="6cOMG4S7jLF" role="2Ry0An">
            <property role="2Ry0Am" value="mps" />
          </node>
        </node>
      </node>
    </node>
    <node concept="398rNT" id="3Laolq23yCL" role="1l3spd">
      <property role="TrG5h" value="mps.macro.project_home" />
      <node concept="398BVA" id="3Laolq23yE3" role="398pKh">
        <ref role="398BVh" node="3Laolq23y$B" resolve="project_home" />
      </node>
    </node>
    <node concept="398rNT" id="3Laolq23yEz" role="1l3spd">
      <property role="TrG5h" value="mbeddr_artifacts" />
      <node concept="398BVA" id="1b83WHYoW8A" role="398pKh">
        <ref role="398BVh" node="3Laolq23y$B" resolve="project_home" />
        <node concept="2Ry0Ak" id="1b83WHYoW8S" role="iGT6I">
          <property role="2Ry0Am" value="build" />
          <node concept="2Ry0Ak" id="1b83WHYoW9E" role="2Ry0An">
            <property role="2Ry0Am" value="dependencies" />
            <node concept="2Ry0Ak" id="1b83WHYoWad" role="2Ry0An">
              <property role="2Ry0Am" value="com.mbeddr.platform" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="398rNT" id="3Laolq23yHr" role="1l3spd">
      <property role="TrG5h" value="iets3_artifacts" />
      <node concept="398BVA" id="3Laolq23yIp" role="398pKh">
        <ref role="398BVh" node="3Laolq23y$B" resolve="project_home" />
        <node concept="2Ry0Ak" id="1fSM__PZYjY" role="iGT6I">
          <property role="2Ry0Am" value="build" />
          <node concept="2Ry0Ak" id="6QHXV$nWyVs" role="2Ry0An">
            <property role="2Ry0Am" value="dependencies" />
            <node concept="2Ry0Ak" id="6QHXV$nWyVt" role="2Ry0An">
              <property role="2Ry0Am" value="org.iets3.opensource" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2sgV4H" id="3Laolq23y$_" role="1l3spa">
      <ref role="1l3spb" to="ffeo:3IKDaVZmzS6" resolve="mps" />
      <node concept="398BVA" id="3Laolq23y$A" role="2JcizS">
        <ref role="398BVh" node="3Laolq23y$$" resolve="mps_home" />
      </node>
    </node>
    <node concept="2sgV4H" id="3Laolq23yV_" role="1l3spa">
      <ref role="1l3spb" to="al5i:3AVJcIMlF8l" resolve="com.mbeddr.platform" />
      <node concept="398BVA" id="3Laolq23yVQ" role="2JcizS">
        <ref role="398BVh" node="3Laolq23yEz" resolve="mbeddr_artifacts" />
      </node>
    </node>
    <node concept="2sgV4H" id="3Laolq23yU_" role="1l3spa">
      <ref role="1l3spb" to="ip48:5wLtKNeSRPl" resolve="org.iets3.opensource" />
      <node concept="398BVA" id="3Laolq23yUQ" role="2JcizS">
        <ref role="398BVh" node="3Laolq23yHr" resolve="iets3_artifacts" />
      </node>
    </node>
    <node concept="1l3spV" id="3Laolq23yA4" role="1l3spN">
      <node concept="m$_wl" id="3Laolq23yA8" role="39821P">
        <ref role="m_rDy" node="3Laolq23y_R" resolve="pikalab.ds.metajade" />
        <node concept="pUk6x" id="3Laolq23yA9" role="pUk7w" />
        <node concept="28jJK3" id="3Laolq23z3H" role="39821P">
          <node concept="398BVA" id="3Laolq23z40" role="28jJRO">
            <ref role="398BVh" node="3Laolq23y$B" resolve="project_home" />
            <node concept="2Ry0Ak" id="3Laolq23z4i" role="iGT6I">
              <property role="2Ry0Am" value="solutions" />
              <node concept="2Ry0Ak" id="3Laolq23z4m" role="2Ry0An">
                <property role="2Ry0Am" value="MetaJade.java.stubs" />
                <node concept="2Ry0Ak" id="3Laolq23z4o" role="2Ry0An">
                  <property role="2Ry0Am" value="lib" />
                  <node concept="2Ry0Ak" id="3Laolq23z4r" role="2Ry0An">
                    <property role="2Ry0Am" value="jade-4.6.0.jar" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="m$_wf" id="3Laolq23y_R" role="3989C9">
      <property role="m$_wk" value="metajade.metajade.languages" />
      <node concept="3_J27D" id="3Laolq23y_S" role="m$_yQ">
        <node concept="3Mxwew" id="3Laolq23y_T" role="3MwsjC">
          <property role="3MwjfP" value="metajade.metajade.languages" />
        </node>
      </node>
      <node concept="3_J27D" id="3Laolq23y_U" role="m$_w8">
        <node concept="3Mxwew" id="3Laolq23y_V" role="3MwsjC">
          <property role="3MwjfP" value="1.0" />
        </node>
      </node>
      <node concept="m$f5U" id="3Laolq23y_W" role="m$_yh">
        <ref role="m$f5T" node="3Laolq23y_Q" resolve="MetaJade" />
      </node>
      <node concept="m$_yC" id="3Laolq23y_X" role="m$_yJ">
        <ref role="m$_y1" to="ffeo:4k71ibbKLe8" resolve="jetbrains.mps.core" />
      </node>
      <node concept="m$_yC" id="3Laolq23R53" role="m$_yJ">
        <ref role="m$_y1" to="ffeo:6WtY9M1bDO_" resolve="jetbrains.mps.ide.java" />
      </node>
      <node concept="m$_yC" id="1fSM__PZYqY" role="m$_yJ">
        <ref role="m$_y1" to="ffeo:5HVSRHdVm9a" resolve="jetbrains.mps.build" />
      </node>
      <node concept="m$_yC" id="3Laolq23R5y" role="m$_yJ">
        <ref role="m$_y1" to="al5i:33r_JpZ6k_l" resolve="com.mbeddr.platform.build" />
      </node>
      <node concept="m$_yC" id="3Laolq23R6g" role="m$_yJ">
        <ref role="m$_y1" to="ip48:5Ky8UT4nhy3" resolve="org.iets3.core.expr.genjava" />
      </node>
      <node concept="m$_yC" id="7OjSnjRDI08" role="m$_yJ">
        <ref role="m$_y1" to="ip48:64GCIgM55Fn" resolve="org.iets3.build.os" />
      </node>
      <node concept="3_J27D" id="3Laolq23y_Y" role="m_cZH">
        <node concept="3Mxwew" id="3Laolq23y_Z" role="3MwsjC">
          <property role="3MwjfP" value="metajade.metajade.languages" />
        </node>
      </node>
      <node concept="2pNNFK" id="3Laolq23yA0" role="20twgj">
        <property role="2pNNFO" value="depends" />
        <node concept="3o6iSG" id="3Laolq23yA1" role="3o6s8t">
          <property role="3o6i5n" value="com.intellij.modules.platform" />
        </node>
      </node>
    </node>
    <node concept="2G$12M" id="3Laolq23y_Q" role="3989C9">
      <property role="TrG5h" value="MetaJade" />
      <node concept="1E1JtD" id="3Laolq23y$H" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="MetaJade.genjava" />
        <property role="3LESm3" value="f599e1b8-cf96-411d-bceb-94edfc2d9045" />
        <node concept="398BVA" id="7ml$O3eLqqY" role="3LF7KH">
          <ref role="398BVh" node="3Laolq23y$B" resolve="project_home" />
          <node concept="2Ry0Ak" id="7ml$O3eLqr3" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="7ml$O3eLqr4" role="2Ry0An">
              <property role="2Ry0Am" value="MetaJade.genjava" />
              <node concept="2Ry0Ak" id="7ml$O3eLqr5" role="2Ry0An">
                <property role="2Ry0Am" value="MetaJade.genjava.mpl" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yAa" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yAb" role="1SiIV1">
            <property role="3bR36h" value="true" />
            <ref role="3bR37D" to="ffeo:mXGwHwhVPj" resolve="JDK" />
          </node>
        </node>
        <node concept="1BupzO" id="3Laolq23yAg" role="3bR31x">
          <property role="3ZfqAx" value="models" />
          <property role="1Hdu6h" value="true" />
          <property role="1HemKv" value="true" />
          <node concept="3LXTmp" id="3Laolq23yWB" role="1HemKq">
            <node concept="398BVA" id="3Laolq23yWr" role="3LXTmr">
              <ref role="398BVh" node="3Laolq23y$B" resolve="project_home" />
              <node concept="2Ry0Ak" id="3Laolq23yWs" role="iGT6I">
                <property role="2Ry0Am" value="languages" />
                <node concept="2Ry0Ak" id="3Laolq23yWt" role="2Ry0An">
                  <property role="2Ry0Am" value="MetaJade.genjava" />
                  <node concept="2Ry0Ak" id="3Laolq23yWu" role="2Ry0An">
                    <property role="2Ry0Am" value="models" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3qWCbU" id="3Laolq23yWC" role="3LXTna">
              <property role="3qWCbO" value="**/*.mps, **/*.mpsr, **/.model" />
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yAj" role="3bR37C">
          <node concept="Rbm2T" id="3Laolq23yAk" role="1SiIV1">
            <ref role="1E1Vl2" to="ffeo:7Kfy9QB6KYb" resolve="jetbrains.mps.baseLanguage" />
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yAl" role="3bR37C">
          <node concept="Rbm2T" id="3Laolq23yAm" role="1SiIV1">
            <ref role="1E1Vl2" to="ffeo:7Kfy9QB6L9O" resolve="jetbrains.mps.lang.smodel" />
          </node>
        </node>
        <node concept="1yeLz9" id="3Laolq23yAn" role="1TViLv">
          <property role="TrG5h" value="MetaJade.genjava.generator" />
          <property role="3LESm3" value="e3776ee5-61ab-443c-b012-15d2bc5e7665" />
          <node concept="1SiIV0" id="3Laolq23yAo" role="3bR37C">
            <node concept="3bR9La" id="3Laolq23yAp" role="1SiIV1">
              <ref role="3bR37D" to="ffeo:7Kfy9QB6LaO" resolve="jetbrains.mps.lang.structure" />
            </node>
          </node>
          <node concept="1SiIV0" id="3Laolq23yAq" role="3bR37C">
            <node concept="3bR9La" id="3Laolq23yAr" role="1SiIV1">
              <ref role="3bR37D" to="ffeo:mXGwHwhVPj" resolve="JDK" />
            </node>
          </node>
          <node concept="1SiIV0" id="3Laolq23yAs" role="3bR37C">
            <node concept="3bR9La" id="3Laolq23yAt" role="1SiIV1">
              <ref role="3bR37D" to="ffeo:7Kfy9QB6KXW" resolve="jetbrains.mps.lang.core" />
            </node>
          </node>
          <node concept="1SiIV0" id="3Laolq23yAu" role="3bR37C">
            <node concept="3bR9La" id="3Laolq23yAv" role="1SiIV1">
              <ref role="3bR37D" to="ffeo:7Kfy9QB6KYb" resolve="jetbrains.mps.baseLanguage" />
            </node>
          </node>
          <node concept="1BupzO" id="3Laolq23yA_" role="3bR31x">
            <property role="3ZfqAx" value="generator/templates" />
            <property role="1Hdu6h" value="true" />
            <property role="1HemKv" value="true" />
            <node concept="3LXTmp" id="3Laolq23yXs" role="1HemKq">
              <node concept="398BVA" id="3Laolq23yXd" role="3LXTmr">
                <ref role="398BVh" node="3Laolq23y$B" resolve="project_home" />
                <node concept="2Ry0Ak" id="3Laolq23yXe" role="iGT6I">
                  <property role="2Ry0Am" value="languages" />
                  <node concept="2Ry0Ak" id="3Laolq23yXf" role="2Ry0An">
                    <property role="2Ry0Am" value="MetaJade.genjava" />
                    <node concept="2Ry0Ak" id="3Laolq23yXg" role="2Ry0An">
                      <property role="2Ry0Am" value="generator" />
                      <node concept="2Ry0Ak" id="3Laolq23yXh" role="2Ry0An">
                        <property role="2Ry0Am" value="templates" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3qWCbU" id="3Laolq23yXt" role="3LXTna">
                <property role="3qWCbO" value="**/*.mps, **/*.mpsr, **/.model" />
              </node>
            </node>
          </node>
          <node concept="1SiIV0" id="3Laolq23yWJ" role="3bR37C">
            <node concept="3bR9La" id="3Laolq23yWK" role="1SiIV1">
              <property role="3bR36h" value="true" />
              <ref role="3bR37D" node="3Laolq23y_n" resolve="MetaJade.java.stubs" />
            </node>
          </node>
          <node concept="1SiIV0" id="3Laolq23yWL" role="3bR37C">
            <node concept="3bR9La" id="3Laolq23yWM" role="1SiIV1">
              <ref role="3bR37D" to="ip48:5wLtKNeSRPD" resolve="org.iets3.core.expr.base" />
            </node>
          </node>
          <node concept="1SiIV0" id="3Laolq23yWN" role="3bR37C">
            <node concept="3bR9La" id="3Laolq23yWO" role="1SiIV1">
              <ref role="3bR37D" node="3Laolq23y$T" resolve="MetaJade.agent" />
            </node>
          </node>
          <node concept="1SiIV0" id="3Laolq23yWP" role="3bR37C">
            <node concept="3bR9La" id="3Laolq23yWQ" role="1SiIV1">
              <ref role="3bR37D" to="ip48:7jAOwAVRc2S" resolve="org.iets3.core.expr.simpleTypes.runtime" />
            </node>
          </node>
          <node concept="1SiIV0" id="3Laolq23yWR" role="3bR37C">
            <node concept="3bR9La" id="3Laolq23yWS" role="1SiIV1">
              <ref role="3bR37D" node="3Laolq23y_h" resolve="MetaJade.java.runtime" />
            </node>
          </node>
          <node concept="1SiIV0" id="3Laolq23yWT" role="3bR37C">
            <node concept="3bR9La" id="3Laolq23yWU" role="1SiIV1">
              <ref role="3bR37D" to="ip48:6JPXQMQs0pX" resolve="org.iets3.core.expr.collections" />
            </node>
          </node>
          <node concept="1SiIV0" id="3Laolq23yWV" role="3bR37C">
            <node concept="3bR9La" id="3Laolq23yWW" role="1SiIV1">
              <ref role="3bR37D" to="ip48:2uR5X5azttH" resolve="org.iets3.core.expr.toplevel" />
            </node>
          </node>
          <node concept="1SiIV0" id="3Laolq23yWZ" role="3bR37C">
            <node concept="3bR9La" id="3Laolq23yX0" role="1SiIV1">
              <ref role="3bR37D" to="ip48:5wLtKNeSRQd" resolve="org.iets3.core.expr.simpleTypes" />
            </node>
          </node>
          <node concept="1SiIV0" id="3Laolq23yX1" role="3bR37C">
            <node concept="3bR9La" id="3Laolq23yX2" role="1SiIV1">
              <ref role="3bR37D" node="3Laolq23y$N" resolve="MetaJade.ontology" />
            </node>
          </node>
          <node concept="1SiIV0" id="3Laolq23yX3" role="3bR37C">
            <node concept="3bR9La" id="3Laolq23yX4" role="1SiIV1">
              <property role="3bR36h" value="true" />
              <ref role="3bR37D" to="ip48:26tZ$Z4rphz" resolve="org.iets3.core.expr.genjava.functionalJava" />
            </node>
          </node>
          <node concept="1SiIV0" id="3Laolq23yX5" role="3bR37C">
            <node concept="3bR9La" id="3Laolq23yX6" role="1SiIV1">
              <ref role="3bR37D" to="ip48:49WTic8jAD5" resolve="org.iets3.core.expr.lambda" />
            </node>
          </node>
          <node concept="1SiIV0" id="3Laolq23yX7" role="3bR37C">
            <node concept="3bR9La" id="3Laolq23yX8" role="1SiIV1">
              <ref role="3bR37D" node="3Laolq23y_5" resolve="MetaJade.core" />
            </node>
          </node>
          <node concept="1SiIV0" id="3Laolq23yX9" role="3bR37C">
            <node concept="3bR9La" id="3Laolq23yXa" role="1SiIV1">
              <ref role="3bR37D" node="3Laolq23y$Z" resolve="MetaJade.aclmessage" />
            </node>
          </node>
          <node concept="1SiIV0" id="3Laolq23yXb" role="3bR37C">
            <node concept="3bR9La" id="3Laolq23yXc" role="1SiIV1">
              <ref role="3bR37D" node="3Laolq23y_b" resolve="MetaJade.behaviour" />
            </node>
          </node>
          <node concept="1SiIV0" id="3Laolq23yXv" role="3bR37C">
            <node concept="3bR9La" id="3Laolq23yXu" role="1SiIV1">
              <ref role="3bR37D" to="ip48:26tZ$Z4rnW$" resolve="org.iets3.core.expr.genjava.toplevel#1899408283182158123" />
            </node>
          </node>
          <node concept="1SiIV0" id="3Laolq23yXx" role="3bR37C">
            <node concept="3bR9La" id="3Laolq23yXw" role="1SiIV1">
              <ref role="3bR37D" to="ip48:26tZ$Z4rnV1" resolve="org.iets3.core.expr.genjava.base#8286534136181746510" />
            </node>
          </node>
        </node>
        <node concept="3rtmxn" id="3Laolq23yW6" role="3bR31x">
          <node concept="3LXTmp" id="3Laolq23yW7" role="3rtmxm">
            <node concept="398BVA" id="4tsgqAdNQSE" role="3LXTmr">
              <ref role="398BVh" node="3Laolq23y$B" resolve="project_home" />
              <node concept="2Ry0Ak" id="4tsgqAdNQSI" role="iGT6I">
                <property role="2Ry0Am" value="languages" />
                <node concept="2Ry0Ak" id="4tsgqAdNQSJ" role="2Ry0An">
                  <property role="2Ry0Am" value="MetaJade.genjava" />
                </node>
              </node>
            </node>
            <node concept="3qWCbU" id="3Laolq23yWc" role="3LXTna">
              <property role="3qWCbO" value="icons/**, resources/**" />
            </node>
          </node>
        </node>
        <node concept="1E0d5M" id="3Laolq23yWD" role="1E1XAP">
          <ref role="1E0d5P" node="3Laolq23y_h" resolve="MetaJade.java.runtime" />
        </node>
        <node concept="1E0d5M" id="3Laolq23yWE" role="1E1XAP">
          <ref role="1E0d5P" node="3Laolq23y_n" resolve="MetaJade.java.stubs" />
        </node>
        <node concept="1SiIV0" id="58wlk62_deE" role="3bR37C">
          <node concept="Rbm2T" id="58wlk62_deF" role="1SiIV1">
            <ref role="1E1Vl2" to="ffeo:7Kfy9QB6L0h" resolve="jetbrains.mps.baseLanguage.collections" />
          </node>
        </node>
        <node concept="1SiIV0" id="58wlk62_deG" role="3bR37C">
          <node concept="Rbm2T" id="58wlk62_deH" role="1SiIV1">
            <ref role="1E1Vl2" to="ffeo:7Kfy9QB6KZG" resolve="jetbrains.mps.baseLanguage.closures" />
          </node>
        </node>
      </node>
      <node concept="1E1JtD" id="3Laolq23y$N" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="MetaJade.ontology" />
        <property role="3LESm3" value="ff6c34ae-1ca3-455f-96b3-8f28007794c9" />
        <node concept="398BVA" id="7ml$O3eLqre" role="3LF7KH">
          <ref role="398BVh" node="3Laolq23y$B" resolve="project_home" />
          <node concept="2Ry0Ak" id="7ml$O3eLqrj" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="7ml$O3eLqrk" role="2Ry0An">
              <property role="2Ry0Am" value="MetaJade.ontology" />
              <node concept="2Ry0Ak" id="7ml$O3eLqrl" role="2Ry0An">
                <property role="2Ry0Am" value="MetaJade.ontology.mpl" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yAC" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yAD" role="1SiIV1">
            <ref role="3bR37D" to="ffeo:7Kfy9QB6KXW" resolve="jetbrains.mps.lang.core" />
          </node>
        </node>
        <node concept="1BupzO" id="3Laolq23yAI" role="3bR31x">
          <property role="3ZfqAx" value="models" />
          <property role="1Hdu6h" value="true" />
          <property role="1HemKv" value="true" />
          <node concept="3LXTmp" id="3Laolq23yXO" role="1HemKq">
            <node concept="398BVA" id="3Laolq23yXC" role="3LXTmr">
              <ref role="398BVh" node="3Laolq23y$B" resolve="project_home" />
              <node concept="2Ry0Ak" id="3Laolq23yXD" role="iGT6I">
                <property role="2Ry0Am" value="languages" />
                <node concept="2Ry0Ak" id="3Laolq23yXE" role="2Ry0An">
                  <property role="2Ry0Am" value="MetaJade.ontology" />
                  <node concept="2Ry0Ak" id="3Laolq23yXF" role="2Ry0An">
                    <property role="2Ry0Am" value="models" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3qWCbU" id="3Laolq23yXP" role="3LXTna">
              <property role="3qWCbO" value="**/*.mps, **/*.mpsr, **/.model" />
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yXy" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yXz" role="1SiIV1">
            <ref role="3bR37D" to="ip48:5wLtKNeSRPD" resolve="org.iets3.core.expr.base" />
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yX$" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yX_" role="1SiIV1">
            <ref role="3bR37D" to="ip48:2uR5X5azttH" resolve="org.iets3.core.expr.toplevel" />
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yXA" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yXB" role="1SiIV1">
            <ref role="3bR37D" to="ip48:5wLtKNeSRQd" resolve="org.iets3.core.expr.simpleTypes" />
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yXQ" role="3bR37C">
          <node concept="1Busua" id="3Laolq23yXR" role="1SiIV1">
            <ref role="1Busuk" to="ip48:2uR5X5azttH" resolve="org.iets3.core.expr.toplevel" />
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yXS" role="3bR37C">
          <node concept="1Busua" id="3Laolq23yXT" role="1SiIV1">
            <ref role="1Busuk" node="3Laolq23y_5" resolve="MetaJade.core" />
          </node>
        </node>
        <node concept="3rtmxn" id="3Laolq23z1Z" role="3bR31x">
          <node concept="3LXTmp" id="3Laolq23z20" role="3rtmxm">
            <node concept="398BVA" id="4tsgqAdNQSi" role="3LXTmr">
              <ref role="398BVh" node="3Laolq23y$B" resolve="project_home" />
              <node concept="2Ry0Ak" id="4tsgqAdNQSm" role="iGT6I">
                <property role="2Ry0Am" value="languages" />
                <node concept="2Ry0Ak" id="4tsgqAdNQSn" role="2Ry0An">
                  <property role="2Ry0Am" value="MetaJade.ontology" />
                </node>
              </node>
            </node>
            <node concept="3qWCbU" id="3Laolq23z25" role="3LXTna">
              <property role="3qWCbO" value="icons/**, resources/**" />
            </node>
          </node>
        </node>
      </node>
      <node concept="1E1JtD" id="3Laolq23y$T" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="MetaJade.agent" />
        <property role="3LESm3" value="32367449-d150-4018-8690-20f09bf1abe2" />
        <node concept="398BVA" id="7ml$O3eLqru" role="3LF7KH">
          <ref role="398BVh" node="3Laolq23y$B" resolve="project_home" />
          <node concept="2Ry0Ak" id="7ml$O3eLqrz" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="7ml$O3eLqr$" role="2Ry0An">
              <property role="2Ry0Am" value="MetaJade.agent" />
              <node concept="2Ry0Ak" id="7ml$O3eLqr_" role="2Ry0An">
                <property role="2Ry0Am" value="MetaJade.agent.mpl" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yAL" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yAM" role="1SiIV1">
            <ref role="3bR37D" to="ffeo:7Kfy9QB6KXW" resolve="jetbrains.mps.lang.core" />
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yAN" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yAO" role="1SiIV1">
            <ref role="3bR37D" to="ffeo:7Kfy9QB6LfQ" resolve="jetbrains.mps.kernel" />
          </node>
        </node>
        <node concept="1BupzO" id="3Laolq23yAT" role="3bR31x">
          <property role="3ZfqAx" value="models" />
          <property role="1Hdu6h" value="true" />
          <property role="1HemKv" value="true" />
          <node concept="3LXTmp" id="3Laolq23yYg" role="1HemKq">
            <node concept="398BVA" id="3Laolq23yY4" role="3LXTmr">
              <ref role="398BVh" node="3Laolq23y$B" resolve="project_home" />
              <node concept="2Ry0Ak" id="3Laolq23yY5" role="iGT6I">
                <property role="2Ry0Am" value="languages" />
                <node concept="2Ry0Ak" id="3Laolq23yY6" role="2Ry0An">
                  <property role="2Ry0Am" value="MetaJade.agent" />
                  <node concept="2Ry0Ak" id="3Laolq23yY7" role="2Ry0An">
                    <property role="2Ry0Am" value="models" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3qWCbU" id="3Laolq23yYh" role="3LXTna">
              <property role="3qWCbO" value="**/*.mps, **/*.mpsr, **/.model" />
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yXU" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yXV" role="1SiIV1">
            <ref role="3bR37D" to="al5i:$bJ0jguQfr" resolve="com.mbeddr.core.base" />
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yXW" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yXX" role="1SiIV1">
            <ref role="3bR37D" to="ip48:5wLtKNeSRPD" resolve="org.iets3.core.expr.base" />
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yXY" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yXZ" role="1SiIV1">
            <ref role="3bR37D" to="ip48:5wLtKNeSRQd" resolve="org.iets3.core.expr.simpleTypes" />
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yY0" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yY1" role="1SiIV1">
            <ref role="3bR37D" to="ip48:49WTic8jAD5" resolve="org.iets3.core.expr.lambda" />
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yY2" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yY3" role="1SiIV1">
            <ref role="3bR37D" node="3Laolq23y_5" resolve="MetaJade.core" />
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yYi" role="3bR37C">
          <node concept="1Busua" id="3Laolq23yYj" role="1SiIV1">
            <ref role="1Busuk" node="3Laolq23y_5" resolve="MetaJade.core" />
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yYk" role="3bR37C">
          <node concept="1Busua" id="3Laolq23yYl" role="1SiIV1">
            <ref role="1Busuk" to="ip48:5wLtKNeSRPD" resolve="org.iets3.core.expr.base" />
          </node>
        </node>
        <node concept="3rtmxn" id="3Laolq23z2l" role="3bR31x">
          <node concept="3LXTmp" id="3Laolq23z2m" role="3rtmxm">
            <node concept="398BVA" id="4tsgqAdNQRU" role="3LXTmr">
              <ref role="398BVh" node="3Laolq23y$B" resolve="project_home" />
              <node concept="2Ry0Ak" id="4tsgqAdNQRY" role="iGT6I">
                <property role="2Ry0Am" value="languages" />
                <node concept="2Ry0Ak" id="4tsgqAdNQRZ" role="2Ry0An">
                  <property role="2Ry0Am" value="MetaJade.agent" />
                </node>
              </node>
            </node>
            <node concept="3qWCbU" id="3Laolq23z2r" role="3LXTna">
              <property role="3qWCbO" value="icons/**, resources/**" />
            </node>
          </node>
        </node>
      </node>
      <node concept="1E1JtD" id="3Laolq23y$Z" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="MetaJade.aclmessage" />
        <property role="3LESm3" value="68d558ed-66cf-46ac-b353-d5fddcc21f72" />
        <node concept="398BVA" id="7ml$O3eLqrI" role="3LF7KH">
          <ref role="398BVh" node="3Laolq23y$B" resolve="project_home" />
          <node concept="2Ry0Ak" id="7ml$O3eLqrN" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="7ml$O3eLqrO" role="2Ry0An">
              <property role="2Ry0Am" value="MetaJade.aclmessage" />
              <node concept="2Ry0Ak" id="7ml$O3eLqrP" role="2Ry0An">
                <property role="2Ry0Am" value="MetaJade.aclmessage.mpl" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yAW" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yAX" role="1SiIV1">
            <ref role="3bR37D" to="ffeo:7Kfy9QB6KXW" resolve="jetbrains.mps.lang.core" />
          </node>
        </node>
        <node concept="1BupzO" id="3Laolq23yB2" role="3bR31x">
          <property role="3ZfqAx" value="models" />
          <property role="1Hdu6h" value="true" />
          <property role="1HemKv" value="true" />
          <node concept="3LXTmp" id="3Laolq23yYI" role="1HemKq">
            <node concept="398BVA" id="3Laolq23yYy" role="3LXTmr">
              <ref role="398BVh" node="3Laolq23y$B" resolve="project_home" />
              <node concept="2Ry0Ak" id="3Laolq23yYz" role="iGT6I">
                <property role="2Ry0Am" value="languages" />
                <node concept="2Ry0Ak" id="3Laolq23yY$" role="2Ry0An">
                  <property role="2Ry0Am" value="MetaJade.aclmessage" />
                  <node concept="2Ry0Ak" id="3Laolq23yY_" role="2Ry0An">
                    <property role="2Ry0Am" value="models" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3qWCbU" id="3Laolq23yYJ" role="3LXTna">
              <property role="3qWCbO" value="**/*.mps, **/*.mpsr, **/.model" />
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yYm" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yYn" role="1SiIV1">
            <ref role="3bR37D" to="ip48:5wLtKNeSRPD" resolve="org.iets3.core.expr.base" />
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yYo" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yYp" role="1SiIV1">
            <ref role="3bR37D" to="ip48:2uR5X5azttH" resolve="org.iets3.core.expr.toplevel" />
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yYq" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yYr" role="1SiIV1">
            <ref role="3bR37D" to="ip48:5wLtKNeSRQd" resolve="org.iets3.core.expr.simpleTypes" />
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yYs" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yYt" role="1SiIV1">
            <ref role="3bR37D" node="3Laolq23y$N" resolve="MetaJade.ontology" />
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yYu" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yYv" role="1SiIV1">
            <ref role="3bR37D" to="ip48:49WTic8jAD5" resolve="org.iets3.core.expr.lambda" />
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yYw" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yYx" role="1SiIV1">
            <ref role="3bR37D" node="3Laolq23y_5" resolve="MetaJade.core" />
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yYK" role="3bR37C">
          <node concept="1Busua" id="3Laolq23yYL" role="1SiIV1">
            <ref role="1Busuk" node="3Laolq23y_5" resolve="MetaJade.core" />
          </node>
        </node>
        <node concept="3rtmxn" id="3Laolq23z2F" role="3bR31x">
          <node concept="3LXTmp" id="3Laolq23z2G" role="3rtmxm">
            <node concept="398BVA" id="4tsgqAdNQT2" role="3LXTmr">
              <ref role="398BVh" node="3Laolq23y$B" resolve="project_home" />
              <node concept="2Ry0Ak" id="4tsgqAdNQT6" role="iGT6I">
                <property role="2Ry0Am" value="languages" />
                <node concept="2Ry0Ak" id="4tsgqAdNQT7" role="2Ry0An">
                  <property role="2Ry0Am" value="MetaJade.aclmessage" />
                </node>
              </node>
            </node>
            <node concept="3qWCbU" id="3Laolq23z2L" role="3LXTna">
              <property role="3qWCbO" value="icons/**, resources/**" />
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="6FCRxOs8skZ" role="3bR37C">
          <node concept="3bR9La" id="6FCRxOs8sl0" role="1SiIV1">
            <ref role="3bR37D" to="ffeo:1H905DlDUSw" resolve="MPS.OpenAPI" />
          </node>
        </node>
      </node>
      <node concept="1E1JtD" id="3Laolq23y_5" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="MetaJade.core" />
        <property role="3LESm3" value="390edf25-bfa2-48c0-b67b-fc8fbd32bfbc" />
        <node concept="398BVA" id="7ml$O3eLqrY" role="3LF7KH">
          <ref role="398BVh" node="3Laolq23y$B" resolve="project_home" />
          <node concept="2Ry0Ak" id="7ml$O3eLqs3" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="7ml$O3eLqs4" role="2Ry0An">
              <property role="2Ry0Am" value="MetaJade.core" />
              <node concept="2Ry0Ak" id="7ml$O3eLqs5" role="2Ry0An">
                <property role="2Ry0Am" value="MetaJade.core.mpl" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yB5" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yB6" role="1SiIV1">
            <ref role="3bR37D" to="ffeo:3MI1gu0QouH" resolve="jetbrains.mps.editor.runtime" />
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yB7" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yB8" role="1SiIV1">
            <ref role="3bR37D" to="ffeo:mXGwHwhVPj" resolve="JDK" />
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yB9" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yBa" role="1SiIV1">
            <ref role="3bR37D" to="ffeo:7Kfy9QB6KXW" resolve="jetbrains.mps.lang.core" />
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yBb" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yBc" role="1SiIV1">
            <ref role="3bR37D" to="ffeo:1TaHNgiIbIZ" resolve="MPS.Editor" />
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yBd" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yBe" role="1SiIV1">
            <ref role="3bR37D" to="ffeo:7Kfy9QB6LfQ" resolve="jetbrains.mps.kernel" />
          </node>
        </node>
        <node concept="1BupzO" id="3Laolq23yBj" role="3bR31x">
          <property role="3ZfqAx" value="models" />
          <property role="1Hdu6h" value="true" />
          <property role="1HemKv" value="true" />
          <node concept="3LXTmp" id="3Laolq23yZo" role="1HemKq">
            <node concept="398BVA" id="3Laolq23yZc" role="3LXTmr">
              <ref role="398BVh" node="3Laolq23y$B" resolve="project_home" />
              <node concept="2Ry0Ak" id="3Laolq23yZd" role="iGT6I">
                <property role="2Ry0Am" value="languages" />
                <node concept="2Ry0Ak" id="3Laolq23yZe" role="2Ry0An">
                  <property role="2Ry0Am" value="MetaJade.core" />
                  <node concept="2Ry0Ak" id="3Laolq23yZf" role="2Ry0An">
                    <property role="2Ry0Am" value="models" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3qWCbU" id="3Laolq23yZp" role="3LXTna">
              <property role="3qWCbO" value="**/*.mps, **/*.mpsr, **/.model" />
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yYM" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yYN" role="1SiIV1">
            <ref role="3bR37D" to="ip48:5wLtKNeSRPD" resolve="org.iets3.core.expr.base" />
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yYQ" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yYR" role="1SiIV1">
            <ref role="3bR37D" to="ip48:3ni3WidJ1j3" resolve="org.iets3.core.expr.mutable" />
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yYS" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yYT" role="1SiIV1">
            <ref role="3bR37D" to="ip48:5wLtKNeSRRB" resolve="org.iets3.core.base" />
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yYU" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yYV" role="1SiIV1">
            <ref role="3bR37D" to="ip48:ub9nkyRnyj" resolve="org.iets3.core.expr.tests" />
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yYW" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yYX" role="1SiIV1">
            <ref role="3bR37D" to="ip48:6JPXQMQs0pX" resolve="org.iets3.core.expr.collections" />
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yYY" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yYZ" role="1SiIV1">
            <ref role="3bR37D" to="al5i:$bJ0jguQfr" resolve="com.mbeddr.core.base" />
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yZ0" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yZ1" role="1SiIV1">
            <ref role="3bR37D" to="ip48:2uR5X5azttH" resolve="org.iets3.core.expr.toplevel" />
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yZ2" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yZ3" role="1SiIV1">
            <ref role="3bR37D" to="ip48:5wLtKNeSRQd" resolve="org.iets3.core.expr.simpleTypes" />
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yZ4" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yZ5" role="1SiIV1">
            <ref role="3bR37D" node="3Laolq23y$N" resolve="MetaJade.ontology" />
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yZ6" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yZ7" role="1SiIV1">
            <ref role="3bR37D" to="ip48:49WTic8jAD5" resolve="org.iets3.core.expr.lambda" />
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yZ8" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yZ9" role="1SiIV1">
            <ref role="3bR37D" to="ip48:OnhZN9YvFC" resolve="org.iets3.core.expr.repl" />
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yZa" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yZb" role="1SiIV1">
            <ref role="3bR37D" node="3Laolq23y_b" resolve="MetaJade.behaviour" />
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yZq" role="3bR37C">
          <node concept="1Busua" id="3Laolq23yZr" role="1SiIV1">
            <ref role="1Busuk" to="ip48:2uR5X5azttH" resolve="org.iets3.core.expr.toplevel" />
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yZs" role="3bR37C">
          <node concept="1Busua" id="3Laolq23yZt" role="1SiIV1">
            <ref role="1Busuk" to="ip48:6JPXQMQs0pX" resolve="org.iets3.core.expr.collections" />
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yZu" role="3bR37C">
          <node concept="1Busua" id="3Laolq23yZv" role="1SiIV1">
            <ref role="1Busuk" to="ip48:49WTic8jAD5" resolve="org.iets3.core.expr.lambda" />
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yZw" role="3bR37C">
          <node concept="1Busua" id="3Laolq23yZx" role="1SiIV1">
            <ref role="1Busuk" to="ip48:ub9nkyRnyj" resolve="org.iets3.core.expr.tests" />
          </node>
        </node>
        <node concept="3rtmxn" id="3Laolq23z31" role="3bR31x">
          <node concept="3LXTmp" id="3Laolq23z32" role="3rtmxm">
            <node concept="398BVA" id="4tsgqAdNQTq" role="3LXTmr">
              <ref role="398BVh" node="3Laolq23y$B" resolve="project_home" />
              <node concept="2Ry0Ak" id="4tsgqAdNQTu" role="iGT6I">
                <property role="2Ry0Am" value="languages" />
                <node concept="2Ry0Ak" id="4tsgqAdNQTv" role="2Ry0An">
                  <property role="2Ry0Am" value="MetaJade.core" />
                </node>
              </node>
            </node>
            <node concept="3qWCbU" id="3Laolq23z37" role="3LXTna">
              <property role="3qWCbO" value="icons/**, resources/**" />
            </node>
          </node>
        </node>
      </node>
      <node concept="1E1JtD" id="3Laolq23y_b" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="MetaJade.behaviour" />
        <property role="3LESm3" value="0be7c48a-9600-4a9c-a497-e40de8415ebd" />
        <node concept="398BVA" id="7ml$O3eLqse" role="3LF7KH">
          <ref role="398BVh" node="3Laolq23y$B" resolve="project_home" />
          <node concept="2Ry0Ak" id="7ml$O3eLqsj" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="7ml$O3eLqsk" role="2Ry0An">
              <property role="2Ry0Am" value="MetaJade.behaviour" />
              <node concept="2Ry0Ak" id="7ml$O3eLqsl" role="2Ry0An">
                <property role="2Ry0Am" value="MetaJade.behaviour.mpl" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yBm" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yBn" role="1SiIV1">
            <ref role="3bR37D" to="ffeo:7Kfy9QB6KXW" resolve="jetbrains.mps.lang.core" />
          </node>
        </node>
        <node concept="1BupzO" id="3Laolq23yBs" role="3bR31x">
          <property role="3ZfqAx" value="models" />
          <property role="1Hdu6h" value="true" />
          <property role="1HemKv" value="true" />
          <node concept="3LXTmp" id="3Laolq23yZS" role="1HemKq">
            <node concept="398BVA" id="3Laolq23yZG" role="3LXTmr">
              <ref role="398BVh" node="3Laolq23y$B" resolve="project_home" />
              <node concept="2Ry0Ak" id="3Laolq23yZH" role="iGT6I">
                <property role="2Ry0Am" value="languages" />
                <node concept="2Ry0Ak" id="3Laolq23yZI" role="2Ry0An">
                  <property role="2Ry0Am" value="MetaJade.behaviour" />
                  <node concept="2Ry0Ak" id="3Laolq23yZJ" role="2Ry0An">
                    <property role="2Ry0Am" value="models" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3qWCbU" id="3Laolq23yZT" role="3LXTna">
              <property role="3qWCbO" value="**/*.mps, **/*.mpsr, **/.model" />
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yZy" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yZz" role="1SiIV1">
            <ref role="3bR37D" to="al5i:$bJ0jguQfr" resolve="com.mbeddr.core.base" />
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yZ$" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yZ_" role="1SiIV1">
            <ref role="3bR37D" to="ip48:5wLtKNeSRPD" resolve="org.iets3.core.expr.base" />
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yZA" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yZB" role="1SiIV1">
            <ref role="3bR37D" node="3Laolq23y$T" resolve="MetaJade.agent" />
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yZC" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yZD" role="1SiIV1">
            <ref role="3bR37D" to="ip48:49WTic8jAD5" resolve="org.iets3.core.expr.lambda" />
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yZE" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yZF" role="1SiIV1">
            <ref role="3bR37D" node="3Laolq23y_5" resolve="MetaJade.core" />
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yZU" role="3bR37C">
          <node concept="1Busua" id="3Laolq23yZV" role="1SiIV1">
            <ref role="1Busuk" node="3Laolq23y_5" resolve="MetaJade.core" />
          </node>
        </node>
        <node concept="3rtmxn" id="3Laolq23z3n" role="3bR31x">
          <node concept="3LXTmp" id="3Laolq23z3o" role="3rtmxm">
            <node concept="398BVA" id="4tsgqAdNQTM" role="3LXTmr">
              <ref role="398BVh" node="3Laolq23y$B" resolve="project_home" />
              <node concept="2Ry0Ak" id="4tsgqAdNQTQ" role="iGT6I">
                <property role="2Ry0Am" value="languages" />
                <node concept="2Ry0Ak" id="4tsgqAdNQTR" role="2Ry0An">
                  <property role="2Ry0Am" value="MetaJade.behaviour" />
                </node>
              </node>
            </node>
            <node concept="3qWCbU" id="3Laolq23z3t" role="3LXTna">
              <property role="3qWCbO" value="icons/**, resources/**" />
            </node>
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="3Laolq23y_h" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="MetaJade.java.runtime" />
        <property role="3LESm3" value="3235da96-e3e2-45f1-8096-7cb7c68bbaca" />
        <node concept="398BVA" id="7ml$O3eLqsu" role="3LF7KH">
          <ref role="398BVh" node="3Laolq23y$B" resolve="project_home" />
          <node concept="2Ry0Ak" id="7ml$O3eLqsz" role="iGT6I">
            <property role="2Ry0Am" value="solutions" />
            <node concept="2Ry0Ak" id="7ml$O3eLqs$" role="2Ry0An">
              <property role="2Ry0Am" value="MetaJade.java.runtime" />
              <node concept="2Ry0Ak" id="7ml$O3eLqs_" role="2Ry0An">
                <property role="2Ry0Am" value="MetaJade.java.runtime.msd" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yBv" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yBw" role="1SiIV1">
            <ref role="3bR37D" to="ffeo:mXGwHwhVPj" resolve="JDK" />
          </node>
        </node>
        <node concept="1BupzO" id="3Laolq23yB_" role="3bR31x">
          <property role="3ZfqAx" value="models" />
          <property role="1Hdu6h" value="true" />
          <property role="1HemKv" value="true" />
          <node concept="3LXTmp" id="3Laolq23z0k" role="1HemKq">
            <node concept="398BVA" id="3Laolq23z08" role="3LXTmr">
              <ref role="398BVh" node="3Laolq23y$B" resolve="project_home" />
              <node concept="2Ry0Ak" id="3Laolq23z09" role="iGT6I">
                <property role="2Ry0Am" value="solutions" />
                <node concept="2Ry0Ak" id="3Laolq23z0a" role="2Ry0An">
                  <property role="2Ry0Am" value="MetaJade.java.runtime" />
                  <node concept="2Ry0Ak" id="3Laolq23z0b" role="2Ry0An">
                    <property role="2Ry0Am" value="models" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3qWCbU" id="3Laolq23z0l" role="3LXTna">
              <property role="3qWCbO" value="**/*.mps, **/*.mpsr, **/.model" />
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yZW" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yZX" role="1SiIV1">
            <property role="3bR36h" value="true" />
            <ref role="3bR37D" node="3Laolq23y_n" resolve="MetaJade.java.stubs" />
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23z00" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23z01" role="1SiIV1">
            <ref role="3bR37D" to="ip48:26tZ$Z4rphz" resolve="org.iets3.core.expr.genjava.functionalJava" />
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="3Laolq23y_n" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="MetaJade.java.stubs" />
        <property role="3LESm3" value="0dfebf0c-13fb-476f-90ad-86743b0c71ae" />
        <node concept="398BVA" id="7ml$O3eLqsI" role="3LF7KH">
          <ref role="398BVh" node="3Laolq23y$B" resolve="project_home" />
          <node concept="2Ry0Ak" id="7ml$O3eLqsN" role="iGT6I">
            <property role="2Ry0Am" value="solutions" />
            <node concept="2Ry0Ak" id="7ml$O3eLqsO" role="2Ry0An">
              <property role="2Ry0Am" value="MetaJade.java.stubs" />
              <node concept="2Ry0Ak" id="7ml$O3eLqsP" role="2Ry0An">
                <property role="2Ry0Am" value="MetaJade.java.stubs.msd" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yBC" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yBD" role="1SiIV1">
            <property role="3bR36h" value="true" />
            <ref role="3bR37D" to="ffeo:mXGwHwhVPj" resolve="JDK" />
          </node>
        </node>
        <node concept="1BupzO" id="3Laolq23yBP" role="3bR31x">
          <property role="3ZfqAx" value="models" />
          <property role="1Hdu6h" value="true" />
          <property role="1HemKv" value="true" />
          <node concept="3LXTmp" id="3Laolq23z0N" role="1HemKq">
            <node concept="398BVA" id="3Laolq23z0B" role="3LXTmr">
              <ref role="398BVh" node="3Laolq23y$B" resolve="project_home" />
              <node concept="2Ry0Ak" id="3Laolq23z0C" role="iGT6I">
                <property role="2Ry0Am" value="solutions" />
                <node concept="2Ry0Ak" id="3Laolq23z0D" role="2Ry0An">
                  <property role="2Ry0Am" value="MetaJade.java.stubs" />
                  <node concept="2Ry0Ak" id="4tsgqAdNQUq" role="2Ry0An">
                    <property role="2Ry0Am" value="" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3qWCbU" id="3Laolq23z0O" role="3LXTna">
              <property role="3qWCbO" value="**/*.mps, **/*.mpsr, **/.model" />
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23z0_" role="3bR37C">
          <node concept="1BurEX" id="3Laolq23z0A" role="1SiIV1">
            <node concept="398BVA" id="3Laolq23z0m" role="1BurEY">
              <ref role="398BVh" node="3Laolq23y$B" resolve="project_home" />
              <node concept="2Ry0Ak" id="3Laolq23z0n" role="iGT6I">
                <property role="2Ry0Am" value="solutions" />
                <node concept="2Ry0Ak" id="3Laolq23z0o" role="2Ry0An">
                  <property role="2Ry0Am" value="MetaJade.java.stubs" />
                  <node concept="2Ry0Ak" id="3Laolq23z0p" role="2Ry0An">
                    <property role="2Ry0Am" value="lib" />
                    <node concept="2Ry0Ak" id="3Laolq23z0q" role="2Ry0An">
                      <property role="2Ry0Am" value="jade-4.6.0.jar" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="3Laolq23y_t" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="MetaJade.generation" />
        <property role="3LESm3" value="c00b198f-b059-42af-a1a4-33b5300b8bb9" />
        <node concept="398BVA" id="7ml$O3eLqt8" role="3LF7KH">
          <ref role="398BVh" node="3Laolq23y$B" resolve="project_home" />
          <node concept="2Ry0Ak" id="7ml$O3eLqtd" role="iGT6I">
            <property role="2Ry0Am" value="solutions" />
            <node concept="2Ry0Ak" id="7ml$O3eLqte" role="2Ry0An">
              <property role="2Ry0Am" value="MetaJade.generation" />
              <node concept="2Ry0Ak" id="7ml$O3eLqtf" role="2Ry0An">
                <property role="2Ry0Am" value="MetaJade.generation.msd" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1BupzO" id="3Laolq23yBW" role="3bR31x">
          <property role="3ZfqAx" value="models" />
          <property role="1Hdu6h" value="true" />
          <property role="1HemKv" value="true" />
          <node concept="3LXTmp" id="3Laolq23z11" role="1HemKq">
            <node concept="398BVA" id="3Laolq23z0P" role="3LXTmr">
              <ref role="398BVh" node="3Laolq23y$B" resolve="project_home" />
              <node concept="2Ry0Ak" id="3Laolq23z0Q" role="iGT6I">
                <property role="2Ry0Am" value="solutions" />
                <node concept="2Ry0Ak" id="3Laolq23z0R" role="2Ry0An">
                  <property role="2Ry0Am" value="MetaJade.generation" />
                  <node concept="2Ry0Ak" id="3Laolq23z0S" role="2Ry0An">
                    <property role="2Ry0Am" value="models" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3qWCbU" id="3Laolq23z12" role="3LXTna">
              <property role="3qWCbO" value="**/*.mps, **/*.mpsr, **/.model" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3LEwk6" id="3Laolq23y_z" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="MetaJade.devkit.java" />
        <property role="3LESm3" value="2a15b00e-495f-4bce-aabb-bf2bffd958aa" />
        <node concept="398BVA" id="7ml$O3eLqty" role="3LF7KH">
          <ref role="398BVh" node="3Laolq23y$B" resolve="project_home" />
          <node concept="2Ry0Ak" id="7ml$O3eLqtB" role="iGT6I">
            <property role="2Ry0Am" value="devkits" />
            <node concept="2Ry0Ak" id="7ml$O3eLqtC" role="2Ry0An">
              <property role="2Ry0Am" value="MetaJade.devkit.java" />
              <node concept="2Ry0Ak" id="7ml$O3eLqtD" role="2Ry0An">
                <property role="2Ry0Am" value="MetaJade.devkit.java.devkit" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3LEDTy" id="3Laolq23yBZ" role="3LEDUa">
          <ref role="3LEDTV" to="ffeo:7Kfy9QB6KYb" resolve="jetbrains.mps.baseLanguage" />
        </node>
        <node concept="3LEDTy" id="3Laolq23yC0" role="3LEDUa">
          <ref role="3LEDTV" to="ffeo:7Kfy9QB6KZG" resolve="jetbrains.mps.baseLanguage.closures" />
        </node>
        <node concept="3LEDTy" id="3Laolq23yC1" role="3LEDUa">
          <ref role="3LEDTV" to="ffeo:7Kfy9QB6L0h" resolve="jetbrains.mps.baseLanguage.collections" />
        </node>
        <node concept="3LEDTy" id="3Laolq23yC2" role="3LEDUa">
          <ref role="3LEDTV" to="ffeo:ymnOULAU0j" resolve="jetbrains.mps.baseLanguage.unitTest" />
        </node>
        <node concept="3LEDTy" id="3Laolq23yC3" role="3LEDUa">
          <ref role="3LEDTV" to="ffeo:7Kfy9QB6KZ0" resolve="jetbrains.mps.baseLanguageInternal" />
        </node>
        <node concept="3LEz8M" id="3Laolq23z13" role="3LEz9a">
          <ref role="3LEz8N" to="ip48:2zpAVpC$xZc" resolve="org.iets3.core.expr.genjava.core.devkit" />
        </node>
        <node concept="3LEz8M" id="3Laolq23z14" role="3LEz9a">
          <ref role="3LEz8N" node="3Laolq23y_J" resolve="MetaJade.devkit.base" />
        </node>
        <node concept="3LEDTy" id="3Laolq23z15" role="3LEDUa">
          <ref role="3LEDTV" node="3Laolq23y$H" resolve="MetaJade.genjava" />
        </node>
        <node concept="3LEDTy" id="3Laolq23z16" role="3LEDUa">
          <ref role="3LEDTV" to="ip48:26tZ$Z4qSzW" resolve="org.iets3.core.expr.genjava.base" />
        </node>
        <node concept="3LEDTy" id="3Laolq23z17" role="3LEDUa">
          <ref role="3LEDTV" to="ip48:lH$Puj5DFq" resolve="org.iets3.core.expr.genjava.contracts" />
        </node>
        <node concept="3LEDTy" id="3Laolq23z18" role="3LEDUa">
          <ref role="3LEDTV" to="ip48:26tZ$Z4qVBy" resolve="org.iets3.core.expr.genjava.simpleTypes" />
        </node>
        <node concept="3LEDTy" id="3Laolq23z19" role="3LEDUa">
          <ref role="3LEDTV" to="ip48:26tZ$Z4qWbm" resolve="org.iets3.core.expr.genjava.tests" />
        </node>
        <node concept="3LEDTy" id="3Laolq23z1a" role="3LEDUa">
          <ref role="3LEDTV" to="ip48:26tZ$Z4qWJe" resolve="org.iets3.core.expr.genjava.toplevel" />
        </node>
      </node>
      <node concept="1E1JtA" id="3Laolq23y_D" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="MetaJade.tests" />
        <property role="3LESm3" value="64aaf09a-926d-4c0f-b294-46fe8dd619c4" />
        <property role="aoJFB" value="eYcmk9QOlj/sources_and_tests" />
        <node concept="398BVA" id="7ml$O3eLqtW" role="3LF7KH">
          <ref role="398BVh" node="3Laolq23y$B" resolve="project_home" />
          <node concept="2Ry0Ak" id="7ml$O3eLqu1" role="iGT6I">
            <property role="2Ry0Am" value="solutions" />
            <node concept="2Ry0Ak" id="7ml$O3eLqu2" role="2Ry0An">
              <property role="2Ry0Am" value="MetaJade.tests" />
              <node concept="2Ry0Ak" id="7ml$O3eLqu3" role="2Ry0An">
                <property role="2Ry0Am" value="MetaJade.tests.msd" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="3Laolq23yC4" role="3bR37C">
          <node concept="3bR9La" id="3Laolq23yC5" role="1SiIV1">
            <ref role="3bR37D" to="ffeo:1TaHNgiIbJ$" resolve="jetbrains.mps.ide.editor" />
          </node>
        </node>
        <node concept="1BupzO" id="3Laolq23yCa" role="3bR31x">
          <property role="3ZfqAx" value="models" />
          <property role="1Hdu6h" value="true" />
          <property role="1HemKv" value="true" />
          <node concept="3LXTmp" id="3Laolq23z1n" role="1HemKq">
            <node concept="398BVA" id="3Laolq23z1b" role="3LXTmr">
              <ref role="398BVh" node="3Laolq23y$B" resolve="project_home" />
              <node concept="2Ry0Ak" id="3Laolq23z1c" role="iGT6I">
                <property role="2Ry0Am" value="solutions" />
                <node concept="2Ry0Ak" id="3Laolq23z1d" role="2Ry0An">
                  <property role="2Ry0Am" value="MetaJade.tests" />
                  <node concept="2Ry0Ak" id="3Laolq23z1e" role="2Ry0An">
                    <property role="2Ry0Am" value="models" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3qWCbU" id="3Laolq23z1o" role="3LXTna">
              <property role="3qWCbO" value="**/*.mps, **/*.mpsr, **/.model" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3LEwk6" id="3Laolq23y_J" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="MetaJade.devkit.base" />
        <property role="3LESm3" value="32b9b56c-baec-45a0-a54b-e6fc781c2c8c" />
        <node concept="398BVA" id="7ml$O3eLqum" role="3LF7KH">
          <ref role="398BVh" node="3Laolq23y$B" resolve="project_home" />
          <node concept="2Ry0Ak" id="7ml$O3eLqur" role="iGT6I">
            <property role="2Ry0Am" value="devkits" />
            <node concept="2Ry0Ak" id="7ml$O3eLqus" role="2Ry0An">
              <property role="2Ry0Am" value="MetaJade.devkit.base" />
              <node concept="2Ry0Ak" id="7ml$O3eLqut" role="2Ry0An">
                <property role="2Ry0Am" value="MetaJade.devkit.base.devkit" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3LEz8M" id="3Laolq23z1p" role="3LEz9a">
          <ref role="3LEz8N" to="ip48:2zpAVpC$XlR" resolve="org.iets3.core.expr.core.devkit" />
        </node>
        <node concept="3LEDTy" id="3Laolq23z1q" role="3LEDUa">
          <ref role="3LEDTV" node="3Laolq23y$T" resolve="MetaJade.agent" />
        </node>
        <node concept="3LEDTy" id="3Laolq23z1r" role="3LEDUa">
          <ref role="3LEDTV" node="3Laolq23y_b" resolve="MetaJade.behaviour" />
        </node>
        <node concept="3LEDTy" id="3Laolq23z1s" role="3LEDUa">
          <ref role="3LEDTV" node="3Laolq23y_5" resolve="MetaJade.core" />
        </node>
        <node concept="3LEDTy" id="3Laolq23z1t" role="3LEDUa">
          <ref role="3LEDTV" node="3Laolq23y$N" resolve="MetaJade.ontology" />
        </node>
        <node concept="3LEDTy" id="3Laolq23z1u" role="3LEDUa">
          <ref role="3LEDTV" node="3Laolq23y$Z" resolve="MetaJade.aclmessage" />
        </node>
        <node concept="3LEDTy" id="3Laolq23z1v" role="3LEDUa">
          <ref role="3LEDTV" to="ip48:3ni3WidJ1j3" resolve="org.iets3.core.expr.mutable" />
        </node>
        <node concept="3LEDTy" id="3Laolq23z1w" role="3LEDUa">
          <ref role="3LEDTV" to="ip48:ub9nkyRnyj" resolve="org.iets3.core.expr.tests" />
        </node>
        <node concept="3LEDTM" id="3Laolq23z1x" role="3LEDUa">
          <ref role="3LEDTN" node="3Laolq23y_t" resolve="MetaJade.generation" />
        </node>
      </node>
      <node concept="1E1JtA" id="7OjSnjRDprJ" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="MetaJade.build" />
        <property role="3LESm3" value="6d71ec95-f9cc-49c3-ba25-0c23cef6f9a2" />
        <node concept="398BVA" id="7ml$O3eLqva" role="3LF7KH">
          <ref role="398BVh" node="3Laolq23y$B" resolve="project_home" />
          <node concept="2Ry0Ak" id="7ml$O3eLqvf" role="iGT6I">
            <property role="2Ry0Am" value="solutions" />
            <node concept="2Ry0Ak" id="7ml$O3eLqvg" role="2Ry0An">
              <property role="2Ry0Am" value="MetaJade.build" />
              <node concept="2Ry0Ak" id="7ml$O3eLqvh" role="2Ry0An">
                <property role="2Ry0Am" value="MetaJade.build.msd" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="7OjSnjRDpxx" role="3bR37C">
          <node concept="3bR9La" id="7OjSnjRDpxy" role="1SiIV1">
            <ref role="3bR37D" to="ip48:7C6tnXfWu6n" resolve="org.iets3.opensource.build" />
          </node>
        </node>
        <node concept="1SiIV0" id="7OjSnjRDpxz" role="3bR37C">
          <node concept="3bR9La" id="7OjSnjRDpx$" role="1SiIV1">
            <ref role="3bR37D" to="ffeo:78GwwOvB3tw" resolve="jetbrains.mps.ide.build" />
          </node>
        </node>
        <node concept="1SiIV0" id="7OjSnjRDpx_" role="3bR37C">
          <node concept="3bR9La" id="7OjSnjRDpxA" role="1SiIV1">
            <ref role="3bR37D" to="al5i:7Pr7tifzlku" resolve="com.mbeddr.platform" />
          </node>
        </node>
        <node concept="1BupzO" id="7OjSnjRDpxN" role="3bR31x">
          <property role="3ZfqAx" value="models" />
          <property role="1Hdu6h" value="true" />
          <property role="1HemKv" value="true" />
          <node concept="3LXTmp" id="7OjSnjRDpxO" role="1HemKq">
            <node concept="398BVA" id="7OjSnjRDpxB" role="3LXTmr">
              <ref role="398BVh" node="3Laolq23y$B" resolve="project_home" />
              <node concept="2Ry0Ak" id="7OjSnjRDpxC" role="iGT6I">
                <property role="2Ry0Am" value="solutions" />
                <node concept="2Ry0Ak" id="7OjSnjRDpxD" role="2Ry0An">
                  <property role="2Ry0Am" value="MetaJade.build" />
                  <node concept="2Ry0Ak" id="7OjSnjRDpxE" role="2Ry0An">
                    <property role="2Ry0Am" value="models" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3qWCbU" id="7OjSnjRDpxP" role="3LXTna">
              <property role="3qWCbO" value="**/*.mps, **/*.mpsr, **/.model" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="22LTRH" id="4eY$d6AuKwr" role="1hWBAP">
      <property role="TrG5h" value="MetaJade" />
      <node concept="22LTRM" id="4eY$d6AuKwV" role="22LTRK">
        <ref role="22LTRN" node="3Laolq23y_D" resolve="MetaJade.tests" />
      </node>
    </node>
  </node>
</model>

