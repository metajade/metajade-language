<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:661ddf73-24b1-44f9-b43f-e0f227db1c73(MetaJade.tests.agent@tests)">
  <persistence version="9" />
  <languages>
    <use id="8585453e-6bfb-4d80-98de-b16074f1d86c" name="jetbrains.mps.lang.test" version="5" />
    <use id="f61473f9-130f-42f6-b98d-6c438812c2f6" name="jetbrains.mps.baseLanguage.unitTest" version="1" />
    <devkit ref="32b9b56c-baec-45a0-a54b-e6fc781c2c8c(MetaJade.devkit.base)" />
  </languages>
  <imports>
    <import index="ekwn" ref="r:9832fb5f-2578-4b58-8014-a5de79da988e(jetbrains.mps.ide.editor.actions)" />
  </imports>
  <registry>
    <language id="8585453e-6bfb-4d80-98de-b16074f1d86c" name="jetbrains.mps.lang.test">
      <concept id="1215507671101" name="jetbrains.mps.lang.test.structure.NodeErrorCheckOperation" flags="ng" index="1TM$A" />
      <concept id="1215526290564" name="jetbrains.mps.lang.test.structure.NodeTypeCheckOperation" flags="ng" index="30Omv">
        <child id="1215526393912" name="type" index="31d$z" />
      </concept>
      <concept id="1215603922101" name="jetbrains.mps.lang.test.structure.NodeOperationsContainer" flags="ng" index="7CXmI">
        <child id="1215604436604" name="nodeOperations" index="7EUXB" />
      </concept>
      <concept id="1228934484974" name="jetbrains.mps.lang.test.structure.PressKeyStatement" flags="nn" index="yd1bK">
        <child id="1228934507814" name="keyStrokes" index="yd6KS" />
      </concept>
      <concept id="7011073693661765739" name="jetbrains.mps.lang.test.structure.InvokeActionStatement" flags="nn" index="2HxZob">
        <child id="1101347953350127927" name="actionReference" index="3iKnsn" />
      </concept>
      <concept id="1229187653856" name="jetbrains.mps.lang.test.structure.EditorTestCase" flags="lg" index="LiM7Y">
        <child id="3143335925185262946" name="testNodeBefore" index="25YQCW" />
        <child id="3143335925185262981" name="testNodeResult" index="25YQFr" />
        <child id="1229187755283" name="code" index="LjaKd" />
      </concept>
      <concept id="1229194968594" name="jetbrains.mps.lang.test.structure.AnonymousCellAnnotation" flags="ng" index="LIFWc">
        <property id="6268941039745498163" name="selectionStart" index="p6zMq" />
        <property id="6268941039745498165" name="selectionEnd" index="p6zMs" />
        <property id="1229194968595" name="cellId" index="LIFWd" />
        <property id="1932269937152561478" name="useLabelSelection" index="OXtK3" />
        <property id="1229432188737" name="isLastPosition" index="ZRATv" />
      </concept>
      <concept id="1227182079811" name="jetbrains.mps.lang.test.structure.TypeKeyStatement" flags="nn" index="2TK7Tu">
        <property id="1227184461946" name="keys" index="2TTd_B" />
      </concept>
      <concept id="5097124989038916362" name="jetbrains.mps.lang.test.structure.TestInfo" flags="ng" index="2XOHcx">
        <property id="5097124989038916363" name="projectPath" index="2XOHcw" />
      </concept>
      <concept id="4239542196496927193" name="jetbrains.mps.lang.test.structure.MPSActionReference" flags="ng" index="1iFQzN">
        <reference id="4239542196496929559" name="action" index="1iFR8X" />
      </concept>
      <concept id="1216913645126" name="jetbrains.mps.lang.test.structure.NodesTestCase" flags="lg" index="1lH9Xt">
        <child id="1217501822150" name="nodesToCheck" index="1SKRRt" />
      </concept>
      <concept id="1216989428737" name="jetbrains.mps.lang.test.structure.TestNode" flags="ng" index="1qefOq">
        <child id="1216989461394" name="nodeToCheck" index="1qenE9" />
      </concept>
    </language>
    <language id="28f9e497-3b42-4291-aeba-0a1039153ab1" name="jetbrains.mps.lang.plugin">
      <concept id="1207318242772" name="jetbrains.mps.lang.plugin.structure.KeyMapKeystroke" flags="ng" index="pLAjd">
        <property id="1207318242774" name="keycode" index="pLAjf" />
      </concept>
    </language>
    <language id="2f7e2e35-6e74-4c43-9fa5-2465d68f5996" name="org.iets3.core.expr.collections">
      <concept id="7554398283339749509" name="org.iets3.core.expr.collections.structure.CollectionType" flags="ng" index="3iBWmN">
        <child id="7554398283339749510" name="baseType" index="3iBWmK" />
      </concept>
      <concept id="7554398283339757344" name="org.iets3.core.expr.collections.structure.ListType" flags="ng" index="3iBYCm" />
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
    </language>
    <language id="390edf25-bfa2-48c0-b67b-fc8fbd32bfbc" name="MetaJade.core">
      <concept id="943645189038563415" name="MetaJade.core.structure.IDefinePerformative" flags="ng" index="2NJI0r">
        <property id="943645189038563416" name="performative" index="2NJI0k" />
      </concept>
      <concept id="2457439430822670749" name="MetaJade.core.structure.Module" flags="ng" index="198UmT" />
      <concept id="2569313154175320625" name="MetaJade.core.structure.IChunkDep" flags="ng" index="1cWWE2">
        <reference id="39011061268502169" name="chunk" index="21GGV3" />
      </concept>
      <concept id="7868029667361394582" name="MetaJade.core.structure.GetLocalName" flags="ng" index="3gvPjt" />
      <concept id="7868029667361405407" name="MetaJade.core.structure.GetName" flags="ng" index="3gvQak" />
      <concept id="8295235836482040281" name="MetaJade.core.structure.AbstractChunk" flags="ng" index="1yxazi">
        <child id="2457439430830142521" name="imports" index="19Gq0t" />
      </concept>
      <concept id="4428678242934220321" name="MetaJade.core.structure.ITopLevelExprContainer" flags="ng" index="3FVrl4">
        <child id="1741182739292157392" name="contents" index="3KDhAD" />
      </concept>
      <concept id="4316580858990611837" name="MetaJade.core.structure.AIDType" flags="ng" index="3NxlSm" />
    </language>
    <language id="cfaa4966-b7d5-4b69-b66a-309a6e1a7290" name="org.iets3.core.expr.base">
      <concept id="7425695345928349204" name="org.iets3.core.expr.base.structure.Type" flags="ng" index="2vmvy6" />
      <concept id="7425695345928347719" name="org.iets3.core.expr.base.structure.Expression" flags="ng" index="2vmvVl" />
      <concept id="7089558164908491660" name="org.iets3.core.expr.base.structure.EmptyExpression" flags="ng" index="2zH6wq" />
      <concept id="7089558164905593724" name="org.iets3.core.expr.base.structure.IOptionallyTyped" flags="ng" index="2zM23E">
        <child id="7089558164905593725" name="type" index="2zM23F" />
      </concept>
      <concept id="7071042522334260296" name="org.iets3.core.expr.base.structure.ITyped" flags="ng" index="2_iKZX">
        <child id="8811147530085329321" name="type" index="2S399n" />
      </concept>
      <concept id="5115872837156802409" name="org.iets3.core.expr.base.structure.UnaryExpression" flags="ng" index="30czhk">
        <child id="5115872837156802411" name="expr" index="30czhm" />
      </concept>
      <concept id="9002563722476995145" name="org.iets3.core.expr.base.structure.DotExpression" flags="ng" index="1QScDb">
        <child id="9002563722476995147" name="target" index="1QScD9" />
      </concept>
    </language>
    <language id="92d2ea16-5a42-4fdf-a676-c7604efe3504" name="de.slisson.mps.richtext">
      <concept id="2557074442922380897" name="de.slisson.mps.richtext.structure.Text" flags="ng" index="19SGf9">
        <child id="2557074442922392302" name="words" index="19SJt6" />
      </concept>
      <concept id="2557074442922438156" name="de.slisson.mps.richtext.structure.Word" flags="ng" index="19SUe$">
        <property id="2557074442922438158" name="escapedValue" index="19SUeA" />
      </concept>
    </language>
    <language id="6b277d9a-d52d-416f-a209-1919bd737f50" name="org.iets3.core.expr.simpleTypes">
      <concept id="5115872837157252552" name="org.iets3.core.expr.simpleTypes.structure.StringLiteral" flags="ng" index="30bdrP" />
      <concept id="5115872837157252551" name="org.iets3.core.expr.simpleTypes.structure.StringType" flags="ng" index="30bdrU" />
    </language>
    <language id="71934284-d7d1-45ee-a054-8c072591085f" name="org.iets3.core.expr.toplevel">
      <concept id="543569365052765011" name="org.iets3.core.expr.toplevel.structure.EmptyToplevelContent" flags="ng" index="_ixoA" />
      <concept id="8811147530085329320" name="org.iets3.core.expr.toplevel.structure.RecordLiteral" flags="ng" index="2S399m">
        <child id="8811147530085329323" name="memberValues" index="2S399l" />
      </concept>
      <concept id="602952467877559919" name="org.iets3.core.expr.toplevel.structure.IRecordDeclaration" flags="ng" index="S5Q1W">
        <child id="602952467877562565" name="members" index="S5Trm" />
      </concept>
      <concept id="8811147530084018370" name="org.iets3.core.expr.toplevel.structure.RecordType" flags="ng" index="2Ss9cW">
        <reference id="8811147530084018371" name="record" index="2Ss9cX" />
      </concept>
      <concept id="8811147530084018361" name="org.iets3.core.expr.toplevel.structure.RecordMember" flags="ng" index="2Ss9d7" />
      <concept id="8811147530084018358" name="org.iets3.core.expr.toplevel.structure.RecordDeclaration" flags="ng" index="2Ss9d8" />
    </language>
    <language id="d4280a54-f6df-4383-aa41-d1b2bffa7eb1" name="com.mbeddr.core.base">
      <concept id="8375407818529178006" name="com.mbeddr.core.base.structure.TextBlock" flags="ng" index="OjmMv">
        <child id="8375407818529178007" name="text" index="OjmMu" />
      </concept>
      <concept id="3857533489766146428" name="com.mbeddr.core.base.structure.ElementDocumentation" flags="ng" index="1z9TsT">
        <child id="4052432714772608243" name="text" index="1w35rA" />
      </concept>
    </language>
    <language id="0be7c48a-9600-4a9c-a497-e40de8415ebd" name="MetaJade.behaviour">
      <concept id="1363295496955285700" name="MetaJade.behaviour.structure.ActivateBehaviour" flags="ng" index="p0Vrc" />
      <concept id="1363295496955285701" name="MetaJade.behaviour.structure.AbstractBehaviourUnaryExpression" flags="ng" index="p0Vrd">
        <child id="4145021229450611113" name="behaviour" index="3QnlHL" />
      </concept>
      <concept id="3401921042422620142" name="MetaJade.behaviour.structure.Behaviour" flags="ng" index="3oGdb7">
        <child id="2058408588520789632" name="action" index="2dXv$O" />
        <child id="266893304458096888" name="behaviourType" index="3u$SbN" />
        <child id="4289783338601281164" name="inputParams" index="3BfUAV" />
      </concept>
      <concept id="4289783338601963020" name="MetaJade.behaviour.structure.BehaviourArgument" flags="ng" index="3Biw4V" />
      <concept id="4289783338602023683" name="MetaJade.behaviour.structure.BehaviourArgRef" flags="ng" index="3BiLgO">
        <reference id="4289783338602023686" name="arg" index="3BiLgL" />
      </concept>
      <concept id="3737182289224794352" name="MetaJade.behaviour.structure.OneShotBehaviour" flags="ng" index="3DZied" />
      <concept id="1741182739291547053" name="MetaJade.behaviour.structure.BehaviourDependency" flags="ng" index="3KE$Bk" />
      <concept id="4145021229450567818" name="MetaJade.behaviour.structure.BehaviourReference" flags="ng" index="3Qnb9i">
        <reference id="4145021229450567819" name="behaviour" index="3Qnb9j" />
      </concept>
    </language>
    <language id="68d558ed-66cf-46ac-b353-d5fddcc21f72" name="MetaJade.aclmessage">
      <concept id="8771781395564190192" name="MetaJade.aclmessage.structure.GetReceiversOp" flags="ng" index="Cg7_4" />
      <concept id="8771781395564137494" name="MetaJade.aclmessage.structure.GetSenderOp" flags="ng" index="CgaMy" />
      <concept id="8771781395564539185" name="MetaJade.aclmessage.structure.GetContentOp" flags="ng" index="FICQ5" />
      <concept id="4949964338176784763" name="MetaJade.aclmessage.structure.SendMessage" flags="ng" index="2JkjsH">
        <child id="2658052393945773747" name="content" index="2rofCy" />
        <child id="2658052393945773748" name="receiver" index="2rofC_" />
        <child id="2658052393945773750" name="type" index="2rofCB" />
      </concept>
      <concept id="8895927397244351935" name="MetaJade.aclmessage.structure.ReceiveMessage" flags="ng" index="2R7DHE">
        <child id="5091083777057681387" name="onReceive" index="3NSspj" />
        <child id="5091083777057681390" name="arg" index="3NSspm" />
      </concept>
      <concept id="140811118312068270" name="MetaJade.aclmessage.structure.ReplyToMessage" flags="ng" index="1eBHpp">
        <child id="140811118312068276" name="reply" index="1eBHp3" />
        <child id="140811118312068275" name="type" index="1eBHp4" />
        <child id="140811118312068272" name="content" index="1eBHp7" />
      </concept>
      <concept id="4946621191997514448" name="MetaJade.aclmessage.structure.ReceivedMessageArg" flags="ng" index="1fMSWk">
        <child id="2695020384284204564" name="type" index="2uPfNm" />
      </concept>
      <concept id="4946621191998473754" name="MetaJade.aclmessage.structure.ReceivedMessageArgRef" flags="ng" index="1fQeJu">
        <reference id="4946621191998473788" name="arg" index="1fQeJS" />
      </concept>
      <concept id="1816631675487503527" name="MetaJade.aclmessage.structure.MessageType" flags="ng" index="1g4zL5">
        <child id="2695020384283400037" name="baseType" index="2uKbmB" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
      <concept id="4222318806802425298" name="jetbrains.mps.lang.core.structure.SuppressErrorsAnnotation" flags="ng" index="15s5l7">
        <property id="8575328350543493365" name="message" index="huDt6" />
        <property id="2423417345669755629" name="filter" index="1eyWvh" />
      </concept>
    </language>
    <language id="ff6c34ae-1ca3-455f-96b3-8f28007794c9" name="MetaJade.ontology">
      <concept id="5525388950973568655" name="MetaJade.ontology.structure.Ontology" flags="ng" index="2C2W_Q">
        <child id="543569365052711058" name="contents" index="_iOnB" />
      </concept>
      <concept id="8320844951296934877" name="MetaJade.ontology.structure.ConceptSchema" flags="ng" index="2MkpuS" />
      <concept id="8320844951296934876" name="MetaJade.ontology.structure.AgentActionSchema" flags="ng" index="2MkpuT" />
    </language>
    <language id="9464fa06-5ab9-409b-9274-64ab29588457" name="org.iets3.core.expr.lambda">
      <concept id="4790956042240983401" name="org.iets3.core.expr.lambda.structure.BlockExpression" flags="ng" index="1aduha">
        <child id="4790956042240983402" name="expressions" index="1aduh9" />
      </concept>
      <concept id="4790956042241053102" name="org.iets3.core.expr.lambda.structure.ValExpression" flags="ng" index="1adJid">
        <child id="4790956042241053105" name="expr" index="1adJii" />
      </concept>
      <concept id="7554398283340318473" name="org.iets3.core.expr.lambda.structure.IArgument" flags="ng" index="3ix9CZ">
        <child id="7554398283340318476" name="type" index="3ix9CU" />
      </concept>
    </language>
    <language id="32367449-d150-4018-8690-20f09bf1abe2" name="MetaJade.agent">
      <concept id="3702022420523728372" name="MetaJade.agent.structure.Agent" flags="ng" index="3eIggM">
        <child id="943645189045937530" name="setup" index="2M3AkQ" />
      </concept>
      <concept id="1741182739291546925" name="MetaJade.agent.structure.AgentDependency" flags="ng" index="3KE$_k" />
    </language>
  </registry>
  <node concept="2XOHcx" id="2umCVQRtQ50">
    <property role="2XOHcw" value="${project_home}" />
  </node>
  <node concept="1lH9Xt" id="2umCVQRtUp4">
    <property role="TrG5h" value="MessageStatements" />
    <node concept="1qefOq" id="2umCVQRtUp$" role="1SKRRt">
      <node concept="15s5l7" id="Oow4MZ29b7" role="lGtFl">
        <property role="1eyWvh" value="FLAVOUR_ISSUE_KIND=&quot;typesystem (typesystem)&quot;;FLAVOUR_MESSAGE=&quot;Warning: value never used&quot;;FLAVOUR_RULE_ID=&quot;[r:3b5d2a4d-f539-4854-bc25-c43da4b5202c(org.iets3.core.expr.lambda.typesystem)/508719611258576809]&quot;;" />
        <property role="huDt6" value="Warning: value never used" />
      </node>
      <node concept="3oGdb7" id="2umCVQRtUpA" role="1qenE9">
        <property role="TrG5h" value="SendPing" />
        <node concept="3Biw4V" id="2umCVQRtUvv" role="3BfUAV">
          <property role="TrG5h" value="dummyAID" />
          <node concept="3NxlSm" id="Oow4MYK3j9" role="3ix9CU" />
        </node>
        <node concept="3DZied" id="2umCVQRtUpC" role="3u$SbN" />
        <node concept="1aduha" id="2umCVQRtV$y" role="2dXv$O">
          <node concept="2zH6wq" id="4TWmWFXY88I" role="1aduh9" />
          <node concept="2JkjsH" id="4TWmWFXY8bU" role="1aduh9">
            <property role="2NJI0k" value="4MI7ZB$IHdc/Inform" />
            <node concept="2S399m" id="4TWmWFXY8KG" role="2rofCy">
              <node concept="2Ss9cW" id="4TWmWFXY8N9" role="2S399n">
                <ref role="2Ss9cX" node="2umCVQRtUsI" resolve="pingMsg" />
              </node>
              <node concept="30bdrP" id="4TWmWFXY8Nj" role="2S399l" />
            </node>
            <node concept="3BiLgO" id="4TWmWFXY8NF" role="2rofC_">
              <ref role="3BiLgL" node="2umCVQRtUvv" resolve="dummyAID" />
            </node>
            <node concept="1g4zL5" id="4TWmWFXY8c0" role="2rofCB">
              <node concept="2Ss9cW" id="4TWmWFXYNfk" role="2uKbmB">
                <ref role="2Ss9cX" node="2umCVQRtUsI" resolve="pingMsg" />
                <node concept="7CXmI" id="4TWmWFXYNmL" role="lGtFl">
                  <node concept="1TM$A" id="4TWmWFXYNmM" role="7EUXB" />
                </node>
              </node>
            </node>
            <node concept="1z9TsT" id="4TWmWFXYXEX" role="lGtFl">
              <node concept="OjmMv" id="4TWmWFXYXEY" role="1w35rA">
                <node concept="19SGf9" id="4TWmWFXYXEZ" role="OjmMu">
                  <node concept="19SUe$" id="4TWmWFXYXF0" role="19SJt6">
                    <property role="19SUeA" value="The content of the message must be an ontology element" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2zH6wq" id="4TWmWFXY8dA" role="1aduh9" />
          <node concept="2JkjsH" id="4TWmWFXYMRw" role="1aduh9">
            <property role="2NJI0k" value="4MI7ZB$IHdc/Inform" />
            <node concept="2S399m" id="4TWmWFXYMRx" role="2rofCy">
              <node concept="2Ss9cW" id="4TWmWFXYMRy" role="2S399n">
                <ref role="2Ss9cX" node="4hvwZTP0kw_" resolve="testConcept" />
              </node>
              <node concept="30bdrP" id="4TWmWFXYMRz" role="2S399l" />
            </node>
            <node concept="3BiLgO" id="4TWmWFXYMR$" role="2rofC_">
              <ref role="3BiLgL" node="2umCVQRtUvv" resolve="dummyAID" />
            </node>
            <node concept="1g4zL5" id="4TWmWFXYMR_" role="2rofCB">
              <node concept="2Ss9cW" id="4TWmWFY0b$Y" role="2uKbmB">
                <ref role="2Ss9cX" node="4hvwZTP0kw_" resolve="testConcept" />
                <node concept="7CXmI" id="4TWmWFY0bIx" role="lGtFl">
                  <node concept="1TM$A" id="4TWmWFY0bIy" role="7EUXB" />
                </node>
              </node>
            </node>
            <node concept="1z9TsT" id="4TWmWFY0b6J" role="lGtFl">
              <node concept="OjmMv" id="4TWmWFY0b6K" role="1w35rA">
                <node concept="19SGf9" id="4TWmWFY0b6L" role="OjmMu">
                  <node concept="19SUe$" id="4TWmWFY0b6M" role="19SJt6">
                    <property role="19SUeA" value="The content of a message must be an AgentAction Schema or a Concept Schema " />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2zH6wq" id="4TWmWFY0cjL" role="1aduh9" />
          <node concept="2JkjsH" id="4TWmWFY0cmT" role="1aduh9">
            <property role="2NJI0k" value="4MI7ZB$IHdc/Inform" />
            <node concept="2S399m" id="4TWmWFY0cA5" role="2rofCy">
              <node concept="2Ss9cW" id="4TWmWFY0cDn" role="2S399n">
                <ref role="2Ss9cX" node="4hvwZTP08y$" resolve="testAction" />
              </node>
              <node concept="30bdrP" id="4TWmWFY0cDr" role="2S399l" />
            </node>
            <node concept="3BiLgO" id="4TWmWFY0cGQ" role="2rofC_">
              <ref role="3BiLgL" node="2umCVQRtUvv" resolve="dummyAID" />
            </node>
            <node concept="1g4zL5" id="4TWmWFY0cmZ" role="2rofCB">
              <node concept="2Ss9cW" id="4TWmWFY0cyE" role="2uKbmB">
                <ref role="2Ss9cX" node="4hvwZTP08y$" resolve="testAction" />
              </node>
            </node>
            <node concept="7CXmI" id="4TWmWFY0cY0" role="lGtFl">
              <node concept="1TM$A" id="4TWmWFY0cY1" role="7EUXB" />
            </node>
            <node concept="1z9TsT" id="4TWmWFY0do6" role="lGtFl">
              <node concept="OjmMv" id="4TWmWFY0do7" role="1w35rA">
                <node concept="19SGf9" id="4TWmWFY0do8" role="OjmMu">
                  <node concept="19SUe$" id="4TWmWFY0do9" role="19SJt6">
                    <property role="19SUeA" value="The performative Inform cannot be used for sending messages with content of type AgentAction" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2zH6wq" id="4TWmWFXYMQi" role="1aduh9" />
          <node concept="2JkjsH" id="4TWmWFXY8w_" role="1aduh9">
            <property role="2NJI0k" value="4MI7ZB$IHdc/Inform" />
            <node concept="1g4zL5" id="4TWmWFXY8wF" role="2rofCB">
              <node concept="2vmvy6" id="4TWmWFXZ90R" role="2uKbmB">
                <node concept="7CXmI" id="4TWmWFY0bkW" role="lGtFl">
                  <node concept="1TM$A" id="4TWmWFY0bkX" role="7EUXB" />
                </node>
              </node>
            </node>
            <node concept="2vmvVl" id="4TWmWFY0bbu" role="2rofC_">
              <node concept="7CXmI" id="4TWmWFY0box" role="lGtFl">
                <node concept="1TM$A" id="4TWmWFY0boy" role="7EUXB" />
              </node>
            </node>
            <node concept="2vmvVl" id="4TWmWFY0beJ" role="2rofCy">
              <node concept="7CXmI" id="4TWmWFY0brE" role="lGtFl">
                <node concept="1TM$A" id="4TWmWFY0brF" role="7EUXB" />
              </node>
            </node>
            <node concept="1z9TsT" id="4TWmWFY0d_U" role="lGtFl">
              <node concept="OjmMv" id="4TWmWFY0d_V" role="1w35rA">
                <node concept="19SGf9" id="4TWmWFY0d_W" role="OjmMu">
                  <node concept="19SUe$" id="4TWmWFY0d_X" role="19SJt6">
                    <property role="19SUeA" value="The type of content of the message, the receiver and the actual content of the message must be specified" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2zH6wq" id="4TWmWFXY8yr" role="1aduh9" />
          <node concept="2R7DHE" id="2VwqaRpzg9X" role="1aduh9">
            <node concept="1fMSWk" id="2VwqaRpzg9Z" role="3NSspm">
              <property role="TrG5h" value="ping" />
              <node concept="1g4zL5" id="2VwqaRpzga1" role="2uPfNm">
                <node concept="2Ss9cW" id="2VwqaRpzhyr" role="2uKbmB">
                  <ref role="2Ss9cX" node="2umCVQRtUsI" resolve="pingMsg" />
                  <node concept="7CXmI" id="2VwqaRpzh$D" role="lGtFl">
                    <node concept="1TM$A" id="2VwqaRpzh$E" role="7EUXB" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="1z9TsT" id="2VwqaRpzgh7" role="lGtFl">
              <node concept="OjmMv" id="2VwqaRpzgh8" role="1w35rA">
                <node concept="19SGf9" id="2VwqaRpzgh9" role="OjmMu">
                  <node concept="19SUe$" id="2VwqaRpzgha" role="19SJt6">
                    <property role="19SUeA" value="A message type, if specified, must be an ontology element" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2zH6wq" id="2umCVQRu4AN" role="1aduh9" />
          <node concept="2R7DHE" id="4TWmWFY0dMh" role="1aduh9">
            <node concept="1fMSWk" id="4TWmWFY0dMj" role="3NSspm">
              <property role="TrG5h" value="test" />
              <node concept="1g4zL5" id="4TWmWFY0dMl" role="2uPfNm">
                <node concept="2Ss9cW" id="4TWmWFY0fgx" role="2uKbmB">
                  <ref role="2Ss9cX" node="4hvwZTP0kw_" resolve="testConcept" />
                </node>
              </node>
            </node>
            <node concept="1aduha" id="4TWmWFY0fsu" role="3NSspj">
              <node concept="1eBHpp" id="4TWmWFY0fDb" role="1aduh9">
                <property role="2NJI0k" value="4MI7ZB$IHfN/Request" />
                <node concept="2S399m" id="4TWmWFY0g2V" role="1eBHp7">
                  <node concept="2Ss9cW" id="4TWmWFY0g7a" role="2S399n">
                    <ref role="2Ss9cX" node="4hvwZTP0kw_" resolve="testConcept" />
                  </node>
                  <node concept="30bdrP" id="4TWmWFY0g7e" role="2S399l" />
                </node>
                <node concept="1g4zL5" id="4TWmWFY0fDd" role="1eBHp4">
                  <node concept="2Ss9cW" id="4TWmWFY0fYz" role="2uKbmB">
                    <ref role="2Ss9cX" node="4hvwZTP0kw_" resolve="testConcept" />
                  </node>
                </node>
                <node concept="1fQeJu" id="4TWmWFY0fLP" role="1eBHp3">
                  <ref role="1fQeJS" node="4TWmWFY0dMj" resolve="test" />
                </node>
                <node concept="7CXmI" id="4TWmWFY0gbC" role="lGtFl">
                  <node concept="1TM$A" id="4TWmWFY0gbD" role="7EUXB" />
                </node>
                <node concept="1z9TsT" id="4TWmWFY0gkG" role="lGtFl">
                  <node concept="OjmMv" id="4TWmWFY0gkH" role="1w35rA">
                    <node concept="19SGf9" id="4TWmWFY0gkI" role="OjmMu">
                      <node concept="19SUe$" id="4TWmWFY0gkJ" role="19SJt6">
                        <property role="19SUeA" value="The performative Request cannot be used for sending message with content of type Concept" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2zH6wq" id="4TWmWFY0dGO" role="1aduh9" />
          <node concept="2R7DHE" id="2VwqaRpzfAl" role="1aduh9">
            <node concept="1fMSWk" id="2VwqaRpzfAn" role="3NSspm">
              <property role="TrG5h" value="test" />
              <node concept="1g4zL5" id="2VwqaRpzfAp" role="2uPfNm">
                <node concept="2Ss9cW" id="2VwqaRpzhB_" role="2uKbmB">
                  <ref role="2Ss9cX" node="4hvwZTP08y$" resolve="testAction" />
                </node>
              </node>
            </node>
            <node concept="1z9TsT" id="2VwqaRpzfS2" role="lGtFl">
              <node concept="OjmMv" id="2VwqaRpzfS3" role="1w35rA">
                <node concept="19SGf9" id="2VwqaRpzfS4" role="OjmMu">
                  <node concept="19SUe$" id="2VwqaRpzfS5" role="19SJt6">
                    <property role="19SUeA" value="The type of the received message is the same as the the expected declared type&#10;Operations on the received message return values of the expected type" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="1aduha" id="2VwqaRpzhEX" role="3NSspj">
              <node concept="1adJid" id="2VwqaRpzhGa" role="1aduh9">
                <property role="TrG5h" value="testMsg" />
                <node concept="1fQeJu" id="2VwqaRpzhNj" role="1adJii">
                  <ref role="1fQeJS" node="2VwqaRpzfAn" resolve="test" />
                  <node concept="7CXmI" id="2VwqaRpzi0X" role="lGtFl">
                    <node concept="1TM$A" id="2VwqaRpzi0Y" role="7EUXB" />
                  </node>
                </node>
                <node concept="1g4zL5" id="2VwqaRpzhXu" role="2zM23F">
                  <node concept="2Ss9cW" id="2VwqaRpzhZe" role="2uKbmB">
                    <ref role="2Ss9cX" node="4hvwZTP0kw_" resolve="testConcept" />
                  </node>
                </node>
              </node>
              <node concept="1adJid" id="2VwqaRpzi4$" role="1aduh9">
                <property role="TrG5h" value="dummy" />
                <node concept="1QScDb" id="2VwqaRpziae" role="1adJii">
                  <node concept="CgaMy" id="2VwqaRpzidB" role="1QScD9" />
                  <node concept="1fQeJu" id="2VwqaRpzi8d" role="30czhm">
                    <ref role="1fQeJS" node="2VwqaRpzfAn" resolve="test" />
                  </node>
                  <node concept="7CXmI" id="2VwqaRpzjmO" role="lGtFl">
                    <node concept="30Omv" id="2VwqaRpzjtJ" role="7EUXB">
                      <node concept="3NxlSm" id="2VwqaRpzjxe" role="31d$z" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="1adJid" id="2VwqaRpzim8" role="1aduh9">
                <property role="TrG5h" value="dummy2" />
                <node concept="1QScDb" id="2VwqaRpzizj" role="1adJii">
                  <node concept="Cg7_4" id="2VwqaRpziA$" role="1QScD9" />
                  <node concept="1fQeJu" id="2VwqaRpziwW" role="30czhm">
                    <ref role="1fQeJS" node="2VwqaRpzfAn" resolve="test" />
                  </node>
                  <node concept="7CXmI" id="2VwqaRpzjBN" role="lGtFl">
                    <node concept="30Omv" id="2VwqaRpzjI0" role="7EUXB">
                      <node concept="3iBYCm" id="2VwqaRpzjL8" role="31d$z">
                        <node concept="3NxlSm" id="2VwqaRpzjLn" role="3iBWmK" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="1adJid" id="2VwqaRpziPH" role="1aduh9">
                <property role="TrG5h" value="dummy3" />
                <node concept="1QScDb" id="2VwqaRpzj5$" role="1adJii">
                  <node concept="FICQ5" id="2VwqaRpzj90" role="1QScD9" />
                  <node concept="1fQeJu" id="2VwqaRpzj2g" role="30czhm">
                    <ref role="1fQeJS" node="2VwqaRpzfAn" resolve="test" />
                  </node>
                  <node concept="7CXmI" id="2VwqaRpzjLB" role="lGtFl">
                    <node concept="30Omv" id="2VwqaRpzjRO" role="7EUXB">
                      <node concept="2Ss9cW" id="2VwqaRpzjUW" role="31d$z">
                        <ref role="2Ss9cX" node="4hvwZTP08y$" resolve="testAction" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2zH6wq" id="4TWmWFY0dRP" role="1aduh9" />
          <node concept="1adJid" id="4TWmWFY0dZ0" role="1aduh9">
            <property role="TrG5h" value="dummyLocalName" />
            <node concept="1QScDb" id="4TWmWFY0eeR" role="1adJii">
              <node concept="3gvPjt" id="4TWmWFY0eiC" role="1QScD9" />
              <node concept="3BiLgO" id="4TWmWFY0ebm" role="30czhm">
                <ref role="3BiLgL" node="2umCVQRtUvv" resolve="dummyAID" />
              </node>
              <node concept="7CXmI" id="4TWmWFY0ex1" role="lGtFl">
                <node concept="30Omv" id="4TWmWFY0eCc" role="7EUXB">
                  <node concept="30bdrU" id="4TWmWFY0eFN" role="31d$z" />
                </node>
              </node>
            </node>
            <node concept="1z9TsT" id="4TWmWFY0fa_" role="lGtFl">
              <node concept="OjmMv" id="4TWmWFY0faA" role="1w35rA">
                <node concept="19SGf9" id="4TWmWFY0faB" role="OjmMu">
                  <node concept="19SUe$" id="4TWmWFY0faC" role="19SJt6">
                    <property role="19SUeA" value="Check that operations on the AID return the expected value" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="1adJid" id="4TWmWFY0eFR" role="1aduh9">
            <property role="TrG5h" value="dummyName" />
            <node concept="1QScDb" id="4TWmWFY0eFS" role="1adJii">
              <node concept="3BiLgO" id="4TWmWFY0eFU" role="30czhm">
                <ref role="3BiLgL" node="2umCVQRtUvv" resolve="dummyAID" />
              </node>
              <node concept="7CXmI" id="4TWmWFY0eFV" role="lGtFl">
                <node concept="30Omv" id="4TWmWFY0eFW" role="7EUXB">
                  <node concept="30bdrU" id="4TWmWFY0eFX" role="31d$z" />
                </node>
              </node>
              <node concept="3gvQak" id="4TWmWFY0ePD" role="1QScD9" />
            </node>
          </node>
        </node>
        <node concept="3KE$Bk" id="4hvwZTOZRfF" role="19Gq0t">
          <ref role="21GGV3" node="4hvwZTOZRf7" resolve="DummyOntology" />
        </node>
        <node concept="3KE$Bk" id="4hvwZTP0p6I" role="19Gq0t">
          <ref role="21GGV3" node="4hvwZTP0oZV" resolve="DummyModule" />
        </node>
      </node>
    </node>
    <node concept="1qefOq" id="4hvwZTP0oVD" role="1SKRRt">
      <node concept="198UmT" id="4hvwZTP0oZV" role="1qenE9">
        <property role="TrG5h" value="DummyModule" />
        <node concept="2Ss9d8" id="2umCVQRtUsI" role="3KDhAD">
          <property role="TrG5h" value="pingMsg" />
          <node concept="2Ss9d7" id="2umCVQRtUt0" role="S5Trm">
            <property role="TrG5h" value="payload" />
            <node concept="30bdrU" id="2umCVQRtUt4" role="2S399n" />
          </node>
        </node>
        <node concept="_ixoA" id="4hvwZTP0oZX" role="3KDhAD" />
      </node>
    </node>
    <node concept="1qefOq" id="4hvwZTOZRdh" role="1SKRRt">
      <node concept="2C2W_Q" id="4hvwZTOZRf7" role="1qenE9">
        <property role="TrG5h" value="DummyOntology" />
        <node concept="2MkpuT" id="4hvwZTP08y$" role="_iOnB">
          <property role="TrG5h" value="testAction" />
          <node concept="2Ss9d7" id="4hvwZTP08yR" role="S5Trm">
            <property role="TrG5h" value="payload" />
            <node concept="30bdrU" id="4hvwZTP08yW" role="2S399n" />
          </node>
        </node>
        <node concept="2MkpuS" id="4hvwZTP0kw_" role="_iOnB">
          <property role="TrG5h" value="testConcept" />
          <node concept="2Ss9d7" id="4hvwZTP0kxx" role="S5Trm">
            <property role="TrG5h" value="payload" />
            <node concept="30bdrU" id="4hvwZTP0kxA" role="2S399n" />
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="LiM7Y" id="3NjOp6FKO80">
    <property role="TrG5h" value="BehaviourActivation" />
    <node concept="3clFbS" id="3NjOp6FL6eq" role="LjaKd">
      <node concept="2HxZob" id="3NjOp6FL6eo" role="3cqZAp">
        <node concept="1iFQzN" id="3NjOp6FL6eu" role="3iKnsn">
          <ref role="1iFR8X" to="ekwn:2XByp9s_j7f" resolve="Complete" />
        </node>
      </node>
      <node concept="2TK7Tu" id="74sp3urWwp5" role="3cqZAp">
        <property role="2TTd_B" value="SendPing" />
      </node>
      <node concept="yd1bK" id="3NjOp6FL6ru" role="3cqZAp">
        <node concept="pLAjd" id="3NjOp6FL6rw" role="yd6KS">
          <property role="pLAjf" value="VK_ENTER" />
        </node>
      </node>
      <node concept="3clFbH" id="3NjOp6FL6rJ" role="3cqZAp" />
      <node concept="yd1bK" id="3NjOp6FL6rW" role="3cqZAp">
        <node concept="pLAjd" id="3NjOp6FL6rX" role="yd6KS">
          <property role="pLAjf" value="VK_TAB" />
        </node>
      </node>
      <node concept="yd1bK" id="2wISjE1pk5N" role="3cqZAp">
        <node concept="pLAjd" id="2wISjE1pk5O" role="yd6KS">
          <property role="pLAjf" value="VK_TAB" />
        </node>
      </node>
      <node concept="yd1bK" id="3NjOp6FL6sj" role="3cqZAp">
        <node concept="pLAjd" id="3NjOp6FL6sk" role="yd6KS">
          <property role="pLAjf" value="VK_ENTER" />
        </node>
      </node>
      <node concept="2TK7Tu" id="3NjOp6FL6yw" role="3cqZAp">
        <property role="2TTd_B" value="activate" />
      </node>
      <node concept="3clFbH" id="3NjOp6FL6yH" role="3cqZAp" />
      <node concept="2HxZob" id="3NjOp6FL6z8" role="3cqZAp">
        <node concept="1iFQzN" id="3NjOp6FL6z9" role="3iKnsn">
          <ref role="1iFR8X" to="ekwn:2XByp9s_j7f" resolve="Complete" />
        </node>
      </node>
      <node concept="yd1bK" id="3NjOp6FL6za" role="3cqZAp">
        <node concept="pLAjd" id="3NjOp6FL6zb" role="yd6KS">
          <property role="pLAjf" value="VK_ENTER" />
        </node>
      </node>
      <node concept="3clFbH" id="3NjOp6FL6yU" role="3cqZAp" />
      <node concept="2HxZob" id="3NjOp6FL6zw" role="3cqZAp">
        <node concept="1iFQzN" id="3NjOp6FL6zx" role="3iKnsn">
          <ref role="1iFR8X" to="ekwn:2XByp9s_j7f" resolve="Complete" />
        </node>
      </node>
      <node concept="yd1bK" id="3NjOp6FL6zy" role="3cqZAp">
        <node concept="pLAjd" id="3NjOp6FL6zz" role="yd6KS">
          <property role="pLAjf" value="VK_ENTER" />
        </node>
      </node>
      <node concept="3clFbH" id="3NjOp6FL6rP" role="3cqZAp" />
    </node>
    <node concept="1qefOq" id="3o3xT4dgkPh" role="25YQFr">
      <node concept="3eIggM" id="3o3xT4dgkPf" role="1qenE9">
        <property role="TrG5h" value="Dummy" />
        <node concept="p0Vrc" id="3o3xT4dgkPD" role="2M3AkQ">
          <node concept="3Qnb9i" id="3o3xT4dgkPP" role="3QnlHL">
            <ref role="3Qnb9j" node="2umCVQRtUpA" resolve="SendPing" />
          </node>
        </node>
        <node concept="3KE$_k" id="2wISjE1pk0T" role="19Gq0t">
          <ref role="21GGV3" node="2umCVQRtUpA" resolve="SendPing" />
        </node>
      </node>
    </node>
    <node concept="1qefOq" id="3o3xT4dgkQ1" role="25YQCW">
      <node concept="3eIggM" id="3o3xT4dgkQ9" role="1qenE9">
        <property role="TrG5h" value="Dummy" />
        <node concept="1cWWE2" id="3o3xT4dgkQd" role="19Gq0t">
          <node concept="LIFWc" id="3o3xT4dgkQf" role="lGtFl">
            <property role="ZRATv" value="true" />
            <property role="OXtK3" value="true" />
            <property role="p6zMq" value="0" />
            <property role="p6zMs" value="0" />
            <property role="LIFWd" value="empty_chunk" />
          </node>
        </node>
        <node concept="2vmvVl" id="3o3xT4dgkQa" role="2M3AkQ" />
      </node>
    </node>
  </node>
</model>

