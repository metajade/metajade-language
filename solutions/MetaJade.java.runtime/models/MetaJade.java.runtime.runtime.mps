<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:c21d77c9-2c31-4cf8-b986-5b0abc8b6d3b(MetaJade.java.runtime.runtime)">
  <persistence version="9" />
  <languages>
    <use id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections" version="1" />
    <use id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage" version="11" />
  </languages>
  <imports>
    <import index="93v9" ref="0dfebf0c-13fb-476f-90ad-86743b0c71ae/java:jade.core(MetaJade.java.stubs/)" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" />
    <import index="rlzj" ref="0dfebf0c-13fb-476f-90ad-86743b0c71ae/java:jade.core.behaviours(MetaJade.java.stubs/)" />
    <import index="33ny" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.util(JDK/)" />
    <import index="adhc" ref="0dfebf0c-13fb-476f-90ad-86743b0c71ae/java:jade.lang.acl(MetaJade.java.stubs/)" />
    <import index="9vs0" ref="0dfebf0c-13fb-476f-90ad-86743b0c71ae/java:jade.domain.FIPAAgentManagement(MetaJade.java.stubs/)" />
    <import index="1ctc" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.util.stream(JDK/)" />
    <import index="9enu" ref="0dfebf0c-13fb-476f-90ad-86743b0c71ae/java:jade.domain(MetaJade.java.stubs/)" />
    <import index="1et" ref="0dfebf0c-13fb-476f-90ad-86743b0c71ae/java:jade.content.onto(MetaJade.java.stubs/)" />
    <import index="xunt" ref="0dfebf0c-13fb-476f-90ad-86743b0c71ae/java:jade.content.lang(MetaJade.java.stubs/)" />
    <import index="kozn" ref="0dfebf0c-13fb-476f-90ad-86743b0c71ae/java:jade.content(MetaJade.java.stubs/)" />
    <import index="2is" ref="708a03ad-8699-43c9-821a-6cd00b68e9f8/java:fj(org.iets3.core.expr.genjava.functionalJava/)" />
    <import index="xofi" ref="0dfebf0c-13fb-476f-90ad-86743b0c71ae/java:jade.content.lang.sl(MetaJade.java.stubs/)" />
    <import index="hfdx" ref="0dfebf0c-13fb-476f-90ad-86743b0c71ae/java:jade.content.onto.basic(MetaJade.java.stubs/)" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1219920932475" name="jetbrains.mps.baseLanguage.structure.VariableArityType" flags="in" index="8X2XB">
        <child id="1219921048460" name="componentType" index="8Xvag" />
      </concept>
      <concept id="1082485599095" name="jetbrains.mps.baseLanguage.structure.BlockStatement" flags="nn" index="9aQIb">
        <child id="1082485599096" name="statements" index="9aQI4" />
      </concept>
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="8118189177080264853" name="jetbrains.mps.baseLanguage.structure.AlternativeType" flags="ig" index="nSUau">
        <child id="8118189177080264854" name="alternative" index="nSUat" />
      </concept>
      <concept id="1465982738277781862" name="jetbrains.mps.baseLanguage.structure.PlaceholderMember" flags="nn" index="2tJIrI" />
      <concept id="1173175405605" name="jetbrains.mps.baseLanguage.structure.ArrayAccessExpression" flags="nn" index="AH0OO">
        <child id="1173175577737" name="index" index="AHEQo" />
        <child id="1173175590490" name="array" index="AHHXb" />
      </concept>
      <concept id="1154032098014" name="jetbrains.mps.baseLanguage.structure.AbstractLoopStatement" flags="nn" index="2LF5Ji">
        <child id="1154032183016" name="body" index="2LFqv$" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1197029447546" name="jetbrains.mps.baseLanguage.structure.FieldReferenceOperation" flags="nn" index="2OwXpG">
        <reference id="1197029500499" name="fieldDeclaration" index="2Oxat5" />
      </concept>
      <concept id="1083245097125" name="jetbrains.mps.baseLanguage.structure.EnumClass" flags="ig" index="Qs71p">
        <child id="1083245396908" name="enumConstant" index="Qtgdg" />
      </concept>
      <concept id="1083245299891" name="jetbrains.mps.baseLanguage.structure.EnumConstantDeclaration" flags="ig" index="QsSxf" />
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1070475354124" name="jetbrains.mps.baseLanguage.structure.ThisExpression" flags="nn" index="Xjq3P" />
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="4952749571008284462" name="jetbrains.mps.baseLanguage.structure.CatchVariable" flags="ng" index="XOnhg" />
      <concept id="1081236700938" name="jetbrains.mps.baseLanguage.structure.StaticMethodDeclaration" flags="ig" index="2YIFZL" />
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1164991038168" name="jetbrains.mps.baseLanguage.structure.ThrowStatement" flags="nn" index="YS8fn">
        <child id="1164991057263" name="throwable" index="YScLw" />
      </concept>
      <concept id="1070533707846" name="jetbrains.mps.baseLanguage.structure.StaticFieldReference" flags="nn" index="10M0yZ">
        <reference id="1144433057691" name="classifier" index="1PxDUh" />
      </concept>
      <concept id="1070534058343" name="jetbrains.mps.baseLanguage.structure.NullLiteral" flags="nn" index="10Nm6u" />
      <concept id="1070534370425" name="jetbrains.mps.baseLanguage.structure.IntegerType" flags="in" index="10Oyi0" />
      <concept id="1070534760951" name="jetbrains.mps.baseLanguage.structure.ArrayType" flags="in" index="10Q1$e">
        <child id="1070534760952" name="componentType" index="10Q1$1" />
      </concept>
      <concept id="1070534934090" name="jetbrains.mps.baseLanguage.structure.CastExpression" flags="nn" index="10QFUN">
        <child id="1070534934091" name="type" index="10QFUM" />
        <child id="1070534934092" name="expression" index="10QFUP" />
      </concept>
      <concept id="1068390468200" name="jetbrains.mps.baseLanguage.structure.FieldDeclaration" flags="ig" index="312cEg" />
      <concept id="1068390468198" name="jetbrains.mps.baseLanguage.structure.ClassConcept" flags="ig" index="312cEu" />
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <property id="1176718929932" name="isFinal" index="3TUv4t" />
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1513279640923991009" name="jetbrains.mps.baseLanguage.structure.IGenericClassCreator" flags="ng" index="366HgL">
        <property id="1513279640906337053" name="inferTypeParams" index="373rjd" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="4269842503726207156" name="jetbrains.mps.baseLanguage.structure.LongLiteral" flags="nn" index="1adDum">
        <property id="4269842503726207157" name="value" index="1adDun" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123165" name="jetbrains.mps.baseLanguage.structure.InstanceMethodDeclaration" flags="ig" index="3clFb_" />
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1082485599094" name="ifFalseStatement" index="9aQIa" />
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580123140" name="jetbrains.mps.baseLanguage.structure.ConstructorDeclaration" flags="ig" index="3clFbW" />
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242867" name="jetbrains.mps.baseLanguage.structure.LongType" flags="in" index="3cpWsb" />
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1068581517677" name="jetbrains.mps.baseLanguage.structure.VoidType" flags="in" index="3cqZAl" />
      <concept id="1079359253375" name="jetbrains.mps.baseLanguage.structure.ParenthesizedExpression" flags="nn" index="1eOMI4">
        <child id="1079359253376" name="expression" index="1eOMHV" />
      </concept>
      <concept id="1081506762703" name="jetbrains.mps.baseLanguage.structure.GreaterThanExpression" flags="nn" index="3eOSWO" />
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1212685548494" name="jetbrains.mps.baseLanguage.structure.ClassCreator" flags="nn" index="1pGfFk" />
      <concept id="1107461130800" name="jetbrains.mps.baseLanguage.structure.Classifier" flags="ng" index="3pOWGL">
        <child id="5375687026011219971" name="member" index="jymVt" unordered="true" />
      </concept>
      <concept id="7812454656619025412" name="jetbrains.mps.baseLanguage.structure.LocalMethodCall" flags="nn" index="1rXfSq" />
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="3093926081414150598" name="jetbrains.mps.baseLanguage.structure.MultipleCatchClause" flags="ng" index="3uVAMA">
        <child id="8276990574895933173" name="catchBody" index="1zc67A" />
        <child id="8276990574895933172" name="throwable" index="1zc67B" />
      </concept>
      <concept id="8276990574909231788" name="jetbrains.mps.baseLanguage.structure.FinallyClause" flags="ng" index="1wplmZ">
        <child id="8276990574909234106" name="finallyBody" index="1wplMD" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1144226303539" name="jetbrains.mps.baseLanguage.structure.ForeachStatement" flags="nn" index="1DcWWT">
        <child id="1144226360166" name="iterable" index="1DdaDG" />
      </concept>
      <concept id="1144230876926" name="jetbrains.mps.baseLanguage.structure.AbstractForStatement" flags="nn" index="1DupvO">
        <child id="1144230900587" name="variable" index="1Duv9x" />
      </concept>
      <concept id="5351203823916750322" name="jetbrains.mps.baseLanguage.structure.TryUniversalStatement" flags="nn" index="3J1_TO">
        <child id="8276990574886367510" name="catchClause" index="1zxBo5" />
        <child id="8276990574886367509" name="finallyClause" index="1zxBo6" />
        <child id="8276990574886367508" name="body" index="1zxBo7" />
      </concept>
      <concept id="1208890769693" name="jetbrains.mps.baseLanguage.structure.ArrayLengthOperation" flags="nn" index="1Rwk04" />
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
      <concept id="1146644623116" name="jetbrains.mps.baseLanguage.structure.PrivateVisibility" flags="nn" index="3Tm6S6" />
      <concept id="1116615150612" name="jetbrains.mps.baseLanguage.structure.ClassifierClassExpression" flags="nn" index="3VsKOn">
        <reference id="1116615189566" name="classifier" index="3VsUkX" />
      </concept>
    </language>
    <language id="fd392034-7849-419d-9071-12563d152375" name="jetbrains.mps.baseLanguage.closures">
      <concept id="1199542442495" name="jetbrains.mps.baseLanguage.closures.structure.FunctionType" flags="in" index="1ajhzC">
        <child id="1199542457201" name="resultType" index="1ajl9A" />
        <child id="1199542501692" name="parameterType" index="1ajw0F" />
      </concept>
      <concept id="1199569711397" name="jetbrains.mps.baseLanguage.closures.structure.ClosureLiteral" flags="nn" index="1bVj0M">
        <child id="1199569906740" name="parameter" index="1bW2Oz" />
        <child id="1199569916463" name="body" index="1bW5cS" />
      </concept>
      <concept id="1225797177491" name="jetbrains.mps.baseLanguage.closures.structure.InvokeFunctionOperation" flags="nn" index="1Bd96e">
        <child id="1225797361612" name="parameter" index="1BdPVh" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="1151688443754" name="jetbrains.mps.baseLanguage.collections.structure.ListType" flags="in" index="_YKpA">
        <child id="1151688676805" name="elementType" index="_ZDj9" />
      </concept>
      <concept id="1153943597977" name="jetbrains.mps.baseLanguage.collections.structure.ForEachStatement" flags="nn" index="2Gpval">
        <child id="1153944400369" name="variable" index="2Gsz3X" />
        <child id="1153944424730" name="inputSequence" index="2GsD0m" />
      </concept>
      <concept id="1153944193378" name="jetbrains.mps.baseLanguage.collections.structure.ForEachVariable" flags="nr" index="2GrKxI" />
      <concept id="1153944233411" name="jetbrains.mps.baseLanguage.collections.structure.ForEachVariableReference" flags="nn" index="2GrUjf">
        <reference id="1153944258490" name="variable" index="2Gs0qQ" />
      </concept>
      <concept id="1237721394592" name="jetbrains.mps.baseLanguage.collections.structure.AbstractContainerCreator" flags="nn" index="HWqM0">
        <child id="1237721435807" name="elementType" index="HW$YZ" />
      </concept>
      <concept id="1160600644654" name="jetbrains.mps.baseLanguage.collections.structure.ListCreatorWithInit" flags="nn" index="Tc6Ow" />
    </language>
  </registry>
  <node concept="312cEu" id="1MgVVihlvkL">
    <property role="TrG5h" value="Behaviours" />
    <node concept="2tJIrI" id="1MgVVihlRbT" role="jymVt" />
    <node concept="3clFbW" id="1MgVVihlvtB" role="jymVt">
      <node concept="3cqZAl" id="1MgVVihlvtD" role="3clF45" />
      <node concept="3Tm6S6" id="1MgVVihlvu1" role="1B3o_S" />
      <node concept="3clFbS" id="1MgVVihlvtF" role="3clF47">
        <node concept="YS8fn" id="1MgVVihlvus" role="3cqZAp">
          <node concept="2ShNRf" id="1MgVVihlvuZ" role="YScLw">
            <node concept="1pGfFk" id="1MgVVihlwZt" role="2ShVmc">
              <ref role="37wK5l" to="wyt6:~IllegalStateException.&lt;init&gt;(java.lang.String)" resolve="IllegalStateException" />
              <node concept="2YIFZM" id="1MgVVihlx6Q" role="37wK5m">
                <ref role="37wK5l" to="wyt6:~String.format(java.lang.String,java.lang.Object...)" resolve="format" />
                <ref role="1Pybhc" to="wyt6:~String" resolve="String" />
                <node concept="Xl_RD" id="1MgVVihlx9S" role="37wK5m">
                  <property role="Xl_RC" value="Class %s cannot be instantiated: it can only contain static methods" />
                </node>
                <node concept="3VsKOn" id="1MgVVihlxpF" role="37wK5m">
                  <ref role="3VsUkX" node="1MgVVihlvkL" resolve="Behaviours" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="1MgVVihlxAF" role="jymVt" />
    <node concept="2YIFZL" id="1MgVVihlAcC" role="jymVt">
      <property role="TrG5h" value="sequence" />
      <node concept="3clFbS" id="1MgVVihlAcG" role="3clF47">
        <node concept="3cpWs8" id="1MgVVihlABb" role="3cqZAp">
          <node concept="3cpWsn" id="1MgVVihlAB9" role="3cpWs9">
            <property role="3TUv4t" value="true" />
            <property role="TrG5h" value="sequenceBehaviour" />
            <node concept="3uibUv" id="1MgVVihlABA" role="1tU5fm">
              <ref role="3uigEE" to="rlzj:~SequentialBehaviour" resolve="SequentialBehaviour" />
            </node>
            <node concept="2ShNRf" id="1MgVVihlADH" role="33vP2m">
              <node concept="1pGfFk" id="1MgVVihlAOM" role="2ShVmc">
                <ref role="37wK5l" to="rlzj:~SequentialBehaviour.&lt;init&gt;()" resolve="SequentialBehaviour" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="1MgVVihlAPZ" role="3cqZAp">
          <node concept="2OqwBi" id="1MgVVihlB7u" role="3clFbG">
            <node concept="37vLTw" id="1MgVVihlAPX" role="2Oq$k0">
              <ref role="3cqZAo" node="1MgVVihlAB9" resolve="sequenceBehaviour" />
            </node>
            <node concept="liA8E" id="1MgVVihlBrz" role="2OqNvi">
              <ref role="37wK5l" to="rlzj:~SequentialBehaviour.addSubBehaviour(jade.core.behaviours.Behaviour)" resolve="addSubBehaviour" />
              <node concept="37vLTw" id="1MgVVihlBui" role="37wK5m">
                <ref role="3cqZAo" node="1MgVVihlAoU" resolve="behaviour" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="1MgVVihlBys" role="3cqZAp">
          <node concept="2OqwBi" id="1MgVVihlCmT" role="3clFbG">
            <node concept="2YIFZM" id="1MgVVihlBE6" role="2Oq$k0">
              <ref role="37wK5l" to="33ny:~Arrays.stream(java.lang.Object[])" resolve="stream" />
              <ref role="1Pybhc" to="33ny:~Arrays" resolve="Arrays" />
              <node concept="37vLTw" id="1MgVVihlBGZ" role="37wK5m">
                <ref role="3cqZAo" node="1MgVVihlAtm" resolve="behaviours" />
              </node>
            </node>
            <node concept="liA8E" id="1MgVVihlDyn" role="2OqNvi">
              <ref role="37wK5l" to="1ctc:~Stream.forEach(java.util.function.Consumer)" resolve="forEach" />
              <node concept="1bVj0M" id="1MgVVihlHy5" role="37wK5m">
                <node concept="3clFbS" id="1MgVVihlHy6" role="1bW5cS">
                  <node concept="3clFbF" id="1MgVVihlHMs" role="3cqZAp">
                    <node concept="2OqwBi" id="1MgVVihlI18" role="3clFbG">
                      <node concept="37vLTw" id="1MgVVihlHMr" role="2Oq$k0">
                        <ref role="3cqZAo" node="1MgVVihlAB9" resolve="sequenceBehaviour" />
                      </node>
                      <node concept="liA8E" id="1MgVVihlIrB" role="2OqNvi">
                        <ref role="37wK5l" to="rlzj:~SequentialBehaviour.addSubBehaviour(jade.core.behaviours.Behaviour)" resolve="addSubBehaviour" />
                        <node concept="37vLTw" id="1MgVVihlJ6V" role="37wK5m">
                          <ref role="3cqZAo" node="1MgVVihlI$2" resolve="b" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="37vLTG" id="1MgVVihlI$2" role="1bW2Oz">
                  <property role="TrG5h" value="b" />
                  <node concept="3uibUv" id="1MgVVihlI$1" role="1tU5fm">
                    <ref role="3uigEE" to="rlzj:~Behaviour" resolve="Behaviour" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="1MgVVihlGfR" role="3cqZAp">
          <node concept="37vLTw" id="1MgVVihlGfP" role="3clFbG">
            <ref role="3cqZAo" node="1MgVVihlAB9" resolve="sequenceBehaviour" />
          </node>
        </node>
      </node>
      <node concept="3uibUv" id="1MgVVihlAkr" role="3clF45">
        <ref role="3uigEE" to="rlzj:~Behaviour" resolve="Behaviour" />
      </node>
      <node concept="3Tm1VV" id="34MKcnMJb" role="1B3o_S" />
      <node concept="37vLTG" id="1MgVVihlAoU" role="3clF46">
        <property role="TrG5h" value="behaviour" />
        <node concept="3uibUv" id="1MgVVihlAoT" role="1tU5fm">
          <ref role="3uigEE" to="rlzj:~Behaviour" resolve="Behaviour" />
        </node>
      </node>
      <node concept="37vLTG" id="1MgVVihlAtm" role="3clF46">
        <property role="TrG5h" value="behaviours" />
        <node concept="8X2XB" id="1MgVVihlAyv" role="1tU5fm">
          <node concept="3uibUv" id="1MgVVihlAui" role="8Xvag">
            <ref role="3uigEE" to="rlzj:~Behaviour" resolve="Behaviour" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="1MgVVihlJhL" role="jymVt" />
    <node concept="2YIFZL" id="1MgVVihlX13" role="jymVt">
      <property role="TrG5h" value="stop" />
      <node concept="3clFbS" id="1MgVVihlX17" role="3clF47">
        <node concept="3clFbF" id="3NjOp6Gkm0M" role="3cqZAp">
          <node concept="2OqwBi" id="3NjOp6Gkm0N" role="3clFbG">
            <node concept="liA8E" id="3NjOp6Gkm0P" role="2OqNvi">
              <ref role="37wK5l" to="93v9:~Agent.doDelete()" resolve="doDelete" />
            </node>
            <node concept="37vLTw" id="3NjOp6Gkmlb" role="2Oq$k0">
              <ref role="3cqZAo" node="3NjOp6GkmgB" resolve="a" />
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="3NjOp6Gkmah" role="3cqZAp">
          <node concept="2YIFZM" id="3NjOp6GkmeP" role="3cqZAk">
            <ref role="37wK5l" to="2is:~Unit.unit()" resolve="unit" />
            <ref role="1Pybhc" to="2is:~Unit" resolve="Unit" />
          </node>
        </node>
      </node>
      <node concept="3uibUv" id="1MgVVihlX15" role="3clF45">
        <ref role="3uigEE" to="2is:~Unit" resolve="Unit" />
      </node>
      <node concept="3Tm1VV" id="1MgVVihlX16" role="1B3o_S" />
      <node concept="37vLTG" id="3NjOp6GkmgB" role="3clF46">
        <property role="TrG5h" value="a" />
        <node concept="3uibUv" id="3NjOp6GkmgA" role="1tU5fm">
          <ref role="3uigEE" to="93v9:~Agent" resolve="Agent" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="28$c8FRSqG1" role="jymVt" />
    <node concept="2YIFZL" id="51gvYr6Ck4V" role="jymVt">
      <property role="TrG5h" value="addBehaviour" />
      <node concept="3clFbS" id="51gvYr6Ck4Y" role="3clF47">
        <node concept="3clFbF" id="51gvYr6Clfl" role="3cqZAp">
          <node concept="2OqwBi" id="51gvYr6ClFW" role="3clFbG">
            <node concept="37vLTw" id="51gvYr6CltW" role="2Oq$k0">
              <ref role="3cqZAo" node="51gvYr6Clna" resolve="a" />
            </node>
            <node concept="liA8E" id="51gvYr6Cm6$" role="2OqNvi">
              <ref role="37wK5l" to="93v9:~Agent.addBehaviour(jade.core.behaviours.Behaviour)" resolve="addBehaviour" />
              <node concept="37vLTw" id="51gvYr6CnGt" role="37wK5m">
                <ref role="3cqZAo" node="51gvYr6Cn5$" resolve="b" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="51gvYr6CnR4" role="3cqZAp">
          <node concept="2YIFZM" id="4JoZLkto_DN" role="3cqZAk">
            <ref role="37wK5l" to="2is:~Unit.unit()" resolve="unit" />
            <ref role="1Pybhc" to="2is:~Unit" resolve="Unit" />
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="51gvYr6Cj93" role="1B3o_S" />
      <node concept="3uibUv" id="51gvYr6CjLY" role="3clF45">
        <ref role="3uigEE" to="2is:~Unit" resolve="Unit" />
      </node>
      <node concept="37vLTG" id="51gvYr6Clna" role="3clF46">
        <property role="TrG5h" value="a" />
        <node concept="3uibUv" id="51gvYr6Cln9" role="1tU5fm">
          <ref role="3uigEE" to="93v9:~Agent" resolve="Agent" />
        </node>
      </node>
      <node concept="37vLTG" id="51gvYr6Cn5$" role="3clF46">
        <property role="TrG5h" value="b" />
        <node concept="3uibUv" id="51gvYr6Cncg" role="1tU5fm">
          <ref role="3uigEE" to="rlzj:~Behaviour" resolve="Behaviour" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="5ZGapTENHD4" role="jymVt" />
    <node concept="2YIFZL" id="80jri$37A" role="jymVt">
      <property role="TrG5h" value="removeBehaviour" />
      <node concept="3clFbS" id="80jri$37D" role="3clF47">
        <node concept="3clFbF" id="80jri$5js" role="3cqZAp">
          <node concept="2OqwBi" id="80jri$5wI" role="3clFbG">
            <node concept="37vLTw" id="80jri$5jr" role="2Oq$k0">
              <ref role="3cqZAo" node="80jri$4Ba" resolve="a" />
            </node>
            <node concept="liA8E" id="80jri$5Xk" role="2OqNvi">
              <ref role="37wK5l" to="93v9:~Agent.removeBehaviour(jade.core.behaviours.Behaviour)" resolve="removeBehaviour" />
              <node concept="37vLTw" id="80jri$62_" role="37wK5m">
                <ref role="3cqZAo" node="80jri$4HF" resolve="b" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="80jri$6gd" role="3cqZAp">
          <node concept="2YIFZM" id="80jri$6u0" role="3cqZAk">
            <ref role="37wK5l" to="2is:~Unit.unit()" resolve="unit" />
            <ref role="1Pybhc" to="2is:~Unit" resolve="Unit" />
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="80jri$2n2" role="1B3o_S" />
      <node concept="3uibUv" id="80jri$35g" role="3clF45">
        <ref role="3uigEE" to="2is:~Unit" resolve="Unit" />
      </node>
      <node concept="37vLTG" id="80jri$4Ba" role="3clF46">
        <property role="TrG5h" value="a" />
        <node concept="3uibUv" id="80jri$4B9" role="1tU5fm">
          <ref role="3uigEE" to="93v9:~Agent" resolve="Agent" />
        </node>
      </node>
      <node concept="37vLTG" id="80jri$4HF" role="3clF46">
        <property role="TrG5h" value="b" />
        <node concept="3uibUv" id="80jri$4Kt" role="1tU5fm">
          <ref role="3uigEE" to="rlzj:~Behaviour" resolve="Behaviour" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="51gvYr6CkT6" role="jymVt" />
    <node concept="2YIFZL" id="1MgVVihnmtN" role="jymVt">
      <property role="TrG5h" value="reply" />
      <node concept="3clFbS" id="1MgVVihnmtR" role="3clF47">
        <node concept="3clFbF" id="3EtpyNPH6we" role="3cqZAp">
          <node concept="2OqwBi" id="3EtpyNPH6Jf" role="3clFbG">
            <node concept="37vLTw" id="3EtpyNPH6wc" role="2Oq$k0">
              <ref role="3cqZAo" node="3EtpyNPH6hX" resolve="a" />
            </node>
            <node concept="liA8E" id="3EtpyNPH771" role="2OqNvi">
              <ref role="37wK5l" to="93v9:~Agent.send(jade.lang.acl.ACLMessage)" resolve="send" />
              <node concept="2YIFZM" id="3EtpyNQu7cs" role="37wK5m">
                <ref role="1Pybhc" node="1MgVVihn4Br" resolve="Messages" />
                <ref role="37wK5l" node="7OgJtfkHHd" resolve="message" />
                <node concept="37vLTw" id="3EtpyNQu8Tf" role="37wK5m">
                  <ref role="3cqZAo" node="3EtpyNQtWlf" resolve="performative" />
                </node>
                <node concept="2OqwBi" id="3EtpyNQubxg" role="37wK5m">
                  <node concept="37vLTw" id="3EtpyNQubiY" role="2Oq$k0">
                    <ref role="3cqZAo" node="3EtpyNPH6hX" resolve="a" />
                  </node>
                  <node concept="liA8E" id="3EtpyNQuc95" role="2OqNvi">
                    <ref role="37wK5l" to="93v9:~Agent.getContentManager()" resolve="getContentManager" />
                  </node>
                </node>
                <node concept="37vLTw" id="7OgJtfkKcu" role="37wK5m">
                  <ref role="3cqZAo" node="3EtpyNQtWZb" resolve="payload" />
                </node>
                <node concept="37vLTw" id="7OgJtfleVz" role="37wK5m">
                  <ref role="3cqZAo" node="7OgJtfleQb" resolve="query" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="3EtpyNPH7qJ" role="3cqZAp">
          <node concept="2YIFZM" id="3EtpyNPH7yn" role="3cqZAk">
            <ref role="37wK5l" to="2is:~Unit.unit()" resolve="unit" />
            <ref role="1Pybhc" to="2is:~Unit" resolve="Unit" />
          </node>
        </node>
      </node>
      <node concept="3uibUv" id="3EtpyNPEb$k" role="3clF45">
        <ref role="3uigEE" to="2is:~Unit" resolve="Unit" />
      </node>
      <node concept="3Tm1VV" id="1MgVVihnmtQ" role="1B3o_S" />
      <node concept="37vLTG" id="3EtpyNPH6hX" role="3clF46">
        <property role="TrG5h" value="a" />
        <node concept="3uibUv" id="3EtpyNPH6o3" role="1tU5fm">
          <ref role="3uigEE" to="93v9:~Agent" resolve="Agent" />
        </node>
      </node>
      <node concept="37vLTG" id="3EtpyNQtWlf" role="3clF46">
        <property role="TrG5h" value="performative" />
        <node concept="3uibUv" id="Oow4MYRauC" role="1tU5fm">
          <ref role="3uigEE" node="1MgVVihmH1q" resolve="Performative" />
        </node>
      </node>
      <node concept="37vLTG" id="3EtpyNQtWZb" role="3clF46">
        <property role="TrG5h" value="payload" />
        <node concept="3uibUv" id="3EtpyNQtXGq" role="1tU5fm">
          <ref role="3uigEE" to="wyt6:~Object" resolve="Object" />
        </node>
      </node>
      <node concept="37vLTG" id="7OgJtfleQb" role="3clF46">
        <property role="TrG5h" value="query" />
        <node concept="3uibUv" id="7OgJtfleSY" role="1tU5fm">
          <ref role="3uigEE" to="adhc:~ACLMessage" resolve="ACLMessage" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="1MgVVihnpMn" role="jymVt" />
    <node concept="2YIFZL" id="7OgJtfkFFk" role="jymVt">
      <property role="TrG5h" value="sendRequest" />
      <node concept="3clFbS" id="7OgJtfkFFl" role="3clF47">
        <node concept="3cpWs8" id="7OgJtfkFFm" role="3cqZAp">
          <node concept="3cpWsn" id="7OgJtfkFFn" role="3cpWs9">
            <property role="TrG5h" value="codec" />
            <node concept="3uibUv" id="7OgJtfkFFo" role="1tU5fm">
              <ref role="3uigEE" to="xunt:~Codec" resolve="Codec" />
            </node>
            <node concept="2ShNRf" id="7OgJtfkFFp" role="33vP2m">
              <node concept="1pGfFk" id="7OgJtfkFFq" role="2ShVmc">
                <ref role="37wK5l" to="xofi:~SLCodec.&lt;init&gt;()" resolve="SLCodec" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="7OgJtfkFFr" role="3cqZAp">
          <node concept="2OqwBi" id="7OgJtfkFFs" role="3clFbG">
            <node concept="37vLTw" id="7OgJtfkFFt" role="2Oq$k0">
              <ref role="3cqZAo" node="7OgJtfkFFH" resolve="a" />
            </node>
            <node concept="liA8E" id="7OgJtfkFFu" role="2OqNvi">
              <ref role="37wK5l" to="93v9:~Agent.send(jade.lang.acl.ACLMessage)" resolve="send" />
              <node concept="2YIFZM" id="7OgJtfkFFv" role="37wK5m">
                <ref role="1Pybhc" node="1MgVVihn4Br" resolve="Messages" />
                <ref role="37wK5l" node="4voUvBeevSu" resolve="message" />
                <node concept="37vLTw" id="7OgJtfkFFw" role="37wK5m">
                  <ref role="3cqZAo" node="7OgJtfkFFL" resolve="performative" />
                </node>
                <node concept="37vLTw" id="7OgJtfkFFx" role="37wK5m">
                  <ref role="3cqZAo" node="7OgJtfkFFn" resolve="codec" />
                </node>
                <node concept="37vLTw" id="7OgJtfkFFy" role="37wK5m">
                  <ref role="3cqZAo" node="7OgJtfkFFJ" resolve="ontology" />
                </node>
                <node concept="2OqwBi" id="7OgJtfkFFz" role="37wK5m">
                  <node concept="37vLTw" id="7OgJtfkFF$" role="2Oq$k0">
                    <ref role="3cqZAo" node="7OgJtfkFFH" resolve="a" />
                  </node>
                  <node concept="liA8E" id="7OgJtfkFF_" role="2OqNvi">
                    <ref role="37wK5l" to="93v9:~Agent.getContentManager()" resolve="getContentManager" />
                  </node>
                </node>
                <node concept="37vLTw" id="7OgJtfkFFA" role="37wK5m">
                  <ref role="3cqZAo" node="7OgJtfkFFN" resolve="payload" />
                </node>
                <node concept="37vLTw" id="7OgJtfkFFB" role="37wK5m">
                  <ref role="3cqZAo" node="7OgJtfkFFP" resolve="receiver" />
                </node>
                <node concept="37vLTw" id="7OgJtfkFFC" role="37wK5m">
                  <ref role="3cqZAo" node="7OgJtfkFFR" resolve="otherReceivers" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="7OgJtfkFFD" role="3cqZAp">
          <node concept="2YIFZM" id="7OgJtfkFFE" role="3cqZAk">
            <ref role="1Pybhc" to="2is:~Unit" resolve="Unit" />
            <ref role="37wK5l" to="2is:~Unit.unit()" resolve="unit" />
          </node>
        </node>
      </node>
      <node concept="3uibUv" id="7OgJtfkFFF" role="3clF45">
        <ref role="3uigEE" to="2is:~Unit" resolve="Unit" />
      </node>
      <node concept="3Tm1VV" id="7OgJtfkFFG" role="1B3o_S" />
      <node concept="37vLTG" id="7OgJtfkFFH" role="3clF46">
        <property role="TrG5h" value="a" />
        <node concept="3uibUv" id="7OgJtfkFFI" role="1tU5fm">
          <ref role="3uigEE" to="93v9:~Agent" resolve="Agent" />
        </node>
      </node>
      <node concept="37vLTG" id="7OgJtfkFFJ" role="3clF46">
        <property role="TrG5h" value="ontology" />
        <node concept="3uibUv" id="7OgJtfkFFK" role="1tU5fm">
          <ref role="3uigEE" to="1et:~Ontology" resolve="Ontology" />
        </node>
      </node>
      <node concept="37vLTG" id="7OgJtfkFFL" role="3clF46">
        <property role="TrG5h" value="performative" />
        <node concept="3uibUv" id="Oow4MYQTc5" role="1tU5fm">
          <ref role="3uigEE" node="1MgVVihmH1q" resolve="Performative" />
        </node>
      </node>
      <node concept="37vLTG" id="7OgJtfkFFN" role="3clF46">
        <property role="TrG5h" value="payload" />
        <node concept="3uibUv" id="7OgJtfkFFO" role="1tU5fm">
          <ref role="3uigEE" to="kozn:~AgentAction" resolve="AgentAction" />
        </node>
      </node>
      <node concept="37vLTG" id="7OgJtfkFFP" role="3clF46">
        <property role="TrG5h" value="receiver" />
        <node concept="3uibUv" id="7OgJtfkFFQ" role="1tU5fm">
          <ref role="3uigEE" to="93v9:~AID" resolve="AID" />
        </node>
      </node>
      <node concept="37vLTG" id="7OgJtfkFFR" role="3clF46">
        <property role="TrG5h" value="otherReceivers" />
        <node concept="8X2XB" id="7OgJtfkFFS" role="1tU5fm">
          <node concept="3uibUv" id="7OgJtfkFFT" role="8Xvag">
            <ref role="3uigEE" to="93v9:~AID" resolve="AID" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="4iLN_rFUd_l" role="jymVt" />
    <node concept="2YIFZL" id="4hvwZTOPLF6" role="jymVt">
      <property role="TrG5h" value="sendEvaluationRequest" />
      <node concept="3clFbS" id="4hvwZTOPLF7" role="3clF47">
        <node concept="3cpWs8" id="4hvwZTOPLF8" role="3cqZAp">
          <node concept="3cpWsn" id="4hvwZTOPLF9" role="3cpWs9">
            <property role="TrG5h" value="codec" />
            <node concept="3uibUv" id="4hvwZTOPLFa" role="1tU5fm">
              <ref role="3uigEE" to="xunt:~Codec" resolve="Codec" />
            </node>
            <node concept="2ShNRf" id="4hvwZTOPLFb" role="33vP2m">
              <node concept="1pGfFk" id="4hvwZTOPLFc" role="2ShVmc">
                <ref role="37wK5l" to="xofi:~SLCodec.&lt;init&gt;()" resolve="SLCodec" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="4hvwZTOPLFd" role="3cqZAp">
          <node concept="2OqwBi" id="4hvwZTOPLFe" role="3clFbG">
            <node concept="37vLTw" id="4hvwZTOPLFf" role="2Oq$k0">
              <ref role="3cqZAo" node="4hvwZTOPLFv" resolve="a" />
            </node>
            <node concept="liA8E" id="4hvwZTOPLFg" role="2OqNvi">
              <ref role="37wK5l" to="93v9:~Agent.send(jade.lang.acl.ACLMessage)" resolve="send" />
              <node concept="2YIFZM" id="4hvwZTOPLFh" role="37wK5m">
                <ref role="1Pybhc" node="1MgVVihn4Br" resolve="Messages" />
                <ref role="37wK5l" node="4hvwZTOPNcV" resolve="message" />
                <node concept="37vLTw" id="4hvwZTOPLFi" role="37wK5m">
                  <ref role="3cqZAo" node="4hvwZTOPLFz" resolve="performative" />
                </node>
                <node concept="37vLTw" id="4hvwZTOPLFj" role="37wK5m">
                  <ref role="3cqZAo" node="4hvwZTOPLF9" resolve="codec" />
                </node>
                <node concept="37vLTw" id="4hvwZTOPLFk" role="37wK5m">
                  <ref role="3cqZAo" node="4hvwZTOPLFx" resolve="ontology" />
                </node>
                <node concept="2OqwBi" id="4hvwZTOPLFl" role="37wK5m">
                  <node concept="37vLTw" id="4hvwZTOPLFm" role="2Oq$k0">
                    <ref role="3cqZAo" node="4hvwZTOPLFv" resolve="a" />
                  </node>
                  <node concept="liA8E" id="4hvwZTOPLFn" role="2OqNvi">
                    <ref role="37wK5l" to="93v9:~Agent.getContentManager()" resolve="getContentManager" />
                  </node>
                </node>
                <node concept="37vLTw" id="4hvwZTOPLFo" role="37wK5m">
                  <ref role="3cqZAo" node="4hvwZTOPLF_" resolve="payload" />
                </node>
                <node concept="37vLTw" id="4hvwZTOPLFp" role="37wK5m">
                  <ref role="3cqZAo" node="4hvwZTOPLFB" resolve="receiver" />
                </node>
                <node concept="37vLTw" id="4hvwZTOPLFq" role="37wK5m">
                  <ref role="3cqZAo" node="4hvwZTOPLFD" resolve="otherReceivers" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="4hvwZTOPLFr" role="3cqZAp">
          <node concept="2YIFZM" id="4hvwZTOPLFs" role="3cqZAk">
            <ref role="37wK5l" to="2is:~Unit.unit()" resolve="unit" />
            <ref role="1Pybhc" to="2is:~Unit" resolve="Unit" />
          </node>
        </node>
      </node>
      <node concept="3uibUv" id="4hvwZTOPLFt" role="3clF45">
        <ref role="3uigEE" to="2is:~Unit" resolve="Unit" />
      </node>
      <node concept="3Tm1VV" id="4hvwZTOPLFu" role="1B3o_S" />
      <node concept="37vLTG" id="4hvwZTOPLFv" role="3clF46">
        <property role="TrG5h" value="a" />
        <node concept="3uibUv" id="4hvwZTOPLFw" role="1tU5fm">
          <ref role="3uigEE" to="93v9:~Agent" resolve="Agent" />
        </node>
      </node>
      <node concept="37vLTG" id="4hvwZTOPLFx" role="3clF46">
        <property role="TrG5h" value="ontology" />
        <node concept="3uibUv" id="4hvwZTOPLFy" role="1tU5fm">
          <ref role="3uigEE" to="1et:~Ontology" resolve="Ontology" />
        </node>
      </node>
      <node concept="37vLTG" id="4hvwZTOPLFz" role="3clF46">
        <property role="TrG5h" value="performative" />
        <node concept="3uibUv" id="Oow4MYQTpX" role="1tU5fm">
          <ref role="3uigEE" node="1MgVVihmH1q" resolve="Performative" />
        </node>
      </node>
      <node concept="37vLTG" id="4hvwZTOPLF_" role="3clF46">
        <property role="TrG5h" value="payload" />
        <node concept="3uibUv" id="4hvwZTOPLFA" role="1tU5fm">
          <ref role="3uigEE" to="kozn:~Predicate" resolve="Predicate" />
        </node>
      </node>
      <node concept="37vLTG" id="4hvwZTOPLFB" role="3clF46">
        <property role="TrG5h" value="receiver" />
        <node concept="3uibUv" id="4hvwZTOPLFC" role="1tU5fm">
          <ref role="3uigEE" to="93v9:~AID" resolve="AID" />
        </node>
      </node>
      <node concept="37vLTG" id="4hvwZTOPLFD" role="3clF46">
        <property role="TrG5h" value="otherReceivers" />
        <node concept="8X2XB" id="4hvwZTOPLFE" role="1tU5fm">
          <node concept="3uibUv" id="4hvwZTOPLFF" role="8Xvag">
            <ref role="3uigEE" to="93v9:~AID" resolve="AID" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="4TWmWFY8gyO" role="jymVt" />
    <node concept="2YIFZL" id="4iLN_rFUfop" role="jymVt">
      <property role="TrG5h" value="sendMessage" />
      <node concept="3clFbS" id="4iLN_rFUfos" role="3clF47">
        <node concept="3clFbF" id="4iLN_rFUg8K" role="3cqZAp">
          <node concept="2OqwBi" id="4iLN_rFUgm2" role="3clFbG">
            <node concept="37vLTw" id="4iLN_rFUg8J" role="2Oq$k0">
              <ref role="3cqZAo" node="4iLN_rFUfX3" resolve="a" />
            </node>
            <node concept="liA8E" id="4iLN_rFUgG9" role="2OqNvi">
              <ref role="37wK5l" to="93v9:~Agent.send(jade.lang.acl.ACLMessage)" resolve="send" />
              <node concept="37vLTw" id="4iLN_rFUgLc" role="37wK5m">
                <ref role="3cqZAo" node="4iLN_rFUfT4" resolve="msg" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="4iLN_rFUgQM" role="3cqZAp">
          <node concept="2YIFZM" id="4iLN_rFUgVO" role="3cqZAk">
            <ref role="37wK5l" to="2is:~Unit.unit()" resolve="unit" />
            <ref role="1Pybhc" to="2is:~Unit" resolve="Unit" />
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="4iLN_rFUf4T" role="1B3o_S" />
      <node concept="3uibUv" id="4iLN_rFUfo4" role="3clF45">
        <ref role="3uigEE" to="2is:~Unit" resolve="Unit" />
      </node>
      <node concept="37vLTG" id="4iLN_rFUfX3" role="3clF46">
        <property role="TrG5h" value="a" />
        <node concept="3uibUv" id="4iLN_rFUfXA" role="1tU5fm">
          <ref role="3uigEE" to="93v9:~Agent" resolve="Agent" />
        </node>
      </node>
      <node concept="37vLTG" id="4iLN_rFUfT4" role="3clF46">
        <property role="TrG5h" value="msg" />
        <node concept="3uibUv" id="4iLN_rFUfT3" role="1tU5fm">
          <ref role="3uigEE" to="adhc:~ACLMessage" resolve="ACLMessage" />
        </node>
      </node>
    </node>
    <node concept="3Tm1VV" id="1MgVVihlvkM" role="1B3o_S" />
  </node>
  <node concept="Qs71p" id="1MgVVihmH1q">
    <property role="TrG5h" value="Performative" />
    <node concept="312cEg" id="1MgVVihmH8o" role="jymVt">
      <property role="TrG5h" value="jadeValue" />
      <property role="3TUv4t" value="true" />
      <node concept="3Tm6S6" id="1MgVVihmH8p" role="1B3o_S" />
      <node concept="10Oyi0" id="1MgVVihmH8q" role="1tU5fm" />
    </node>
    <node concept="2tJIrI" id="1MgVVihmH8r" role="jymVt" />
    <node concept="3clFbW" id="1MgVVihmH8s" role="jymVt">
      <node concept="3cqZAl" id="1MgVVihmH8t" role="3clF45" />
      <node concept="3clFbS" id="1MgVVihmH8u" role="3clF47">
        <node concept="3clFbF" id="1MgVVihmH8v" role="3cqZAp">
          <node concept="37vLTI" id="1MgVVihmH8w" role="3clFbG">
            <node concept="37vLTw" id="1MgVVihmH8x" role="37vLTx">
              <ref role="3cqZAo" node="1MgVVihmH8_" resolve="jadeValue" />
            </node>
            <node concept="2OqwBi" id="1MgVVihmH8y" role="37vLTJ">
              <node concept="Xjq3P" id="1MgVVihmH8z" role="2Oq$k0" />
              <node concept="2OwXpG" id="1MgVVihmH8$" role="2OqNvi">
                <ref role="2Oxat5" node="1MgVVihmH8o" resolve="jadeValue" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="1MgVVihmH8_" role="3clF46">
        <property role="TrG5h" value="jadeValue" />
        <node concept="10Oyi0" id="1MgVVihmH8A" role="1tU5fm" />
      </node>
    </node>
    <node concept="2tJIrI" id="1MgVVihmH8H" role="jymVt" />
    <node concept="3clFb_" id="1MgVVihmH8I" role="jymVt">
      <property role="TrG5h" value="getJadeValue" />
      <node concept="3Tm1VV" id="1MgVVihmH8J" role="1B3o_S" />
      <node concept="3clFbS" id="1MgVVihmH8K" role="3clF47">
        <node concept="3cpWs6" id="1MgVVihmH8L" role="3cqZAp">
          <node concept="37vLTw" id="1MgVVihmH8M" role="3cqZAk">
            <ref role="3cqZAo" node="1MgVVihmH8o" resolve="jadeValue" />
          </node>
        </node>
      </node>
      <node concept="10Oyi0" id="1MgVVihmH8N" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="1MgVVihmH7A" role="jymVt" />
    <node concept="QsSxf" id="1MgVVihmH64" role="Qtgdg">
      <property role="TrG5h" value="ACCEPT_PROPOSAL" />
      <ref role="37wK5l" node="1MgVVihmH8s" resolve="Performative" />
      <node concept="10M0yZ" id="1MgVVihmHt4" role="37wK5m">
        <ref role="3cqZAo" to="adhc:~ACLMessage.ACCEPT_PROPOSAL" resolve="ACCEPT_PROPOSAL" />
        <ref role="1PxDUh" to="adhc:~ACLMessage" resolve="ACLMessage" />
      </node>
    </node>
    <node concept="QsSxf" id="1MgVVihmHu1" role="Qtgdg">
      <property role="TrG5h" value="AGREE" />
      <ref role="37wK5l" node="1MgVVihmH8s" resolve="Performative" />
      <node concept="10M0yZ" id="1MgVVihmHHN" role="37wK5m">
        <ref role="3cqZAo" to="adhc:~ACLMessage.AGREE" resolve="AGREE" />
        <ref role="1PxDUh" to="adhc:~ACLMessage" resolve="ACLMessage" />
      </node>
    </node>
    <node concept="QsSxf" id="1MgVVihmHJo" role="Qtgdg">
      <property role="TrG5h" value="CANCEL" />
      <ref role="37wK5l" node="1MgVVihmH8s" resolve="Performative" />
      <node concept="10M0yZ" id="1MgVVihmHXi" role="37wK5m">
        <ref role="3cqZAo" to="adhc:~ACLMessage.CANCEL" resolve="CANCEL" />
        <ref role="1PxDUh" to="adhc:~ACLMessage" resolve="ACLMessage" />
      </node>
    </node>
    <node concept="QsSxf" id="1MgVVihmHZv" role="Qtgdg">
      <property role="TrG5h" value="CFP" />
      <ref role="37wK5l" node="1MgVVihmH8s" resolve="Performative" />
      <node concept="10M0yZ" id="1MgVVihmIkk" role="37wK5m">
        <ref role="3cqZAo" to="adhc:~ACLMessage.CFP" resolve="CFP" />
        <ref role="1PxDUh" to="adhc:~ACLMessage" resolve="ACLMessage" />
      </node>
    </node>
    <node concept="QsSxf" id="1MgVVihmIn9" role="Qtgdg">
      <property role="TrG5h" value="CONFIRM" />
      <ref role="37wK5l" node="1MgVVihmH8s" resolve="Performative" />
      <node concept="10M0yZ" id="1MgVVihmIIL" role="37wK5m">
        <ref role="3cqZAo" to="adhc:~ACLMessage.CONFIRM" resolve="CONFIRM" />
        <ref role="1PxDUh" to="adhc:~ACLMessage" resolve="ACLMessage" />
      </node>
    </node>
    <node concept="QsSxf" id="1MgVVihmIMe" role="Qtgdg">
      <property role="TrG5h" value="DISCONFIRM" />
      <ref role="37wK5l" node="1MgVVihmH8s" resolve="Performative" />
      <node concept="10M0yZ" id="1MgVVihmJho" role="37wK5m">
        <ref role="3cqZAo" to="adhc:~ACLMessage.DISCONFIRM" resolve="DISCONFIRM" />
        <ref role="1PxDUh" to="adhc:~ACLMessage" resolve="ACLMessage" />
      </node>
    </node>
    <node concept="QsSxf" id="1MgVVihmJlt" role="Qtgdg">
      <property role="TrG5h" value="FAILURE" />
      <ref role="37wK5l" node="1MgVVihmH8s" resolve="Performative" />
      <node concept="10M0yZ" id="1MgVVihmJKn" role="37wK5m">
        <ref role="3cqZAo" to="adhc:~ACLMessage.FAILURE" resolve="FAILURE" />
        <ref role="1PxDUh" to="adhc:~ACLMessage" resolve="ACLMessage" />
      </node>
    </node>
    <node concept="QsSxf" id="1MgVVihmJP4" role="Qtgdg">
      <property role="TrG5h" value="INFORM" />
      <ref role="37wK5l" node="1MgVVihmH8s" resolve="Performative" />
      <node concept="10M0yZ" id="1MgVVihmKec" role="37wK5m">
        <ref role="3cqZAo" to="adhc:~ACLMessage.INFORM" resolve="INFORM" />
        <ref role="1PxDUh" to="adhc:~ACLMessage" resolve="ACLMessage" />
      </node>
    </node>
    <node concept="QsSxf" id="1MgVVihmKjx" role="Qtgdg">
      <property role="TrG5h" value="INFORM_IF" />
      <ref role="37wK5l" node="1MgVVihmH8s" resolve="Performative" />
      <node concept="10M0yZ" id="1MgVVihmL9t" role="37wK5m">
        <ref role="3cqZAo" to="adhc:~ACLMessage.INFORM_IF" resolve="INFORM_IF" />
        <ref role="1PxDUh" to="adhc:~ACLMessage" resolve="ACLMessage" />
      </node>
    </node>
    <node concept="QsSxf" id="1MgVVihmLfq" role="Qtgdg">
      <property role="TrG5h" value="INFORM_REF" />
      <ref role="37wK5l" node="1MgVVihmH8s" resolve="Performative" />
      <node concept="10M0yZ" id="1MgVVihmLUg" role="37wK5m">
        <ref role="3cqZAo" to="adhc:~ACLMessage.INFORM_REF" resolve="INFORM_REF" />
        <ref role="1PxDUh" to="adhc:~ACLMessage" resolve="ACLMessage" />
      </node>
    </node>
    <node concept="QsSxf" id="1MgVVihmM0P" role="Qtgdg">
      <property role="TrG5h" value="NOT_UNDERSTOOD" />
      <ref role="37wK5l" node="1MgVVihmH8s" resolve="Performative" />
      <node concept="10M0yZ" id="1MgVVihmMU3" role="37wK5m">
        <ref role="3cqZAo" to="adhc:~ACLMessage.NOT_UNDERSTOOD" resolve="NOT_UNDERSTOOD" />
        <ref role="1PxDUh" to="adhc:~ACLMessage" resolve="ACLMessage" />
      </node>
    </node>
    <node concept="QsSxf" id="1MgVVihmN1g" role="Qtgdg">
      <property role="TrG5h" value="PROPOSE" />
      <ref role="37wK5l" node="1MgVVihmH8s" resolve="Performative" />
      <node concept="10M0yZ" id="1MgVVihmNFS" role="37wK5m">
        <ref role="3cqZAo" to="adhc:~ACLMessage.PROPOSE" resolve="PROPOSE" />
        <ref role="1PxDUh" to="adhc:~ACLMessage" resolve="ACLMessage" />
      </node>
    </node>
    <node concept="QsSxf" id="1MgVVihmNNH" role="Qtgdg">
      <property role="TrG5h" value="QUERY_IF" />
      <ref role="37wK5l" node="1MgVVihmH8s" resolve="Performative" />
      <node concept="10M0yZ" id="1MgVVihmOHL" role="37wK5m">
        <ref role="3cqZAo" to="adhc:~ACLMessage.QUERY_IF" resolve="QUERY_IF" />
        <ref role="1PxDUh" to="adhc:~ACLMessage" resolve="ACLMessage" />
      </node>
    </node>
    <node concept="QsSxf" id="1MgVVihmOXZ" role="Qtgdg">
      <property role="TrG5h" value="QUERY_REF" />
      <ref role="37wK5l" node="1MgVVihmH8s" resolve="Performative" />
      <node concept="10M0yZ" id="1MgVVihmPUY" role="37wK5m">
        <ref role="3cqZAo" to="adhc:~ACLMessage.QUERY_REF" resolve="QUERY_REF" />
        <ref role="1PxDUh" to="adhc:~ACLMessage" resolve="ACLMessage" />
      </node>
    </node>
    <node concept="QsSxf" id="1MgVVihmQ43" role="Qtgdg">
      <property role="TrG5h" value="REFUSE" />
      <ref role="37wK5l" node="1MgVVihmH8s" resolve="Performative" />
      <node concept="10M0yZ" id="1MgVVihmQSP" role="37wK5m">
        <ref role="3cqZAo" to="adhc:~ACLMessage.REFUSE" resolve="REFUSE" />
        <ref role="1PxDUh" to="adhc:~ACLMessage" resolve="ACLMessage" />
      </node>
    </node>
    <node concept="QsSxf" id="1MgVVihmR2y" role="Qtgdg">
      <property role="TrG5h" value="REJECT_PROPOSAL" />
      <ref role="37wK5l" node="1MgVVihmH8s" resolve="Performative" />
      <node concept="10M0yZ" id="1MgVVihmSi1" role="37wK5m">
        <ref role="3cqZAo" to="adhc:~ACLMessage.REJECT_PROPOSAL" resolve="REJECT_PROPOSAL" />
        <ref role="1PxDUh" to="adhc:~ACLMessage" resolve="ACLMessage" />
      </node>
    </node>
    <node concept="QsSxf" id="1MgVVihmSsm" role="Qtgdg">
      <property role="TrG5h" value="REQUEST" />
      <ref role="37wK5l" node="1MgVVihmH8s" resolve="Performative" />
      <node concept="10M0yZ" id="1MgVVihmTyD" role="37wK5m">
        <ref role="3cqZAo" to="adhc:~ACLMessage.REQUEST" resolve="REQUEST" />
        <ref role="1PxDUh" to="adhc:~ACLMessage" resolve="ACLMessage" />
      </node>
    </node>
    <node concept="QsSxf" id="1MgVVihmTHA" role="Qtgdg">
      <property role="TrG5h" value="REQUEST_WHEN" />
      <ref role="37wK5l" node="1MgVVihmH8s" resolve="Performative" />
      <node concept="10M0yZ" id="1MgVVihmVMr" role="37wK5m">
        <ref role="3cqZAo" to="adhc:~ACLMessage.REQUEST_WHEN" resolve="REQUEST_WHEN" />
        <ref role="1PxDUh" to="adhc:~ACLMessage" resolve="ACLMessage" />
      </node>
    </node>
    <node concept="QsSxf" id="1MgVVihmVY0" role="Qtgdg">
      <property role="TrG5h" value="REQUEST_WHENEVER" />
      <ref role="37wK5l" node="1MgVVihmH8s" resolve="Performative" />
      <node concept="10M0yZ" id="1MgVVihmXe8" role="37wK5m">
        <ref role="3cqZAo" to="adhc:~ACLMessage.REQUEST_WHENEVER" resolve="REQUEST_WHENEVER" />
        <ref role="1PxDUh" to="adhc:~ACLMessage" resolve="ACLMessage" />
      </node>
    </node>
    <node concept="QsSxf" id="1MgVVihmXql" role="Qtgdg">
      <property role="TrG5h" value="SUBSCRIBE" />
      <ref role="37wK5l" node="1MgVVihmH8s" resolve="Performative" />
      <node concept="10M0yZ" id="1MgVVihmYuh" role="37wK5m">
        <ref role="3cqZAo" to="adhc:~ACLMessage.SUBSCRIBE" resolve="SUBSCRIBE" />
        <ref role="1PxDUh" to="adhc:~ACLMessage" resolve="ACLMessage" />
      </node>
    </node>
    <node concept="QsSxf" id="1MgVVihmYF6" role="Qtgdg">
      <property role="TrG5h" value="PROXY" />
      <ref role="37wK5l" node="1MgVVihmH8s" resolve="Performative" />
      <node concept="10M0yZ" id="1MgVVihmZMg" role="37wK5m">
        <ref role="3cqZAo" to="adhc:~ACLMessage.PROXY" resolve="PROXY" />
        <ref role="1PxDUh" to="adhc:~ACLMessage" resolve="ACLMessage" />
      </node>
    </node>
    <node concept="QsSxf" id="1MgVVihmZZH" role="Qtgdg">
      <property role="TrG5h" value="PROPAGATE" />
      <ref role="37wK5l" node="1MgVVihmH8s" resolve="Performative" />
      <node concept="10M0yZ" id="1MgVVihn10e" role="37wK5m">
        <ref role="3cqZAo" to="adhc:~ACLMessage.PROPAGATE" resolve="PROPAGATE" />
        <ref role="1PxDUh" to="adhc:~ACLMessage" resolve="ACLMessage" />
      </node>
    </node>
    <node concept="3Tm1VV" id="1MgVVihmH1r" role="1B3o_S" />
  </node>
  <node concept="312cEu" id="1MgVVihn4Br">
    <property role="TrG5h" value="Messages" />
    <node concept="3clFbW" id="1MgVVihn4Ct" role="jymVt">
      <node concept="3cqZAl" id="1MgVVihn4Cv" role="3clF45" />
      <node concept="3Tm6S6" id="1MgVVihn4CR" role="1B3o_S" />
      <node concept="3clFbS" id="1MgVVihn4Cx" role="3clF47">
        <node concept="YS8fn" id="1MgVVihn4Dq" role="3cqZAp">
          <node concept="2ShNRf" id="1MgVVihn4E5" role="YScLw">
            <node concept="1pGfFk" id="1MgVVihn4WZ" role="2ShVmc">
              <ref role="37wK5l" to="wyt6:~IllegalStateException.&lt;init&gt;(java.lang.String)" resolve="IllegalStateException" />
              <node concept="2YIFZM" id="1MgVVihn51R" role="37wK5m">
                <ref role="1Pybhc" to="wyt6:~String" resolve="String" />
                <ref role="37wK5l" to="wyt6:~String.format(java.lang.String,java.lang.Object...)" resolve="format" />
                <node concept="Xl_RD" id="1MgVVihn53J" role="37wK5m">
                  <property role="Xl_RC" value="Class %s cannot be instantiated: it can only contain static methods" />
                </node>
                <node concept="3VsKOn" id="1MgVVihn5jT" role="37wK5m">
                  <ref role="3VsUkX" node="1MgVVihn4Br" resolve="Messages" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="1MgVVihn5rX" role="jymVt" />
    <node concept="2YIFZL" id="4voUvBeevSu" role="jymVt">
      <property role="TrG5h" value="message" />
      <node concept="3clFbS" id="4voUvBeevSx" role="3clF47">
        <node concept="3cpWs8" id="4voUvBeexnX" role="3cqZAp">
          <node concept="3cpWsn" id="4voUvBeexnY" role="3cpWs9">
            <property role="TrG5h" value="message" />
            <node concept="3uibUv" id="4voUvBeexnZ" role="1tU5fm">
              <ref role="3uigEE" to="adhc:~ACLMessage" resolve="ACLMessage" />
            </node>
            <node concept="2ShNRf" id="4voUvBeexo0" role="33vP2m">
              <node concept="1pGfFk" id="4voUvBeexo1" role="2ShVmc">
                <ref role="37wK5l" to="adhc:~ACLMessage.&lt;init&gt;(int)" resolve="ACLMessage" />
                <node concept="2OqwBi" id="Oow4MYQVdY" role="37wK5m">
                  <node concept="37vLTw" id="4voUvBeMeIC" role="2Oq$k0">
                    <ref role="3cqZAo" node="4voUvBeeweC" resolve="performative" />
                  </node>
                  <node concept="liA8E" id="Oow4MYQVV8" role="2OqNvi">
                    <ref role="37wK5l" node="1MgVVihmH8I" resolve="getJadeValue" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="4voUvBeexHZ" role="3cqZAp">
          <node concept="2OqwBi" id="4voUvBeexZ_" role="3clFbG">
            <node concept="37vLTw" id="4voUvBeexHX" role="2Oq$k0">
              <ref role="3cqZAo" node="4voUvBeexnY" resolve="message" />
            </node>
            <node concept="liA8E" id="4voUvBeeyhD" role="2OqNvi">
              <ref role="37wK5l" to="adhc:~ACLMessage.setLanguage(java.lang.String)" resolve="setLanguage" />
              <node concept="2OqwBi" id="4voUvBeey_I" role="37wK5m">
                <node concept="37vLTw" id="4voUvBeeyme" role="2Oq$k0">
                  <ref role="3cqZAo" node="4voUvBeewjB" resolve="codec" />
                </node>
                <node concept="liA8E" id="4voUvBeeyQS" role="2OqNvi">
                  <ref role="37wK5l" to="xunt:~Codec.getName()" resolve="getName" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="4voUvBeez11" role="3cqZAp">
          <node concept="2OqwBi" id="4voUvBeezgU" role="3clFbG">
            <node concept="37vLTw" id="4voUvBeez0Z" role="2Oq$k0">
              <ref role="3cqZAo" node="4voUvBeexnY" resolve="message" />
            </node>
            <node concept="liA8E" id="4voUvBeez_U" role="2OqNvi">
              <ref role="37wK5l" to="adhc:~ACLMessage.setOntology(java.lang.String)" resolve="setOntology" />
              <node concept="2OqwBi" id="4voUvBeezSM" role="37wK5m">
                <node concept="37vLTw" id="4voUvBeezF9" role="2Oq$k0">
                  <ref role="3cqZAo" node="4voUvBeewsu" resolve="ontology" />
                </node>
                <node concept="liA8E" id="4voUvBee$aF" role="2OqNvi">
                  <ref role="37wK5l" to="1et:~Ontology.getName()" resolve="getName" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="4voUvBee$hd" role="3cqZAp" />
        <node concept="3J1_TO" id="4voUvBee$vC" role="3cqZAp">
          <node concept="3uVAMA" id="4voUvBee$_l" role="1zxBo5">
            <node concept="XOnhg" id="4voUvBee$_m" role="1zc67B">
              <property role="TrG5h" value="ontoEx" />
              <node concept="nSUau" id="4voUvBee$_n" role="1tU5fm">
                <node concept="3uibUv" id="4voUvBee$Es" role="nSUat">
                  <ref role="3uigEE" to="1et:~OntologyException" resolve="OntologyException" />
                </node>
              </node>
            </node>
            <node concept="3clFbS" id="4voUvBee$_o" role="1zc67A">
              <node concept="3clFbF" id="4voUvBee_kN" role="3cqZAp">
                <node concept="2OqwBi" id="4voUvBee_Ky" role="3clFbG">
                  <node concept="37vLTw" id="4voUvBee_kM" role="2Oq$k0">
                    <ref role="3cqZAo" node="4voUvBee$_m" resolve="ontoEx" />
                  </node>
                  <node concept="liA8E" id="4voUvBeeA8y" role="2OqNvi">
                    <ref role="37wK5l" to="wyt6:~Throwable.printStackTrace()" resolve="printStackTrace" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3uVAMA" id="4voUvBee_1y" role="1zxBo5">
            <node concept="XOnhg" id="4voUvBee_1z" role="1zc67B">
              <property role="TrG5h" value="codecEx" />
              <node concept="nSUau" id="4voUvBee_1$" role="1tU5fm">
                <node concept="3uibUv" id="4voUvBee_6T" role="nSUat">
                  <ref role="3uigEE" to="xunt:~Codec$CodecException" resolve="Codec.CodecException" />
                </node>
              </node>
            </node>
            <node concept="3clFbS" id="4voUvBee_1_" role="1zc67A">
              <node concept="3clFbF" id="4voUvBeeAeW" role="3cqZAp">
                <node concept="2OqwBi" id="4voUvBeeAGj" role="3clFbG">
                  <node concept="37vLTw" id="4voUvBeeAeV" role="2Oq$k0">
                    <ref role="3cqZAo" node="4voUvBee_1z" resolve="codecEx" />
                  </node>
                  <node concept="liA8E" id="4voUvBeeB76" role="2OqNvi">
                    <ref role="37wK5l" to="wyt6:~Throwable.printStackTrace()" resolve="printStackTrace" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbS" id="4voUvBee$vE" role="1zxBo7">
            <node concept="3clFbF" id="4voUvBeeC6l" role="3cqZAp">
              <node concept="2OqwBi" id="4voUvBeeCoo" role="3clFbG">
                <node concept="37vLTw" id="4voUvBeeC6j" role="2Oq$k0">
                  <ref role="3cqZAo" node="4voUvBeewKC" resolve="manager" />
                </node>
                <node concept="liA8E" id="4voUvBeeCEn" role="2OqNvi">
                  <ref role="37wK5l" to="kozn:~ContentManager.fillContent(jade.lang.acl.ACLMessage,jade.content.ContentElement)" resolve="fillContent" />
                  <node concept="37vLTw" id="4voUvBeeD6T" role="37wK5m">
                    <ref role="3cqZAo" node="4voUvBeexnY" resolve="message" />
                  </node>
                  <node concept="2ShNRf" id="7OgJtfkOH0" role="37wK5m">
                    <node concept="1pGfFk" id="7OgJtfkQ2P" role="2ShVmc">
                      <property role="373rjd" value="true" />
                      <ref role="37wK5l" to="hfdx:~Action.&lt;init&gt;(jade.core.AID,jade.content.Concept)" resolve="Action" />
                      <node concept="37vLTw" id="7OgJtfkWa9" role="37wK5m">
                        <ref role="3cqZAo" node="4voUvBeex7K" resolve="receiver" />
                      </node>
                      <node concept="37vLTw" id="7OgJtfkWpA" role="37wK5m">
                        <ref role="3cqZAo" node="4voUvBeewUx" resolve="content" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="4voUvBeexoc" role="3cqZAp">
              <node concept="2OqwBi" id="4voUvBeexod" role="3clFbG">
                <node concept="37vLTw" id="4voUvBeexoe" role="2Oq$k0">
                  <ref role="3cqZAo" node="4voUvBeexnY" resolve="message" />
                </node>
                <node concept="liA8E" id="4voUvBeexof" role="2OqNvi">
                  <ref role="37wK5l" to="adhc:~ACLMessage.addReceiver(jade.core.AID)" resolve="addReceiver" />
                  <node concept="37vLTw" id="4voUvBeexog" role="37wK5m">
                    <ref role="3cqZAo" node="4voUvBeex7K" resolve="receiver" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="1DcWWT" id="4voUvBeexoh" role="3cqZAp">
              <node concept="3clFbS" id="4voUvBeexoi" role="2LFqv$">
                <node concept="3clFbF" id="4voUvBeexoj" role="3cqZAp">
                  <node concept="2OqwBi" id="4voUvBeexok" role="3clFbG">
                    <node concept="37vLTw" id="4voUvBeexol" role="2Oq$k0">
                      <ref role="3cqZAo" node="4voUvBeexnY" resolve="message" />
                    </node>
                    <node concept="liA8E" id="4voUvBeexom" role="2OqNvi">
                      <ref role="37wK5l" to="adhc:~ACLMessage.addReceiver(jade.core.AID)" resolve="addReceiver" />
                      <node concept="37vLTw" id="4voUvBeexon" role="37wK5m">
                        <ref role="3cqZAo" node="4voUvBeexoo" resolve="r" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3cpWsn" id="4voUvBeexoo" role="1Duv9x">
                <property role="TrG5h" value="r" />
                <node concept="3uibUv" id="4voUvBeexop" role="1tU5fm">
                  <ref role="3uigEE" to="93v9:~AID" resolve="AID" />
                </node>
              </node>
              <node concept="37vLTw" id="4voUvBeexoq" role="1DdaDG">
                <ref role="3cqZAo" node="4voUvBeexcV" resolve="otherReceivers" />
              </node>
            </node>
            <node concept="3cpWs6" id="7OgJtfjGIf" role="3cqZAp">
              <node concept="37vLTw" id="7OgJtfjGSr" role="3cqZAk">
                <ref role="3cqZAo" node="4voUvBeexnY" resolve="message" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="4voUvBeexor" role="3cqZAp">
          <node concept="37vLTw" id="4voUvBeexos" role="3clFbG">
            <ref role="3cqZAo" node="4voUvBeexnY" resolve="message" />
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="4voUvBeevG_" role="1B3o_S" />
      <node concept="3uibUv" id="4voUvBeevQS" role="3clF45">
        <ref role="3uigEE" to="adhc:~ACLMessage" resolve="ACLMessage" />
      </node>
      <node concept="37vLTG" id="4voUvBeeweC" role="3clF46">
        <property role="TrG5h" value="performative" />
        <node concept="3uibUv" id="Oow4MYQUB7" role="1tU5fm">
          <ref role="3uigEE" node="1MgVVihmH1q" resolve="Performative" />
        </node>
      </node>
      <node concept="37vLTG" id="4voUvBeewjB" role="3clF46">
        <property role="TrG5h" value="codec" />
        <node concept="3uibUv" id="4voUvBeewmY" role="1tU5fm">
          <ref role="3uigEE" to="xunt:~Codec" resolve="Codec" />
        </node>
      </node>
      <node concept="37vLTG" id="4voUvBeewsu" role="3clF46">
        <property role="TrG5h" value="ontology" />
        <node concept="3uibUv" id="4voUvBeewui" role="1tU5fm">
          <ref role="3uigEE" to="1et:~Ontology" resolve="Ontology" />
        </node>
      </node>
      <node concept="37vLTG" id="4voUvBeewKC" role="3clF46">
        <property role="TrG5h" value="manager" />
        <node concept="3uibUv" id="4voUvBeewMA" role="1tU5fm">
          <ref role="3uigEE" to="kozn:~ContentManager" resolve="ContentManager" />
        </node>
      </node>
      <node concept="37vLTG" id="4voUvBeewUx" role="3clF46">
        <property role="TrG5h" value="content" />
        <node concept="3uibUv" id="4voUvBeO9PC" role="1tU5fm">
          <ref role="3uigEE" to="kozn:~AgentAction" resolve="AgentAction" />
        </node>
      </node>
      <node concept="37vLTG" id="4voUvBeex7K" role="3clF46">
        <property role="TrG5h" value="receiver" />
        <node concept="3uibUv" id="4voUvBeex9E" role="1tU5fm">
          <ref role="3uigEE" to="93v9:~AID" resolve="AID" />
        </node>
      </node>
      <node concept="37vLTG" id="4voUvBeexcV" role="3clF46">
        <property role="TrG5h" value="otherReceivers" />
        <node concept="8X2XB" id="4voUvBeexgC" role="1tU5fm">
          <node concept="3uibUv" id="4voUvBeexeT" role="8Xvag">
            <ref role="3uigEE" to="93v9:~AID" resolve="AID" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="7OgJtfkIrq" role="jymVt" />
    <node concept="2YIFZL" id="4hvwZTOPNcV" role="jymVt">
      <property role="TrG5h" value="message" />
      <node concept="3clFbS" id="4hvwZTOPNcW" role="3clF47">
        <node concept="3cpWs8" id="4hvwZTOPNcX" role="3cqZAp">
          <node concept="3cpWsn" id="4hvwZTOPNcY" role="3cpWs9">
            <property role="TrG5h" value="message" />
            <node concept="3uibUv" id="4hvwZTOPNcZ" role="1tU5fm">
              <ref role="3uigEE" to="adhc:~ACLMessage" resolve="ACLMessage" />
            </node>
            <node concept="2ShNRf" id="4hvwZTOPNd0" role="33vP2m">
              <node concept="1pGfFk" id="4hvwZTOPNd1" role="2ShVmc">
                <ref role="37wK5l" to="adhc:~ACLMessage.&lt;init&gt;(int)" resolve="ACLMessage" />
                <node concept="2OqwBi" id="Oow4MYQW8y" role="37wK5m">
                  <node concept="37vLTw" id="4hvwZTOPNd2" role="2Oq$k0">
                    <ref role="3cqZAo" node="4hvwZTOPNe4" resolve="performative" />
                  </node>
                  <node concept="liA8E" id="Oow4MYQWo2" role="2OqNvi">
                    <ref role="37wK5l" node="1MgVVihmH8I" resolve="getJadeValue" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="4hvwZTOPNd3" role="3cqZAp">
          <node concept="2OqwBi" id="4hvwZTOPNd4" role="3clFbG">
            <node concept="37vLTw" id="4hvwZTOPNd5" role="2Oq$k0">
              <ref role="3cqZAo" node="4hvwZTOPNcY" resolve="message" />
            </node>
            <node concept="liA8E" id="4hvwZTOPNd6" role="2OqNvi">
              <ref role="37wK5l" to="adhc:~ACLMessage.setLanguage(java.lang.String)" resolve="setLanguage" />
              <node concept="2OqwBi" id="4hvwZTOPNd7" role="37wK5m">
                <node concept="37vLTw" id="4hvwZTOPNd8" role="2Oq$k0">
                  <ref role="3cqZAo" node="4hvwZTOPNe6" resolve="codec" />
                </node>
                <node concept="liA8E" id="4hvwZTOPNd9" role="2OqNvi">
                  <ref role="37wK5l" to="xunt:~Codec.getName()" resolve="getName" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="4hvwZTOPNda" role="3cqZAp">
          <node concept="2OqwBi" id="4hvwZTOPNdb" role="3clFbG">
            <node concept="37vLTw" id="4hvwZTOPNdc" role="2Oq$k0">
              <ref role="3cqZAo" node="4hvwZTOPNcY" resolve="message" />
            </node>
            <node concept="liA8E" id="4hvwZTOPNdd" role="2OqNvi">
              <ref role="37wK5l" to="adhc:~ACLMessage.setOntology(java.lang.String)" resolve="setOntology" />
              <node concept="2OqwBi" id="4hvwZTOPNde" role="37wK5m">
                <node concept="37vLTw" id="4hvwZTOPNdf" role="2Oq$k0">
                  <ref role="3cqZAo" node="4hvwZTOPNe8" resolve="ontology" />
                </node>
                <node concept="liA8E" id="4hvwZTOPNdg" role="2OqNvi">
                  <ref role="37wK5l" to="1et:~Ontology.getName()" resolve="getName" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="4hvwZTOPNdh" role="3cqZAp" />
        <node concept="3J1_TO" id="4hvwZTOPNdi" role="3cqZAp">
          <node concept="3uVAMA" id="4hvwZTOPNdj" role="1zxBo5">
            <node concept="XOnhg" id="4hvwZTOPNdk" role="1zc67B">
              <property role="TrG5h" value="ontoEx" />
              <node concept="nSUau" id="4hvwZTOPNdl" role="1tU5fm">
                <node concept="3uibUv" id="4hvwZTOPNdm" role="nSUat">
                  <ref role="3uigEE" to="1et:~OntologyException" resolve="OntologyException" />
                </node>
              </node>
            </node>
            <node concept="3clFbS" id="4hvwZTOPNdn" role="1zc67A">
              <node concept="3clFbF" id="4hvwZTOPNdo" role="3cqZAp">
                <node concept="2OqwBi" id="4hvwZTOPNdp" role="3clFbG">
                  <node concept="37vLTw" id="4hvwZTOPNdq" role="2Oq$k0">
                    <ref role="3cqZAo" node="4hvwZTOPNdk" resolve="ontoEx" />
                  </node>
                  <node concept="liA8E" id="4hvwZTOPNdr" role="2OqNvi">
                    <ref role="37wK5l" to="wyt6:~Throwable.printStackTrace()" resolve="printStackTrace" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3uVAMA" id="4hvwZTOPNds" role="1zxBo5">
            <node concept="XOnhg" id="4hvwZTOPNdt" role="1zc67B">
              <property role="TrG5h" value="codecEx" />
              <node concept="nSUau" id="4hvwZTOPNdu" role="1tU5fm">
                <node concept="3uibUv" id="4hvwZTOPNdv" role="nSUat">
                  <ref role="3uigEE" to="xunt:~Codec$CodecException" resolve="Codec.CodecException" />
                </node>
              </node>
            </node>
            <node concept="3clFbS" id="4hvwZTOPNdw" role="1zc67A">
              <node concept="3clFbF" id="4hvwZTOPNdx" role="3cqZAp">
                <node concept="2OqwBi" id="4hvwZTOPNdy" role="3clFbG">
                  <node concept="37vLTw" id="4hvwZTOPNdz" role="2Oq$k0">
                    <ref role="3cqZAo" node="4hvwZTOPNdt" resolve="codecEx" />
                  </node>
                  <node concept="liA8E" id="4hvwZTOPNd$" role="2OqNvi">
                    <ref role="37wK5l" to="wyt6:~Throwable.printStackTrace()" resolve="printStackTrace" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbS" id="4hvwZTOPNd_" role="1zxBo7">
            <node concept="3clFbF" id="4hvwZTOPNdA" role="3cqZAp">
              <node concept="2OqwBi" id="4hvwZTOPNdB" role="3clFbG">
                <node concept="37vLTw" id="4hvwZTOPNdC" role="2Oq$k0">
                  <ref role="3cqZAo" node="4hvwZTOPNea" resolve="manager" />
                </node>
                <node concept="liA8E" id="4hvwZTOPNdD" role="2OqNvi">
                  <ref role="37wK5l" to="kozn:~ContentManager.fillContent(jade.lang.acl.ACLMessage,jade.content.ContentElement)" resolve="fillContent" />
                  <node concept="37vLTw" id="4hvwZTOPNdE" role="37wK5m">
                    <ref role="3cqZAo" node="4hvwZTOPNcY" resolve="message" />
                  </node>
                  <node concept="37vLTw" id="4hvwZTOPOAM" role="37wK5m">
                    <ref role="3cqZAo" node="4hvwZTOPNec" resolve="content" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="4hvwZTOPNdJ" role="3cqZAp">
              <node concept="2OqwBi" id="4hvwZTOPNdK" role="3clFbG">
                <node concept="37vLTw" id="4hvwZTOPNdL" role="2Oq$k0">
                  <ref role="3cqZAo" node="4hvwZTOPNcY" resolve="message" />
                </node>
                <node concept="liA8E" id="4hvwZTOPNdM" role="2OqNvi">
                  <ref role="37wK5l" to="adhc:~ACLMessage.addReceiver(jade.core.AID)" resolve="addReceiver" />
                  <node concept="37vLTw" id="4hvwZTOPNdN" role="37wK5m">
                    <ref role="3cqZAo" node="4hvwZTOPNee" resolve="receiver" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="1DcWWT" id="4hvwZTOPNdO" role="3cqZAp">
              <node concept="3clFbS" id="4hvwZTOPNdP" role="2LFqv$">
                <node concept="3clFbF" id="4hvwZTOPNdQ" role="3cqZAp">
                  <node concept="2OqwBi" id="4hvwZTOPNdR" role="3clFbG">
                    <node concept="37vLTw" id="4hvwZTOPNdS" role="2Oq$k0">
                      <ref role="3cqZAo" node="4hvwZTOPNcY" resolve="message" />
                    </node>
                    <node concept="liA8E" id="4hvwZTOPNdT" role="2OqNvi">
                      <ref role="37wK5l" to="adhc:~ACLMessage.addReceiver(jade.core.AID)" resolve="addReceiver" />
                      <node concept="37vLTw" id="4hvwZTOPNdU" role="37wK5m">
                        <ref role="3cqZAo" node="4hvwZTOPNdV" resolve="r" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3cpWsn" id="4hvwZTOPNdV" role="1Duv9x">
                <property role="TrG5h" value="r" />
                <node concept="3uibUv" id="4hvwZTOPNdW" role="1tU5fm">
                  <ref role="3uigEE" to="93v9:~AID" resolve="AID" />
                </node>
              </node>
              <node concept="37vLTw" id="4hvwZTOPNdX" role="1DdaDG">
                <ref role="3cqZAo" node="4hvwZTOPNeg" resolve="otherReceivers" />
              </node>
            </node>
            <node concept="3cpWs6" id="4hvwZTOPNdY" role="3cqZAp">
              <node concept="37vLTw" id="4hvwZTOPNdZ" role="3cqZAk">
                <ref role="3cqZAo" node="4hvwZTOPNcY" resolve="message" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="4hvwZTOPNe0" role="3cqZAp">
          <node concept="37vLTw" id="4hvwZTOPNe1" role="3clFbG">
            <ref role="3cqZAo" node="4hvwZTOPNcY" resolve="message" />
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="4hvwZTOPNe2" role="1B3o_S" />
      <node concept="3uibUv" id="4hvwZTOPNe3" role="3clF45">
        <ref role="3uigEE" to="adhc:~ACLMessage" resolve="ACLMessage" />
      </node>
      <node concept="37vLTG" id="4hvwZTOPNe4" role="3clF46">
        <property role="TrG5h" value="performative" />
        <node concept="3uibUv" id="Oow4MYQUNF" role="1tU5fm">
          <ref role="3uigEE" node="1MgVVihmH1q" resolve="Performative" />
        </node>
      </node>
      <node concept="37vLTG" id="4hvwZTOPNe6" role="3clF46">
        <property role="TrG5h" value="codec" />
        <node concept="3uibUv" id="4hvwZTOPNe7" role="1tU5fm">
          <ref role="3uigEE" to="xunt:~Codec" resolve="Codec" />
        </node>
      </node>
      <node concept="37vLTG" id="4hvwZTOPNe8" role="3clF46">
        <property role="TrG5h" value="ontology" />
        <node concept="3uibUv" id="4hvwZTOPNe9" role="1tU5fm">
          <ref role="3uigEE" to="1et:~Ontology" resolve="Ontology" />
        </node>
      </node>
      <node concept="37vLTG" id="4hvwZTOPNea" role="3clF46">
        <property role="TrG5h" value="manager" />
        <node concept="3uibUv" id="4hvwZTOPNeb" role="1tU5fm">
          <ref role="3uigEE" to="kozn:~ContentManager" resolve="ContentManager" />
        </node>
      </node>
      <node concept="37vLTG" id="4hvwZTOPNec" role="3clF46">
        <property role="TrG5h" value="content" />
        <node concept="3uibUv" id="4hvwZTOPNed" role="1tU5fm">
          <ref role="3uigEE" to="kozn:~Predicate" resolve="Predicate" />
        </node>
      </node>
      <node concept="37vLTG" id="4hvwZTOPNee" role="3clF46">
        <property role="TrG5h" value="receiver" />
        <node concept="3uibUv" id="4hvwZTOPNef" role="1tU5fm">
          <ref role="3uigEE" to="93v9:~AID" resolve="AID" />
        </node>
      </node>
      <node concept="37vLTG" id="4hvwZTOPNeg" role="3clF46">
        <property role="TrG5h" value="otherReceivers" />
        <node concept="8X2XB" id="4hvwZTOPNeh" role="1tU5fm">
          <node concept="3uibUv" id="4hvwZTOPNei" role="8Xvag">
            <ref role="3uigEE" to="93v9:~AID" resolve="AID" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="4hvwZTOPNa8" role="jymVt" />
    <node concept="2YIFZL" id="7OgJtfkHHd" role="jymVt">
      <property role="TrG5h" value="message" />
      <node concept="3clFbS" id="7OgJtfkHHe" role="3clF47">
        <node concept="3cpWs8" id="7OgJtflaDd" role="3cqZAp">
          <node concept="3cpWsn" id="7OgJtflaDe" role="3cpWs9">
            <property role="TrG5h" value="reply" />
            <node concept="3uibUv" id="7OgJtflaDf" role="1tU5fm">
              <ref role="3uigEE" to="adhc:~ACLMessage" resolve="ACLMessage" />
            </node>
            <node concept="2OqwBi" id="7OgJtflbnp" role="33vP2m">
              <node concept="37vLTw" id="7OgJtflb0v" role="2Oq$k0">
                <ref role="3cqZAo" node="7OgJtfkYpV" resolve="query" />
              </node>
              <node concept="liA8E" id="7OgJtflbZq" role="2OqNvi">
                <ref role="37wK5l" to="adhc:~ACLMessage.createReply()" resolve="createReply" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="7OgJtfkHHz" role="3cqZAp" />
        <node concept="3J1_TO" id="7OgJtfkHH$" role="3cqZAp">
          <node concept="3uVAMA" id="7OgJtfl4Ar" role="1zxBo5">
            <node concept="XOnhg" id="7OgJtfl4As" role="1zc67B">
              <property role="TrG5h" value="ungrEx" />
              <node concept="nSUau" id="7OgJtfl4At" role="1tU5fm">
                <node concept="3uibUv" id="7OgJtfl4Ge" role="nSUat">
                  <ref role="3uigEE" to="1et:~UngroundedException" resolve="UngroundedException" />
                </node>
              </node>
            </node>
            <node concept="3clFbS" id="7OgJtfl4Au" role="1zc67A">
              <node concept="3clFbF" id="7OgJtfl5aR" role="3cqZAp">
                <node concept="2OqwBi" id="7OgJtfl5xh" role="3clFbG">
                  <node concept="37vLTw" id="7OgJtfl5aQ" role="2Oq$k0">
                    <ref role="3cqZAo" node="7OgJtfl4As" resolve="ungrEx" />
                  </node>
                  <node concept="liA8E" id="7OgJtfl65C" role="2OqNvi">
                    <ref role="37wK5l" to="wyt6:~Throwable.printStackTrace()" resolve="printStackTrace" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3uVAMA" id="7OgJtfkHH_" role="1zxBo5">
            <node concept="XOnhg" id="7OgJtfkHHA" role="1zc67B">
              <property role="TrG5h" value="ontoEx" />
              <node concept="nSUau" id="7OgJtfkHHB" role="1tU5fm">
                <node concept="3uibUv" id="7OgJtfkHHC" role="nSUat">
                  <ref role="3uigEE" to="1et:~OntologyException" resolve="OntologyException" />
                </node>
              </node>
            </node>
            <node concept="3clFbS" id="7OgJtfkHHD" role="1zc67A">
              <node concept="3clFbF" id="7OgJtfkHHE" role="3cqZAp">
                <node concept="2OqwBi" id="7OgJtfkHHF" role="3clFbG">
                  <node concept="37vLTw" id="7OgJtfkHHG" role="2Oq$k0">
                    <ref role="3cqZAo" node="7OgJtfkHHA" resolve="ontoEx" />
                  </node>
                  <node concept="liA8E" id="7OgJtfkHHH" role="2OqNvi">
                    <ref role="37wK5l" to="wyt6:~Throwable.printStackTrace()" resolve="printStackTrace" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3uVAMA" id="7OgJtfkHHI" role="1zxBo5">
            <node concept="XOnhg" id="7OgJtfkHHJ" role="1zc67B">
              <property role="TrG5h" value="codecEx" />
              <node concept="nSUau" id="7OgJtfkHHK" role="1tU5fm">
                <node concept="3uibUv" id="7OgJtfkHHL" role="nSUat">
                  <ref role="3uigEE" to="xunt:~Codec$CodecException" resolve="Codec.CodecException" />
                </node>
              </node>
            </node>
            <node concept="3clFbS" id="7OgJtfkHHM" role="1zc67A">
              <node concept="3clFbF" id="7OgJtfkHHN" role="3cqZAp">
                <node concept="2OqwBi" id="7OgJtfkHHO" role="3clFbG">
                  <node concept="37vLTw" id="7OgJtfkHHP" role="2Oq$k0">
                    <ref role="3cqZAo" node="7OgJtfkHHJ" resolve="codecEx" />
                  </node>
                  <node concept="liA8E" id="7OgJtfkHHQ" role="2OqNvi">
                    <ref role="37wK5l" to="wyt6:~Throwable.printStackTrace()" resolve="printStackTrace" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbS" id="7OgJtfkHHR" role="1zxBo7">
            <node concept="3cpWs8" id="7OgJtfl2Va" role="3cqZAp">
              <node concept="3cpWsn" id="7OgJtfl2Vb" role="3cpWs9">
                <property role="TrG5h" value="content" />
                <node concept="3uibUv" id="7OgJtfl2Vc" role="1tU5fm">
                  <ref role="3uigEE" to="kozn:~ContentElement" resolve="ContentElement" />
                </node>
                <node concept="2OqwBi" id="7OgJtfl3sc" role="33vP2m">
                  <node concept="37vLTw" id="7OgJtfl3aV" role="2Oq$k0">
                    <ref role="3cqZAo" node="7OgJtfkHIp" resolve="manager" />
                  </node>
                  <node concept="liA8E" id="7OgJtfl3NV" role="2OqNvi">
                    <ref role="37wK5l" to="kozn:~ContentManager.extractContent(jade.lang.acl.ACLMessage)" resolve="extractContent" />
                    <node concept="37vLTw" id="7OgJtfl3WX" role="37wK5m">
                      <ref role="3cqZAo" node="7OgJtfkYpV" resolve="query" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="7OgJtfkZ84" role="3cqZAp">
              <node concept="2OqwBi" id="7OgJtfkZtr" role="3clFbG">
                <node concept="37vLTw" id="7OgJtfkZ82" role="2Oq$k0">
                  <ref role="3cqZAo" node="7OgJtflaDe" resolve="reply" />
                </node>
                <node concept="liA8E" id="7OgJtfkZZb" role="2OqNvi">
                  <ref role="37wK5l" to="adhc:~ACLMessage.setPerformative(int)" resolve="setPerformative" />
                  <node concept="2OqwBi" id="Oow4MYRcd7" role="37wK5m">
                    <node concept="37vLTw" id="7OgJtfl0Kg" role="2Oq$k0">
                      <ref role="3cqZAo" node="7OgJtfkHIj" resolve="performative" />
                    </node>
                    <node concept="liA8E" id="Oow4MYRcRt" role="2OqNvi">
                      <ref role="37wK5l" node="1MgVVihmH8I" resolve="getJadeValue" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="7OgJtfkHHS" role="3cqZAp">
              <node concept="2OqwBi" id="7OgJtfkHHT" role="3clFbG">
                <node concept="37vLTw" id="7OgJtfkHHU" role="2Oq$k0">
                  <ref role="3cqZAo" node="7OgJtfkHIp" resolve="manager" />
                </node>
                <node concept="liA8E" id="7OgJtfkHHV" role="2OqNvi">
                  <ref role="37wK5l" to="kozn:~ContentManager.fillContent(jade.lang.acl.ACLMessage,jade.content.ContentElement)" resolve="fillContent" />
                  <node concept="37vLTw" id="7OgJtfkHHW" role="37wK5m">
                    <ref role="3cqZAo" node="7OgJtflaDe" resolve="reply" />
                  </node>
                  <node concept="2ShNRf" id="7OgJtfkKHH" role="37wK5m">
                    <node concept="1pGfFk" id="7OgJtfkO1W" role="2ShVmc">
                      <property role="373rjd" value="true" />
                      <ref role="37wK5l" to="hfdx:~Result.&lt;init&gt;(jade.content.Concept,java.lang.Object)" resolve="Result" />
                      <node concept="1eOMI4" id="7OgJtfl8nJ" role="37wK5m">
                        <node concept="10QFUN" id="7OgJtfl8nG" role="1eOMHV">
                          <node concept="3uibUv" id="7OgJtfl8nL" role="10QFUM">
                            <ref role="3uigEE" to="kozn:~Concept" resolve="Concept" />
                          </node>
                          <node concept="37vLTw" id="7OgJtfl8W6" role="10QFUP">
                            <ref role="3cqZAo" node="7OgJtfl2Vb" resolve="content" />
                          </node>
                        </node>
                      </node>
                      <node concept="37vLTw" id="7OgJtfkWEc" role="37wK5m">
                        <ref role="3cqZAo" node="7OgJtfkHIr" resolve="obj" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3cpWs6" id="7OgJtfkHId" role="3cqZAp">
              <node concept="37vLTw" id="7OgJtfkHIe" role="3cqZAk">
                <ref role="3cqZAo" node="7OgJtflaDe" resolve="reply" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="7OgJtfkHIf" role="3cqZAp">
          <node concept="37vLTw" id="7OgJtfkHIg" role="3clFbG">
            <ref role="3cqZAo" node="7OgJtflaDe" resolve="reply" />
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="7OgJtfkHIh" role="1B3o_S" />
      <node concept="3uibUv" id="7OgJtfkHIi" role="3clF45">
        <ref role="3uigEE" to="adhc:~ACLMessage" resolve="ACLMessage" />
      </node>
      <node concept="37vLTG" id="7OgJtfkHIj" role="3clF46">
        <property role="TrG5h" value="performative" />
        <node concept="3uibUv" id="Oow4MYRbH4" role="1tU5fm">
          <ref role="3uigEE" node="1MgVVihmH1q" resolve="Performative" />
        </node>
      </node>
      <node concept="37vLTG" id="7OgJtfkHIp" role="3clF46">
        <property role="TrG5h" value="manager" />
        <node concept="3uibUv" id="7OgJtfkHIq" role="1tU5fm">
          <ref role="3uigEE" to="kozn:~ContentManager" resolve="ContentManager" />
        </node>
      </node>
      <node concept="37vLTG" id="7OgJtfkHIr" role="3clF46">
        <property role="TrG5h" value="obj" />
        <node concept="3uibUv" id="7OgJtfkHIs" role="1tU5fm">
          <ref role="3uigEE" to="wyt6:~Object" resolve="Object" />
        </node>
      </node>
      <node concept="37vLTG" id="7OgJtfkYpV" role="3clF46">
        <property role="TrG5h" value="query" />
        <node concept="3uibUv" id="7OgJtfkYA1" role="1tU5fm">
          <ref role="3uigEE" to="adhc:~ACLMessage" resolve="ACLMessage" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="7OgJtfkHFF" role="jymVt" />
    <node concept="2YIFZL" id="4qBaryPVfEB" role="jymVt">
      <property role="TrG5h" value="extractContent" />
      <node concept="3clFbS" id="4qBaryPVfEE" role="3clF47">
        <node concept="3cpWs8" id="4qBaryPrcop" role="3cqZAp">
          <node concept="3cpWsn" id="4qBaryPrcoq" role="3cpWs9">
            <property role="TrG5h" value="content" />
            <node concept="3uibUv" id="4qBaryPrcor" role="1tU5fm">
              <ref role="3uigEE" to="kozn:~ContentElement" resolve="ContentElement" />
            </node>
          </node>
        </node>
        <node concept="3J1_TO" id="4qBaryPrcos" role="3cqZAp">
          <node concept="3uVAMA" id="4qBaryPrcot" role="1zxBo5">
            <node concept="XOnhg" id="4qBaryPrcou" role="1zc67B">
              <property role="TrG5h" value="ungroundedEx" />
              <node concept="nSUau" id="4qBaryPrcov" role="1tU5fm">
                <node concept="3uibUv" id="4qBaryPrcow" role="nSUat">
                  <ref role="3uigEE" to="1et:~UngroundedException" resolve="UngroundedException" />
                </node>
              </node>
            </node>
            <node concept="3clFbS" id="4qBaryPrcox" role="1zc67A">
              <node concept="3clFbF" id="4qBaryPrcoy" role="3cqZAp">
                <node concept="2OqwBi" id="4qBaryPrcoz" role="3clFbG">
                  <node concept="37vLTw" id="4qBaryPrco$" role="2Oq$k0">
                    <ref role="3cqZAo" node="4qBaryPrcou" resolve="ungroundedEx" />
                  </node>
                  <node concept="liA8E" id="4qBaryPrco_" role="2OqNvi">
                    <ref role="37wK5l" to="wyt6:~Throwable.printStackTrace()" resolve="printStackTrace" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3uVAMA" id="4qBaryPrcoA" role="1zxBo5">
            <node concept="XOnhg" id="4qBaryPrcoB" role="1zc67B">
              <property role="TrG5h" value="ontoEx" />
              <node concept="nSUau" id="4qBaryPrcoC" role="1tU5fm">
                <node concept="3uibUv" id="4qBaryPrcoD" role="nSUat">
                  <ref role="3uigEE" to="1et:~OntologyException" resolve="OntologyException" />
                </node>
              </node>
            </node>
            <node concept="3clFbS" id="4qBaryPrcoE" role="1zc67A">
              <node concept="3clFbF" id="4qBaryPrcoF" role="3cqZAp">
                <node concept="2OqwBi" id="4qBaryPrcoG" role="3clFbG">
                  <node concept="37vLTw" id="4qBaryPrcoH" role="2Oq$k0">
                    <ref role="3cqZAo" node="4qBaryPrcoB" resolve="ontoEx" />
                  </node>
                  <node concept="liA8E" id="4qBaryPrcoI" role="2OqNvi">
                    <ref role="37wK5l" to="wyt6:~Throwable.printStackTrace()" resolve="printStackTrace" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbS" id="4qBaryPrcoJ" role="1zxBo7">
            <node concept="3clFbF" id="4qBaryPrcoK" role="3cqZAp">
              <node concept="37vLTI" id="4qBaryPrcoL" role="3clFbG">
                <node concept="37vLTw" id="4qBaryPrcoM" role="37vLTJ">
                  <ref role="3cqZAo" node="4qBaryPrcoq" resolve="content" />
                </node>
                <node concept="2OqwBi" id="4qBaryPrcoN" role="37vLTx">
                  <node concept="2OqwBi" id="4qBaryPrcoO" role="2Oq$k0">
                    <node concept="liA8E" id="4qBaryPrcoP" role="2OqNvi">
                      <ref role="37wK5l" to="93v9:~Agent.getContentManager()" resolve="getContentManager" />
                    </node>
                    <node concept="37vLTw" id="4qBaryPrcoQ" role="2Oq$k0">
                      <ref role="3cqZAo" node="4qBaryPVqQe" resolve="agent" />
                    </node>
                  </node>
                  <node concept="liA8E" id="4qBaryPrcp3" role="2OqNvi">
                    <ref role="37wK5l" to="kozn:~ContentManager.extractContent(jade.lang.acl.ACLMessage)" resolve="extractContent" />
                    <node concept="37vLTw" id="4qBaryPrcp4" role="37wK5m">
                      <ref role="3cqZAo" node="4qBaryPVmsS" resolve="msg" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="4qBaryPVrLo" role="3cqZAp">
              <node concept="2OqwBi" id="4qBaryPVrNU" role="3clFbG">
                <node concept="37vLTw" id="4qBaryPVrLm" role="2Oq$k0">
                  <ref role="3cqZAo" node="4qBaryPVrpJ" resolve="runnable" />
                </node>
                <node concept="1Bd96e" id="4qBaryPVs0M" role="2OqNvi">
                  <node concept="37vLTw" id="4qBaryPVseI" role="1BdPVh">
                    <ref role="3cqZAo" node="4qBaryPrcoq" resolve="content" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3uVAMA" id="4qBaryPrcqF" role="1zxBo5">
            <node concept="3clFbS" id="4qBaryPrcqG" role="1zc67A">
              <node concept="3clFbF" id="4qBaryPrcqH" role="3cqZAp">
                <node concept="2OqwBi" id="4qBaryPrcqI" role="3clFbG">
                  <node concept="37vLTw" id="4qBaryPrcqJ" role="2Oq$k0">
                    <ref role="3cqZAo" node="4qBaryPrcqL" resolve="codecEx" />
                  </node>
                  <node concept="liA8E" id="4qBaryPrcqK" role="2OqNvi">
                    <ref role="37wK5l" to="wyt6:~Throwable.printStackTrace()" resolve="printStackTrace" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="XOnhg" id="4qBaryPrcqL" role="1zc67B">
              <property role="TrG5h" value="codecEx" />
              <node concept="nSUau" id="4qBaryPrcqM" role="1tU5fm">
                <node concept="3uibUv" id="4qBaryPrcqN" role="nSUat">
                  <ref role="3uigEE" to="xunt:~Codec$CodecException" resolve="Codec.CodecException" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="4qBaryPrcqO" role="3cqZAp">
          <node concept="2YIFZM" id="4qBaryPVDyq" role="3cqZAk">
            <ref role="37wK5l" to="2is:~Unit.unit()" resolve="unit" />
            <ref role="1Pybhc" to="2is:~Unit" resolve="Unit" />
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="4qBaryPVf0y" role="1B3o_S" />
      <node concept="3uibUv" id="4qBaryPVqho" role="3clF45">
        <ref role="3uigEE" to="2is:~Unit" resolve="Unit" />
      </node>
      <node concept="37vLTG" id="4qBaryPVqQe" role="3clF46">
        <property role="TrG5h" value="agent" />
        <node concept="3uibUv" id="4qBaryPVqWu" role="1tU5fm">
          <ref role="3uigEE" to="93v9:~Agent" resolve="Agent" />
        </node>
      </node>
      <node concept="37vLTG" id="4qBaryPVmsS" role="3clF46">
        <property role="TrG5h" value="msg" />
        <node concept="3uibUv" id="4qBaryPVmsR" role="1tU5fm">
          <ref role="3uigEE" to="adhc:~ACLMessage" resolve="ACLMessage" />
        </node>
      </node>
      <node concept="37vLTG" id="4qBaryPVrpJ" role="3clF46">
        <property role="TrG5h" value="runnable" />
        <node concept="1ajhzC" id="4qBaryPVrAh" role="1tU5fm">
          <node concept="3uibUv" id="4qBaryPVrCS" role="1ajl9A">
            <ref role="3uigEE" to="2is:~Unit" resolve="Unit" />
          </node>
          <node concept="3uibUv" id="4qBaryPVs69" role="1ajw0F">
            <ref role="3uigEE" to="kozn:~ContentElement" resolve="ContentElement" />
          </node>
        </node>
      </node>
    </node>
    <node concept="3Tm1VV" id="1MgVVihn4Bs" role="1B3o_S" />
  </node>
  <node concept="312cEu" id="5n0A64gs2fQ">
    <property role="TrG5h" value="Services" />
    <node concept="3clFbW" id="5n0A64gs2gG" role="jymVt">
      <node concept="3cqZAl" id="5n0A64gs2gI" role="3clF45" />
      <node concept="3Tm6S6" id="5n0A64gs2h6" role="1B3o_S" />
      <node concept="3clFbS" id="5n0A64gs2gK" role="3clF47">
        <node concept="YS8fn" id="5n0A64gs2hD" role="3cqZAp">
          <node concept="2ShNRf" id="5n0A64gs2ik" role="YScLw">
            <node concept="1pGfFk" id="5n0A64gs2_m" role="2ShVmc">
              <ref role="37wK5l" to="wyt6:~IllegalStateException.&lt;init&gt;(java.lang.String)" resolve="IllegalStateException" />
              <node concept="2YIFZM" id="5n0A64gs2DV" role="37wK5m">
                <ref role="1Pybhc" to="wyt6:~String" resolve="String" />
                <ref role="37wK5l" to="wyt6:~String.format(java.lang.String,java.lang.Object...)" resolve="format" />
                <node concept="Xl_RD" id="5n0A64gs2FJ" role="37wK5m">
                  <property role="Xl_RC" value="Class %s cannot be instantiated: it can only contain static methods" />
                </node>
                <node concept="3VsKOn" id="5n0A64gs2WP" role="37wK5m">
                  <ref role="3VsUkX" node="5n0A64gs2fQ" resolve="Services" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="5n0A64gs34R" role="jymVt" />
    <node concept="2YIFZL" id="581O36Of3xj" role="jymVt">
      <property role="TrG5h" value="searchService" />
      <node concept="3clFbS" id="581O36Of3xk" role="3clF47">
        <node concept="3cpWs6" id="581O36Of3yk" role="3cqZAp">
          <node concept="1rXfSq" id="581O36OfjW3" role="3cqZAk">
            <ref role="37wK5l" node="581O36Ofgir" resolve="searchServiceHandler" />
            <node concept="37vLTw" id="581O36OfkeG" role="37wK5m">
              <ref role="3cqZAo" node="581O36Of3yp" resolve="agent" />
            </node>
            <node concept="37vLTw" id="581O36Ofkob" role="37wK5m">
              <ref role="3cqZAo" node="581O36Of3yr" resolve="serviceType" />
            </node>
            <node concept="1adDum" id="581O36Ofn4U" role="37wK5m">
              <property role="1adDun" value="-1L" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="581O36Of3yo" role="1B3o_S" />
      <node concept="37vLTG" id="581O36Of3yp" role="3clF46">
        <property role="TrG5h" value="agent" />
        <node concept="3uibUv" id="581O36Of3yq" role="1tU5fm">
          <ref role="3uigEE" to="93v9:~Agent" resolve="Agent" />
        </node>
      </node>
      <node concept="37vLTG" id="581O36Of3yr" role="3clF46">
        <property role="TrG5h" value="serviceType" />
        <node concept="17QB3L" id="581O36Of3ys" role="1tU5fm" />
      </node>
      <node concept="3uibUv" id="7vs$7fjKJ5$" role="3clF45">
        <ref role="3uigEE" to="93v9:~AID" resolve="AID" />
      </node>
    </node>
    <node concept="2tJIrI" id="581O36Of3tt" role="jymVt" />
    <node concept="2YIFZL" id="581O36Ofgir" role="jymVt">
      <property role="TrG5h" value="searchServiceHandler" />
      <node concept="3clFbS" id="581O36Ofgiu" role="3clF47">
        <node concept="3cpWs8" id="581O36OfhpZ" role="3cqZAp">
          <node concept="3cpWsn" id="581O36Ofhq0" role="3cpWs9">
            <property role="TrG5h" value="sd" />
            <node concept="3uibUv" id="581O36Ofhq1" role="1tU5fm">
              <ref role="3uigEE" to="9vs0:~ServiceDescription" resolve="ServiceDescription" />
            </node>
            <node concept="2ShNRf" id="581O36Ofhq2" role="33vP2m">
              <node concept="1pGfFk" id="581O36Ofhq3" role="2ShVmc">
                <ref role="37wK5l" to="9vs0:~ServiceDescription.&lt;init&gt;()" resolve="ServiceDescription" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="581O36Ofhq4" role="3cqZAp">
          <node concept="2OqwBi" id="581O36Ofhq5" role="3clFbG">
            <node concept="37vLTw" id="581O36Ofhq6" role="2Oq$k0">
              <ref role="3cqZAo" node="581O36Ofhq0" resolve="sd" />
            </node>
            <node concept="liA8E" id="581O36Ofhq7" role="2OqNvi">
              <ref role="37wK5l" to="9vs0:~ServiceDescription.setType(java.lang.String)" resolve="setType" />
              <node concept="37vLTw" id="581O36Ofhq8" role="37wK5m">
                <ref role="3cqZAo" node="581O36Ofh9z" resolve="sType" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="581O36Ofhq9" role="3cqZAp" />
        <node concept="3cpWs8" id="581O36Ofhqa" role="3cqZAp">
          <node concept="3cpWsn" id="581O36Ofhqb" role="3cpWs9">
            <property role="TrG5h" value="dfd" />
            <node concept="3uibUv" id="581O36Ofhqc" role="1tU5fm">
              <ref role="3uigEE" to="9vs0:~DFAgentDescription" resolve="DFAgentDescription" />
            </node>
            <node concept="2ShNRf" id="581O36Ofhqd" role="33vP2m">
              <node concept="1pGfFk" id="581O36Ofhqe" role="2ShVmc">
                <ref role="37wK5l" to="9vs0:~DFAgentDescription.&lt;init&gt;()" resolve="DFAgentDescription" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="581O36Ofhqf" role="3cqZAp">
          <node concept="2OqwBi" id="581O36Ofhqg" role="3clFbG">
            <node concept="37vLTw" id="581O36Ofhqh" role="2Oq$k0">
              <ref role="3cqZAo" node="581O36Ofhqb" resolve="dfd" />
            </node>
            <node concept="liA8E" id="581O36Ofhqi" role="2OqNvi">
              <ref role="37wK5l" to="9vs0:~DFAgentDescription.addServices(jade.domain.FIPAAgentManagement.ServiceDescription)" resolve="addServices" />
              <node concept="37vLTw" id="581O36Ofhqj" role="37wK5m">
                <ref role="3cqZAo" node="581O36Ofhq0" resolve="sd" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="581O36Ofhqk" role="3cqZAp" />
        <node concept="3cpWs8" id="581O36Ofhql" role="3cqZAp">
          <node concept="3cpWsn" id="581O36Ofhqm" role="3cpWs9">
            <property role="TrG5h" value="searchConstraints" />
            <node concept="3uibUv" id="581O36Ofhqn" role="1tU5fm">
              <ref role="3uigEE" to="9vs0:~SearchConstraints" resolve="SearchConstraints" />
            </node>
            <node concept="2ShNRf" id="581O36Ofhqo" role="33vP2m">
              <node concept="1pGfFk" id="581O36Ofhqp" role="2ShVmc">
                <ref role="37wK5l" to="9vs0:~SearchConstraints.&lt;init&gt;()" resolve="SearchConstraints" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="581O36Ofhqq" role="3cqZAp">
          <node concept="2OqwBi" id="581O36Ofhqr" role="3clFbG">
            <node concept="37vLTw" id="581O36Ofhqs" role="2Oq$k0">
              <ref role="3cqZAo" node="581O36Ofhqm" resolve="searchConstraints" />
            </node>
            <node concept="liA8E" id="581O36Ofhqt" role="2OqNvi">
              <ref role="37wK5l" to="9vs0:~SearchConstraints.setMaxResults(java.lang.Long)" resolve="setMaxResults" />
              <node concept="37vLTw" id="581O36OfkNF" role="37wK5m">
                <ref role="3cqZAo" node="581O36OfheE" resolve="maxResults" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="581O36Ofhqv" role="3cqZAp" />
        <node concept="3J1_TO" id="581O36Ofhqw" role="3cqZAp">
          <node concept="3uVAMA" id="581O36Ofhqx" role="1zxBo5">
            <node concept="XOnhg" id="581O36Ofhqy" role="1zc67B">
              <property role="TrG5h" value="fe" />
              <node concept="nSUau" id="581O36Ofhqz" role="1tU5fm">
                <node concept="3uibUv" id="581O36Ofhq$" role="nSUat">
                  <ref role="3uigEE" to="9enu:~FIPAException" resolve="FIPAException" />
                </node>
              </node>
            </node>
            <node concept="3clFbS" id="581O36Ofhq_" role="1zc67A">
              <node concept="3clFbF" id="581O36OfhqA" role="3cqZAp">
                <node concept="2OqwBi" id="581O36OfhqB" role="3clFbG">
                  <node concept="37vLTw" id="581O36OfhqC" role="2Oq$k0">
                    <ref role="3cqZAo" node="581O36Ofhqy" resolve="fe" />
                  </node>
                  <node concept="liA8E" id="581O36OfhqD" role="2OqNvi">
                    <ref role="37wK5l" to="wyt6:~Throwable.printStackTrace()" resolve="printStackTrace" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbS" id="581O36OfhqE" role="1zxBo7">
            <node concept="3cpWs8" id="7vs$7fjKO2U" role="3cqZAp">
              <node concept="3cpWsn" id="7vs$7fjKO30" role="3cpWs9">
                <property role="TrG5h" value="dfds" />
                <node concept="10Q1$e" id="7vs$7fjKO32" role="1tU5fm">
                  <node concept="3uibUv" id="7vs$7fjKO34" role="10Q1$1">
                    <ref role="3uigEE" to="9vs0:~DFAgentDescription" resolve="DFAgentDescription" />
                  </node>
                </node>
                <node concept="2YIFZM" id="581O36OfhqJ" role="33vP2m">
                  <ref role="1Pybhc" to="9enu:~DFService" resolve="DFService" />
                  <ref role="37wK5l" to="9enu:~DFService.search(jade.core.Agent,jade.domain.FIPAAgentManagement.DFAgentDescription,jade.domain.FIPAAgentManagement.SearchConstraints)" resolve="search" />
                  <node concept="37vLTw" id="581O36OfhqK" role="37wK5m">
                    <ref role="3cqZAo" node="581O36OfgRJ" resolve="agent" />
                  </node>
                  <node concept="37vLTw" id="581O36OfhqL" role="37wK5m">
                    <ref role="3cqZAo" node="581O36Ofhqb" resolve="dfd" />
                  </node>
                  <node concept="37vLTw" id="581O36OfhqM" role="37wK5m">
                    <ref role="3cqZAo" node="581O36Ofhqm" resolve="searchConstraints" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbJ" id="7vs$7fjKL$Q" role="3cqZAp">
              <node concept="3clFbS" id="7vs$7fjKL$S" role="3clFbx">
                <node concept="3cpWs6" id="7vs$7fjL4eI" role="3cqZAp">
                  <node concept="2OqwBi" id="7vs$7fjL1zC" role="3cqZAk">
                    <node concept="AH0OO" id="7vs$7fjKZRz" role="2Oq$k0">
                      <node concept="3cmrfG" id="7vs$7fjL0gM" role="AHEQo">
                        <property role="3cmrfH" value="0" />
                      </node>
                      <node concept="37vLTw" id="7vs$7fjKZz3" role="AHHXb">
                        <ref role="3cqZAo" node="7vs$7fjKO30" resolve="dfds" />
                      </node>
                    </node>
                    <node concept="liA8E" id="7vs$7fjL2JM" role="2OqNvi">
                      <ref role="37wK5l" to="9vs0:~DFAgentDescription.getName()" resolve="getName" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3eOSWO" id="7vs$7fjKZ5q" role="3clFbw">
                <node concept="2OqwBi" id="7vs$7fjKUuC" role="3uHU7B">
                  <node concept="37vLTw" id="7vs$7fjKPZy" role="2Oq$k0">
                    <ref role="3cqZAo" node="7vs$7fjKO30" resolve="dfds" />
                  </node>
                  <node concept="1Rwk04" id="7vs$7fjKV1d" role="2OqNvi" />
                </node>
                <node concept="3cmrfG" id="7vs$7fjKX3g" role="3uHU7w">
                  <property role="3cmrfH" value="0" />
                </node>
              </node>
              <node concept="9aQIb" id="7vs$7fjKR$k" role="9aQIa">
                <node concept="3clFbS" id="7vs$7fjKR$l" role="9aQI4">
                  <node concept="3cpWs6" id="7vs$7fjKS4p" role="3cqZAp">
                    <node concept="10Nm6u" id="7vs$7fjKSFu" role="3cqZAk" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="581O36OfhqY" role="3cqZAp">
          <node concept="10Nm6u" id="7vs$7fjKTFj" role="3cqZAk" />
        </node>
      </node>
      <node concept="3Tm6S6" id="581O36OffJ2" role="1B3o_S" />
      <node concept="37vLTG" id="581O36OfgRJ" role="3clF46">
        <property role="TrG5h" value="agent" />
        <node concept="3uibUv" id="581O36OfgRI" role="1tU5fm">
          <ref role="3uigEE" to="93v9:~Agent" resolve="Agent" />
        </node>
      </node>
      <node concept="37vLTG" id="581O36Ofh9z" role="3clF46">
        <property role="TrG5h" value="sType" />
        <node concept="17QB3L" id="581O36Ofhc1" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="581O36OfheE" role="3clF46">
        <property role="TrG5h" value="maxResults" />
        <node concept="3cpWsb" id="581O36OfkGW" role="1tU5fm" />
      </node>
      <node concept="3uibUv" id="7vs$7fjKK4o" role="3clF45">
        <ref role="3uigEE" to="93v9:~AID" resolve="AID" />
      </node>
    </node>
    <node concept="2tJIrI" id="581O36OffaJ" role="jymVt" />
    <node concept="2YIFZL" id="581O36Ogp9s" role="jymVt">
      <property role="TrG5h" value="registerService" />
      <node concept="3clFbS" id="581O36Ogp9v" role="3clF47">
        <node concept="3clFbF" id="581O36Ogt9d" role="3cqZAp">
          <node concept="1rXfSq" id="581O36Ogt9c" role="3clFbG">
            <ref role="37wK5l" node="5n0A64gsoaq" resolve="registerServiceHandler" />
            <node concept="37vLTw" id="581O36OgtmD" role="37wK5m">
              <ref role="3cqZAo" node="581O36Ogqpv" resolve="agent" />
            </node>
            <node concept="37vLTw" id="581O36OgtJ8" role="37wK5m">
              <ref role="3cqZAo" node="581O36OgqGZ" resolve="sName" />
            </node>
            <node concept="37vLTw" id="581O36Ogu5p" role="37wK5m">
              <ref role="3cqZAo" node="581O36OgqWx" resolve="sType" />
            </node>
            <node concept="2ShNRf" id="581O36Og$$i" role="37wK5m">
              <node concept="Tc6Ow" id="581O36Og$yr" role="2ShVmc">
                <node concept="17QB3L" id="581O36Og$ys" role="HW$YZ" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="3EtpyNPPjoY" role="3cqZAp">
          <node concept="2YIFZM" id="3EtpyNPPjRq" role="3cqZAk">
            <ref role="37wK5l" to="2is:~Unit.unit()" resolve="unit" />
            <ref role="1Pybhc" to="2is:~Unit" resolve="Unit" />
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="581O36OgoEs" role="1B3o_S" />
      <node concept="3uibUv" id="3EtpyNPPk20" role="3clF45">
        <ref role="3uigEE" to="2is:~Unit" resolve="Unit" />
      </node>
      <node concept="37vLTG" id="581O36Ogqpv" role="3clF46">
        <property role="TrG5h" value="agent" />
        <node concept="3uibUv" id="581O36Ogqpu" role="1tU5fm">
          <ref role="3uigEE" to="93v9:~Agent" resolve="Agent" />
        </node>
      </node>
      <node concept="37vLTG" id="581O36OgqGZ" role="3clF46">
        <property role="TrG5h" value="sName" />
        <node concept="17QB3L" id="581O36OgqM6" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="581O36OgqWx" role="3clF46">
        <property role="TrG5h" value="sType" />
        <node concept="17QB3L" id="581O36Ogr1U" role="1tU5fm" />
      </node>
    </node>
    <node concept="2tJIrI" id="581O36Ogq5i" role="jymVt" />
    <node concept="2YIFZL" id="581O36OgrJz" role="jymVt">
      <property role="TrG5h" value="registerService" />
      <node concept="3clFbS" id="581O36OgrJ$" role="3clF47">
        <node concept="3clFbF" id="581O36Og$Mm" role="3cqZAp">
          <node concept="1rXfSq" id="581O36Og$Ml" role="3clFbG">
            <ref role="37wK5l" node="5n0A64gsoaq" resolve="registerServiceHandler" />
            <node concept="37vLTw" id="581O36Og_0w" role="37wK5m">
              <ref role="3cqZAo" node="581O36OgrJB" resolve="agent" />
            </node>
            <node concept="37vLTw" id="581O36Og_q9" role="37wK5m">
              <ref role="3cqZAo" node="581O36OgrJD" resolve="sName" />
            </node>
            <node concept="37vLTw" id="581O36Og_L$" role="37wK5m">
              <ref role="3cqZAo" node="581O36OgrJF" resolve="sType" />
            </node>
            <node concept="37vLTw" id="581O36OgA1Y" role="37wK5m">
              <ref role="3cqZAo" node="581O36OgrJH" resolve="ontologies" />
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="3EtpyNPPhjw" role="3cqZAp">
          <node concept="2YIFZM" id="3EtpyNPPhW6" role="3cqZAk">
            <ref role="37wK5l" to="2is:~Unit.unit()" resolve="unit" />
            <ref role="1Pybhc" to="2is:~Unit" resolve="Unit" />
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="581O36OgrJ_" role="1B3o_S" />
      <node concept="3uibUv" id="3EtpyNPPgKH" role="3clF45">
        <ref role="3uigEE" to="2is:~Unit" resolve="Unit" />
      </node>
      <node concept="37vLTG" id="581O36OgrJB" role="3clF46">
        <property role="TrG5h" value="agent" />
        <node concept="3uibUv" id="581O36OgrJC" role="1tU5fm">
          <ref role="3uigEE" to="93v9:~Agent" resolve="Agent" />
        </node>
      </node>
      <node concept="37vLTG" id="581O36OgrJD" role="3clF46">
        <property role="TrG5h" value="sName" />
        <node concept="17QB3L" id="581O36OgrJE" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="581O36OgrJF" role="3clF46">
        <property role="TrG5h" value="sType" />
        <node concept="17QB3L" id="581O36OgrJG" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="581O36OgrJH" role="3clF46">
        <property role="TrG5h" value="ontologies" />
        <node concept="_YKpA" id="581O36OgrJI" role="1tU5fm">
          <node concept="17QB3L" id="581O36OgrJJ" role="_ZDj9" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="581O36Ogoib" role="jymVt" />
    <node concept="2YIFZL" id="5n0A64gsoaq" role="jymVt">
      <property role="TrG5h" value="registerServiceHandler" />
      <node concept="3clFbS" id="5n0A64gsoat" role="3clF47">
        <node concept="3cpWs8" id="5n0A64gsoOG" role="3cqZAp">
          <node concept="3cpWsn" id="5n0A64gsoOH" role="3cpWs9">
            <property role="TrG5h" value="sd" />
            <node concept="3uibUv" id="5n0A64gsoOI" role="1tU5fm">
              <ref role="3uigEE" to="9vs0:~ServiceDescription" resolve="ServiceDescription" />
            </node>
            <node concept="2ShNRf" id="5n0A64gsoRQ" role="33vP2m">
              <node concept="1pGfFk" id="5n0A64gspdv" role="2ShVmc">
                <ref role="37wK5l" to="9vs0:~ServiceDescription.&lt;init&gt;()" resolve="ServiceDescription" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5n0A64gspMq" role="3cqZAp">
          <node concept="2OqwBi" id="5n0A64gspZU" role="3clFbG">
            <node concept="37vLTw" id="5n0A64gspMo" role="2Oq$k0">
              <ref role="3cqZAo" node="5n0A64gsoOH" resolve="sd" />
            </node>
            <node concept="liA8E" id="5n0A64gsqcB" role="2OqNvi">
              <ref role="37wK5l" to="9vs0:~ServiceDescription.setName(java.lang.String)" resolve="setName" />
              <node concept="37vLTw" id="5n0A64gsqev" role="37wK5m">
                <ref role="3cqZAo" node="5n0A64gsoGj" resolve="sName" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5n0A64gspfW" role="3cqZAp">
          <node concept="2OqwBi" id="5n0A64gspt4" role="3clFbG">
            <node concept="37vLTw" id="5n0A64gspfU" role="2Oq$k0">
              <ref role="3cqZAo" node="5n0A64gsoOH" resolve="sd" />
            </node>
            <node concept="liA8E" id="5n0A64gspHu" role="2OqNvi">
              <ref role="37wK5l" to="9vs0:~ServiceDescription.setType(java.lang.String)" resolve="setType" />
              <node concept="37vLTw" id="5n0A64gspJz" role="37wK5m">
                <ref role="3cqZAo" node="5n0A64gsoJj" resolve="sType" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="581O36OgiI1" role="3cqZAp">
          <node concept="2OqwBi" id="581O36OgjdQ" role="3clFbG">
            <node concept="37vLTw" id="581O36OgiHZ" role="2Oq$k0">
              <ref role="3cqZAo" node="5n0A64gsoOH" resolve="sd" />
            </node>
            <node concept="liA8E" id="581O36OgjJc" role="2OqNvi">
              <ref role="37wK5l" to="9vs0:~ServiceDescription.addLanguages(java.lang.String)" resolve="addLanguages" />
              <node concept="10M0yZ" id="581O36OgkES" role="37wK5m">
                <ref role="3cqZAo" to="9enu:~FIPANames$ContentLanguage.FIPA_SL" resolve="FIPA_SL" />
                <ref role="1PxDUh" to="9enu:~FIPANames$ContentLanguage" resolve="FIPANames.ContentLanguage" />
              </node>
            </node>
          </node>
        </node>
        <node concept="2Gpval" id="581O36Ogf43" role="3cqZAp">
          <node concept="2GrKxI" id="581O36Ogf45" role="2Gsz3X">
            <property role="TrG5h" value="o" />
          </node>
          <node concept="37vLTw" id="581O36Ogg8G" role="2GsD0m">
            <ref role="3cqZAo" node="581O36Ogfjs" resolve="ontologies" />
          </node>
          <node concept="3clFbS" id="581O36Ogf49" role="2LFqv$">
            <node concept="3clFbF" id="581O36Of0l7" role="3cqZAp">
              <node concept="2OqwBi" id="581O36Of0Dp" role="3clFbG">
                <node concept="37vLTw" id="581O36Of0l5" role="2Oq$k0">
                  <ref role="3cqZAo" node="5n0A64gsoOH" resolve="sd" />
                </node>
                <node concept="liA8E" id="581O36Of17T" role="2OqNvi">
                  <ref role="37wK5l" to="9vs0:~ServiceDescription.addOntologies(java.lang.String)" resolve="addOntologies" />
                  <node concept="2GrUjf" id="581O36OggJo" role="37wK5m">
                    <ref role="2Gs0qQ" node="581O36Ogf45" resolve="o" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="581O36OgkQF" role="3cqZAp" />
        <node concept="3cpWs8" id="5n0A64gsque" role="3cqZAp">
          <node concept="3cpWsn" id="5n0A64gsquf" role="3cpWs9">
            <property role="TrG5h" value="dfd" />
            <node concept="3uibUv" id="5n0A64gsqug" role="1tU5fm">
              <ref role="3uigEE" to="9vs0:~DFAgentDescription" resolve="DFAgentDescription" />
            </node>
            <node concept="2ShNRf" id="5n0A64gsqB1" role="33vP2m">
              <node concept="1pGfFk" id="5n0A64gsrlh" role="2ShVmc">
                <ref role="37wK5l" to="9vs0:~DFAgentDescription.&lt;init&gt;()" resolve="DFAgentDescription" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5n0A64gsrps" role="3cqZAp">
          <node concept="2OqwBi" id="5n0A64gsrBp" role="3clFbG">
            <node concept="37vLTw" id="5n0A64gsrpq" role="2Oq$k0">
              <ref role="3cqZAo" node="5n0A64gsquf" resolve="dfd" />
            </node>
            <node concept="liA8E" id="5n0A64gsrTf" role="2OqNvi">
              <ref role="37wK5l" to="9vs0:~DFAgentDescription.setName(jade.core.AID)" resolve="setName" />
              <node concept="2OqwBi" id="5n0A64gss9L" role="37wK5m">
                <node concept="37vLTw" id="5n0A64gsrVz" role="2Oq$k0">
                  <ref role="3cqZAo" node="5n0A64gsoyS" resolve="a" />
                </node>
                <node concept="liA8E" id="5n0A64gsssD" role="2OqNvi">
                  <ref role="37wK5l" to="93v9:~Agent.getAID()" resolve="getAID" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5n0A64gssy4" role="3cqZAp">
          <node concept="2OqwBi" id="5n0A64gssLC" role="3clFbG">
            <node concept="37vLTw" id="5n0A64gssy2" role="2Oq$k0">
              <ref role="3cqZAo" node="5n0A64gsquf" resolve="dfd" />
            </node>
            <node concept="liA8E" id="5n0A64gssZq" role="2OqNvi">
              <ref role="37wK5l" to="9vs0:~DFAgentDescription.addServices(jade.domain.FIPAAgentManagement.ServiceDescription)" resolve="addServices" />
              <node concept="37vLTw" id="5n0A64gst2S" role="37wK5m">
                <ref role="3cqZAo" node="5n0A64gsoOH" resolve="sd" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5n0A64gst5w" role="3cqZAp" />
        <node concept="3J1_TO" id="5n0A64gst9o" role="3cqZAp">
          <node concept="3uVAMA" id="5n0A64gswRp" role="1zxBo5">
            <node concept="XOnhg" id="5n0A64gswRq" role="1zc67B">
              <property role="TrG5h" value="ex" />
              <node concept="nSUau" id="5n0A64gswRr" role="1tU5fm">
                <node concept="3uibUv" id="5n0A64gswW3" role="nSUat">
                  <ref role="3uigEE" to="wyt6:~Exception" resolve="Exception" />
                </node>
              </node>
            </node>
            <node concept="3clFbS" id="5n0A64gswRs" role="1zc67A">
              <node concept="3clFbF" id="5n0A64gsxfp" role="3cqZAp">
                <node concept="2OqwBi" id="5n0A64gsxwR" role="3clFbG">
                  <node concept="37vLTw" id="5n0A64gsxfo" role="2Oq$k0">
                    <ref role="3cqZAo" node="5n0A64gswRq" resolve="ex" />
                  </node>
                  <node concept="liA8E" id="5n0A64gsxLX" role="2OqNvi">
                    <ref role="37wK5l" to="wyt6:~Throwable.printStackTrace()" resolve="printStackTrace" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbS" id="5n0A64gst9q" role="1zxBo7">
            <node concept="3cpWs8" id="5n0A64gstoD" role="3cqZAp">
              <node concept="3cpWsn" id="5n0A64gstoE" role="3cpWs9">
                <property role="TrG5h" value="dfds" />
                <node concept="10Q1$e" id="5n0A64gstoF" role="1tU5fm">
                  <node concept="3uibUv" id="5n0A64gstoG" role="10Q1$1">
                    <ref role="3uigEE" to="9vs0:~DFAgentDescription" resolve="DFAgentDescription" />
                  </node>
                </node>
                <node concept="2YIFZM" id="5n0A64gstF$" role="33vP2m">
                  <ref role="1Pybhc" to="9enu:~DFService" resolve="DFService" />
                  <ref role="37wK5l" to="9enu:~DFService.search(jade.core.Agent,jade.domain.FIPAAgentManagement.DFAgentDescription)" resolve="search" />
                  <node concept="37vLTw" id="5n0A64gstS3" role="37wK5m">
                    <ref role="3cqZAo" node="5n0A64gsoyS" resolve="a" />
                  </node>
                  <node concept="37vLTw" id="5n0A64gsu7X" role="37wK5m">
                    <ref role="3cqZAo" node="5n0A64gsquf" resolve="dfd" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbJ" id="5n0A64gsueT" role="3cqZAp">
              <node concept="3clFbS" id="5n0A64gsueV" role="3clFbx">
                <node concept="3clFbF" id="5n0A64gsv$r" role="3cqZAp">
                  <node concept="2YIFZM" id="5n0A64gsvGS" role="3clFbG">
                    <ref role="1Pybhc" to="9enu:~DFService" resolve="DFService" />
                    <ref role="37wK5l" to="9enu:~DFService.deregister(jade.core.Agent,jade.domain.FIPAAgentManagement.DFAgentDescription)" resolve="deregister" />
                    <node concept="37vLTw" id="5n0A64gsvLk" role="37wK5m">
                      <ref role="3cqZAo" node="5n0A64gsoyS" resolve="a" />
                    </node>
                    <node concept="37vLTw" id="5n0A64gsvZQ" role="37wK5m">
                      <ref role="3cqZAo" node="5n0A64gsquf" resolve="dfd" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3eOSWO" id="5n0A64gsvlR" role="3clFbw">
                <node concept="3cmrfG" id="5n0A64gsvqg" role="3uHU7w">
                  <property role="3cmrfH" value="0" />
                </node>
                <node concept="2OqwBi" id="5n0A64gsuqp" role="3uHU7B">
                  <node concept="37vLTw" id="5n0A64gsuiM" role="2Oq$k0">
                    <ref role="3cqZAo" node="5n0A64gstoE" resolve="dfds" />
                  </node>
                  <node concept="1Rwk04" id="5n0A64gsuyT" role="2OqNvi" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="5n0A64gswek" role="3cqZAp">
              <node concept="2YIFZM" id="5n0A64gswu1" role="3clFbG">
                <ref role="1Pybhc" to="9enu:~DFService" resolve="DFService" />
                <ref role="37wK5l" to="9enu:~DFService.register(jade.core.Agent,jade.domain.FIPAAgentManagement.DFAgentDescription)" resolve="register" />
                <node concept="37vLTw" id="5n0A64gswz4" role="37wK5m">
                  <ref role="3cqZAo" node="5n0A64gsoyS" resolve="a" />
                </node>
                <node concept="37vLTw" id="5n0A64gswMT" role="37wK5m">
                  <ref role="3cqZAo" node="5n0A64gsquf" resolve="dfd" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3cqZAl" id="5n0A64gsox3" role="3clF45" />
      <node concept="3Tm6S6" id="581O36OglFb" role="1B3o_S" />
      <node concept="37vLTG" id="5n0A64gsoyS" role="3clF46">
        <property role="TrG5h" value="a" />
        <node concept="3uibUv" id="5n0A64gsoyR" role="1tU5fm">
          <ref role="3uigEE" to="93v9:~Agent" resolve="Agent" />
        </node>
      </node>
      <node concept="37vLTG" id="5n0A64gsoGj" role="3clF46">
        <property role="TrG5h" value="sName" />
        <node concept="17QB3L" id="5n0A64gsoGU" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="5n0A64gsoJj" role="3clF46">
        <property role="TrG5h" value="sType" />
        <node concept="17QB3L" id="5n0A64gsoJZ" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="581O36Ogfjs" role="3clF46">
        <property role="TrG5h" value="ontologies" />
        <node concept="_YKpA" id="581O36OgfqI" role="1tU5fm">
          <node concept="17QB3L" id="581O36Ogfwp" role="_ZDj9" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="5n0A64gs$m5" role="jymVt" />
    <node concept="2YIFZL" id="5n0A64gsyC6" role="jymVt">
      <property role="TrG5h" value="deregisterService" />
      <node concept="3clFbS" id="5n0A64gsyCa" role="3clF47">
        <node concept="3cpWs8" id="5n0A64gszcA" role="3cqZAp">
          <node concept="3cpWsn" id="5n0A64gszcB" role="3cpWs9">
            <property role="TrG5h" value="sd" />
            <node concept="3uibUv" id="5n0A64gszcC" role="1tU5fm">
              <ref role="3uigEE" to="9vs0:~ServiceDescription" resolve="ServiceDescription" />
            </node>
            <node concept="2ShNRf" id="5n0A64gszcD" role="33vP2m">
              <node concept="1pGfFk" id="5n0A64gszcE" role="2ShVmc">
                <ref role="37wK5l" to="9vs0:~ServiceDescription.&lt;init&gt;()" resolve="ServiceDescription" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5n0A64gszcF" role="3cqZAp">
          <node concept="2OqwBi" id="5n0A64gszcG" role="3clFbG">
            <node concept="37vLTw" id="5n0A64gszcH" role="2Oq$k0">
              <ref role="3cqZAo" node="5n0A64gszcB" resolve="sd" />
            </node>
            <node concept="liA8E" id="5n0A64gszcI" role="2OqNvi">
              <ref role="37wK5l" to="9vs0:~ServiceDescription.setType(java.lang.String)" resolve="setType" />
              <node concept="37vLTw" id="5n0A64gszcJ" role="37wK5m">
                <ref role="3cqZAo" node="5n0A64gsz91" resolve="serviceType" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5n0A64gszcK" role="3cqZAp">
          <node concept="2OqwBi" id="5n0A64gszcL" role="3clFbG">
            <node concept="37vLTw" id="5n0A64gszcM" role="2Oq$k0">
              <ref role="3cqZAo" node="5n0A64gszcB" resolve="sd" />
            </node>
            <node concept="liA8E" id="5n0A64gszcN" role="2OqNvi">
              <ref role="37wK5l" to="9vs0:~ServiceDescription.setName(java.lang.String)" resolve="setName" />
              <node concept="37vLTw" id="5n0A64gszcO" role="37wK5m">
                <ref role="3cqZAo" node="5n0A64gsz2R" resolve="serviceName" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5n0A64gszcP" role="3cqZAp" />
        <node concept="3cpWs8" id="5n0A64gszcQ" role="3cqZAp">
          <node concept="3cpWsn" id="5n0A64gszcR" role="3cpWs9">
            <property role="TrG5h" value="dfd" />
            <node concept="3uibUv" id="5n0A64gszcS" role="1tU5fm">
              <ref role="3uigEE" to="9vs0:~DFAgentDescription" resolve="DFAgentDescription" />
            </node>
            <node concept="2ShNRf" id="5n0A64gszcT" role="33vP2m">
              <node concept="1pGfFk" id="5n0A64gszcU" role="2ShVmc">
                <ref role="37wK5l" to="9vs0:~DFAgentDescription.&lt;init&gt;()" resolve="DFAgentDescription" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5n0A64gszcV" role="3cqZAp">
          <node concept="2OqwBi" id="5n0A64gszcW" role="3clFbG">
            <node concept="37vLTw" id="5n0A64gszcX" role="2Oq$k0">
              <ref role="3cqZAo" node="5n0A64gszcR" resolve="dfd" />
            </node>
            <node concept="liA8E" id="5n0A64gszcY" role="2OqNvi">
              <ref role="37wK5l" to="9vs0:~DFAgentDescription.setName(jade.core.AID)" resolve="setName" />
              <node concept="2OqwBi" id="5n0A64gszcZ" role="37wK5m">
                <node concept="37vLTw" id="5n0A64gszd0" role="2Oq$k0">
                  <ref role="3cqZAo" node="5n0A64gsyRZ" resolve="agent" />
                </node>
                <node concept="liA8E" id="5n0A64gszd1" role="2OqNvi">
                  <ref role="37wK5l" to="93v9:~Agent.getAID()" resolve="getAID" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5n0A64gszd2" role="3cqZAp">
          <node concept="2OqwBi" id="5n0A64gszd3" role="3clFbG">
            <node concept="37vLTw" id="5n0A64gszd4" role="2Oq$k0">
              <ref role="3cqZAo" node="5n0A64gszcR" resolve="dfd" />
            </node>
            <node concept="liA8E" id="5n0A64gszd5" role="2OqNvi">
              <ref role="37wK5l" to="9vs0:~DFAgentDescription.addServices(jade.domain.FIPAAgentManagement.ServiceDescription)" resolve="addServices" />
              <node concept="37vLTw" id="5n0A64gszd6" role="37wK5m">
                <ref role="3cqZAo" node="5n0A64gszcB" resolve="sd" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5n0A64gszd7" role="3cqZAp" />
        <node concept="3J1_TO" id="5n0A64gszd8" role="3cqZAp">
          <node concept="3uVAMA" id="5n0A64gszd9" role="1zxBo5">
            <node concept="XOnhg" id="5n0A64gszda" role="1zc67B">
              <property role="TrG5h" value="ex" />
              <node concept="nSUau" id="5n0A64gszdb" role="1tU5fm">
                <node concept="3uibUv" id="5n0A64gszdc" role="nSUat">
                  <ref role="3uigEE" to="wyt6:~Exception" resolve="Exception" />
                </node>
              </node>
            </node>
            <node concept="3clFbS" id="5n0A64gszdd" role="1zc67A">
              <node concept="3clFbF" id="5n0A64gszde" role="3cqZAp">
                <node concept="2OqwBi" id="5n0A64gszdf" role="3clFbG">
                  <node concept="37vLTw" id="5n0A64gszdg" role="2Oq$k0">
                    <ref role="3cqZAo" node="5n0A64gszda" resolve="ex" />
                  </node>
                  <node concept="liA8E" id="5n0A64gszdh" role="2OqNvi">
                    <ref role="37wK5l" to="wyt6:~Throwable.printStackTrace()" resolve="printStackTrace" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbS" id="5n0A64gszdi" role="1zxBo7">
            <node concept="3cpWs8" id="5n0A64gszdj" role="3cqZAp">
              <node concept="3cpWsn" id="5n0A64gszdk" role="3cpWs9">
                <property role="TrG5h" value="dfds" />
                <node concept="10Q1$e" id="5n0A64gszdl" role="1tU5fm">
                  <node concept="3uibUv" id="5n0A64gszdm" role="10Q1$1">
                    <ref role="3uigEE" to="9vs0:~DFAgentDescription" resolve="DFAgentDescription" />
                  </node>
                </node>
                <node concept="2YIFZM" id="5n0A64gszdn" role="33vP2m">
                  <ref role="37wK5l" to="9enu:~DFService.search(jade.core.Agent,jade.domain.FIPAAgentManagement.DFAgentDescription)" resolve="search" />
                  <ref role="1Pybhc" to="9enu:~DFService" resolve="DFService" />
                  <node concept="37vLTw" id="5n0A64gszdo" role="37wK5m">
                    <ref role="3cqZAo" node="5n0A64gsyRZ" resolve="agent" />
                  </node>
                  <node concept="37vLTw" id="5n0A64gszdp" role="37wK5m">
                    <ref role="3cqZAo" node="5n0A64gszcR" resolve="dfd" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbJ" id="5n0A64gszdq" role="3cqZAp">
              <node concept="3clFbS" id="5n0A64gszdr" role="3clFbx">
                <node concept="3clFbF" id="5n0A64gszds" role="3cqZAp">
                  <node concept="2YIFZM" id="5n0A64gszdt" role="3clFbG">
                    <ref role="37wK5l" to="9enu:~DFService.deregister(jade.core.Agent,jade.domain.FIPAAgentManagement.DFAgentDescription)" resolve="deregister" />
                    <ref role="1Pybhc" to="9enu:~DFService" resolve="DFService" />
                    <node concept="37vLTw" id="5n0A64gszdu" role="37wK5m">
                      <ref role="3cqZAo" node="5n0A64gsyRZ" resolve="agent" />
                    </node>
                    <node concept="37vLTw" id="5n0A64gszdv" role="37wK5m">
                      <ref role="3cqZAo" node="5n0A64gszcR" resolve="dfd" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3eOSWO" id="5n0A64gszdw" role="3clFbw">
                <node concept="3cmrfG" id="5n0A64gszdx" role="3uHU7w">
                  <property role="3cmrfH" value="0" />
                </node>
                <node concept="2OqwBi" id="5n0A64gszdy" role="3uHU7B">
                  <node concept="37vLTw" id="5n0A64gszdz" role="2Oq$k0">
                    <ref role="3cqZAo" node="5n0A64gszdk" resolve="dfds" />
                  </node>
                  <node concept="1Rwk04" id="5n0A64gszd$" role="2OqNvi" />
                </node>
              </node>
            </node>
          </node>
          <node concept="1wplmZ" id="3EtpyNPPk$3" role="1zxBo6">
            <node concept="3clFbS" id="3EtpyNPPk$4" role="1wplMD">
              <node concept="3cpWs6" id="3EtpyNPPl58" role="3cqZAp">
                <node concept="2YIFZM" id="3EtpyNPPlMg" role="3cqZAk">
                  <ref role="37wK5l" to="2is:~Unit.unit()" resolve="unit" />
                  <ref role="1Pybhc" to="2is:~Unit" resolve="Unit" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3uibUv" id="3EtpyNPPkd7" role="3clF45">
        <ref role="3uigEE" to="2is:~Unit" resolve="Unit" />
      </node>
      <node concept="3Tm1VV" id="5n0A64gsyC9" role="1B3o_S" />
      <node concept="37vLTG" id="5n0A64gsyRZ" role="3clF46">
        <property role="TrG5h" value="agent" />
        <node concept="3uibUv" id="5n0A64gsyRY" role="1tU5fm">
          <ref role="3uigEE" to="93v9:~Agent" resolve="Agent" />
        </node>
      </node>
      <node concept="37vLTG" id="5n0A64gsz2R" role="3clF46">
        <property role="TrG5h" value="serviceName" />
        <node concept="17QB3L" id="5n0A64gsz4Y" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="5n0A64gsz91" role="3clF46">
        <property role="TrG5h" value="serviceType" />
        <node concept="17QB3L" id="5n0A64gszah" role="1tU5fm" />
      </node>
    </node>
    <node concept="3Tm1VV" id="5n0A64gs2fR" role="1B3o_S" />
  </node>
</model>

