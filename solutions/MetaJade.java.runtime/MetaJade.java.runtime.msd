<?xml version="1.0" encoding="UTF-8"?>
<solution name="MetaJade.java.runtime" uuid="3235da96-e3e2-45f1-8096-7cb7c68bbaca" moduleVersion="0" compileInMPS="true">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot location="models" />
    </modelRoot>
  </models>
  <facets>
    <facet type="java">
      <classes generated="true" path="${module}/classes_gen" />
    </facet>
  </facets>
  <sourcePath />
  <dependencies>
    <dependency reexport="true">0dfebf0c-13fb-476f-90ad-86743b0c71ae(MetaJade.java.stubs)</dependency>
    <dependency reexport="false">6354ebe7-c22a-4a0f-ac54-50b52ab9b065(JDK)</dependency>
    <dependency reexport="false">708a03ad-8699-43c9-821a-6cd00b68e9f8(org.iets3.core.expr.genjava.functionalJava)</dependency>
  </dependencies>
  <languageVersions>
    <language slang="l:f3061a53-9226-4cc5-a443-f952ceaf5816:jetbrains.mps.baseLanguage" version="11" />
    <language slang="l:fd392034-7849-419d-9071-12563d152375:jetbrains.mps.baseLanguage.closures" version="0" />
    <language slang="l:83888646-71ce-4f1c-9c53-c54016f6ad4f:jetbrains.mps.baseLanguage.collections" version="1" />
    <language slang="l:ceab5195-25ea-4f22-9b92-103b95ca8c0c:jetbrains.mps.lang.core" version="2" />
    <language slang="l:9ded098b-ad6a-4657-bfd9-48636cfe8bc3:jetbrains.mps.lang.traceable" version="0" />
  </languageVersions>
  <dependencyVersions>
    <module reference="6354ebe7-c22a-4a0f-ac54-50b52ab9b065(JDK)" version="0" />
    <module reference="8bfe6a31-9cd5-412d-a48d-c869222924bb(MetaJade.common)" version="0" />
    <module reference="3235da96-e3e2-45f1-8096-7cb7c68bbaca(MetaJade.java.runtime)" version="0" />
    <module reference="0dfebf0c-13fb-476f-90ad-86743b0c71ae(MetaJade.java.stubs)" version="0" />
    <module reference="573b79af-a569-4d17-be0a-e7fb9a4a6c4c(MetaJade.platform)" version="0" />
    <module reference="708a03ad-8699-43c9-821a-6cd00b68e9f8(org.iets3.core.expr.genjava.functionalJava)" version="0" />
  </dependencyVersions>
</solution>

