<?xml version="1.0" encoding="UTF-8"?>
<solution name="MetaJade.generation" uuid="c00b198f-b059-42af-a1a4-33b5300b8bb9" moduleVersion="0" compileInMPS="true">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot location="models" />
    </modelRoot>
  </models>
  <facets>
    <facet type="java">
      <classes generated="true" path="${module}/classes_gen" />
    </facet>
  </facets>
  <sourcePath />
  <languageVersions>
    <language slang="l:ceab5195-25ea-4f22-9b92-103b95ca8c0c:jetbrains.mps.lang.core" version="2" />
    <language slang="l:7ab1a6fa-0a11-4b95-9e48-75f363d6cb00:jetbrains.mps.lang.generator.plan" version="1" />
  </languageVersions>
  <dependencyVersions>
    <module reference="23af69b8-6233-4fe0-8cdb-0d92b86c10a6(MetaJade.agent.genplan)" version="0" />
    <module reference="c00b198f-b059-42af-a1a4-33b5300b8bb9(MetaJade.generation)" version="0" />
  </dependencyVersions>
</solution>

