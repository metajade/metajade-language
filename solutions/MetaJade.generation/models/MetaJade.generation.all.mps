<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:2fe61982-6cc3-497e-a564-b7d3377de173(MetaJade.generation.all)">
  <persistence version="9" />
  <attribute name="doNotGenerate" value="true" />
  <languages>
    <use id="7ab1a6fa-0a11-4b95-9e48-75f363d6cb00" name="jetbrains.mps.lang.generator.plan" version="1" />
  </languages>
  <imports>
    <import index="my7v" ref="r:027c556e-4b3f-4530-bd2a-73e4f72d1501(MetaJade.generation.java)" />
  </imports>
  <registry>
    <language id="7ab1a6fa-0a11-4b95-9e48-75f363d6cb00" name="jetbrains.mps.lang.generator.plan">
      <concept id="3705377275350227759" name="jetbrains.mps.lang.generator.plan.structure.IncludePlan" flags="ng" index="NozSJ">
        <reference id="3705377275350227762" name="plan" index="NozSM" />
      </concept>
      <concept id="1820634577908471803" name="jetbrains.mps.lang.generator.plan.structure.Plan" flags="ng" index="2VgMpV">
        <child id="1820634577908471815" name="steps" index="2VgMA7" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="2VgMpV" id="4YVd2MBhpwJ">
    <property role="TrG5h" value="MetaJadeGenplan" />
    <node concept="NozSJ" id="1MgVVihl6BI" role="2VgMA7">
      <ref role="NozSM" to="my7v:36QeQ7wk363" resolve="JavaMetaJade" />
    </node>
  </node>
</model>

