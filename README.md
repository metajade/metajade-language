# MetaJade

MetaJade is an Agent Programming Language built with the [Meta Programming System](https://www.jetbrains.com/mps/).

The DSL is based on:

- [`iets3.opensource`](https://github.com/IETS3/iets3.opensource), which implements expressions, types and function
independently of the target language to which the DSL generates;
- the [Jade](https://jade.tilab.com) platform.

## Installation

To use the language, both as a language developer and as a language user, it is possible to download the source
code or a Maven package. In both the use cases, it is necessary to have installed Java 11, the version used by
MPS 2021.2.x. If that version of MPS is already installed, it can be referenced by specifying its installation path in 
the `mpsHomeDir` gradle property. If this property is not specified and the build is launched from Gradle, then
the build script will take care of downloading the correct version of MPS for the build. Otherwise, it is necessary
to manually install MPS 2021.2.x from the JetBrains archives [here](https://www.jetbrains.com/mps/download/previous.html).

### Maven package

A [maven package](https://gitlab.com/metajade/metajade-language/-/packages) deployed to the Package Registry of this repository is used to distribute the languages and ease their usage in other MPS projects.

An example usage of the package is available at the [MetaJade Examples](https://gitlab.com/metajade/metajade-examples) repository.

### Gradle build

First the git repository must be cloned or downloaded:

```shell
git clone https://gitlab.com/metajade/metajade-language
cd ./metajade-language
```

To build the project:

```shell
./gradlew build
```
Then the project can opened in MPS. Inf Gradle was launched from within MPS then the workbench should be restarted.

### MPS build

First the git repository must be cloned or downloaded:

```shell
git clone https://gitlab.com/metajade/metajade-language
cd ./metajade-language
```

Then to download the dependencies:

```shell
./gradlew resolveLanguageLibs
```

Then the project can be built in MPS. It is recommended to build first the languages and solutions under the `base`
virtual package, by right clicking on the package and choosing `Make Selected Modules`, then the one under the `java`
 package. For the first build it may be necessary to [ignore the errors](https://specificlanguages.com/posts/2022-02/17-errors-before-generation/).

## Test execution

To launch the tests of the language it is necessary to create a _path variable_, with key `project_home` and as value
the MPS project directory, as shown [here](https://specificlanguages.com/posts/how-to-create-testinfo-node-for-your-tests/).

Tests live under the `MetaJade.tests` solution, under the `base` virtual package. To execute them, right click on
the test nodes or on the test module and choose _run tests_.
